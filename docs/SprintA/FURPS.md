# Supplementary Specification (FURPS+)

## Functionality

It specifies features that are not related to use cases, namely: Auditing, Reporting, Locale/Languages, and Security.

  > **Location/Languages:**

 - “The application must support, at least, the Portuguese and the English languages.”
 
 > **Security:**

 - "All those who wish to use the application must be authenticated with a password holding seven alphanumeric characters, including three capital letters and two digits.”

 - “Only the nurses are allowed to access all user’s health data.”
 
 > **Audit and Report:**
 
 - The administrator is responsible for configuring and managing all the information (“An Administrator is responsible for properly configuring and managing the core information (e.g.: type of vaccines, vaccines, vaccination centers, employees) required for this application to be operated daily by SNS users, nurses, receptionists, etc. ”). 
 
 - “Any Administrator uses the application to register centers, SNS users, center coordinators, receptionists, and nurses enrolled in the vaccination process.”
 
 - “The application should use object serialization to ensure data persistence between two runs of the application.”
 
## Usability

_Evaluates the user interface. It has several subcategories,
among them: error prevention; interface aesthetics and design; help and
documentation; consistency and standards._

- “The user should introduce ... the type of vaccine to be administered (by default, the system suggests the one related to the ongoing outbreak).”

- “If the user authorizes the sending of the SMS, the application should send an SMS message when the vaccination event is scheduled and registered in the system. Some users (e.g.: older ones) may want to go to a healthcare center to schedule the vaccine appointment with the help of a receptionists at one vaccination center.”

- “The user manual must be delivered with the application.”

- “The application must support, at least, the Portuguese and the English languages.”

## Reliability
_(Refers to the integrity, compliance and interoperability of the software. The requirements to be considered are: frequency and severity of failure, possibility of recovery, possibility of prediction, accuracy, average time between failures.)_

- "(The goal of the performance analysis is to decrease the number of clients in the center, from the moment they register at the arrival, until the moment they receive the SMS informing they can leave the vaccination center.) To evaluate this, it proceeds as follows: **for any time interval on one day**, the difference between the number of new clients arrival and the number of clients leaving the center **every five-minute period is computed**."
- "(...)the application should implement a bruteforce algorithm (an algorithm which examines all the contiguous subsequences) to determine the contiguous subsequence with maximum sum. The implemented algorithm should be analyzed in terms of its worst-case time complexity, and it should be compared to a benchmark algorithm provided. The computational complexity analysis (of the brute-force algorithm and any sorting algorithms implemented within this application), must be accompanied by the observation of the execution time of the algorithms for inputs of variable size, in order to observe the asymptotic behavior. The worst-case time complexity analysis of the algorithms should be properly documented in the user manual of the application (in the annexes)."

## Performance
_Evaluates the performance requirements of the software, namely: response time, start-up time, recovery time, memory consumption, CPU usage, load capacity and application availability._

- "The application should implement a bruteforce algorithm (an algorithm which examines all the contiguous subsequences) to determine the
  contiguous subsequence with maximum sum"
- "The implemented algorithm should be analyzed in
  terms of its worst-case time complexity, and it should be compared to a benchmark algorithm
  provided."
- "The computational complexity analysis (of the brute-force algorithm and any sorting
  algorithms implemented within this application), must be accompanied by the observation of the
  execution time of the algorithms for inputs of variable size"




## Supportability

The supportability requirements gathers several characteristics, such as: testability, adaptability, maintainability, compatibility, configurability, installability, scalability and more.

- “As Coronavirus might not be the last pandemic in our lifetime, the application should be designed to easily support managing other future pandemic events requiring a massive vaccination of the population.”

- “The software application should also be conceived having in mind that it can be further commercialized to other companies and/or organizations and/or healthcare systems besides DGS.”


### Design Constraints

_(Specifies or constraints the system design process. Examples may include: programming languages, software process, mandatory standards/patterns, use of development tools, class library, etc.)_
  
- "The application must be developed in Java language using the IntelliJ IDE or NetBeans."

- "The application graphical interface is to be developed in JavaFX 11."

- "During the system development, the team must: (i) adopt best practices for identifying requirements, and for OO software analysis and design; (ii) adopt recognized coding standards (e.g.,CamelCase); (iii) use Javadoc to generate useful documentation for Java code."

- "The unit tests should be implemented using the JUnit 5 framework."

- "The JaCoCo plugin should be used to generate the coverage report."

- "All the images/figures produced during the software development process should be recorded in SVG format."

- "The application should use object serialization to ensure data persistence between two runs of the application."

### Implementation Constraints

_Specifies or constraints the code or construction of a system such
such as: mandatory standards/patterns, implementation languages,
database integrity, resource limits, operating system._

- "The application must be developed in Java language using the IntelliJ IDE or NetBeans."
- "All those who wish to use the application must be authenticated with a password holding seven alphanumeric characters,including three capital letters and two digits"
- "The application must support, at least, the Portuguese and the English languages"
- "During the system development, the team must adopt best practices for identifying
  requirements, and for OO software analysis and design.
- "During the system development, the team must adopt recognized coding standards (e.g.,
  CamelCase);"
- "During the system development, the team must use Javadoc to generate useful documentation for Java code."
- "The development team must implement unit tests for all methods, except for methods that
  implement Input/Output operations."
- "The unit tests should be implemented using the JUnit 5
  framework."
- " The JaCoCo plugin should be used to generate the coverage report."





### Interface Constraints
_Specifies or constraints the features inherent to the interaction of the
system being developed with other external systems._

- "The application graphical interface is to be developed in JavaFX 11."



(fill in here )

### Physical Constraints

_Specifies a limitation or physical requirement regarding the hardware used to house the system, as for example: material, shape, size or weight._

(fill in here )