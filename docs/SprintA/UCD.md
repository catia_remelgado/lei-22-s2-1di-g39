# Use Case Diagram (UCD)

**In the scope of this project, there is a direct relationship of _1 to 1_ between Use Cases (UC) and User Stories (US).**

However, be aware, this is a pedagogical simplification. On further projects and curricular units might also exist _1 to N **and/or** N to 1 relationships between US and UC.

**Insert below the Use Case Diagram in a SVG format**

![Use Case Diagram](UCD.svg)


**For each UC/US, it must be provided evidences of applying main activities of the software development process (requirements, analysis, design, tests and code). Gather those evidences on a separate file for each UC/US and set up a link as suggested below.**

# Use Cases / User Stories
| UC/US  | Description                             |                   
|:-------|:----------------------------------------|
| US 001 | [Scheduling](../SprintD/US001.md)                  |
| US 002 | [IssueCertificate](../SprintD/US002.md)            |
| US 003 | [RegisterAdverseReactions](../SprintD/US003.md)    |
| US 004 | [RecordVaccineAdministration](../SprintD/US004.md) |
| US 005 | [ConfirmScheduling](../SprintD/US008.md)           |
| US 006 | [MonitorVaccinationProcess](../SprintD/US006.md)   |
| US 007 | [Register_HC_Centers](../SprintB/US09/US007.md)             |
| US 008 | [Register_SNS_Users](../SprintD/US008.md)          |
| US 009 | [Register_V_Center](../SprintB/US10/US009.md)		   |
| US 010 | [RegisterEmployees](../SprintB/US12/US010.md)	   |
| US 011 | [ListEmployee](../SprintB/US13/US011.md)	   |
| US 012 | [RegisterNewVaccineType](../SprintD/US012.md) |
| US 013 | [RegisterNewVaccineBrand](../SprintD/US013.md) |
| US 014 | [RequestIssuanceCertificate](../SprintB/US014.md) |
| US 015 | [ObtainCharts](../SprintB/US015.md) |
| US 016 | [EvaluateCenterPerformance](../SprintB/US016.md) |
| US 017 | [ObtainGeneratedReports](../SprintD/US017.md)   |
| US 018 | [CheckSNSusersHealthHistory](../SprintD/US018.md)  |
| US 019 | [AnalyzeOtherCentersData](../SprintD/US019.md)     |
| US 020 | [ConfirmThatTheUserIsReadyToTakeTheVaccine](../SprintD/US020.md)|
| US 021 | [CheckToVaccinateList](US0216.md)            |
| US 022 | [CheckVaccineToBeAdministered](../SprintB/US022.md)	   |
