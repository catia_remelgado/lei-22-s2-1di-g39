# OO Analysis #

The construction process of the domain model is based on the client specifications, especially the nouns (for _concepts_) and verbs (for _relations_) used. 

## Rationale to identify domain conceptual classes ##
To identify domain conceptual classes, start by making a list of candidate conceptual classes inspired by the list of categories suggested in the book "Applying UML and Patterns: An Introduction to Object-Oriented Analysis and Design and Iterative Development". 


### _Conceptual Class Category List_ ###

**Business Transactions**

* Vaccine administration

---

**Transaction Line Items**

*

---

**Product/Service related to a Transaction or Transaction Line Item**

* Vaccine
* Application

---


**Transaction Records**

* Vaccine certificate

---  


**Roles of People or Organizations**

* Administrator
* Employee
* SNS user
* Nurse
* Receptionist
* Center Coordinator


 
---


**Places**

* Vaccination Center
* Healthcare Center
* Center   
---

**Noteworthy Events**

* Scheduling
* Vaccine administration

---


**Physical Objects**

* Vaccine
 
---


**Descriptions of Things**

* Vaccine type
* Vaccine Brand
* Age Group

---


**Catalogs**

* Time Between Doses

---


**Containers**

*

---


**Elements of Containers**

*

---


**Organizations**

* Company 

---

**Other External/Collaborating Systems**

*

 
---


**Records of finance, work, contracts, legal matters**

* SMS Message

* Vaccination Certificate

---


**Financial Instruments**

*

---


**Documents mentioned/used to perform some work/**


* Report
* Chart
---




###**Rationale to identify associations between conceptual classes**###

An association is a relationship between instances of objects that indicates a relevant connection and that is worth of remembering, or it is derivable from the List of Common Associations: 

+ **_A_** is physically or logically part of **_B_**
+ **_A_** is physically or logically contained in/on **_B_**
+ **_A_** is a description for **_B_**
+ **_A_** known/logged/recorded/reported/captured in **_B_**
+ **_A_** uses or manages or owns **_B_**
+ **_A_** is related with a transaction (item) of **_B_**
+ etc.



| Concept (A) 		|  Association   	|  Concept (B) |
|----------	   		|:-------------:		|------:       |
| Vaccine Administration|identified by <br /> of a certain <br />|Vaccine  Administration and Age Group <br />Vaccine Brand |
| Vaccine Brand 	| of a <br /> created by  | Vaccine Type <br /> Administrator   |
| Application |owned by <br /> manages | Company <br /> Scheduling |
| Administrator| manages <br /> appointed by <br /> registers <br /> registers<br /> registers | Application <br /> Company <br /> Center Coordinator <br /> SNS User <br /> Receptionist |
| Employee| can be <br /> works at| Receptionist <br /> Center  |
| SNSUser  	|  belongs to <br /> takes   		 	| Age Group <br /> Vaccine Administration   |
| Nurse  	| is <br /> issues <br /> registered by <br /> check condition of |   Employee <br /> Vaccination Certificate <br /> Administrator <br /> SNS User |
| Receptionist  	|   confirms <br /> registered by	 	|   Scheduling <br /> Administrator |
| Center Coordinator   	|  analyzes <br /> runs <br /> analyzes   		 	|  Chart <br /> Center <br /> Report  |
|  Center  	|   is place of <br /> can be <br /> can be 	| Scheduling <br /> Vaccination Center  <br /> HealthCare Center|
| Vaccination Center  	|   administers 	 	| Vaccine Type|
| Healthcare Center  	|   registered by <br /> administers  	| Administrator <br /> Vaccine Type|
| Scheduling  	|   generates <br /> created by <br /> to take 	 	| SMS <br /> SNS User <br /> Vaccine |
| Company  	|   Manages| Center |
|Vaccine Type| of a <br /> created by |Vaccine <br /> Adminisrator |
|SMS Message|received by | SNS User |
|Vaccination Certificate| requested by |SNS User |
|Vaccine	|   administered by <br /> taken at | Nurse <br /> Center  |
| Report|   evaluates performance of| Center |
| Chart|   evaluates performance of| Center |
| Age group|   identifies | Vaccine Administration And Age Group |
|Vaccine Administration And Age Group |has | Time Between Doses|
|Time Between Doses| | |


## Domain Model

**Do NOT forget to identify concepts atributes too.**

**Insert below the Domain Model Diagram in a SVG format**

![DM.svg](DM.svg)



