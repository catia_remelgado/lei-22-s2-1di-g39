# US 03 Register an SNS User

## 1. Requirements Engineering

### 1.1. User Story Description

As a receptionist I want to register SNS users, using the data of each user.

### 1.2. Customer Specifications and Clarifications 

**From the specifications document:**

> A receptionist can check-in an SNS User

**From the client clarifications:**

> **Question:** What is the structure of an SNS number?
>  
> **Answer:** 9 digit number

***

> **Question:** How will be a SNS user registered in the system (what data, besides age, is needed)?
>  
> **Answer:** *Name, Address, Phone Number, E-mail, Birth Date, Sex, SNS User Number and Citizen Card Number.*

### 1.3. Acceptance Criteria

* **AC1:** The SNS number must be in a valid format(9 digits)

* **AC2:** The SNS user information must be correct

* **AC3:** Birthdate must be in a valid format

* **AC4:** Phone number must be in a valid format (9 digits)

* **AC5:** Email must be in a valid format 

* **AC6:** Sex Options : Male/Female/None

* **AC7:** Citizen card number must be in a valid format (7/8 digits)

### 1.4. Found out Dependencies

* There is a dependency to "US007 Register centers" since at least one center must exist for registration to be possible.

* There is a dependency to "US010 Register new types of vaccines" because there must be at least one type of vaccine to be able to register.

* There is a dependency to "US011 Register new vaccine brands" because there must be at least one vaccine brand in order to be able to register.

* There is a dependency to "US008 Register SNS Users" because there must be at least one SNS user.

* There is a dependency to "US009 Register employees" as there must be at least one employee (a receptionist) for the registration to be done.

* There is a dependency to "US001 Schedule a vaccine" since at least one SNS user should have scheduled a vaccine to be necessary to do the registration.

### 1.5 Input and Output Data

* Typed data:
    * Name
    * Address
    * Phone Number
    * Email
    * Birth Date
    * Sex
    * SNS Number
    * Citizen Card Number
	
* Selected data:

**Output Data:**

* A message stating that registration was successful or not.

### 1.6. System Sequence Diagram (SSD)

**Alternative 1**

![US03_SSD](US03_SSD.svg)

### 1.7 Other Relevant Remarks

N/A


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

![US03_MD](US03_MD.svg)

### 2.2. Other Remarks

N/A


## 3. Design - User Story Realization 

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1  		 |....interacting with the actor?                                             |AddSNSUserUI                  |Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model.|
| 			  	 |... coordinating the US?                                                    |SNSController          |Controller|
| Step 2         |...runs the receptionist menu?| ReceptionistUI|IE: shows the options that the receptionist has in the app|
| Step 3 		 |...receive login from receptionist?                                        |AuthUI                          |IE: receives all logins. |
| Step 4 		 |...check if the receptionist login is valid?( controls authentications)			                                  |AuthController                       |IE: can validate logins of all users( knows all users).|
| Step 5 		 |...saves the data entered by the receptionist?				                          |SNSUser                         |IE: knows the necessary information and requests it.|
| Step 6  		 |...reads the information provided by the receptionist from the user data?						                                                  |AddSNSUserUI|IE: can read console data.|              
| Step 7  		 |...checks if the user already exists?	                                                                          |SNSController| IE: check the information provided.(Controller)|
| Step 8  		 |...informing operation success?							                                                  |AddSNSUserUI|IE: checks if the user was created correctly.|              


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * SNSUser
 * AddSNSUserUI
 * AuthController
 * SNSController
 * ReceptionistUI

Other software classes (i.e. Pure Fabrication) identified:

 * SNSUserStore

External software classes:

 * AuthFacade
 * AuthUI

## 3.2. Sequence Diagram (SD)

*In this section, it is suggested to present an UML dynamic view stating the sequence of domain related software objects' interactions that allows to fulfill the requirement.* 

![US03_SD](US03_SD.svg)

## 3.3. Class Diagram (CD)

*In this section, it is suggested to present an UML static view representing the main domain related software classes that are involved in fulfilling the requirement as well as and their relations, attributes and methods.*

![US03_CD](US03_CD.svg)

# 4. Tests 

*In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of requirements fulfilling.* 

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

**Test 1:** Insert an unnamed user. 

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {

    SNSUser istance = new SNSUser (123456789," ",01-01-2000,"email@email.com",111111111,"M",12312312,"address")
	}

**Test 2:** Insert a User with a Invalid SNS Number.

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowed() {

    SNSUser istance = new SNSUser (1234567899,"name",01-01-2000,"email@email.com",111111111,"M",12312312,"address")
	}



# 5. Construction (Implementation)

## Class SNSController

    public class SNSController {

    private App app;
    private SNSUserStore snsUserStore;


    public SNSController()
    {
        this.snsUserStore = new SNSUserStore();
    }

    public SNSUserStore getSNSUserStore() {
        return this.snsUserStore;
    }

    // Singleton.
    private static SNSController singleton = null;
    public static SNSController getInstance()
    {
        if(singleton == null)
        {
            synchronized(SNSController.class)
            {
                singleton = new SNSController();
            }
        }
        return singleton;
    }

## Class SNSUser
    public class SNSUser {

    private static int COUNTER = 1;

    private int id;
    private int snsNumber;
    private String name;
    private Date birthdate;
    private Email email;
    private int phoneNumber;
    private Gender gender;
    private int CCnumber;
    private String address;

    public SNSUser(int snsNumber, String name, Date birthdate, Email email, int phoneNumber, Gender gender, int CCnumber, String address) {

        if (snsNumber < 100000000 || snsNumber > 999999999) {
            throw new IllegalArgumentException("Invalid SNS number.");
        }

        if (name == null) {
            throw new IllegalArgumentException("Name is null.");
        }


        this.id = SNSUser.COUNTER++;
        this.snsNumber = snsNumber;
        this.name = name;
        this.birthdate = birthdate;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.gender = gender;
        this.CCnumber = CCnumber;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public int getSnsNumber() {
        return snsNumber;
    }

    public void setSnsNumber(int snsNumber) {
        this.snsNumber = snsNumber;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public int getCCnumber() {
        return CCnumber;
    }

    public void setCCnumber(int CCnumber) {
        this.CCnumber = CCnumber;
    }
    } 

## Class AddSNSUserUI
    public class AddSNSUserUI implements Runnable {
    @Override
    public void run() {

        boolean success = true;

        Integer snsNumber = Utils.readIntegerFromConsole("SNS Number: ");
        String name = Utils.readLineFromConsole("Name: ");
        Date birthdate = Utils.readDateFromConsole("Birthdate (dd-mm-aaaa): ");
        String email = Utils.readLineFromConsole("Email: ");
        Integer phoneNumber =Utils.readIntegerFromConsole("PhoneNumber:  ");
        String genderString = Utils.readLineFromConsole("Gender (M - Male, F - Female, N - None): ");
        Integer CCnumber = Utils.readIntegerFromConsole("CC number: ");
        String address = Utils.readLineFromConsole("Address: ");


        try {

            Gender gender = Gender.NONE;

            if (genderString.equals("M") || genderString.equals("m")) {
                gender = Gender.MALE;
            } else {
                if (genderString.equals("F") || genderString.equals("f")) {
                    gender = Gender.FEMALE;
                }
            }


            SNSController.getInstance().getSNSUserStore().addNewSNSUser(snsNumber, name, birthdate, email, phoneNumber, gender, CCnumber, address);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            success = false;
        }

        if (success) {
            System.out.println("Info: User was successfully created.");
        } else {
            System.out.println("Info: User was not successfully created.");
        }
    }
    }





