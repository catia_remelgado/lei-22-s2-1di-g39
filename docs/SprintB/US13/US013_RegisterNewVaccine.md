# US 013 - Register vaccine brand and administration

## 1. Requirements Engineering

### 1.1. User Story Description
As an administrator, I want to register the vaccine brand and its administration.

### 1.2. Customer Specifications and Clarifications

**From the specifications document:**

> "Yet, it is worth noticing that for each type of vaccine, several vaccines might exist, each one demanding a distinct administration process." - This means that for a type of vaccine, several brands exist and each brand has its administration process.

> "The vaccine administration process comprises (i) one or more age groups" - For each brand, there exists age groups which have distinct administration processes.

> "[...] and (ii) per age group, the doses to be administered (e.g.: 1, 2, 3), the vaccine dosage (e.g.: 30 ml), and the time interval regarding the previously administered dose." - Each age group has a different quantity of doses and dosage and the time between doses varies.

> Dosage is measured in ml.

> "[...] it is important to notice that between doses (e.g.: between the 1st and 2nd doses) the dosage to be administered might vary as well as the time interval" - For each brand, the time between doses can vary, as well as the dosage on each dose.

> "An Administrator is responsible for properly configuring and managing the core information (e.g.: type of vaccines, vaccines [...]". - The Administrator is responsible for creating new vaccine brands in the system, which in turn means that he needs to create the corresponding administration process.

> "After giving the vaccine to the user, each nurse registers the event in the system, more precisely, registers the vaccine type (e.g.: Covid-19), vaccine name/brand (e.g.: Astra Zeneca, Moderna, Pfizer), and the **lot number used**" - A vaccine brand has a lot number.

> "Afterwards, the nurse sends the user to a recovery room, to stay there for a given recovery period (e.g.: 30 minutes)" - A vaccine brand has a recovery period.

**From the client clarifications:**

> **Question:** What is the structure of a lot number?
>
> **Answer:** "The lot number has five alphanumeric characters an Hyphen and two numerical characters (example: 21C16-05)"

-

> **Question:** Does each vaccine have a different recovery period?
>
> **Answer:** To be answered.

-

> **Question**: "As to the interval between doses, what time format should be used? (e.g. days, weeks, months)"

> **Answer**: "Number of days".

### 1.3. Acceptance Criteria
• None addressed so far


### 1.4. Found out Dependencies

* There is a dependency to "US12 Register new vaccine type" since at least a vaccine type should exist before creating a vaccine brand of that type.


### 1.5 Input and Output Data
**Input Data:**
* Typed data:
  * (for a vaccine brand)
    * a name,
    * lot number,
    * recovery period.
  
  * (for the vaccine administration)
    * number of doses,
    * the time between doses,
    * dosage for each dose,
    * minimum age,
    * maximum age.

* Selected data:
  * vaccine type.

**Output:**
* What was registered
* (In)Success of the operation


### 1.6. System Sequence Diagram (SSD)

*Insert here a SSD depicting the envisioned Actor-System interactions and throughout which data is inputted and outputted to fulfill the requirement. All interactions must be numbered.*

![US13_SSD](US13_SSD.svg)


### 1.7 Other Relevant Remarks

* User story happens everytime the company receives a new vaccine brand to administer.
*Use this section to capture other relevant information that is related with this US such as (i) special requirements ; (ii) data and/or technology variations; (iii) how often this US is held.*


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt
*In this section, it is suggested to present an excerpt of the domain model that is seen as relevant to fulfill this requirement.*

![US13_MD](US13_MD.svg)

### 2.2. Other Remarks

*Use this section to capture some aditional notes/remarks that must be taken into consideration into the design activity. In some case, it might be usefull to add other analysis artifacts (e.g. activity or state diagrams).*



## 3. Design - User Story Realization

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for...                | Answer                       | Justification (with patterns)                                                                                 |
| :------------- | :---------------------                                     | :------------                | :----------------------------                                                                                 |
| Step 1         | ....interacting with the actor?                            | CreateVaccineUI              | Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model. |
|                | ... coordinating the US?                                   | CreateVaccineController      | Controller                                                                                                    |
|                | ... instantiating a new Vaccine and Administration?        | VaccineStore                 | Creator (Rule 1): VaccineStore contains all Vaccines and Administrations.                                     |
| Step 2         | ...knows all vaccine types?                                | VaccineTypeStore             | IE: has all the vaccine type information                                                                      |
| Step 3         | ...saves the selected vaccine type?                        | Vaccine                      | IE: The Vaccine has an associated vaccine type                                                                |
| Step 4         |                                                            |                              |                                                                                                               |
| Step 5         | ...saves vaccine inputted data?                            | Vaccine                      | IE:Vaccine has its own data                                                                                   |
|                | ..confirms vaccine data locally?                           | Vaccine                      | IE: Vaccine has its own data                                                                                  |
| Step 6         |                                                            |                              |                                                                                                               |
| Step 7         |                                                            |                              |                                                                                                               |
| Step 8         |                                                            |                              |                                                                                                               |
| Step 9         | ...saves vaccine administration process and inputted data? | VaccineAdministrationProcess | IE:VaccineAdministrationProcess has its own data                                                              |
|                | ...instantiating a new Age Group?                          | VaccineAdministrationProcess | IE:VaccineAdministrationProcess closely uses an age group                                                     |
|                | ...validating an Age Group locally?                        | AgeGroup                     | IE:AgeGroup knows its own data                                                                                |
|                | ...instantiating a new vaccine administration process?     | Vaccine                      | Creator (Rule 1): in the DM Vaccine has its vaccine administration processes                                  |
|                | ...validates vaccine administration process locally        | VaccineAdministrationProcess | IE:VaccineAdministrationProcess knows its own data to verify it                                               |
| Step 10        |                                                            |                              |                                                                                                               |
| Step 11        | ...saves a vaccine administration process?                 | Vaccine                      | IE:Vaccine contains all administration processess of itself                                                   |
| Step 12        | ...informing operation success?                            | CreateVaccineUI              | IE:responsible for user interaction                                                                           |
| Step 13        |                                                            |                              |                                                                                                               |
| Step 14        |                                                            |                              |                                                                                                               |
| Step 15        | ...saves input data about dose administration?             | DoseAdministration           | IE:DoseAdministration has its own data                                                                        |
|                | ...instantiating new dose administration                   | VaccineAdministrationProcess | Creator(Rule 1): in the DM VaccineAdministrationProcess has different doses                                   |
|                | ...validates dose administration locally?                  | DoseAdministration           | IE:knows own data to verify it                                                                                |
| Step 16        |                                                            |                              |                                                                                                               |
| Step 17        | ...saves dose administration?                              | VaccineAdministrationProcess | IE:VaccineAdministrationProcess contains several doses                                                        |
| Step 18        | ...informing operation success                             | CreateVaccineUI              | IE:responsible for user interaction                                                                           |
| Step 19        |                                                            |                              |                                                                                                               |
| Step 20        | ..confirms vaccine data globally?                          | VaccineStore                 | IE: Contains all vaccines                                                                                     |
|                | ...saving vaccine and administrations?                     | VaccineStore                 | IE:contains all information about vaccines and administrations                                                |
| Step 21        | ...informing operation success?                            | CreateVaccineUI              | IE:responsible for user interaction                                                                           |

### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are:

* VaccineStore
* AgeGroup 
* VaccineAdministrationProcess
* DoseAdministration


Other software classes (i.e. Pure Fabrication) identified:
* CreateVaccineUI
* CreateVaccineController

## 3.2. Sequence Diagram (SD)

*In this section, it is suggested to present an UML dynamic view stating the sequence of domain related software objects' interactions that allows to fulfill the requirement.*

![US13_SD](US13_SD.svg)

## 3.3. Class Diagram (CD)

*In this section, it is suggested to present an UML static view representing the main domain related software classes that are involved in fulfilling the requirement as well as and their relations, attributes and methods.*

![US13_CD](US13_CD.svg)

# 4. Tests
*In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of requirements fulfilling.*

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

**Test 1:** Check that it is not possible to create an instance of the Example class with null values.

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

*It is also recommended to organize this content by subsections.*

# 5. Construction (Implementation)

*In this section, it is suggested to provide, if necessary, some evidence that the construction/implementation is in accordance with the previously carried out design. Furthermore, it is recommeded to mention/describe the existence of other relevant (e.g. configuration) files and highlight relevant commits.*

*It is also recommended to organize this content by subsections.*

# 6. Integration and Demo

*In this section, it is suggested to describe the efforts made to integrate this functionality with the other features of the system.*


# 7. Observations

*In this section, it is suggested to present a critical perspective on the developed work, pointing, for example, to other alternatives and or future related work.*





