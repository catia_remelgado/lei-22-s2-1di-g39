# US 001 – Schedule a vaccine 

## 1. Requirements Engineering
(Brief description)

An SNS user wants to schedule a vaccine. To schedule, the user needs to introduce his/her SNS user number, select the vaccination center, the date, and the time (s)he wants to be vaccinated and the type of vaccine  to be administered (by default, the system suggests the one related to the ongoing outbreak).  The user introduces the data and is told that the center, day, time and type of vaccine are available in those criteria. The user confirms the center, day, time and type of vaccine. The user authorizes incoming SMS Messages about the scheduling from the organization. The user arrives at the schedules center, day and time.

“To take a vaccine, the SNS user should use the application to schedule his/her vaccination. The user should introduce his/her SNS user number, select the vaccination center, the date, and the time (s)he wants to be vaccinated as well as the type of vaccine to be administered (by default, the system suggests the one related to the ongoing outbreak). Then, the application should check the vaccination center capacity for that day/time and, if possible, confirm that the vaccination is scheduled and inform the user that (s)he should be at the selected vaccination center at the scheduled day and time. The SNS user may also authorize the DGS to send a SMS message with information about the scheduled appointment. If the user authorizes the sending of the SMS, the application should send an SMS message when the vaccination event is scheduled and registered in the system. Some users (e.g.: older ones) may want to go to a healthcare center to schedule the vaccine appointment with the help of a nurse. On the scheduled day and time, the SNS user should go to the vaccination center to get the vaccine.”



### 1.1. User Story Description

As an SNS user I want to schedule a vaccine

### 1.2. Customer Specifications and Clarifications 



**From the specifications document:**

>	A scheduling is consisted of an SNS user (their number), a vaccination center, a day, a time, and a type of vaccine.

>	The user should have control if he wants to receive or not a text message with the scheduling information. 

>	A scheduling can be done by a nurse relating to a certain user.



**From the client clarifications:**

> **Question:** To schedule a vaccine, do you need to have an SNS User account with password and such first, or does the SNS Number suffice to schedule the vaccine?
>  
> **Answer:** *to be clarified*

-

> **Question:** To schedule a vaccine, does the user have an option to chosse vaccine brand, or only type (relating to a disease and not a company)?
>  
> **Answer:** *to be clarified*

-

> **Question:** Does the center, day and time have to be chosen by the user, or can the system choose for them? (i.e. are the fields of center, day and time and maybe even vaccine brand required?)
>  
> **Answer:** *to be clarified*

-

> **Question:** What is the structure of an SNS number?
>  
> **Answer:** *to be clarified*

-

> **Question:** If the suggested center, day and time for scheduling by the SNS user are not available, either due to capacity, or overlapping schedulings, should the system allow to change the fields and even suggest a scheduling on the same center, but different day and time (one that is available)?, or should the process just start over for the SNS user?
>  
> **Answer:** *to be clarified*

-

> **Question:** Is there any difference between a scheduling done by a nurse (i.e. an older user asks for help with scheduling)  or an SNS user?>  
> **Answer:** *to be clarified*


### 1.3. Acceptance Criteria

•	**AC1:** The user should be able to schedule a vaccine in a chosen center, day and time choosing a certain type of vaccine.

•	**AC2:** There mustn’t be overlapping schedulings.

•	**AC3:** The user should be able to confirm, if available, center, day, time and type of vaccine.

•	**AC4:** The user should have an option of allowing/not allowing SMS from DGS, and having the option to schedule the vaccine at a health center.

•	**AC5:** the SNS number should be in the correct structure (to be found out)


### 1.4. Found out Dependencies

* There is a dependency to "US007 Register centers" since at least a center must exist to schedule a vaccine at a center.

* There is a dependency to "US008 Register SNS users " since the SNS user must be registered to be able to schedule a vaccine.

* There is a dependency to "US010 Register new types of vaccines" since there should be at least one type of vaccine to be able to schedule.

* There is a dependency to "US010 Register new vaccine brands" since there should be at least one vaccine brand to be able to schedule.

### 1.5 Input and Output Data

**Input Data:**

* Typed data:
    * SNS number
	
* Selected data:
    * center
    * day
    * time
    * type of vaccine
    * Yes/No to SMS

**Output Data:**

*Success of operation
*SMS with scheduling info

### 1.6. System Sequence Diagram (SSD)

*Insert here a SSD depicting the envisioned Actor-System interactions and throughout which data is inputted and outputted to fulfill the requirement. All interactions must be numbered.*

![USXXX-SSD](USXXX-SSD.svg)


### 1.7 Other Relevant Remarks

*Use this section to capture other relevant information that is related with this US such as (i) special requirements ; (ii) data and/or technology variations; (iii) how often this US is held.* 


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 
*In this section, it is suggested to present an excerpt of the domain model that is seen as relevant to fulfill this requirement.* 

![USXXX-MD](USXXX-MD.svg)

### 2.2. Other Remarks

*Use this section to capture some aditional notes/remarks that must be taken into consideration into the design activity. In some case, it might be usefull to add other analysis artifacts (e.g. activity or state diagrams).* 



## 3. Design - User Story Realization 

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1  		 |							 |             |                              |
| Step 2  		 |							 |             |                              |
| Step 3  		 |							 |             |                              |
| Step 4  		 |							 |             |                              |
| Step 5  		 |							 |             |                              |
| Step 6  		 |							 |             |                              |              
| Step 7  		 |							 |             |                              |
| Step 8  		 |							 |             |                              |
| Step 9  		 |							 |             |                              |
| Step 10  		 |							 |             |                              |  


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * Class1
 * Class2
 * Class3

Other software classes (i.e. Pure Fabrication) identified: 
 * xxxxUI  
 * xxxxController

## 3.2. Sequence Diagram (SD)

*In this section, it is suggested to present an UML dynamic view stating the sequence of domain related software objects' interactions that allows to fulfill the requirement.* 

![USXXX-SD](USXXX-SD.svg)

## 3.3. Class Diagram (CD)

*In this section, it is suggested to present an UML static view representing the main domain related software classes that are involved in fulfilling the requirement as well as and their relations, attributes and methods.*

![USXXX-CD](USXXX-CD.svg)

# 4. Tests 
*In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of requirements fulfilling.* 

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

**Test 1:** Check that it is not possible to create an instance of the Example class with null values. 

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

*It is also recommended to organize this content by subsections.* 

# 5. Construction (Implementation)

*In this section, it is suggested to provide, if necessary, some evidence that the construction/implementation is in accordance with the previously carried out design. Furthermore, it is recommeded to mention/describe the existence of other relevant (e.g. configuration) files and highlight relevant commits.*

*It is also recommended to organize this content by subsections.* 

# 6. Integration and Demo 

*In this section, it is suggested to describe the efforts made to integrate this functionality with the other features of the system.*


# 7. Observations

*In this section, it is suggested to present a critical perspective on the developed work, pointing, for example, to other alternatives and or future related work.*
