# US003 - Register Adverse reactions

## 1. Requirements Engineering

A nurse registers the adverse reactions related to a certain scheduling, notifying if everything went as normal by SMS/email.

" Afterwards, the nurse sends the user to a recovery room,
to stay there for a given recovery period (e.g.: 30 minutes). If there are no problems, after the given
recovery period, the user should leave the vaccination center. The system should be able to notify
(e.g.: SMS or email) the user that his/her recovery period has ended. If the nurse identifies any
adverse reactions during that recovery period, the nurse should record the adverse reactions in the
system."

### 1.1. User Story Description

As a nurse I want to register adverse reactions of a scheduling.

### 1.2. Customer Specifications and Clarifications

**From the specifications document:**

>	During the recovery period, if there are any adverse reactions, they should be registered
>   They are associated with a scheduling
>   Each vaccine brand has its recovery period


**From the client clarifications:**

> **Question:** Are the adverse reactions selected data (to which we can continuously add on to), or is it a text box of each event in each scheduling, which then correlate to the vaccine brand?
>
> **Answer:** *to be clarified*

-
### 1.3. Acceptance Criteria

•	**AC1:** Adverse reactions should be related to a scheduling.
•	**AC2:** The user only receives the SMS/email to leave the recovery room after the recovery period has passed.



### 1.4. Found out Dependencies

* There is a dependency to "US001 Schedule a vaccine" since at least one SNS user should have scheduled and taken a vaccine to register possible adverse reactions
* There is a dependency to "US004 Record Administration of Vaccine" since the record of the vaccine administration must happen before being able to register any adverse reactions.
* There is a dependency to "US007 Register centers" since at least a center must exist to take a vaccine and register possible adverse reactions.
* There is a dependency to "US008 Register SNS users " since the SNS user must be registered to be able to take a vaccine and register possible adverse reactions.
* There is a dependency to "US010 Register new types of vaccines" since there should be at least one type of vaccine to be able be vaccinated and register possible adverse reactions.
* There is a dependency to "US010 Register new vaccine brands" since there should be at least one vaccine brand to be able to take a vaccine and to know a recovery period to register possible adverse reactions.


### 1.5 Input and Output Data

**Input Data:**

* Typed data:
    * To be clarified

* Selected data:
    * To be clarified

**Output Data:**

*Success of operation (recorded adverse reactions successfully or not)
*SMS/Email to user after recovery period allowing him/her ot leave the room


### 1.6. System Sequence Diagram (SSD)

*Insert here a SSD depicting the envisioned Actor-System interactions and throughout which data is inputted and outputted to fulfill the requirement. All interactions must be numbered.*

![USXXX-SSD](USXXX-SSD.svg)


### 1.7 Other Relevant Remarks

*Use this section to capture other relevant information that is related with this US such as (i) special requirements ; (ii) data and/or technology variations; (iii) how often this US is held.*


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt
*In this section, it is suggested to present an excerpt of the domain model that is seen as relevant to fulfill this requirement.*

![USXXX-MD](USXXX-MD.svg)

### 2.2. Other Remarks

*Use this section to capture some aditional notes/remarks that must be taken into consideration into the design activity. In some case, it might be usefull to add other analysis artifacts (e.g. activity or state diagrams).*



## 3. Design - User Story Realization

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1  		 |							 |             |                              |
| Step 2  		 |							 |             |                              |
| Step 3  		 |							 |             |                              |
| Step 4  		 |							 |             |                              |
| Step 5  		 |							 |             |                              |
| Step 6  		 |							 |             |                              |              
| Step 7  		 |							 |             |                              |
| Step 8  		 |							 |             |                              |
| Step 9  		 |							 |             |                              |
| Step 10  		 |							 |             |                              |  


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are:

* Class1
* Class2
* Class3

Other software classes (i.e. Pure Fabrication) identified:
* xxxxUI
* xxxxController

## 3.2. Sequence Diagram (SD)

*In this section, it is suggested to present an UML dynamic view stating the sequence of domain related software objects' interactions that allows to fulfill the requirement.*

![USXXX-SD](USXXX-SD.svg)

## 3.3. Class Diagram (CD)

*In this section, it is suggested to present an UML static view representing the main domain related software classes that are involved in fulfilling the requirement as well as and their relations, attributes and methods.*

![USXXX-CD](USXXX-CD.svg)

# 4. Tests
*In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of requirements fulfilling.*

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

**Test 1:** Check that it is not possible to create an instance of the Example class with null values.

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

*It is also recommended to organize this content by subsections.*

# 5. Construction (Implementation)

*In this section, it is suggested to provide, if necessary, some evidence that the construction/implementation is in accordance with the previously carried out design. Furthermore, it is recommeded to mention/describe the existence of other relevant (e.g. configuration) files and highlight relevant commits.*

*It is also recommended to organize this content by subsections.*

# 6. Integration and Demo

*In this section, it is suggested to describe the efforts made to integrate this functionality with the other features of the system.*


# 7. Observations

*In this section, it is suggested to present a critical perspective on the developed work, pointing, for example, to other alternatives and or future related work.*





