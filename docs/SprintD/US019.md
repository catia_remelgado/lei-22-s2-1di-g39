# US 019 Analyze other center’s data

## 1. Requirements Engineering
(Brief description)

The Center Coordinator wants to analyze data from another vaccination center.
To do this analysis, the Center Coordinator must launch the application and request the analyze for a certain center (entered by the Coordinator). After that, the system will ask for the center and after being entered by the Center Coordinator the system will do a validation and show the analyze.
Finally, the system will inform the coordinator that the operation was successful.

“Each vaccination center has a Center Coordinator that has the responsibility to manage the Covid19 vaccination process. The Center Coordinator wants to monitor the vaccination process, to see statistics and charts, to evaluate the performance of the vaccination process, generate reports and analyze data from other centers, including data from law systems.”.

### 1.1. User Story Description

A Center Coordinator who wants to analyze data from another vaccination center.

### 1.2. Customer Specifications and Clarifications 

**From the specifications document:**

> The center coordinator wants to monitor and analyze the vaccination process of another center. 	

**From the client clarifications:**

> **Question:** Can only Center coordinators do this analyze?
>  
> **Answer:** *to be clarified*

-

> **Question:** How the analysis will be done?
>  
> **Answer:** *to be clarified*

-

> **Question:** Where will be the centers that the center coordinator can choose?
>  
> **Answer:** *to be clarified*



### 1.3. Acceptance Criteria

¢	**AC1:** The center must exist

### 1.4. Found out Dependencies

* There is a dependency to "US007 Register centers" because it is necessary to have at least one center for the analysis to work.

* There is a dependency to "US004 Record administration of vaccine” because the vaccination record is important for analysis.

* There is a dependency to "US001 Schedule a vaccine" since at least one SNS user should have scheduled a vaccine to have data to analyze

* There is a dependency to "US010 Register new types of vaccines" since there should be at least one type of vaccine to be able to do the analyze.

* There is a dependency to "US010 Register new vaccine brands" since there should be at least one vaccine brand to be able to do the analyze.

* There is a dependency to "US008 Register SNS Users" because it takes at least one SNS user so that there is something to see analyzed.


### 1.5 Input and Output Data

* Typed data:
	* Vaccination Center Name
	
* Selected data:
	* name of vaccination centers
	* information from vaccination centers

**Output Data:**

* A dataset with the vaccination center analyze.


### 1.6. System Sequence Diagram (SSD)

*Insert here a SSD depicting the envisioned Actor-System interactions and throughout which data is inputted and outputted to fulfill the requirement. All interactions must be numbered.*

![USXXX-SSD](USXXX-SSD.svg)


### 1.7 Other Relevant Remarks

*Use this section to capture other relevant information that is related with this US such as (i) special requirements ; (ii) data and/or technology variations; (iii) how often this US is held.*


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt
*In this section, it is suggested to present an excerpt of the domain model that is seen as relevant to fulfill this requirement.*

![USXXX-MD](USXXX-MD.svg)

### 2.2. Other Remarks

*Use this section to capture some aditional notes/remarks that must be taken into consideration into the design activity. In some case, it might be usefull to add other analysis artifacts (e.g. activity or state diagrams).*



## 3. Design - User Story Realization

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1  		 |							 |             |                              |
| Step 2  		 |							 |             |                              |
| Step 3  		 |							 |             |                              |
| Step 4  		 |							 |             |                              |
| Step 5  		 |							 |             |                              |
| Step 6  		 |							 |             |                              |              
| Step 7  		 |							 |             |                              |
| Step 8  		 |							 |             |                              |
| Step 9  		 |							 |             |                              |
| Step 10  		 |							 |             |                              |  


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are:

* Class1
* Class2
* Class3

Other software classes (i.e. Pure Fabrication) identified:
* xxxxUI
* xxxxController

## 3.2. Sequence Diagram (SD)

*In this section, it is suggested to present an UML dynamic view stating the sequence of domain related software objects' interactions that allows to fulfill the requirement.*

![USXXX-SD](USXXX-SD.svg)

## 3.3. Class Diagram (CD)

*In this section, it is suggested to present an UML static view representing the main domain related software classes that are involved in fulfilling the requirement as well as and their relations, attributes and methods.*

![USXXX-CD](USXXX-CD.svg)

# 4. Tests
*In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of requirements fulfilling.*

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

**Test 1:** Check that it is not possible to create an instance of the Example class with null values.

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

*It is also recommended to organize this content by subsections.*

# 5. Construction (Implementation)

*In this section, it is suggested to provide, if necessary, some evidence that the construction/implementation is in accordance with the previously carried out design. Furthermore, it is recommeded to mention/describe the existence of other relevant (e.g. configuration) files and highlight relevant commits.*

*It is also recommended to organize this content by subsections.*

# 6. Integration and Demo

*In this section, it is suggested to describe the efforts made to integrate this functionality with the other features of the system.*


# 7. Observations

*In this section, it is suggested to present a critical perspective on the developed work, pointing, for example, to other alternatives and or future related work.*

