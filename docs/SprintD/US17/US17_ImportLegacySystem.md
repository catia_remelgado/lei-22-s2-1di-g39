# US 006 - To create a Task 

## 1. Requirements Engineering


### 1.1. User Story Description


As a center coordinator, I want to import data from a legacy system that was used in the past to manage centers.


### 1.2. Customer Specifications and Clarifications 


**From the specifications document:**

>	**The Center Coordinator wants to** monitor the vaccination process, to see statistics and charts, to evaluate the performance of the vaccination process, generate reports and **analyze data from other centers, including data from legacy systems**.



**From the client clarifications:**

>**Client:** That file is from a legacy system, tha uses a different date format. The date should be converted when loading the data into the application that we are developing. [Moodle link](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16770#p21591)

> **Question:** Should each algorithm be capable of both sortings or is one of the algorithms supposed to do one (e.g. arrival time) and the other the remaining sorting criteria (e.g. leaving time)? [Moodle link](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16767#p21503)
>  
> **Answer:** Each algorithm should be capable of doing both sortings. The application should be prepared to run both algorithms. The algorithm that will be used to sort data should be defined in a configuration file.

---

> **Question:** It should be an option to choose to either sort by arrival time or by the center leaving time? [Moodle link](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16788#p21528)
>  
> **Answer:** The user can choose to sort by arrival time or by the center leaving time.

---

> **Question:** Dear Client,
>
>I was analysing the csv file that should be imported for US17 (the one that is in moodle),
>
>I noticed that the date attributes are written like this 5/30/2022 I thought that the date format should be DD/MM/YYYY.
>
>I also noticed, that the time is written like this, 9:43, I also thought that the time format should be written like this HH:MM, (in this case it would be 09:43).
>
>Are the date and time formats diferent for US17?
>
>Best Regards,
>
>G63
>
>
> [Moodle link](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16789#p21529)
>
> **Answer:** That file is from a legacy system, that uses a different date and time format. The date and time should be converted when loading the data into the application that we are developing.

----

> **Question:** I noticed that some postal codes in the address does not follow the format of XXXX-YYY. For example some of them are XXXX-Y. Are we supposed to be able to load those users as well? [Moodle link](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16797#p21537)
>
> **Answer:** Yes.

---

> **Question:** in a meeting you already clarified that when uploading a file from a legacy system the application should check if the SNS Users are already registered and if not US 014 should be put to use. My question is now if only one or two SNS Users are not registered, should the whole legacy file be discarded? [Moodle link](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16886#p21650)
>
> **Answer:** SNS users that are not registered should be loaded/registered. The other SNS users should not be registered again and should be ignored.

---

> **Question:** The file loaded in US17 have only one day to analyse or it could have more than one day?  [Moodle link](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=17144)
>
> **Answer:** The file can have data from more than one day.

---

> **Question:** Should the configuration file be defined , manually, before strating the program? Or Should an administrator or another DGS entity be able to alter the file in a user interface? This question is also important for US06 and US16 since these US also use configuration files, will the same process be applied to them? [Moodle link](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=17143)
>
> **Answer:** The configuration file should be edited manually.

---

> **Question:** You answered to a previous question saying that the user should be able to sort by ascending or descending order. Should the user choose in the UI, the order in which the information should be presented? Or should this feature be defined in the configuration file? [Moodle link](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=17070)
>
> **Answer:** The center coordinator must use the GUI to select the sorting type (ascending or descending).

---

> **Question:** When sorting data by arrival time or central leaving time, should we sort from greater to smallest or from smallest to greater? [Moodle link](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16978)
>
> **Answer:** The user must be able to sort in ascending and descending order.

---


> **Question:**  Also, should we consider only time or date also? So, for example, if we sort from smaller to greater and consider a date also, 20/11/2020 11:00 would go before 20/12/2020 08:00. Without considering the date (only time) it would be 20/12/2020 08:00 before 20/11/2020 11:00. [Moodle link](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16978)
>
> **Answer:**  Date and time should be used to sort the data. Sort the data by date and then by time.


---


> **Question:** Should the vaccine named Spikevax, (the one in the given CSV file for US17), be registered before loading the CSV file? [Moodle link](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16969)
>
> **Answer:** Yes.

---


> **Question:** Is there any correct format for the lot number? Should we simply assume that the lot number will always appear like this 21C16-05 ,like it's written in the file, and not validate it? [Moodle link](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16922)
>
> **Answer:**The lot number has five alphanumeric characters an hyphen and two numerical characters (examples: 21C16-05 and A1C16-22 )

---


> **Answer:** US14 and US17 are two different and independent features of the system. In US17, if the SNS user does not exist in the system, the vaccination of this SNS user should not be loaded. The system should continue processing the CSV file until all vaccinations are processed. [Moodle link](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16921#p21820)


---

### 1.3. Acceptance Criteria

* **AC1:** Two sorting algorithms should be implemented (to be chosen manually by the coordinator **in the configuration file**). (The imported data should be presented to the user sorted by arrival time **or** by the center leaving time.)
* **AC2:** The name of the SNS user and the vaccine type Short Description attribute should also be presented to the user.
* **AC3:** The center coordinator must be able to choose the file that is to be uploaded.
* **AC4:** Worst-case time complexity of each algorithm should be documented in the application user manual (in the annexes) that must be delivered with the application.
* **AC5:** The SNS user number must be validated and exists in the system and if it's not the line(s) must be ignored.
* **AC6:** The lot number of the vaccine must be verified, its format to be valid is to be composed by five  alphanumeric characters an hyphen and two numerical characters and if it's not the line(s) must be ignored.
* **AC7:** It's necessary to verify if the vaccine it's on the system and if it's not the line(s) must be ignored.
* **AC8:** The following data must be presented to the user: SNS user number, SNS user's name, vaccine name, vaccine's short description, vaccine administered dose, scheduled appointment date and time, the user's arrival date and time for the appointment, nurse's vaccine's administration date and time and the user's leaving center date and time.


### 1.4. Found out Dependencies

* There is a dependency to "US03 Register SNS User" since users must exist to exist vaccine administrations.
* There is a dependency to "US09 Register New Vaccination Center" since each Center Coordinator is associated to a Vaccination Center, therefore there must be a Vaccination Center in the system to associate one with a Center Coordinator and also the legacy system data belongs to a vaccination center.
* There is a dependency to "US10 Register New Employee" since a Center Coordinator must exist (be registered in the system) to load a csv file.
* There is a dependency to "US12 Specify New Vaccine Type" since Vaccine Types must exist to be possible to show a Short Description about them and having a vaccine.
* There is a dependency to "US13 Specify a New Vaccine Type and Its Administration Process" since a vaccine must exist to be associated with its short description.
* There is a dependency to "US14 Load Set Of Users" since users must exist to exist vaccine administrations.





### 1.5 Input and Output Data


**Input Data:**
	
* Selected data:
  * File to load 
  * How data will be sorted (by arrival time or by leaving time)
  * The order in which data will be sorted (ascending or descending)


**Output Data:**

* Imported data sorted by the previously chosen options

### 1.6. System Sequence Diagram (SSD)


![US17_SSD](US17_SSD.svg)


### 1.7 Other Relevant Remarks

* 


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 

![US17_MD](US17_MD.svg)

### 2.2. Other Remarks

n/a


## 3. Design - User Story Realization 

### 3.1. Rationale

| Interaction ID | Question: Which class is responsible for...             | Answer                      | Justification (with patterns)                                                                                 |
| :------------- | :---------------------                                  | :------------               | :----------------------------                                                                                 |
| Step 1         | ... interacting with the actor?                         | ImportLegacyUI              | Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model. |
|                | ... coordinating the US?                                | ImportLegacyController      | Controller                                                                                                    |
| Step 2         | ... reading the csv file?                               | LoadFromFileCSV             | IE: this class knows how to read csv files.                                                                   |
| Step 3         |                                                         |                             |                                                                                                               |
| Step 4         | ...treating/processing the csv file's data?             | TreatLegacySystemData       | PureFabrication: there's no need to assign this responsibility to any existing class in the Domain Model.     |
|                | ...validating the dates?                                | DateUtils                   | IE: Knows its own data                                                                                        |
|                | ...validating the times?                                | TimeUtils                   | IE: Knows its own data                                                                                        |
|                | ...validating the vaccines lot numbers?                 | VaccineAdministration       | IE: knows its own data                                                                                        |
|                | ...validating the SNS user numbers?                     | SNSUser                     | IE: knows its own data                                                                                        |
|                | ...having the names of all sns users?                   | SNSUserStore                | PureFabrication/IE: knows its own data                                                                        |
|                | ...having all vaccine names?                            | VaccineStore                | PureFabrication/IE: knows its own data                                                                        |
|                | ...saving all the data?                                 | VaccinationCenter           | HAs the atribute of VaccineAdministration                                                                     |
|                | ...passing the data to a vaccine administration object? | VaccineAdministrationMapper |                                                                                                               |
|                |                                                         | VaccineAdministrationDTO    |                                                                                                               |
| Step 5         | ...showing the information to the user?                 | ImportLegacyUI              | Interacts with the actor                                                                                      |

### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * LoadFromCSVFile
 * DateUtils
 * TimeUtils
 * SNSUSer
 * VaccinationCenter
 * VaccineAdministration

Other software classes (i.e. Pure Fabrication) identified: 

 * ImportLegacyUI  
 * ImportLegacyController
 * TreatLegacyData
 * SNSUserStore
 * VaccineStore
 * VaccineAdministrationMapper
 * VaccineAdministrationDTO


## 3.2. Sequence Diagram (SD)

![US17_SD](US17_SD.svg)

## 3.3. Class Diagram (CD)

![US17_CD](US17_CD.svg)

# 4. Tests

# 5. Construction (Implementation)

# 6. Integration and Demo

# 7. Observations





