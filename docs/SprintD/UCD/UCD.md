# Use Case Diagram (UCD)

**In the scope of this project, there is a direct relationship of _1 to 1_ between Use Cases (UC) and User Stories (US)
.**

However, be aware, this is a pedagogical simplification. On further projects and curricular units might also exist _1 to
N **and/or** N to 1 relationships between US and UC.

**Insert below the Use Case Diagram in a SVG format**

![Use Case Diagram](UCD.svg)

**For each UC/US, it must be provided evidences of applying main activities of the software development process (
requirements, analysis, design, tests and code). Gather those evidences on a separate file for each UC/US and set up a
link as suggested below.**

# Use Cases / User Stories

| UC/US    | Description                                                                                               |
| :------- | :----------------------------------------                                                                 |
| US 001   | [ScheduleVaccine](../../SprintD/US01/US01_ScheduleVaccine.md)                                             |
| US 002   | [ScheduleVaccineForSNSUser](../../SprintD/US02/US02_ScheduleVaccine.md)                                   |
| US 003   | [RegisterSNSUser](../../SprintD/US03/US03_RegisterSNSUser.md)                                             |
| US 004   | [RegisterArrivalSNSUser](../../SprintD/US04/US04_RegisterArrivalSNSUser.md)                               |
| US 005   | [ConsultWaitingRoomList](../../SprintD/US05/US05_ConsultWaitingRoom.md)                                   |
| US 006   | [RecordDailyVaccinatedPeople](../../SprintD/US06/US06_RecordDailyVaccinatedPeople.md)                     |
| US 007   | [RecordAdverseReactions](../../SprintD/US07/US07_RecordAdverseReactions.md)                               |
| US 008   | [RecordVaccineAdministrationAndSendSMS](../../SprintD/US08/US08_RecordVaccineAdministrationAndSendSMS.md) |
| US 009   | [Register_V_Center](../../SprintD/US09/US09_RegisterNewVaccinationCenter.md)                              |
| US 010   | [RegisterEmployees](../../SprintD/US10/US10.md)                                                           |
| US 011   | [ListEmployee](../../SprintD/US11/US11_GetEmployeeList.md)                                                |
| US 012   | [RegisterNewVaccineType](../US12/US12.md)                                                                 |
| US 013   | [RegisterNewVaccineAndAdministrationProcess](../../SprintD/US13/US13_RegisterNewVaccine.md)               |
| US 014   | [LoadUsersFromFile](../../SprintD/US14/US14_LoadFileOfUser.md)                                            |
| US 015   | [StatisticsOfFullyVaccinated](../../SprintD/US15/US15_StatisticsOfFullyVaccinated.md)                     |
| US 016   | [EvaluateCenterPerformance](../../SprintD/US16/US16_EvaluateCenterPerformance.md)                         |
| US 017   | [ImportDataFromLegacy](../../SprintD/US17/US17_ImportDataFromLegacy.md)                                   |
| US 018   | [ListVaccines](../../SprintD/US18/US18_ListVaccines.md)                                                   |