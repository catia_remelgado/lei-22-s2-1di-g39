# OO Analysis #

The construction process of the domain model is based on the client specifications, especially the nouns (for _concepts_) and verbs (for _relations_) used. 

## Rationale to identify domain conceptual classes ##
To identify domain conceptual classes, start by making a list of candidate conceptual classes inspired by the list of categories suggested in the book "Applying UML and Patterns: An Introduction to Object-Oriented Analysis and Design and Iterative Development". 


### _Conceptual Class Category List_ ###

**Business Transactions**

* Vaccine administration

---

**Transaction Line Items**

*

---

**Product/Service related to a Transaction or Transaction Line Item**

* Vaccine

---


**Transaction Records**

* Vaccine certificate

---  


**Roles of People or Organizations**

* Administrator
* Employee
* SNS user
* Nurse
* Receptionist
* Center Coordinator


 
---


**Places**

* Vaccination Center
* Healthcare Center
* MassCommunityVaccinationCenter   
---

**Noteworthy Events**

* Appointment
* VaccineAdministrationProcess
* DoseAdministration
* AdverseReaction
* **Appointment**

---


**Physical Objects**

* Vaccine
 
---


**Descriptions of Things**

* Vaccine type
* Vaccine Brand
* Age Group
* Address
* Date
* Time

---


**Catalogs**



---


**Containers**

* **WaitingRoom**

---


**Elements of Containers**

***Arrival**
***Recovery**

---


**Organizations**

* Company 

---

**Other External/Collaborating Systems**

*

 
---


**Records of finance, work, contracts, legal matters**

* Message
***Email**
* Vaccination Certificate
* CenterPerformance

---


**Financial Instruments**

*

---


**Documents mentioned/used to perform some work/**


---




###**Rationale to identify associations between conceptual classes**###

An association is a relationship between instances of objects that indicates a relevant connection and that is worth of remembering, or it is derivable from the List of Common Associations: 

+ **_A_** is physically or logically part of **_B_**
+ **_A_** is physically or logically contained in/on **_B_**
+ **_A_** is a description for **_B_**
+ **_A_** known/logged/recorded/reported/captured in **_B_**
+ **_A_** uses or manages or owns **_B_**
+ **_A_** is related with a transaction (item) of **_B_**
+ etc.



| Concept (A)                      | Association                                                                                           | Concept (B)                                                                                                             |
| :------------------------------: | :---------------------------------------------------------------------------------------------------: | :---------------------------------------------------------------------------------------------------------------------: |
| VaccineAdministrationProcess     | has <br /> administered <br /> of                                                                     | DoseAdministration <br /> AgeGroup <br />  Vaccine                                                                      |
| Administrator                    | appointed by <br /> registers <br /> registers<br /> registers <br /> creates                         | Company <br /> Center Coordinator <br /> SNS User <br /> Receptionist <br /> Vaccine                                    |
| Employee                         | works at <br/> acts as <br/> lives in                                                                 | VaccinationCenter <br/> SNSUser <br/>    Address                                                                        |
| SNSUser                          | belongs to <br/>   may develop  <br/>   performs <br/> lives in  <br/>   performs                     | Age Group <br /> AdverseReaction <br/> Arrival <br/> Address </br> Arrival                                              |
| Nurse                            | issues <br /> registered by <br /> checks condition of <br/> registers <br/> is<br/>checks<br/>       | Vaccination Certificate <br /> Administrator <br /> SNS User <br/> AdverseReaction <br/>Employee<br/> **WaitingRoom**   |
| Receptionist                     | confirms<br/>is                                                                                       | Appointment<br/>Employee                                                                                                |
| Center Coordinator               | runs  <br/>is  <br/> **analyzes** <br/> **loads**                                                     | VaccinationCenter <br/>Employee  </br> **CenterPerformance** </br> **CSVFile**                                          |
| Vaccination Center               | is place of  <br/>  registered by<br/> is located on  <br/> has  <br/> **has**                        | Appointment <br/>Administrator <br/> Address <br/>WaitingRoom  </br> **CenterPerformance**                              |
| Healthcare Center                | administers  <br/>is                                                                                  | VaccineType  <br/>VaccinationCenter                                                                                     |
| Appointment                      | created by <br /> to take <br/> has a certain <br/> has a certain                                     | SNS User <br /> Vaccine <br/> Date <br/> Time                                                                           |
| Company                          | Manages <br/>provides<br/>administers<br/>administers<br/>owns<br/>employ                             | VaccinationCenter<br/>Vaccine<br/>VaccineAdministrationProcess<br/>VaccineType<br/>SNSUser<br/>Employee                 |
| Vaccine Type                     | created by                                                                                            | Administrator                                                                                                           |
| SMS                              | is <br/> to remind of                                                                                 | Message <br/>  Scheduling                                                                                               |
| Vaccination Certificate          | requested by                                                                                          | SNS User                                                                                                                |
| Vaccine                          | administered by <br/>is of<br/> may provoke                                                           | Nurse <br /> VaccineType  <br/> AdverseReaction                                                                         |
| MassCommunityVaccinationCenter   | administers<br/>is                                                                                    | VaccineType <br/>VaccinationCenter                                                                                      |
| Message                          | has a certain <br/> has a certain <br/> sent from <br/> received by                                   | Date <br/> Time <br/> VaccinationCenter <br/> SNSUser                                                                   |
| Appointment                      | has a certain <br/> has a certain <br/> to take <br/> created by                                      | Date <br/> Time <br/> Vaccine <br/> SNSUser                                                                             |
| Email                            | is                                                                                                    | Message                                                                                                                 |
| WaitingRoom                      | to take <br/>has<br/>**has**                                                                          | Vaccine <br/>Arrival<br/>**Recovery**                                                                                   |
| DoseAdministration               | may provoke <br/> after recovery sends                                                                | AdverseReaction <br/>Message                                                                                            |
| Arrival                          | to attend <br/>has an entry<br/>has an entry date                                                     | Appointment <br/>Time <br/>  Date                                                                                       |
| **CSVFile**                      | registers </br> **has previous data of**                                                              | SNS User  </br> **VaccinationCenter**                                                                                   |
| **Recovery**                     | **of an <br/>has an exit<br/>has an exit**                                                            | **Appointment <br/>Time <br/>  Date**                                                                                   |
| **CenterPerformance**            | **occurs on <br/>has a start<br/>has a finish<br/> of**                                               | **Date <br/>Time<br/>Time<br/>VaccineAdministration**                                                                   |
| **VaccineAdministration**        | **occurs on <br/>occurs on <br/>occurs on <br/>performed on<br/>occurs on<br/>of a**                  | **Date <br/>Time <br/>VaccinationCenter <br/>SNSUser<br/>Vaccine**                                                      |

**Bold text**: New information added in this Sprint.


## Domain Model

**Do NOT forget to identify concepts atributes too.**

**Insert below the Domain Model Diagram in a SVG format**

![DM.svg](DM.svg)



