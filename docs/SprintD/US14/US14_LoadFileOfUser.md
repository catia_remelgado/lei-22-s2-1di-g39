# US 14 - Load a file a set of users.

## 1. Requirements Engineering

### 1.1. User Story Description

As an administrator, I want to load a set of users from a CSV file.

### 1.2. Customer Specifications and Clarifications

**From the specifications document:**

>

**From the client clarifications:**

- Q1: When the admin wants to upload a CSV file to be read, should the file be stored at a specific location on the computer (e.g. the desktop) or should the admin be able to choose the file he wants to upload in a file explorer?
> The Administrator should write the file path. In Sprint C we do not ask students to develop a graphical user interface.
[Question on the moodle](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16332#p20954)

- Q2: The CSV file only contains information about SNS users of if the CSV file may also contain some information about employees from that vaccination center.
> The CSV file only contains information about SNS users.

- Q3: What would be the sequence of parameters to be read on the CSV? For example: "Name | User Number".
> Name, Sex, Birth Date, Address, Phone Number, E-mail, SNS User Number and Citizen Card Number.
[Question on the moodle]()

- Q4: When the system is reading the file, should it discard all the lines that contain input mistakes?
> no response
[Question on the moodle](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16519#p21191)

- Q5: Should the set of users be associated to a specific Vaccination Center?
> no response
[Question on the moodle](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16509#p21181)

- Q6: In witch format will be given the date of birth (YYYY/MM/DD or DD/MM/YYYY)?
> The dates registered in the system should follow the Portuguese format (dd/mm/yyyy)
[Question on the moodle](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16490#p21162)

- Q7: Should our application detect if the CSV file to be loaded contains the header, or should we ask the user if is submitting a file with a header or not?
> The application should automatically detect the CSV file type.
[Question on the moodle](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16567#p21257)


### 1.3. Acceptance Criteria

* **AC1:** One type must have a header, column separation is done using “;” character;
* **AC2:** One type does not have a header, column separation is done using “,” character.

### 1.4. Found out Dependencies
<!-- - US3: As a receptionist, I want to register a SNS user.-->

### 1.5 Input and Output Data

**Input Data:**

*Typed data:* File path
* Selected data: *none*

**Output Data:**
* List of users which were loaded.
* (In)Success of the operation.

### 1.6. System Sequence Diagram (SSD)

![US14_SSD](US14_SSD.svg)


### 1.7 Other Relevant Remarks

* The created task stays in a "not published" state in order to distinguish from "published" tasks.

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

![US14_MD](US14_MD.svg)
<!-- -->
### 2.2. Other Remarks

No other remarks have been made

## 3. Design - User Story Realization

### 3.1. Rationale


| Interaction ID | Question: Which class is responsible for...                        | Answer                      | Justification (with patterns)                                                                                                                                                                                                                                                                                                            |
|:---------------|:-------------------------------------------------------------------|:----------------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Step 1:        | ... interacting with the actor?                                    | LoadUsersFromFileUI         | **Pure Fabrication:** there is no reason to assign this responsibility to any existing class in the Domain Model.                                                                                                                                                                                                                        |
|                | ... coordination de US?                                            | LoadUsersFromFileController | Controller (Handles a UI event).                                                                                                                                                                                                                                                                                                         |
| Step 2:        |                                                                    |                             |                                                                                                                                                                                                                                                                                                                                          |
| Step 3:        | ... instantiating a file object?                                   | LoadFileController          | **Creator (R1):** It's easier to work with the object itself.                                                                                                                                                                                                                                                                            | 
|                | ... validating if the file exists and have the required extension? | FileHandler                 | **IE:** the object knows it's own data.                                                                                                                                                                                                                                                                                                  |
|                | ... reading the data present in the file?                          | FileHandler                 | **IE:** the object knows where is the file to get the information.                                                                                                                                                                                                                                                                       |
| Step 4:        |                                                                    |                             |                                                                                                                                                                                                                                                                                                                                          |
| Step 5:        | ... transfer the data read in the file to the domain?              | SNSUserDTO                  | **DTO:** When there is so much data to transfer, it is better to opt by using a DTO in order to reduce coupling between the controller and domain.                                                                                                                                                                                       |
|                | ... saving the read data?                                          | SNSUser                     | **IE:** a SNS User knows its own data.                                                                                                                                                                                                                                                                                                   |
|                | ... instantiating a new SNS User?                                  | SNSUserMapper               | **DTO** using **Mapper**, **Creator (R1)** and **HC+LC:** By the application of the Creator (R1) it would be the "Company". But, by applying HC + LC to the "Company", and since we are using the DTO pattern to transfer the data between the UI layer and the domain layer, this delegates that responsibility to the "SNSUserMapper". |
|                | ... knows SNSUserStore?                                            | Company                     | **IE:** Company knows the SNSUserStore to which it is delegating some tasks.                                                                                                                                                                                                                                                             |
|                | ... validating all data (local validation)?                        | SNSUser                     | **IE:** an object knows its own data.                                                                                                                                                                                                                                                                                                    |
|                | ... validating all data (global validation)?                       | SNSUserStore                | **IE:** knows all the SNS Users.                                                                                                                                                                                                                                                                                                         |
|                | ... saving the SNSUser?                                            | SNSUserStore                | **IE:** Knows all SNS Users.                                                                                                                                                                                                                                                                                                             |
| Step 6:        | ... informing operation success?                                   | LoadUsersFromFileUI         | **IE:** is responsible for user interactions.                                                                                                                                                                                                                                                                                            |


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are:

*LoadFile

Other software classes (i.e. Pure Fabrication) identified:

* ImportCSVFileUI
* ImportCSVFileController

## 3.2. Sequence Diagram (SD)
![US14_SD](US14_SD.svg)


## 3.3. Class Diagram (CD)
![US14_CD](US14_CD.svg)


# 4. Tests

**Test 1:** Check that it is not possible to create an instance of the Task class with null values.

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Task instance = new Task(null, null, null, null, null, null, null);
	}

**Test 2:** Check that it is not possible to create an instance of the Task class with a reference containing less than
five chars - AC2.

	@Test(expected = IllegalArgumentException.class)
		public void ensureReferenceMeetsAC2() {
		Category cat = new Category(10, "Category 10");
		
		Task instance = new Task("Ab1", "Task Description", "Informal Data", "Technical Data", 3, 3780, cat);
	}

*It is also recommended to organize this content by subsections.*

# 5. Construction (Implementation)

## Class CreateTaskController

		public boolean createTask(String ref, String designation, String informalDesc, 
			String technicalDesc, Integer duration, Double cost, Integer catId)() {
		
			Category cat = this.platform.getCategoryById(catId);
			
			Organization org;
			// ... (omitted)
			
			this.task = org.createTask(ref, designation, informalDesc, technicalDesc, duration, cost, cat);
			
			return (this.task != null);
		}

## Class Organization

		public Task createTask(String ref, String designation, String informalDesc, 
			String technicalDesc, Integer duration, Double cost, Category cat)() {
		
	
			Task task = new Task(ref, designation, informalDesc, technicalDesc, duration, cost, cat);
			if (this.validateTask(task))
				return task;
			return null;
		}

# 6. Integration and Demo

* A new option on the Employee menu options was added.

* Some demo purposes some tasks are bootstrapped while system starts.

# 7. Observations

Platform and Organization classes are getting too many responsibilities due to IE pattern and, therefore, they are
becoming huge and harder to maintain.

Is there any way to avoid this to happen?





