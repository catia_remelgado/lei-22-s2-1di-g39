# US01 - Schedule Vaccine

## 1. Requirements Engineering


### 1.1. User Story Description

As a SNS user, I intend to use the application to schedule a vaccine.

### 1.2. Customer Specifications and Clarifications 

**From the specifications document:**

> "DGS is responsible for the Vaccination Program in Portugal and needs a software application to manage the vaccination process and to **allow SNS users to schedule a vaccine** (...)."

> "To take a vaccine, the SNS user should use the application to schedule his/her vaccination. The user should introduce his/her SNS user number, select the vaccination center, the date, and the time (s)he wants to be vaccinated as well as the type of vaccine to be administered (by default, the system suggests the one related to the ongoing outbreak). Then, the application should check the vaccination center capacity for that day/time and, if possible, confirm that the vaccination is scheduled and inform the user that (s)he should be at the selected vaccination center at the scheduled day and time.  The SNS user may also authorize the DGS to send a SMS message with information about the scheduled appointment. If the user authorizes the sending of the SMS, the application should send an SMS message when the vaccination event is scheduled and registered in the system"

**From the client clarifications:**

>"Dear students,
> 
>The acceptance criteria for US1 and US2 are: a. A SNS user cannot schedule the same vaccine more than once. b. The algorithm should check if the SNS User is within the age and time since the last vaccine".
> 
>This means:
>
>a. At a given moment, the SNS user cannot have more than one vaccine (of a given type) scheduled;
>
>b. The algorithm has to check which vaccine the SNS user took before and check if the conditions (age and time since the last vaccine) are met. If the conditions are met the vaccination event should be scheduled and registered in the system. When scheduling the first dose there is no need to check these conditions.
>
>Best regards,
>
>Carlos Ferreira"
> [link to Moodle](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16655#p21366)
 
-
> "Dear students,
>
>In a previous message I said: "The acceptance criteria for US1 and US2 are: a. A SNS user cannot schedule the same vaccine more than once. b. The algorithm should check if the SNS User is within the age and time since the last vaccine".
>
>We can always prepare a system for these two acceptance criteria. Even so, as we are in the final stage of Sprint C, we will drop the acceptance criteria b ( The algorithm should check if the SNS User is within the age and time since the last vaccine). This acceptance criteria will be included in Sprint D.
>
>Best regards,
>Carlos Ferreira"
> [link to Moodle](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16655#p21387)
 
-
>**Question**: Regarding vaccination centers, in the project description is said: “Both kinds of vaccination centers are characterized by a name, an address, a phone number, an e-mail address, a fax number, a website address, opening and closing hours, slot duration (e.g.: 5 minutes) and the maximum number of vaccines that can be given per slot (e.g.: 10 vaccines per slot).”
>
>First, it’s important to clarify that our team understands slot as a “Vaccination room where a person is vaccinated by a nurse”.
>
>Our doubts are:
>
>1- Shouldn’t the vaccination center be also characterized by the number of available slots that it has? (So, for example, some vaccination center has 10 available slots and following the given example in the project description, each slot can give maximum 10 vaccines, that means this vaccination center would be able to administer 100 vaccines a day.) Or should we consider that at each center exists only one slot? [link to Moodle](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16532#p21205)
>
> **Answer**:  1- There is only one vaccination room per center.
 
-
>**Question**: What kind of information should be included in an SMS Message to warn of a scheduling? (for example, the SNS number, center, day, time and vaccine type)?[link to Moodle](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=15508#p19949)
> 
> **Answer**: Date, Time and vaccination center.
 
-
> **Question**: We are unsure if it's in this user stories that's asked to implement the "send a SMS message with information about the scheduled appointment" found on the Project Description available in moodle. Could you clarify? [link to Moodle](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16457#p21238)
> 
> **Answer**: In a previous clarification that I made on this forum, I said: "[The user should receive a] SMS Message to warn of a scheduling [and the message] should include: Date, Time and vaccination center". Teams must record the answers! A file named SMS.txt should be used to receive/record the SMS messages. We will not use a real word service to send SMSs.
 
-
>**Question**: Does the user have to enter the date and time they want or does the system have to show the available dates and times for the user to select?[link to Moodle](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16615#p21323)
> 
> **Answer**:  In this sprint the actor should introduce (using the keyboard) the date and time (s)he wants to be vaccinated.


### 1.3. Acceptance Criteria

* **AC1:** An SNS user cannot schedule the same vaccine (type) more than once.
* **AC2:** The SNS user must write the time and date (not select).
* **AC3:** The system must verify if the chosen vaccination center is available to give the user the vacccine in the typed time and date.
* **AC4:** The message must contain date, time and vaccination center and be saved on a txt file.
* **AC?:** The algorithm should check if the SNS User is within the age and time since the last vaccine. (**This criteria was dropped last minute.**)

### 1.4. Found out Dependencies

* There is a dependency to "US03 Register an SNS User" since the SNSUser must be registered in the system to be able to schedule their vaccine.
* There is a dependency to "US09 Register a New Vaccination Center" since vaccination(s) center(s) must be registered in the system so the user can choose at which center they want to be vaccinated.
* There is a dependency to "US12 Register new vaccine type" since the vaccine type must be available in the system for the SNSUser choose which one they want to schedule.
* There is a dependency to "US13 Register new vaccine" since this has the data to confirm if the SNSUser has the age and the recovery time to take the vaccine they are scheduling. (**This dependency is dropped due to acceptance criteria being dropped last minute.**)


### 1.5 Input and Output Data


**Input Data:**
* Typed data:
    * SNSUSer number
    * date
    * time
  
* Selected data:
  * type of vaccine
  * vaccination center
  * Yes (authorizes to receive a message) or No (does not authorize toreceive a message)
  
**Output Data:**

* Confirmation if the SNSUser can schedule the vaccine (for that center, day and time)
* Asks if the SNSUser authorizes the DGS to send them an SMS message with information about the scheduled appointment
* Confirmation of user's choices

### 1.6. System Sequence Diagram (SSD)


![US01_SSD](US01_SSD.svg)


**Other alternatives might exist.**

### 1.7 Other Relevant Remarks

n/a


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 

![US01_MD](US01_MD.svg)

### 2.2. Other Remarks

n/a


## 3. Design - User Story Realization 

### 3.1. Rationale



| Interaction ID | Question: Which class is responsible for...                                      | Answer                 | Justification (with patterns)                                                                                 |
| :------------- | :---------------------                                                           | :------------          | :----------------------------                                                                                 |
| Step 1         | ...interacting with the SNSUser?                                                 | AppointmentUI          | Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model. |
|                | ...coordinating the US?                                                          | AppointmentController  | Controller: This class is responsible for coordinating the system operations beyond the UI.                   |
|                | ...instantiating a new vaccine schedule?                                         | AppointmentStore       |                                                                                                               |
| Step 2         |                                                                                  |                        |                                                                                                               |
| Step 3         | ...verifying if the inputted SNSUser number is correct?                          | SNSUser                | IE: SNSUser knows the SNSUser number format.                                                                      |
| Step 4         | ...knowing the vaccine types to show?                                            | VaccineTypeStore       | IE: VaccineTypeStore has all vaccine types.                                                                       |
| Step 5         | ...verifying if is the first time that the user schedules selected vaccine type? | AppointmentStore       | IE: AppointmentStore has all data about schedules.                                                                |
| Step 6         | ...knowing the vaccination centers to show?                                      | VaccinationCenterStore | IE: VaccinationCenterStore has all vaccination centers.                                                           |
| Step 7         |                                                                                  |                        |                                                                                                               |
| Step 8         |                                                                                  |                        |                                                                                                               |
| Step 9         | ...verifying if the date is correct?                                             | DateUtils              | IE: DateUtils knows everything about its format.                                                                  |
|                | ...verifying if it's possible to schedule for this day?                          | AppointmentStore       | IE: AppointmentStore has all schedules.                                                                           |
| Step 10        |                                                                                  |                        |                                                                                                               |
| Step 11        | ...verifying if the time is correct?                                             | TimeUtils              | IE: TimeUtils knows everything about its format.                                                                  |
|                | ...verifying if it's possible to make a schedule with the selected data?         | AppointmentStore       | IE: AppointmentStore has all schedules.                                                                           |
|                | ...creating a schedule with the data?                                            | Appointment            | IE: Appointment contains all data about itself.                                                                   |
|                | ...saving all the info about the schedule?                                       | AppointmentStore       | IE: AppointmentStore has all schedules.                                                                           |
| Step 12        | ...informing the operation success?                                              | AppointmentUI          | ScheduleVaccineUI interacts with the SNSUser.                                                                 |
| Step 13        | ...sending the message?                                                          | SMS                    | SMS is responsible for sending messages.                                                                      |
| Step 14        | ...informing the operation, success?                                             | AppointmentUI          | ScheduleVaccineUI interacts with the SNSUser.                                                                 |

### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * TimeUtils
 * DateUtils
 * SNSUser (already exists from another Sprint)
 * SMS

Other software classes (i.e. Pure Fabrication) identified: 

 * AppointmentUI  
 * AppointmentController
 * AppointmentStore
 * VaccineTypeStore (Already exists from another Sprint)


## 3.2. Sequence Diagram (SD)

![US01_SD](US01_SD.svg)

## 3.3. Class Diagram (CD)

![US01_CD](US01_CD.svg)

# 4. Tests 


# 5. Construction (Implementation)



# 6. Integration and Demo 

* Created SNSUser UI.

* Created AppointmentUI.


# 7. Observations







