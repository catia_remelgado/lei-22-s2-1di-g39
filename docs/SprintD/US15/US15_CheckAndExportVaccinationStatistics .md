# US 15 - CheckAndExportVaccinationStatistics 

## 1. Requirements Engineering

<!--As a center coordinator, I intend to check and export vaccination statistics. 
I want to export, to a csv file, the total number of fully vaccinated users per day. -->

### 1.1. User Story Description

As a center coordinator, I intend to check and export vaccination statistics.   
I want to export, to a csv file, the total number of fully vaccinated users per day.


### 1.2. Customer Specifications and Clarifications 


**From the specifications document:**

>"The Center Coordinator wants to monitor the vaccination process, to see statistics and charts, to evaluate the performance of the vaccination process, generate reports and analyze data from other  enters, including data from legacy systems."


**From the client clarifications:**

> **Question 1:** The user story says we should export the total number of fully vaccinated users per day. What constitutes fully vaccinated in the context of our program?    
> **Answer:** No response.
> [Question on the moodle](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16778#p21516)

> **Question2:** [1] When exporting vaccination statistics,do we export the data from all days available in the system or does the center coordinator chooses the time interval?   
> [2] Is there any kind of format our exported data should follow?  
> **Answer:** 1- The user should define a time interval (two dates). 2- Data format: date; number of fully vaccinated user.
> [Question on the moodle](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16799#p21539)

>**Question 3:** Is the exportation of the CSV file that contains the total number of fully vaccinated users per day, the only feature that needs to be implemented in code, for US15?    
>**Answer:** "Yes"
> [Question on the moodle](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16801#p21541)

>**Question 4:** Should the user introduce the name of the file  intended to export the vaccination statistics ?    
>**Answer:** "The user should introduce the name of the file."
> [Question on the moodle](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16812#p21554)

>**Question 5:** Are the vaccination statistics refering only to the fully vaccinated users or refering to something more ?    
> **Answer:** "Only to fully vaccinated users."
> [Question on the moodle](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16812#p21554)

>**Question 6:** In this US should the Center Coordinator check and export the Vaccination Statistics of the Center where he/she works at or should just check and export the Vaccination Statistics of all centers?  
> **Answer:** "The center coordinator can only export statistics from the vaccination center that he coordinates."
> [Question on the moodle](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16830#p21572)

> **Question 7:** In a previous forum answer, the client said the vaccination statistics should refer to the center, for which the coordinator is responsible. Given that users are not connected to a specific center how do we decide which center has the user fully vaccinated.  
> **Answer:** No response.
> [Question on the moodle](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16872#p21636)

> **Question 8:** In a previous answer you said "Data format: date; number of fully vaccinated user.". So our question is: Should we group all sns users fully vaccinated per day of different vaccine types into a total number of that day? Or should we divide the number by vaccine types?  
> **Answer:** No response.
> [Question on the moodle](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16977#p21758)

>**Question 9:**  
> **Answer:**

<!--por BARROS 1211299 - Tuesday, 7 de June de 2022 às 12:38 -->
### 1.3. Acceptance Criteria

* **AC1:** In the the data specified list the users that are full vaccinated;
* **AC2:** Time interval: beginning date, end date;

### 1.4. Found out Dependencies
* There is a dependency to "US13 Specify a new vaccine and its administration process" since to count the number of users who are fully vaccinated, the vaccine needs to be registered.
* There is a dependency to "US08 Record the administration of a vaccine to a SNS user" since it is necessary for the nurse to register the administration of the vaccine so that the user is accounted for.

### 1.5 Input and Output Data

**Input Data:**

* Typed data:  

  * Time interval (two dates);  
  * File name;    

* Selected data:  
    *  none.


**Output Data:**

   * File csv with vaccination statistics  
   * (In)Success of the operation

### 1.6. System Sequence Diagram (SSD)

![US15_SSD](US15_SSD.svg)


### 1.7 Other Relevant Remarks

* User story happens whenever a center coordinator would like to know a center's vaccination statistics (total number of fully vaccinated users per day).

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 

<!-- ![US006_MD](US006_MD.svg) -->

### 2.2. Other Remarks

n/a


## 3. Design - User Story Realization 

### 3.1. Rationale

**SSD - Alternative 1 is adopted.**

| Interaction ID | Question: Which class is responsible for...                       | Answer               | Justification (with patterns)                                                                                 |
| :------------- |:------------------------------------------------------------------|:---------------------| :----------------------------                                                                                 |
| Step 1         | ... interacting with the actor?                                   | StatisticsUI         | Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model. |
|                | ... coordinating the US?                                          | StatisticsController | Controller                                                                                                    |
|                | ... knowing the center that correspond to the center coordinator? | 00000000000000009    | IE: cf. A&A component documentation.                                                                          |
|                | ... knowing to which organization the user belongs to?            | Platform             | IE: has registed all Organizations                                                                            |
|                |                                                                   | Organization         | IE: knows/has its own Employees                                                                               |
|                |                                                                   | Employee             | IE: knows its own data (e.g. email)                                                                           |
| Step 2         |                                                                   |                      |                                                                                                               |
| Step 3         | ...saving the inputted data?                                      | Task                 | IE: object created in step 1 has its own data.                                                                |

### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * Organization
 * Platform
 * Task

Other software classes (i.e. Pure Fabrication) identified: 

 * CreateTaskUI  
 * CreateTaskController


## 3.2. Sequence Diagram (SD)

**Alternative 1**
<!--
![US006_SD](US006_SD.svg)

**Alternative 2**

![US006_SD](US006_SD_v2.svg)
-->
## 3.3. Class Diagram (CD)

**From alternative 1**
<!--
![US006_CD](US006_CD.svg)
-->
# 4. Tests 
<!--
**Test 1:** Check that it is not possible to create an instance of the Task class with null values. 

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Task instance = new Task(null, null, null, null, null, null, null);
	}
	

**Test 2:** Check that it is not possible to create an instance of the Task class with a reference containing less than five chars - AC2. 

	@Test(expected = IllegalArgumentException.class)
		public void ensureReferenceMeetsAC2() {
		Category cat = new Category(10, "Category 10");
		
		Task instance = new Task("Ab1", "Task Description", "Informal Data", "Technical Data", 3, 3780, cat);
	}

-->
*It is also recommended to organize this content by subsections.* 

# 5. Construction (Implementation)


## Class CreateTaskController 

		public boolean createTask(String ref, String designation, String informalDesc, 
			String technicalDesc, Integer duration, Double cost, Integer catId)() {
		
			Category cat = this.platform.getCategoryById(catId);
			
			Organization org;
			// ... (omitted)
			
			this.task = org.createTask(ref, designation, informalDesc, technicalDesc, duration, cost, cat);
			
			return (this.task != null);
		}


## Class Organization


		public Task createTask(String ref, String designation, String informalDesc, 
			String technicalDesc, Integer duration, Double cost, Category cat)() {
		
	
			Task task = new Task(ref, designation, informalDesc, technicalDesc, duration, cost, cat);
			if (this.validateTask(task))
				return task;
			return null;
		}



# 6. Integration and Demo 

* A new option on the Employee menu options was added.

* Some demo purposes some tasks are bootstrapped while system starts.


# 7. Observations

Platform and Organization classes are getting too many responsibilities due to IE pattern and, therefore, they are becoming huge and harder to maintain. 

Is there any way to avoid this to happen?





