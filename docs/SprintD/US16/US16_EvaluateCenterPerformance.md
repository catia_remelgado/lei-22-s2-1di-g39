# US 016 - Evaluate Center Performance

## 1. Requirements Engineering

### 1.1. User Story Description
As a center coordinator, I intend to ananlyze the performance of a center.

### 1.2. Customer Specifications and Clarifications

**From the specifications document:**

> "The Center Coordinator wants to [...] to evaluate the performance of the vaccination process..." - The center coordinator is responsible for analysing the efficiency of a vaccination center.

> "The goal of the performance analysis is to decrease the number of clients in the center, from the moment they register at the arrival, until the moment they receive the SMS informing they can leave the vaccination center. " - The analysis is made to decrease the number of people in the vaccination center, thus reducing the spread of any disease and improve efficiency.

> "...implement a procedure that, for a specific day, and time intervals of *m* minutes (for *m* = 30,20,10,5, and 1, for example) chosen by the coordinator of the center..." - center coordinator is able to evaluate center performance related to a specific time interval of a set of pre-defined time intervals.


> "... the procedure creates a list of length 720/*m* (respectively, list of length 24, 36, 72, 144, 720)..." - the procedure will have to create a sublist, each element of the sublist corresponding to an *m* minutes interval.

> "...the *i*-th value of the list is the difference between the number of new clients arriving and the number of clients leaving the center in that *i*-th time interval" - each element corresponds to the influx/outflow of people in the chosen time interval of *m* minutes.

> "The output should be the input list, the maximum sum contiguous sublist and its sum, and the time intervals corresponding  to this contiguous sublist" - after asking for a performance analysis, the center coordinator can visualize the original list of influx/outflow of people in the center per minute, a list of influx of maximum number people in a certain time interval, the quantity of people coming in this time interval, as well as the time interval ( start time and finish time ), that there was the largest influx of people. 

**From the client clarifications:**

> **Question:** "From the Sprint D requirements it is possible to understand that we ought to implement a procedure that creates a list with the differences between the number of new clients arriving and the number of leaving clients for each time interval. My question then is, should this list strictly data from the legacy system (csv file from moodle which is loaded in US17), or should it also include data from our system?"

> **Answer:** US 16 is for all the data that exists in the system.

-

>**Question:** "I would like to know if we could strict the user to pick only those intervals (m) (i.e. 1, 5, 10, 20, 30) as options for analyzing the performance of a center, since picking intervals is dependent on the list which is 720/m (which the length is an integer result). If we let the user pick an interval that results in a non-integer result, this will result in an invalid list since some data for the performance analysis will be lost. Can you provide a clarification on this situation?"

>*Answer:*** The user can introduce any interval value. The system should validate the interval value introduced by the user.

-

>**Question:** "I would like to ask that if to analyse the performance of a center, we can assume (as a pre requirement) that the center coordinator was already attributed to a specific vaccination center and proceed with the US as so (like the center coordinator does not have to choose at a certain point where he is working. This is already treated before this US happens). Could you clarify this?"

> **Answer:** A center coordinator can only coordinate one vaccination center. The center coordinator can only analyze the performance of the center that he coordinates.

-

> **Question** : "Do we need to select the day to analyse from 8:00 to 20:00?"
> **Answer** : The center coordinator should select the day for which he wants to analyse the performance of the vaccination center

-

> **Question**: "Is the time of departure of an SNS user the time he got vaccinated plus the recovery time or do we have another way of knowing it?"
> **Answer** : The time of departure of an SNS user is the time he got vaccinated plus the recovery time.

-


>**Question**: "In US 16, should the coordinator have the option to choose which algorithm to run (e.g. via a configuration file or while running the application) in order to determine the goal sublist, or is the Benchmark Algorithm strictly for drawing comparisons with the Bruteforce one?"
>**Answer**: The algorithm to run should be defined in a configuration file.

-

### 1.3. Acceptance Criteria
• **AC1** : The created list should contain the influx/outflow of people in a particular center (difference between entrances and exits)

• **AC2** : A contiguous sublist of the maximum sum  of influx/outflow of people should be presented.

• **AC3** : The sum of the maximum sum contiguous sublist should be presented.

• **AC4** : The time interval of the maximum sum contiguous sublist should be presented.

### 1.4. Found out Dependencies

* There is a dependency to "US01 Schedule Vaccine"/ "US02 Schedule Vaccine for an SNS user" since there must exist appointment for a user to be able to come in and out of a center.
* There is a dependency to "US03 Register SNS users"/"US14 Load a set of users from a CSV file" since there should be existing users, since they are te ones to come in and out of the center.
* There is a dependency to "US4 Register the arrival of a SNS user" since should be able to arrive at a center, so an influx of people can exist.
* There is a dependency to "UC8 Record administration of vaccine and send SMS after recovery period" since the user has to first receive a vaccine administration before he can leave a center.
* There is a dependency to "UC9 Register vaccination center" since we can only know the performance of a center if it exists.
* There is a dependency to "UC10 Register employees" since employees must work at a center to provide a flow of people to a center.
* There is a dependency to "UC12 Register new type of vaccine" since a vaccine type must exist so the user receives a vaccine administration and is at a center.
* There is a dependency to "UC13 Register new vaccine and its administration process" since a vaccine and administration process must exist so the user receives a vaccine administration and is at a center.


### 1.5 Input and Output Data
**Input Data:**
* Typed data:
  * time interval.

* Selected data:
  * a day.
  

**Output:**
* list of influx/outflow of people per unit
* maximum sum contiguous sublist
* time interval (start and finish time) of maximum sum contiguous sublist
* maximum sum
* (In)Success of the operation


### 1.6. System Sequence Diagram (SSD)

*Insert here a SSD depicting the envisioned Actor-System interactions and throughout which data is inputted and outputted to fulfill the requirement. All interactions must be numbered.*

![US16_SSD](US16_SSD.svg)


### 1.7 Other Relevant Remarks

* User story happens whenever a center coordinator would like to know a center's performance of a day.
* Arrival and Exit time must be registered during the vaccination process to be able to generate statistics
*Use this section to capture other relevant information that is related with this US such as (i) special requirements ; (ii) data and/or technology variations; (iii) how often this US is held.*


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt
*In this section, it is suggested to present an excerpt of the domain model that is seen as relevant to fulfill this requirement.*

![US16_MD](US16_MD.svg)

### 2.2. Other Remarks

*Use this section to capture some aditional notes/remarks that must be taken into consideration into the design activity. In some case, it might be usefull to add other analysis artifacts (e.g. activity or state diagrams).*



## 3. Design - User Story Realization

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for...                                    | Answer                                 | Justification (with patterns)                                                                                       |
| :------------- | :---------------------                                                         | :------------                          | :----------------------------                                                                                       |
| Step 1         | ....interacting with the actor?                                                | EvaluateCenterPerformanceUI            | Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model.       |
| Step 2         |                                                                                |                                        |                                                                                                                     |
| Step 3         |                                                                                |                                        |                                                                                                                     |
| Step 4         |                                                                                |                                        |                                                                                                                     |
| Step 5         | ... coordinating the US?                                                       | EvaluateCenterPerformanceController    | Controller                                                                                                          |
|                | ... knowing the vaccination center store?                                      | Company                                | IE: Company knows all vaccination centers it owns                                                                   |
|                | ... knowing all centers to search the corresponding center coordinator?        | VaccinationCenterStore                 | IE: VaccinationCenterStore knows all vaccination centers                                                            |
|                | ... knowing the center coordinator of a center?                                | VaccinationCenter                      | IE: VaccinationCenter knows who is it's own center coordinator                                                      |
|                | ... knowing all vaccine administrations of a certain day?                      | VaccinationCenter                      | IE: VaccinationCenter knows all vaccine administrations that occur in it                                            |
|                | ...evaluating a center's performance?                                          | VaccinationCenter                      | IE: VaccinationCenter has all information to know its performance                                                   |
|                | ...verifying time interval for performance?                                    | VaccinationCenter                      | IE: VaccinationCenter in the domain model knows its own open and close hours to verify if the time interval is even |
|                | ...knowing all vaccine administrations of a center and their entry/exit times? | VaccinationCenter                      | IE: In the domain model, VaccinationCenter knows its own vaccine administrations                                    |
|                | ...having the data to perform the calculations?                                | VaccineAdministration                  | IE: In the domain model, Center Performance is of vaccine administrations                                           |
|                | ...calculating performance regarding inflow/outflow of people of a center?     | CenterPerformance                      | IE: In the domain model, CenterPerformance contains center performance data                                         |
|                | ...retrieving data regarding the method of calculation?                        | GetPropertyValues                      | Pure Fabrication: created to provide HC LC                                                                          |
|                | ...deciding which algorithm to use?                                            | CenterPerformance                      | IE: CenterPerformance knows how it should perform its own calculation                                               |
|                | ...calculating the maximum sum contiguous sublist?                             | BenchmarkCPAdapter/BruteforceCPAdapter | Protected variations was used to provide HC LC, implementing CenterPerformanceInterface                             |
|                | ...knowing start and finish time of worst performance?                         | CenterPerformance                      | IE: In the domain model, CenterPerformance contains the data and times regarding worst center performance           |
|                | ...instantiating a center performance object?                                  | VaccinationCenter                      | Creator (rule 4): VaccinationCenter has all the data used to initialize the Center Performance object               |
|                | ...saving information regarding performance?                                   | CenterPerformance                      | IE: In the domain model, CenterPerformance describes the center's performance                                       |
| Step 6         | ...showing output data to the user?                                            | EvaluateCenterPerformanceUI            | Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model.       |

### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are:

* VaccinationCenter
* VaccineAdministration
* CenterPerformance

Other software classes (i.e. Pure Fabrication) identified:
* EvaluateCenterPerformanceUI
* EvaluateCenterPerformanceController
* VaccinationCenterStore
* BenchmarkCPAdapter
* BruteforceCPAdapter
* GetPropertyValues


## 3.2. Sequence Diagram (SD)

*In this section, it is suggested to present an UML dynamic view stating the sequence of domain related software objects' interactions that allows to fulfill the requirement.*

![US16_SD](US16_SD.svg)

## 3.3. Class Diagram (CD)

*In this section, it is suggested to present an UML static view representing the main domain related software classes that are involved in fulfilling the requirement as well as and their relations, attributes and methods.*

![US16_CD](US16_CD.svg)

# 4. Tests
*In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of requirements fulfilling.*

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

*Each type of test is represented roughly here*

##Difference List

**Test 1:** Check that the input list is correctly generated.

	@Test
    public void TestDifferenceListEvenDivision() throws IOException {

        //Setting dummy vaccination center
        VaccinationCenter vc = new VaccinationCenter("healthcare center",
                "Centro de Saúde do Barão do Corvo",
                "R. Barão do Corvo ",
                676, "4400-037", " Vila Nova de Gaia",
                223747010,
                "usf.arcoprado@arsnorte.min-saude.pt",
                93848374,
                "baraodocorvo.com", "08:00", "10:00", 45, 30, 2);
        //Setting dummy vaccine administrations
        VaccineAdministration va1 = new VaccineAdministration(
                161593121,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(8, 1),
                new TimeUtils(8),
                new TimeUtils(8, 17),
                new TimeUtils(9, 25));
        VaccineAdministration va2 = new VaccineAdministration(
                161593120,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(8, 24),
                new TimeUtils(8),
                new TimeUtils(9, 11),
                new TimeUtils(9, 43));

        VaccineAdministration va3 = new VaccineAdministration(
                161593121,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(8, 45),
                new TimeUtils(9, 7),
                new TimeUtils(9, 17),
                new TimeUtils(9, 25));
        VaccineAdministration va4 = new VaccineAdministration(
                161593121,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(9, 20),
                new TimeUtils(9, 21),
                new TimeUtils(9, 22),
                new TimeUtils(9, 40));
        VaccineAdministration va5 = new VaccineAdministration(
                161593121,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(9, 20),
                new TimeUtils(9, 21),
                new TimeUtils(9, 22),
                new TimeUtils(9, 40));

        VaccineAdministration va6 = new VaccineAdministration(
                161593121,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(9, 20),
                new TimeUtils(9, 21),
                new TimeUtils(9, 22),
                new TimeUtils(9, 40));

        //creating dummy vaccine administration list
        List<VaccineAdministration> vaList = new ArrayList<>();
        vaList.add(va1);
        vaList.add(va2);
        vaList.add(va3);
        vaList.add(va4);
        vaList.add(va5);
        vaList.add(va5);

        //creating expected difference list result
        List<Integer> expectedList = new ArrayList<>();

        expectedList.add(1); //8:00 - 8:19:59
        expectedList.add(1); //8:20 - 8:39:59
        expectedList.add(1); //8:40 - 8:59:59
        expectedList.add(0); //9:00 - 9:19:59
        expectedList.add(1); //9:20 - 9:39:59
        expectedList.add(-4); //9:40 - 9:59:59

        //calculating actual difference list
        CenterPerformance cp = new CenterPerformance(vc, vaList, 20);

        cp.evaluatePerformance();
        //asserting values
        List<Integer> calculatedList = cp.getDifferenceList();
        for (int i = 0; i < expectedList.size(); i++) {
            Assertions.assertEquals(expectedList.get(i), calculatedList.get(i));

        }
    }

**Test 2:** Check that performance values are generated as expected

	@Test
    public void TestMaxSumSublist() throws IOException {

        //Setting dummy vaccination center
        VaccinationCenter vc = new VaccinationCenter("healthcare center",
                "Centro de Saúde do Barão do Corvo",
                "R. Barão do Corvo ",
                676, "4400-037", " Vila Nova de Gaia",
                223747010,
                "usf.arcoprado@arsnorte.min-saude.pt",
                93848374,
                "baraodocorvo.com", "08:00", "10:00", 45, 30, 2);
        //Setting dummy vaccine administrations
        VaccineAdministration va1 = new VaccineAdministration(
                161593121,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(8, 1),
                new TimeUtils(8),
                new TimeUtils(8, 17),
                new TimeUtils(9, 25));
        VaccineAdministration va2 = new VaccineAdministration(
                161593120,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(8, 24),
                new TimeUtils(8),
                new TimeUtils(9, 11),
                new TimeUtils(9, 43));

        VaccineAdministration va3 = new VaccineAdministration(
                161593121,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(8, 45),
                new TimeUtils(9, 7),
                new TimeUtils(9, 17),
                new TimeUtils(9, 25));

        //creating dummy vaccine administration list
        List<VaccineAdministration> vaList = new ArrayList<>();
        vaList.add(va1);
        vaList.add(va2);
        vaList.add(va3);

        //creating expected difference list result
        List<Integer> expectedList = new ArrayList<>();

        expectedList.add(1); //8:00 - 8:19:59
        expectedList.add(1); //8:20 - 8:39:59
        expectedList.add(1); //8:40 - 8:59:59
        expectedList.add(0); //9:00 - 9:19:59
        expectedList.add(-2); //9:20 - 9:39:59
        expectedList.add(-1); //9:40 - 9:59:59

        //calculating actual difference list
        CenterPerformance cp = new CenterPerformance(vc, vaList, 20);


        cp.evaluatePerformance();

        //setting expected values
        List<Integer> expectedSubList= new ArrayList<>();
        expectedSubList.add(1);
        expectedSubList.add(1);
        expectedSubList.add(1);

        int expectedMaxSum=3;
        TimeUtils expectedStartTime=new TimeUtils(8,0,0);
        TimeUtils expectedEndTime=new TimeUtils(8,40,0);

        //asserting values
        Assertions.assertEquals(expectedMaxSum,cp.getMaxSum());
        Assertions.assertEquals(expectedStartTime,cp.getStartTime());
        Assertions.assertEquals(expectedEndTime,cp.getEndTime());
        for (int i = 0; i < expectedSubList.size(); i++) {
            Assertions.assertEquals(expectedSubList.get(i), cp.getMaxSumSubList().get(i));

        }
    }

# 5. Construction (Implementation)

*In this section, it is suggested to provide, if necessary, some evidence that the construction/implementation is in accordance with the previously carried out design. Furthermore, it is recommended to mention/describe the existence of other relevant (e.g. configuration) files and highlight relevant commits.*

## Class CenterPerformance

          /**
           * creates the ist that represents the influx/outflow of people in a certain vaccination center
           *
           * @param vaList vaccine administration's list of a vaccination center
           * @param m      time interval in which to analyze the center's performance
           * @return list that represents the influx/outflow of people in a certain vaccination center
           */
          public List<Integer> createDifferenceList(VaccinationCenter vc, List<VaccineAdministration> vaList, int m) {
              boolean wholeDifference=false;
              differenceList = new ArrayList<>();
              TimeUtils startTime = new TimeUtils(TimeUtils.setTime(vc.getOpenHour()));
              TimeUtils endTime = (new TimeUtils(startTime)).addMinutes(m);
              if(endTime.isBiggerOrEquals(TimeUtils.setTime(vc.getCloseHour()))){
                  wholeDifference=true;
              }
      
              int contIn = 0;
              int contOut = 0;
      
              do {
                  for (VaccineAdministration va : vaList) {
                      if (va.getArrivalTime().isBiggerOrEquals(startTime) && endTime.isBigger(va.getArrivalTime())) {
                          contIn++;
      
                      }
                      if (va.getLeavingTime().isBiggerOrEquals(startTime) && endTime.isBigger(va.getLeavingTime())) {
                          contOut++;
                      }
                  }
      
                  differenceList.add(contIn - contOut);
      
                  contIn = 0;
                  contOut = 0;
      
                  startTime.addMinutes(m);
                  endTime.addMinutes(m);
      
              } while (TimeUtils.setTime(vc.getCloseHour()).isBiggerOrEquals(endTime) && !wholeDifference);
      
              return differenceList;
          }

          
          /**
           * evaluates center performance
           *
           */
          public void evaluatePerformance() throws IOException {
              GetPropertyValues getProps = new GetPropertyValues();
              this.differenceList = createDifferenceList(vc, vaList, m);
              String algorithm = getProps.getPropValues("MaxSumSublistAlgorithm");
      
              if (algorithm.equals("Bruteforce")) {
                  BruteforceCPAdapter bfAdapter = new BruteforceCPAdapter(this);
                  bfAdapter.evaluateCenterPerformance();
              } else if (algorithm.equals("Benchmark")) {
                  BenchmarkCPAdapter bmAdapter = new BenchmarkCPAdapter(this);
                  bmAdapter.evaluateCenterPerformance();
              }
      
              System.out.println("Execution time in seconds: "+(double) execTime /1000000+" ms");
      
      
          }

##Configuration file content

      Company.Designation = DGS/SNS Portugal
      MaxSumSublistAlgorithm = Benchmark
      Sort = Quick sort


*It is also recommended to organize this content by subsections.*

# 6. Integration and Demo

*In this section, it is suggested to describe the efforts made to integrate this functionality with the other features of the system.*

* A login menu in javafx was added to facilitate interaction
* Data regarding vaccine administration of a center is used and there are verifications if the data exists and if it is analysable
* A graph was generated to improve user experience

# 7. Observations

*In this section, it is suggested to present a critical perspective on the developed work, pointing, for example, to other alternatives and or future related work.*





