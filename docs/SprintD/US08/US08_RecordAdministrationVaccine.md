# US 08 Record the administration of a vaccine.
## 1. Requirements Engineering

### 1.1. User Story Description

As a nurse, I want to record the administration of a vaccine to a SNS User.

### 1.2. Customer Specifications and Clarifications 

**From the specifications document:**

> A nurse can record the administration of a vaccine to a SNS User.

**From the client clarifications:**

> **Question:** Supposing that the SNS user has already received a dose of a given vaccine type (for example, COVID-19), the user can only receive the same vaccine or a different one with the same vaccine type?
>  
> **Answer:** The SNS user can only receive the same vaccine.
> 
> **Related information:**
>A SNS user is fully vaccinated when he receives all doses of a given vaccine.
>A SNS user that has received a single-dose vaccine is considered fully vaccinated and will not take more doses.
>A SNS user that is fully vaccinated will not be able to schedule a new vaccine of the type for which he is already fully vaccinated.

***

> **Question:** To access the user info - scheduled vaccine type and vaccination history -, should the nurse enter user's SNS number?
>  
> **Answer:** The nurse should select a SNS user from a list of users that are in the center to take the vaccine.

***

> **Question:**  
> **1**-The system displays the list of possible vaccines to be administered (considering the age group of the user); then the nurse selects the dose she is going to administer and gets information about the dosage. But wouldn't it be more correct, since the system knows the vaccination history, in other words, if the user has already take x dose(s) of that vaccine, to simply show the dose and the respective dosage and not ask for the nurse to arbitrarily select it?
>
> **2** - After giving the vaccine to the user, how should the nurse register the vaccine type? by the code?
>
> **Answer:** 
> 
> **1-** If it is the first dose, the application should show the list of possible vaccines to be administered. If is is not a single dose vaccine, when the SNS user arrives to take the vaccine, the system should simply show the dose and the respective dosage.
>
>**2-** A vaccine is associated with a given vaccine type. Therefore, there is no need to register the vaccine type.
>Moreover, the nurse should also register the vaccine lot number (the lot number has five alphanumeric characters an hyphen and two numerical characters (example: 21C16-05)).

***

> **Question:**
> 
>  **1.** Nurse calls one user that is waiting in the waiting room to be vaccinated
> 
>  **2.** Nurse checks the user's health data as well as which vaccine to administer
> 
>  **3.** Nurse administers the vaccine and registers its information in the system.
>
>  The doubt is: do you want US08 to cover steps 2 and 3, or just step 3?"
>
> **Answer:** 
> 
> 1.The nurse selects a SNS user from a list.
> 
> 2.Checks user's Name, Age and Adverse Reactions registered in the system.
> 
> 3.Registers information about the administered vaccine.

***

> **Question:** Regarding the recovery period, how should we define it? Is it the same for all vaccines or should the nurse specify in each case what the recovery time is?
>
> **Answer:** The recovery period/time is the same for all vaccines. The recovery period/time should be defined in a configuration file.

***

> **Question:** 
> 
> **1:** Is the nurse responsible for registering in the system the recovery period?
>
> **2:** If there are no adverse reactions detected/registered, after the given recovery period, the system notifies the user that his/her recovery period has ended, right? 
> 
>**3:** If there are adverse reactions detected/registered, the system should not do anything additional?
>
> **Answer:** 
> 
> **1-** No. The recovery period starts automatically after registering the administration of a given vaccine.
>
> **2 and 3-** US7 and US 8 are independent user stories.

***

> **Question:** In US 08 says: "At the end of the recovery period, the user should receive a SMS message informing the SNS user that he can leave the vaccination center." How should the SNS user receive and have access to the SMS message?
>
> **Answer:**  I already answered this question. Please pay attention to the client answers.
>A file named SMS.txt should be used to receive/record the SMS messages. We will not use a real word service to send SMSs.

***

> **Question:**
>
> **Answer:**


### 1.3. Acceptance Criteria

* **AC1:** The SNS number must be in a valid format(9 digits)


* **AC2:** The nurse should select a vaccine and the administered dose number.


* **AC3:** SNS Users’ list in the Waiting Room should be presented by order of arrival.


* **AC4:** Vaccination center must exists.


* **AC5:** LotNumber (has five alphanumeric characters one hyphen and two numerical characters) must be in a valid format.


### 1.4. Found out Dependencies

* There is a dependency to "US01 Schedule a vaccine" since at least one SNS user should have scheduled a vaccine to be necessary to do the record of the administration.
* There is a dependency to "US03 Register SNS User" because there must be at least one SNS user.
* There is a dependency to "US04 Register the Arrival of a SNS User" because the user must already be in the center for the administration of the vaccine to be possible.
* There is a dependency to "US09 Register New Vaccination Center" since at least one center must exist.
* There is a dependency to "US10 Register Employee" as there must be at least one employee (a nurse) for the record of the administration to be possible.
* There is a dependency to "US12 Register new vaccine type" since at least a vaccine type should exist before creating a vaccine brand of that type.
* There is a dependency to "US13 Register New Vaccine" because there must exist at least one vaccine. 

### 1.5 Input and Output Data

**Input Data:**


* Typed data:
    * Lot number

* Selected data:
    * List of scheduled vaccines
    * SNS Users’ list in the Waiting Room
    * List of vaccines
  
**Output Data:**
* SMS stating that the SNS User can leave the center.
* (In)Success of the operation


### 1.6. System Sequence Diagram (SSD)

**Alternative 1**

![US08_SSD](US08_SSD.svg)

### 1.7 Other Relevant Remarks

N/A


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

![US08_MD](US08_MD.svg)

### 2.2. Other Remarks

N/A


## 3. Design - User Story Realization 

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1  		 |....interacting with the actor?                                            |RecordVaccineAdministrationUI                       |Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model.|
| 			  	 |...coordinating the US?                                                    |RecordVaccineAdministrationController               |Controller|
| 			  	 |...knowing the user's session's vaccination center?                        |EmployeeSession                                     |IE: EmployeeSession knows information about the user's session|
| 			  	 |...runs the nurse menu?                                                    |NurseUI                                             |Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model.|
| Step 2 		 |...asks the nurse the data?                                                |RecordVaccineAdministrationUI                       |IE: asks all the data.                           |
| Step 3		 |...colect the data entered by the Nurse?      				             |VaccineAdministration                               |IE: colect all the data entered by the receptionist.|
| Step 4  		 |...knows all the vaccines?                                                 |Vaccine                                             |IE: knows the vaccines                         |
| Step 5  		 |...saves the options selected by the nurse                                 |VaccineAdministration                               |IE: Knows all the information about Vaccine Administrations|
| Step 6  		 |                                                                           |                                                    |IE: can add a User to the Recovery Room|
| Step 7  		 |...add the User to the Recovery Room                                       |RecoveryRoom                                        |IE: can add a User to the Recovery Room|
|                |...knows the Recovery Room?                                                | VaccinationCenter                                  |IE: knows everything in the Center. | 
| Step 8  		 |...send a message to SNS User?					                         |SMS                                                 |IE: can send messages to user at any time.|              


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * WaitingRoom
 * RecoveryRoom
 * Recovery
 * VaccinationCenter
 * VaccineAdministration
 * SMS


 Other software classes (i.e. Pure Fabrication) identified:

 * RecordVaccineAdministrationUI
 * RecordVaccineAdministrationController
 * EmployeeSession
 * NurseUI
 

## 3.2. Sequence Diagram (SD)

*In this section, it is suggested to present an UML dynamic view stating the sequence of domain related software objects' interactions that allows to fulfill the requirement.* 

![US08_SD](US08_SD.svg)

## 3.3. Class Diagram (CD)

*In this section, it is suggested to present an UML static view representing the main domain related software classes that are involved in fulfilling the requirement as well as and their relations, attributes and methods.*

![US08_CD](US08_CD.svg)

# 4. Tests 

*In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of requirements fulfilling.* 

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

 **Test 1:** Check if the its possible to Record Vaccine Administration
      
      String lotNumber = "21C15-16";

        try {
            VaccinationCenter vc = new VaccinationCenter("healthcare center",
                    "Centro de Saúde do Barão do Corvo",
                    "R. Barão do Corvo ",
                    676, "4400-037", " Vila Nova de Gaia",
                    223747010,
                    "usf.arcoprado@arsnorte.min-saude.pt",
                    93848374,
                    "baraodocorvo.com",
                    "09:00", "20:00", 45, 30, 2);

            App app = App.getInstance();
            Company company = app.getCompany();
            VaccinationCenterStore createVaccinationCenterController = company.createVaccinationCenter();
            createVaccinationCenterController.addVaccinationCenter(vc);


            //            CreateVaccinationCenterController.createVaccinationCenter("aaaa","aa","rua",10,"4000-000","porto",123123123,"centro@gmail.com",123123123,"https://aaa.com","10:00","20:00",120,12,10);
            App.getInstance().getCompany().getSnsUserStore().addNewSNSUser(223456789, "João", DateUtils.setDate("01-01-2003", "-"), "joao@gmail.com", 123123123, Gender.MALE, 11231234, "porto");
            SNSUser snsuser = App.getInstance().getCompany().getSnsUserStore().get(223456789);
            App.getInstance().getCompany().getVaccineTypeStore().addVaccineType("1234", "aaaa");
            VaccineType type = App.getInstance().getCompany().getVaccineTypeStore().getVaccineTypeList().get(0);


            VaccineStore vaccineStore = CreateVaccineController.getInstance().getVaccineStore();
            Vaccine vaccine= new Vaccine(1234,"Vaccine Name","testeBrand","rna",type);
            VaccineAdministrationProcess vaccineAdministrationProcess = new VaccineAdministrationProcess(10,21,2);
            vaccine.addVaccineAdministrationProcess(vaccineAdministrationProcess);
            vaccineStore.saveVaccine(vaccine);
            DoseAdministration doseAdministrationProcess = new DoseAdministration(1,0.2f,180,30);
            vaccineAdministrationProcess.addDoseAdministration(doseAdministrationProcess);
            VaccinationCenter center = App.getInstance().getCompany().getVaccinationCenterStore().getAllVaccinationCenters().get(0);
            AppointmentController.getInstance().scheduleAppointment(223456789, type, center, DateUtils.setDate("01-01-2002", "-"), TimeUtils.setTime("11:00"));
            Appointment ap = App.getInstance().getCompany().getAppointmentStore().get(snsuser, DateUtils.setDate("01-01-2002", "-"), TimeUtils.setTime("11:00"));
            center.addToWaitingRoom(snsuser, TimeUtils.setTime("11:00"), ap);

             VaccineShot vaccineShot = new VaccineShot(vaccine,DateUtils.setDate("01-01-2003","-"));
              snsuser.getTakenVaccines().add(0,vaccineShot);

            RecordVaccineAdministrationController  controller = new RecordVaccineAdministrationController();
            


        } catch (Exception exception) {
        }
        }
    }

# 5. Construction (Implementation)

## Class RecordVaccineAdministrationUI
  
    public class RecordVaccineAdministrationUI implements Runnable {

    @FXML
    private Button btnClose;

    @FXML
    private Button btnOk;

    @FXML
    private TextField txtName;

    @FXML
    private TextField txtLotNumber;

    @FXML
    private ListView<Integer> listWaitingRoom;

    @FXML
    private TextField txtAdvRec;

    @FXML
    private ListView<String> listVaccines;

    @FXML
    private TextField txtAge;

    @FXML
    private Label lblLotNumber;


    private RecordVaccineAdministrationController controller;

    private HashMap<Integer, SNSUser> snsUserList;

    private int age;

    private SNSUser snsUser=null;

    String lotNumber;

    @FXML
    public void SelectSNSUser(MouseEvent mouseEvent) throws UserUnknownException {

        int usernumber;
        ObservableList list = listWaitingRoom.getSelectionModel().getSelectedItems();

        Integer[] array = new Integer[this.snsUserList.keySet().size()];
        int i = 0;
        for (Integer number : this.snsUserList.keySet()) {
            array[i++] = number;
        }
        usernumber = (Integer) list.get(0);


        txtName.setText(snsUserList.get(usernumber).getName());

        age = DateUtils.currentDate().getYear() - snsUserList.get(usernumber).getBirthdate().getYear();
        txtAge.setText(toString());
        txtAdvRec.setText("No adverse Reactions");

        snsUser = App.getInstance().getCompany().getSnsUserStore().get(usernumber);

        List<Vaccine> vaccineList = controller.getVaccinationList(App.getInstance().getCompany().getSnsUserStore().get(usernumber));


            for (Vaccine vaccine : vaccineList) {
                listVaccines.getItems().add(vaccine.getName());
            }
            listVaccines.setDisable(false);
            listVaccines.getSelectionModel();

    }

    @Override
    public String toString() {
        return "" + age +
                "";
    }

    @FXML
    public void SelectVaccine(MouseEvent mouseEvent) {
        txtLotNumber.setDisable(false);

        if (listVaccines.getItems().size() > 0) {

            ObservableList list = listVaccines.getSelectionModel().getSelectedItems();
            String vaccineName = (String) list.get(0);

            btnOk.setDisable(false);
        }
    }
    @FXML
    public void OkMenu(javafx.event.ActionEvent actionEvent) throws UserAlreadyExistsException {
      //  boolean sucess=false ;
         lotNumber = txtLotNumber.getText();

        if (!controller.verifyIflotNumberIsCorrect(lotNumber)){
            lblLotNumber.setText("Lot Number: Invalid Lot Number(example: 21C16-05)!");
            lblLotNumber.setTextFill(Color.web("#FF0000"));
            return;
        }

        if (listVaccines.getItems().size() == 0) {
            return;
        }

        List<Vaccine> vaccineList = controller.getVaccinationList(snsUser);
        ObservableList list = listVaccines.getSelectionModel().getSelectedItems();
        String vaccineName = (String) list.get(0);

        Vaccine selected = null;

        for (Vaccine vaccine : vaccineList) {
            if (vaccine.getName().equals(vaccineName)) {
                selected = vaccine;
                break;
            }
        }

        Stage stage = (Stage) btnOk.getScene().getWindow();
        controller.VaccineShot(snsUser, selected,lotNumber);
        controller.AddRecoveryRoom(snsUser);
        new SMS().SendMessage();
        controller.leaveCenter(snsUser);
        stage.close();
    }
    @FXML
    public void CloseMenu(javafx.event.ActionEvent actionEvent) {
        Stage stage = (Stage) btnClose.getScene().getWindow();
        stage.close();
        NurseUI menu = new NurseUI();
        menu.run();


    }

    public RecordVaccineAdministrationGUI() {

        this.controller = new RecordVaccineAdministrationController();
        this.snsUserList = controller.getSNSUsersWaiting();

    }


    @Override
    public void run() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(MainApp.class.getResource("/fxml/record-vaccine-administration.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 750, 750);
            this.initialize();
        } catch (IOException e) {
            System.out.println("Something went wrong!");
        }
    }



    @FXML
    void initialize() {
        assert btnClose != null : "fx:id=\"btnClose\" was not injected: check your FXML file 'record-vaccine-administration.fxml'.";
        assert btnOk != null : "fx:id=\"btnOk\" was not injected: check your FXML file 'record-vaccine-administration.fxml'.";
        assert txtName != null : "fx:id=\"txtName\" was not injected: check your FXML file 'record-vaccine-administration.fxml'.";
        assert txtLotNumber != null : "fx:id=\"txtLotNumber\" was not injected: check your FXML file 'record-vaccine-administration.fxml'.";
        assert listWaitingRoom != null : "fx:id=\"listWaitingRoom\" was not injected: check your FXML file 'record-vaccine-administration.fxml'.";
        assert txtAdvRec != null : "fx:id=\"txtAdvRec\" was not injected: check your FXML file 'record-vaccine-administration.fxml'.";
        assert listVaccines != null : "fx:id=\"listVaccines\" was not injected: check your FXML file 'record-vaccine-administration.fxml'.";
        assert txtAge != null : "fx:id=\"txtAge\" was not injected: check your FXML file 'record-vaccine-administration.fxml'.";

        listWaitingRoom.getItems().addAll(snsUserList.keySet());

    }

}


## Class RecordVaccineAdministrationController 

    public class RecordVaccineAdministrationController {

    /**
     * The Vaccine Store object.
     */
    private VaccineStore vaccineStore;

    /**
     * Get the Vaccine Store
     * @return returns the Vaccine Store
     */
    public VaccineStore getVaccineStore() {
        return this.vaccineStore;
    }

    /**
     *  Method to get the Waiting Room
     * @return returns the center Waiting Room
     */
    public WaitingRoom getWaitingRoom(){
        VaccinationCenter center = EmployeeSession.getVaccinationCenter();
        return center.getWaitingRoom();
    }

    /**
     *  Method to get the Recovery Room
     * @return returns the center Recovery Room
     */
    public RecoveryRoom getRecoveryRoom(){
        VaccinationCenter center = EmployeeSession.getVaccinationCenter();
        return center.getRecoveryRoom();
    }

    /**
     *  Method to get the Users in the Waiting Room
     * @return returns one HashMap with snsNumbers and users
     */
    public HashMap<Integer,SNSUser> getSNSUsersWaiting() {
        WaitingRoom waitingRoom = this.getWaitingRoom();
        HashMap<Integer,SNSUser> snsUserList = new HashMap<>();

        for (Arrival arrival : waitingRoom.getArrivalList()) {
            try {
                int n = arrival.getSNSNumber();;
                snsUserList.put(n, App.getInstance().getCompany().getSnsUserStore().get(n));
            } catch (UserUnknownException exception) {

            }
        }

        return snsUserList;
    }

    /**
     * Method to get the Vaccination List of an User
     * @param user One SNS User
     * @return returns the Vaccination List of an User
     */
    public List<Vaccine> getVaccinationList(SNSUser user) {
        List<Vaccine> result = new ArrayList<>();

        // Check if it is the first vac.
        List<VaccineShot> takenVaccines = user.getTakenVaccines();


        int listSize = takenVaccines.size();

        if (listSize > 0) {
            Vaccine vaccine = takenVaccines.get(0).getVaccine();

            List<VaccineAdministrationProcess> processes = vaccine.getVaccineAdministrationProcess();

            boolean addToResult = false;

            for (VaccineAdministrationProcess process : processes) {
                if (process.validateAdministration(user.getAge(), listSize, DateUtils.currentDate().difference(takenVaccines.get(listSize-1).getDate()))) {
                    addToResult = true;
                    break;
                }
            }

            if (addToResult) {
                result.add(vaccine);
            }
        }

        if (takenVaccines.size() == 0) {

            List<Vaccine> vaccineList = App.getInstance().getCompany().getVaccineStore().getVaccineList();

            for (Vaccine vaccine : vaccineList) {

                List<VaccineAdministrationProcess> processes = vaccine.getVaccineAdministrationProcess();

                boolean addToResult = false;


                for (VaccineAdministrationProcess process : processes) {
                    if (process.validateAdministration(user.getAge(), 1, 0)) {
                        addToResult = true;
                        break;
                    }
                }

                if (addToResult) {
                    result.add(vaccine);
                }
            }
        }

        return result;
    }


    /**
     * Method to add a SNS User to the Recovery Room
     * @param user The SNS User
     */
    public void AddRecoveryRoom(SNSUser user) throws UserAlreadyExistsException {

        RecoveryRoom recoveryRoom = this.getRecoveryRoom();

        recoveryRoom.add(user);

    }

    /**
     * Method to give a VaccineShot to a user and add to the VaccineAdministration
     * @param user The SNS User
     * @param vaccine The Vaccine administered
     * @param lotNumber The Vaccine Lot number
     */
    public void VaccineShot(SNSUser user, Vaccine vaccine,String lotNumber){

        WaitingRoom waitingRoom = this.getWaitingRoom();
        Arrival arrival = waitingRoom.remove(user);

        VaccineShot shot = new VaccineShot(vaccine, DateUtils.currentDate());

        user.getTakenVaccines().add(shot);

        VaccineAdministrationStore vaccineAdministrationStore = App.getInstance().getCompany().getVaccineAdministrationStore();

        int snsNumber = user.getSnsNumber();
        String vaccineName = vaccine.getName();
        int doseNumber = user.getTakenVaccines().size();
        DateUtils administrationDate = shot.getDate();
        TimeUtils arrivalTime = arrival.getArrivalTime();
        TimeUtils scheduledTime = arrival.getScheduledTime();
        TimeUtils administrationTime=TimeUtils.currentTime();
        TimeUtils leavingTime = TimeUtils.currentTime().addTime(new TimeUtils(0, vaccine.getVaccineAdministrationProcess().get(0).getDoseAdministration(doseNumber).getRecoveryTime()));

        VaccineAdministration vaccineAdministration = new VaccineAdministration(snsNumber, vaccineName, doseNumber, lotNumber, administrationDate, arrivalTime, scheduledTime, administrationTime, leavingTime);

        vaccineAdministrationStore.add(vaccineAdministration);

    }

    /**
     * Method to check if the Lot number is correct
     * @param lotNumber The vaccine Lot number
     * @return returns the true (if the Lot number is in a valid format) or false otherwise
     */
    public boolean verifyIflotNumberIsCorrect(String lotNumber) {

        // ^[a-z,A-Z,0-9]{5}-\d{2}$

        char[] array = lotNumber.toCharArray();

        if (array.length != 8) {
            return false;
        }

        for (int i = 0; i < 5; i++) {
            if (!isAlpha(array[i]) && !isDigit(array[i])) {
                return false;
            }
        }

        if (array[5] != '-') {
            return false;
        }

        for (int i = 6; i <  8; i++) {
            if (!isDigit(array[i])) {
                return false;
            }
        }

        return true;
    }
    /**
     * Method to see if a character is an alphanumeric character
     * @param c The character
     * @return Return true (if the character is a alphanumeric character) or false otherwise
     */
    private boolean isAlpha(char c) {
        return c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z';
    }

    /**
     * Method to see if a character is a digit
     * @param c The character
     * @return Return true (if the character is a digit) or false otherwise
     */
    private boolean isDigit(char c) {
        return c >= '0' && c <= '9';
    }

    /**
     * Method to remove a SNS User from the Center
     * @param snsUser The SNS User
     */
    public void leaveCenter(SNSUser snsUser) {
        RecoveryRoom recoveryRoom = this.getRecoveryRoom();
        Recovery recovery = recoveryRoom.remove(snsUser);

    }

}


## Class VaccineAdministration

    public class VaccineAdministration implements Serializable {

    /**
     * The SNS number of the user
     */
    private int snsNumber;

    /**
     * The Vaccine Name
     */
    private String vaccineName;

    /**
     * The Vaccine dose number
     */
    private int doseNumber;

    /**
     * The Vaccine lot number
     */
    private String lotNumber;

    /**
     *  Nurse Administration Date
     */
    private DateUtils administrationDate;

    /**
     *  SNS User Arrival Time
     */
    private TimeUtils arrivalTime;

    /**
     *  Scheduled Time
     */
    private TimeUtils scheduledTime;

    /**
     *  Admiinistration time
     */
    private TimeUtils administrationTime;

    /**
     * SNS User Leaving Time
     */
    private TimeUtils leavingTime;

    /**
     *
     * @param snsNumber The SNS User number
     * @param vaccineName The Vaccine Name
     * @param doseNumber The dose Number
     * @param lotNumber The Vaccine Lot Number
     * @param administrationDate Nurse Administration Date
     * @param arrivalTime SNS User arrival Time
     * @param scheduledTime The Scheduled Time
     * @param leavingTime SNS User leaving Time
     */
    public VaccineAdministration(int snsNumber, String vaccineName, int doseNumber, String lotNumber, DateUtils administrationDate, TimeUtils arrivalTime, TimeUtils scheduledTime,TimeUtils administrationTime  ,TimeUtils leavingTime) {

        this.snsNumber = snsNumber;
        this.vaccineName = vaccineName;
        this.doseNumber = doseNumber;
        this.lotNumber = lotNumber;
        this.administrationDate = administrationDate;
        this.arrivalTime = arrivalTime;
        this.scheduledTime = scheduledTime;
        this.administrationTime=administrationTime;
        this.leavingTime = leavingTime;
    }

    /**
     * Get SNS User Number.
     * @return Returns the SNS User Number.
     */
    public int getSnsNumber() {
        return snsNumber;
    }

    /**
     * Get Vaccine Name.
     * @return Returns the Vaccine Name.
     */
    public String getVaccineName() {
        return vaccineName;
    }

    /**
     * Get Dose Number.
     * @return Returns the Dose Number.
     */
    public int getDoseNumber() {
        return doseNumber;
    }

    /**
     * Get Lot Number.
     * @return Returns the Lot Number.
     */
    public String getLotNumber() {
        return lotNumber;
    }

    /**
     * Get the Nurse Administration Date.
     * @return Returns the Nurse Administration Date.
     */
    public DateUtils getAdministrationDate() {
        return administrationDate;
    }

    /**
     * Get the SNS User Arrival Time.
     * @return Returns SNS User Arrival Time.
     */
    public TimeUtils getArrivalTime() {
        return arrivalTime;
    }

    /**
     * Get the appointment Scheduled Time.
     * @return Returns Appointment Scheduled Time.
     */
    public TimeUtils getScheduledTime() {
        return scheduledTime;
    }

    /**
     * Get the SNS User Leaving Time.
     * @return Returns SNS User Leaving Time.
     */
    public TimeUtils getLeavingTime() {
        return leavingTime;
    }

    /**
     * Set the SNS User Number.
     * @param snsNumber The SNS User Number.
     */
    public void setSnsNumber(int snsNumber) {
        this.snsNumber = snsNumber;
    }

    /**
     * Set the Vaccine Name.
     * @param vaccineName The Vaccine Name.
     */
    public void setVaccineName(String vaccineName) {
        this.vaccineName = vaccineName;
    }

    /**
     * Set the number of Doses administered.
     * @param doseNumber The dose Number.
     */
    public void setDoseNumber(int doseNumber) {
        this.doseNumber = doseNumber;
    }

    /**
     * Set the Vaccine Lot Number.
     * @param lotNumber The Vaccine Lot number.
     */
    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }

    /**
     * Set the Administration Date.
     * @param administrationDate Nurse Administration Date.
     */
    public void setAdministrationDate(DateUtils administrationDate) {
        this.administrationDate = administrationDate;
    }

    /**
     * Set the user arrival Time.
     * @param arrivalTime The SNS User Arrival Time.
     */
    public void setArrivalTime(TimeUtils arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    /**
     * Set the appointment scheduled Time.
     * @param scheduledTime The Appointment Scheduled Time.
     */
    public void setScheduledTime(TimeUtils scheduledTime) {
        this.scheduledTime = scheduledTime;
    }

    /**
     * Set the user Leaving Time.
     * @param leavingTime The SNS User leaving Time.
     */
    public void setLeavingTime(TimeUtils leavingTime) {
        this.leavingTime = leavingTime;
    }

    public TimeUtils getAdministrationTime() {
        return administrationTime;
    }



    /**
     * method to verify if a lot number is in the correct format
     * (has five alphanumeric characters an hyphen and two numerical characters (examples: 21C16-05 and A1C16-22 ))
     * link to moodle: https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16922#p21821
     * @param lotNumber
     * @return
     */
    public static boolean confirmLotNumber(String lotNumber){
        boolean correct = false;
        //separates string in parts (criteria to separate is what is the content of string separator being | an or)
        String[] passInArray = lotNumber.split("-");
        //transforms the separated string into an array
        ArrayList<String> passArrayList = new ArrayList<String>(Arrays.asList(passInArray));

        try {
            try {
                int num = Integer.parseInt(passArrayList.get(1));
                correct = true;
                if (passArrayList.get(1).split("").length!=2){
                    correct = false;
                }
            } catch (NumberFormatException e){
                correct = false;
            }
            if (correct== true){
                if (passArrayList.get(0).split("").length!=5){
                    correct= false;
                }
            }
        } catch (IndexOutOfBoundsException e){
            correct = false;
        }

        return correct;
    }
    }

