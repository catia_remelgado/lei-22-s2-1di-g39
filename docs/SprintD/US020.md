# US 020 Confirm that the user is ready to take the vaccine 

## 1. Requirements Engineering
(Brief description)

The receptionist wants to confirm if the person is ready to take the vaccine.
On the scheduled day and time, the SNS user should go to the vaccination center to get the vaccine.
When the SNS user arrives at the vaccination center, a receptionist sends to the system the SNS number of the patient
and the date and time the patient arrived registering the arrival of the user to take the respective vaccine.
With this, the system must carry out a validation to see if the SNS patient has the vaccine for that day and time and if the SNS number is correct. If the information is correct, the receptionist acknowledges the system that the user is ready to take the vaccine and the system will inform the receptionist that the operation was successful.

“On the scheduled day and time, the SNS user should go to the vaccination center to get the vaccine.
When the SNS user arrives at the vaccination center, a receptionist registers the arrival of the user to
take the respective vaccine. The receptionist asks the SNS user for his/her SNS user number and
confirms that he/she has the vaccine scheduled for the that day and time. If the information is
correct, the receptionist acknowledges the system that the user is ready to take the vaccine. Then,
the receptionist should send the SNS user to a waiting room where (s)he should wait for his/her
time.”

### 1.1. User Story Description

A receptionist who wants to confirm if the person is ready to take the vaccine

### 1.2. Customer Specifications and Clarifications 

**From the specifications document:**

> A receptionist can check if the person is ready to take the vaccine.	

**From the client clarifications:**

> **Question:** Can only receptionist do this check?
>  
> **Answer:** *to be clarified*

-

> **Question:** What is the structure of an SNS number?
>  
> **Answer:** *to be clarified*

-

> **Question:** What kind of SNS user data are needed?
>  
> **Answer:** *to be clarified*

### 1.3. Acceptance Criteria

¢	**AC1:** The SNS number must be in a valid format

¢	**AC2:** Day and time must be in a valid format


### 1.4. Found out Dependencies

* There is a dependency to "US007 Register centers" because it takes at least one center to be necessary to confirm if the person is ready to take the vaccine.

* There is a dependency to "US001 Schedule a vaccine" since at least one SNS user should have scheduled a vaccine to be necessary to confirm if the person is ready to take the vaccine.

* There is a dependency to "US009 Register employees because there needs to be at least one receptionist for it to be possible to confirm if the person is ready to take the vaccine.

* There is a dependency to "US008 Register SNS Users" because it takes at least one SNS user so that the receptionist can check if the SNS user exist.

### 1.5 Input and Output Data

* Typed data:
	* SNS number
	* Day and time
	
* Selected data:
	* SNS users
	* Scheduled vaccines

**Output Data:**

* If the sns patient is ready to take the vaccine or not.


### 1.6. System Sequence Diagram (SSD)

*Insert here a SSD depicting the envisioned Actor-System interactions and throughout which data is inputted and outputted to fulfill the requirement. All interactions must be numbered.*

![USXXX-SSD](USXXX-SSD.svg)


### 1.7 Other Relevant Remarks

*Use this section to capture other relevant information that is related with this US such as (i) special requirements ; (ii) data and/or technology variations; (iii) how often this US is held.*


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt
*In this section, it is suggested to present an excerpt of the domain model that is seen as relevant to fulfill this requirement.*

![USXXX-MD](USXXX-MD.svg)

### 2.2. Other Remarks

*Use this section to capture some aditional notes/remarks that must be taken into consideration into the design activity. In some case, it might be usefull to add other analysis artifacts (e.g. activity or state diagrams).*



## 3. Design - User Story Realization

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1  		 |							 |             |                              |
| Step 2  		 |							 |             |                              |
| Step 3  		 |							 |             |                              |
| Step 4  		 |							 |             |                              |
| Step 5  		 |							 |             |                              |
| Step 6  		 |							 |             |                              |              
| Step 7  		 |							 |             |                              |
| Step 8  		 |							 |             |                              |
| Step 9  		 |							 |             |                              |
| Step 10  		 |							 |             |                              |  


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are:

* Class1
* Class2
* Class3

Other software classes (i.e. Pure Fabrication) identified:
* xxxxUI
* xxxxController

## 3.2. Sequence Diagram (SD)

*In this section, it is suggested to present an UML dynamic view stating the sequence of domain related software objects' interactions that allows to fulfill the requirement.*

![USXXX-SD](USXXX-SD.svg)

## 3.3. Class Diagram (CD)

*In this section, it is suggested to present an UML static view representing the main domain related software classes that are involved in fulfilling the requirement as well as and their relations, attributes and methods.*

![USXXX-CD](USXXX-CD.svg)

# 4. Tests
*In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of requirements fulfilling.*

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

**Test 1:** Check that it is not possible to create an instance of the Example class with null values.

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

*It is also recommended to organize this content by subsections.*

# 5. Construction (Implementation)

*In this section, it is suggested to provide, if necessary, some evidence that the construction/implementation is in accordance with the previously carried out design. Furthermore, it is recommeded to mention/describe the existence of other relevant (e.g. configuration) files and highlight relevant commits.*

*It is also recommended to organize this content by subsections.*

# 6. Integration and Demo

*In this section, it is suggested to describe the efforts made to integrate this functionality with the other features of the system.*


# 7. Observations

*In this section, it is suggested to present a critical perspective on the developed work, pointing, for example, to other alternatives and or future related work.*
