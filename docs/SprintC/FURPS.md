# Supplementary Specification (FURPS+)

## Functionality

It specifies features that are not related to use cases, namely: Auditing, Reporting, Locale/Languages, and Security.

  > **Location/Languages:**

 - “The application must support, at least, the Portuguese and the English languages.”
 
 > **Security:**

 - "All those who wish to use the application must be authenticated with a password holding seven alphanumeric characters, including three capital letters and two digits.”

 - “Only the nurses are allowed to access all user’s health data.”
 
 > **Audit**

- "The computational complexity analysis (of the brute-force algorithm and any sorting algorithms implemented within this application), must be accompanied by the observation of the execution time of the algorithms for inputs of variable size"

- “The application should use object serialization to ensure data persistence between two runs of the application.”

  
>**Reporting**

- "The Center Coordinator wants to monitor the vaccination process, to see **statistics and charts**, to evaluate the **performance of the vaccination process, generate reports and analyze data from other centers**, including data from legacy systems."

>**Mail**

- "If the user authorizes the sending of the SMS, the application should send an SMS message when the vaccination event is scheduled and registered in the system."

- "The system should be able to notify (e.g.: SMS or email) the user that his/her recovery period has ended"

## Usability

_Evaluates the user interface. It has several subcategories,
among them: error prevention; interface aesthetics and design; help and
documentation; consistency and standards._

>**Accessibility**
- “The user should introduce ... the type of vaccine to be administered (**by default, the system suggests the one related to the ongoing outbreak**).”

- “The application must support, at least, the Portuguese and the English languages.”

>**Help,Documentation**
- “The user manual must be delivered with the application.”



## Reliability
_(Refers to the integrity, compliance and interoperability of the software. The requirements to be considered are: frequency and severity of failure, possibility of recovery, possibility of prediction, accuracy, average time between failures.)_
<!---
- "(The goal of the performance analysis is to decrease the number of clients in the center, from the moment they register at the arrival, until the moment they receive the SMS informing they can leave the vaccination center.) To evaluate this, it proceeds as follows: **for any time interval on one day**, the difference between the number of new clients arrival and the number of clients leaving the center **every five-minute period is computed**."
- "(...)the application should implement a bruteforce algorithm (an algorithm which examines all the contiguous subsequences) to determine the contiguous subsequence with maximum sum. The implemented algorithm should be analyzed in terms of its worst-case time complexity, and it should be compared to a benchmark algorithm provided. The computational complexity analysis (of the brute-force algorithm and any sorting algorithms implemented within this application), must be accompanied by the observation of the execution time of the algorithms for inputs of variable size, in order to observe the asymptotic behavior. The worst-case time complexity analysis of the algorithms should be properly documented in the user manual of the application (in the annexes)."
--->
## Performance
_Evaluates the performance requirements of the software, namely: response time, start-up time, recovery time, memory consumption, CPU usage, load capacity and application availability._

- "The application should implement a bruteforce algorithm (an algorithm which examines all the contiguous subsequences) to determine the contiguous subsequence with maximum sum"
- "The implemented algorithm should be analyzed in terms of its worst-case time complexity, and it should be compared to a benchmark algorithm provided."




## Supportability

The supportability requirements gathers several characteristics, such as: testability, adaptability, maintainability, compatibility, configurability, installability, scalability and more.

- “As Coronavirus might not be the last pandemic in our lifetime, the application should be designed to easily support managing other future pandemic events requiring a massive vaccination of the population.”

- “The software application should also be conceived having in mind that it can be further commercialized to other companies and/or organizations and/or healthcare systems besides DGS.”

- "As the allocation of receptionists and nurses to vaccination centers might be complex, by now, the system might assume that receptionists and nurses can work on any vaccination center."

-"The Center Coordinator wants to monitor the vaccination process, to see statistics and charts, to evaluate the performance of the vaccination process, generate reports and analyze data from other centers, including **data from legacy systems.**"

### Design Constraints

_(Specifies or constraints the system design process. Examples may include: programming languages, software process, mandatory standards/patterns, use of development tools, class library, etc.)_
  
- "The application must be developed in Java language using the IntelliJ IDE or NetBeans."

- "The application graphical interface is to be developed in JavaFX 11."

- "All the images/figures produced during the software development process should be recorded in SVG format."

- "During the system development, the team must: (i) adopt best practices for identifying requirements, and for OO software analysis and design;"

### Implementation Constraints

_Specifies or constraints the code or construction of a system such
such as: mandatory standards/patterns, implementation languages,
database integrity, resource limits, operating system._

- "The application must be developed in Java language using the IntelliJ IDE or NetBeans."
- "All those who wish to use the application must be authenticated with a password holding seven alphanumeric characters,including three capital letters and two digits"
- "The application must support, at least, the Portuguese and the English languages"
- "During the system development, the team must adopt best practices for identifying requirements, and for OO software analysis and design.
- "During the system development, the team must adopt recognized coding standards (e.g., CamelCase);"
- "During the system development, the team must use Javadoc to generate useful documentation for Java code."
- "The development team must implement unit tests for all methods, except for methods that implement Input/Output operations."
- "The unit tests should be implemented using the JUnit 5 framework."
- " The JaCoCo plugin should be used to generate the coverage report."
- "So, the application should implement a bruteforce algorithm (an algorithm which examines all the contiguous subsequences) to determine the contiguous subsequence with maximum sum."





### Interface Constraints
_Specifies or constraints the features inherent to the interaction of the
system being developed with other external systems._

<!---"The application graphical interface is to be developed in JavaFX 11."-->




### Physical Constraints

_Specifies a limitation or physical requirement regarding the hardware used to house the system, as for example: material, shape, size or weight._
