# US 09 - As an administrator, I want to register a vaccination center to respond to a certain pandemic.

## 1. Requirements Engineering

*”The DGS has Administrators who administer the application. Any Administrator uses the application to register centers,
SNS users, center coordinators, receptionists, and nurses enrolled in the vaccination process.”*

### 1.1. User Story Description

*As an administrator, I want to register vaccination centers.*

### 1.2. Customer Specifications and Clarifications

**From the client clarifications:**

**Client did not respond to any questions asked by other students.**
<!--- > **Question:** 
>  
> **Answer:** *to be clarified* --->

### 1.3. Acceptance Criteria

- None addressed so far.

### 1.4. Found out Dependencies

- There are no dependencies so far.

<!--- Identify here any found out dependency to other US and/or requirements. --->

### 1.5 Input and Output Data

**Input Data:**

* Typed data:
    * Name of the center;
    * Location of the center;
    * Phone number;
    * e-mail address;
    * fax number;
    * website address;
    * opening and closing hours;
    * slot duration;
    * maximum number of vaccines that can be given per slot.
    * maximum capacity
    * type
* Selected data:
    * Register vaccination center.

**Output Data:**

    *Message informing if the registration was successful

### 1.6. System Sequence Diagram (SSD)

<!---*Insert here a SSD depicting the envisioned Actor-System interactions and throughout which data is inputted and outputted to fulfill the requirement. All interactions must be numbered.* --->

![US09_SSD](US09_SSD.svg)

### 1.7 Other Relevant Remarks

*Use this section to capture other relevant information that is related with this US such as (i) special requirements
; (ii) data and/or technology variations; (iii) how often this US is held.*

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

<!--- *In this section, it is suggested to present an excerpt of the domain model that is seen as relevant to fulfill this requirement.* --->

![US009_MD](US009_MD.svg)

### 2.2. Other Remarks

*Use this section to capture some aditional notes/remarks that must be taken into consideration into the design
activity. In some case, it might be usefull to add other analysis artifacts (e.g. activity or state diagrams).*

## 3. Design - User Story Realization

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID                                  | Question: Which class is responsible for... | Answer                            | Justification (with patterns)  |
|:------------------------------------------------|:--------------------------------------------|:----------------------------------|:---------------------------- |
| Step 1 Ask to register a new vaccination center | N/A                                         | CreateVaccinationCenterUI         |  Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model.           |
| Step 2 Request Data		  	                        | N/A                                         | CreateVaccinationCenterController | Controller                             |
| Step 3 Types request data                       | N/A							                                  |                                   |                              |
| Step 4 Shows data and requests information      | N/A                                         
| Step 5 Confirms data		  	                       | N/A                                                     |                                   |
| Step 6 Informs the operation was sucessuful     | ... saving the data?                        | CreateVaccinationCenter                            | I.E: Has all the parameters      
### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are:

* Class1
* Class2
* Class3

Other software classes (i.e. Pure Fabrication) identified:

* xxxxUI
* xxxxController

## 3.2. Sequence Diagram (SD)

<!---*In this section, it is suggested to present an UML dynamic view stating the sequence of domain related software objects' interactions that allows to fulfill the requirement.*--->

![US09-SD](US009_SD.svg)

## 3.3. Class Diagram (CD)

<!---*In this section, it is suggested to present an UML static view representing the main domain related software classes that are involved in fulfilling the requirement as well as and their relations, attributes and methods.*--->

![US09_CD](US009_CD.svg)

# 4. Tests

*In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of
requirements fulfilling.*

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

**Test 1:** Check that it is not possible to create an instance of the Example class with null values.

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

*It is also recommended to organize this content by subsections.*

# 5. Construction (Implementation)

*In this section, it is suggested to provide, if necessary, some evidence that the construction/implementation is in
accordance with the previously carried out design. Furthermore, it is recommeded to mention/describe the existence of
other relevant (e.g. configuration) files and highlight relevant commits.*

*It is also recommended to organize this content by subsections.*

# 6. Integration and Demo

*In this section, it is suggested to describe the efforts made to integrate this functionality with the other features
of the system.*

# 7. Observations

*In this section, it is suggested to present a critical perspective on the developed work, pointing, for example, to
other alternatives and or future related work.*
