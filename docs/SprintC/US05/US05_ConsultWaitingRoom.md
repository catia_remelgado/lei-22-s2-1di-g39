# US 05 - Consult Waiting Room

## 1. Requirements Engineering

### 1.1. User Story Description
As a nurse,I intend to consult the users in the waiting room of a vaccination center

### 1.2. Customer Specifications and Clarifications

**From the specifications document:**
 

> "If the information is correct, the receptionist acknowledges the system that the user is ready to take the vaccine." - The receptionist registers the arrival of the user.

> "Then, the receptionist should send the SNS user to a waiting room where (s)he should wait for his/her time." - The receptionist forwards the user to a waiting room, so (s)he can wait for the scheduled time.

> "At any time, a nurse responsible for administering the vaccine will use the application to check the list of SNS users that are present in the vaccination center to take the vaccine" - The nurse can consult the users in the waiting room in a list.

> "[...] and will call one SNS user to administer him/her the vaccine"- Out of the users in the list, the nurse can pick one to administer a vaccine.

> "Usually, the user that has arrived firstly will be the first one to be vaccinated (like a FIFO queue)". - The list is ordered by longest time of arrival by default.

> "However, sometimes, due to operational issues, that might not happen" - Does not provide detail about operational issues, but implies that a FIFO queue might not always be possible or followed by the nurse.

**From the client clarifications:**

> **Question:** What information about the Users (name, SNS number, etc) should the system display when listing them?

> **Answer:** "Name, Sex, Birth Date, SNS User Number and Phone Number."

-

> **Question:** Regarding US05, the listing is supposed to be for the day itself or for a specific day?

> **Answer:** " The list should show the users in the waiting room of a vaccination center."

-
> **Question:** Regarding the US05. In the PI description it is said that, by now, the nurses and the receptionists will work at any center. Will this information remain the same on this Sprint, or will they work at a specific center?

> **Answer:** "Nurses and receptionists can work in any vaccination center."

-
> **Question:** Regarding US 05, what does consulting constitute in this context? Does it refer only to seeing who is present and deciding who gets the vaccine or is checking the user info to administer the vaccine, registering the process, and sending it to the recovery room also part of this US?

> **Answer:** "The goal is to check the list of users that are waiting and ready to take the vaccine."

-

> **Question:** Does the nurse have to choose the vaccination center before executing the list or if that information comes from employee file?
 
> **Answer:** "When the nurse starts to use the application, firstly, the nurse should select the vaccination center where she his working. The nurse wants to check the list of SNS users that are waiting in the vaccination center where she is working."


### 1.3. Acceptance Criteria
• **AC1** : SNS Users’ list should be presented by order of arrival.
• **AC2** : The receptionist may be able to forward and register users in the waiting room.
• **AC3** : The nurse must be able to consult the users in a waiting room at the moment.
• **AC4** : The following attributes of an SNS user must be sown in the list: Name, Sex, Birth Date, SNS User Number and Phone Number.
• **AC5** : Following the login, the nurse must be able to choose which center she is working at.



### 1.4. Found out Dependencies

* There is a dependency to "US10 Register Employee" since at least a nurse should exist so (s)he can ask for a waiting room list
* There is a dependency to "US03 Register SNS User" since at least a user should exist so (s)he can wait in a waiting room
* There is a dependency to "US01 Schedule a vaccine" since at least a vaccine scheduling should exist to be able to vaccinate a user
* There is a dependency to "US04 Register the arrival of an SNS user to take the vaccine" since the user must be at the vaccination center to wait in a waiting room
* There is a dependency to "US04 Register vaccination center" since there must exist a vaccination center where the waiting room is located


### 1.5 Input and Output Data
**Input Data:**
* Selected data:
  * Vaccination Center


**Output:**
* A list of the users in the waiting room
* (In)Success of the operation


### 1.6. System Sequence Diagram (SSD)

*Insert here a SSD depicting the envisioned Actor-System interactions and throughout which data is inputted and outputted to fulfill the requirement. All interactions must be numbered.*

![US05_SSD](US05_SSD.svg)


### 1.7 Other Relevant Remarks

* User story happens everytime the nurse want to see the waiting room list, typically after another user's vaccination.
*Use this section to capture other relevant information that is related with this US such as (i) special requirements ; (ii) data and/or technology variations; (iii) how often this US is held.*


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt
*In this section, it is suggested to present an excerpt of the domain model that is seen as relevant to fulfill this requirement.*

![US05_MD](US05_MD.svg)

### 2.2. Other Remarks

*Use this section to capture some aditional notes/remarks that must be taken into consideration into the design activity. In some case, it might be usefull to add other analysis artifacts (e.g. activity or state diagrams).*



## 3. Design - User Story Realization

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for...                                          | Answer                          | Justification (with patterns)                                                                                 |
| :------------- | :---------------------                                                               | :------------                   | :----------------------------                                                                                 |
| Step 1         | ....interacting with the actor?                                                      | ConsultWaitingRoomUI            | Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model. |
|                | ... coordinating the US?                                                             | ConsultingWaitingRoomController | Controller                                                                                                    |
|                | ...knowing the user's session's vaccination center?                                  | EmployeeSession                 | IE: EmployeeSession knows information about the user's session                                                |
|                | ...knowing the waiting room of a certain center?                                     | VaccinationCenter               | IE: In the domain model, VaccinationCenter has its own waiting room                                           |
|                | ...selecting necessary information about user, arrival time and appointment?         | WaitingRoomMapper               | DTO pattern was used to simplify data transfer                                                                |
|                | ...instantiating a waitingRoomDTO to facilitate data transfer?                       | WaitingRoomMapper               | DTO pattern was used to simplify data transfer                                                                |
|                | ...knowing the arrivals made in the waiting room;                                    | WaitingRoom                     | IE: Waiting room has many Arrivals in the Domain Model. It contains all information about arrivals.           |
|                | ...knowing arrival information?                                                      | Arrival                         | IE: Arrival knows its own information                                                                         |
|                | ...instantiating an arrivalDTO in the waitingRoomDTO with only selected information? | WaitingRoomMapper               | DTO pattern was used to simplify data transfer                                                                |
|                | ...containing only the specific information about an arrival?                        | ArrivalDTO                      | DTO pattern was used to simplify data transfer                                                                |
|                | ...adding arrivals to the waiting room?                                              | WaitingRoomDTO                  | DTO pattern was used to simplify data transfer                                                                |
| Step 2         | ...showing the waiting list for that day?                                            | ConsultWaitingRoomUI            | IE:Responsible for user interactions                                                                          |

### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are:

* WaitingRoom
* Arrival
* VaccinationCenter


Other software classes (i.e. Pure Fabrication) identified:
* ConsultWaitingRoomUI
* ConsultWaitingRoomController
* EmployeeSession
* WaitingRoomMapper
* WaitingRoomDTO
* ArrivalDTO

## 3.2. Sequence Diagram (SD)

*In this section, it is suggested to present an UML dynamic view stating the sequence of domain related software objects' interactions that allows to fulfill the requirement.*

![US05_SD](US05_SD.svg)

## 3.3. Class Diagram (CD)

*In this section, it is suggested to present an UML static view representing the main domain related software classes that are involved in fulfilling the requirement as well as and their relations, attributes and methods.*

![US05_CD](US05_CD.svg)

# 4. Tests
*In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of requirements fulfilling.*

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

*Each type of test is represented roughly here*

##TestConsultWaitingRoom

**Test 1:** Check that DTO's are correctly formed from a set of arrivals.

	@Test
    public void TestConsultWaitingRoomRegular(){

        //Setting dummy vaccination center
        VaccinationCenter vc = new VaccinationCenter("healthcare center",
                "Centro de Saúde do Barão do Corvo",
                "R. Barão do Corvo 676, 4400-037 Vila Nova de Gaia",
                223747010,
                "usf.arcoprado@arsnorte.min-saude.pt",
                93848374,
                "baraodocorvo.com",
                "09:00", "20:00", 45, 30, 2);

        //Seting expected result
        ArrayList<Arrival> arrivalList=new ArrayList<>();
        ArrivalDTO ar1=new ArrivalDTO(new TimeUtils(12,43,15),123456788,"João Ratão",new DateUtils(1997,12,13),"MALE",new TimeUtils(13,0,0),738273849);
        ArrivalDTO ar2=new ArrivalDTO(new TimeUtils(15,5,37),738273849,"Maria Joaninha",new DateUtils(2005,7,1),"FEMALE",new TimeUtils(15,30,0),273849365);
        ArrivalDTO ar3=new ArrivalDTO(new TimeUtils(16,58,23),829384028,"Carochinha",new DateUtils(1967,9,29),"FEMALE",new TimeUtils(17,0,0),374859376);
        WaitingRoomDTO wrDTOexpected=new WaitingRoomDTO();
        wrDTOexpected.add(ar1);
        wrDTOexpected.add(ar2);
        wrDTOexpected.add(ar3);
        ArrayList<ArrivalDTO> arrivalListDTOExpected=wrDTOexpected.getArrivalDTOList();


        //Setting converted result
        ConsultWaitingRoomController ctrl = new ConsultWaitingRoomController();

        Arrival arr1=new Arrival(
                new TimeUtils(12,43,15),
                new SNSUser(
                        123456788,
                        "João Ratão",
                        new DateUtils(1997,12,13),
                        new Email("joaoratao@mail.com"),
                        738273849,
                        Gender.MALE,
                        172839489,
                        "wfdnjkkjnsjsd"),
                new Appointment(
                        293849589,
                        new Date(),
                        new TimeUtils(13,0,0)));

        Arrival arr2=new Arrival(
                new TimeUtils(15,5,37),
                new SNSUser(
                        738273849,
                        "Maria Joaninha",
                        new DateUtils(2005,7,1),
                        new Email("mariajoaninha@mail.com"),
                        273849365,
                        Gender.FEMALE,
                        172839409,
                        "wfdnjkkjnskjsd"),
                new Appointment(
                        738273849,
                        new Date(),
                        new TimeUtils(15,30,0)));

        Arrival arr3=new Arrival(
                new TimeUtils(16,58,23),
                new SNSUser(
                        829384028,
                        "Carochinha",
                        new DateUtils(1967,9,29),
                        new Email("carochinha@mail.com"),
                        374859376,
                        Gender.FEMALE,
                        152839409,
                        "wfdnjkkjnjsd"),
                new Appointment(
                        829384028,
                        new Date(),
                        new TimeUtils(17,0,0)));

        arrivalList.add(arr1);
        arrivalList.add(arr2);
        arrivalList.add(arr3);

        WaitingRoom wr=new WaitingRoom(arrivalList);

        vc.setWaitingRoom(wr);

        ArrayList<ArrivalDTO> arrivalListDTO=ctrl.getWaitingRoom(vc).getArrivalDTOList();
        for (int i = 0; i < arrivalListDTOExpected.size() ; i++) {
            Assertions.assertEquals(arrivalListDTOExpected.get(i).getArrivalTime(),arrivalListDTO.get(i).getArrivalTime());
            Assertions.assertEquals(arrivalListDTOExpected.get(i).getSNSNumber(),arrivalListDTO.get(i).getSNSNumber());
            Assertions.assertEquals(arrivalListDTOExpected.get(i).getName(),arrivalListDTO.get(i).getName());
            Assertions.assertEquals(arrivalListDTOExpected.get(i).getBirthDate(),arrivalListDTO.get(i).getBirthDate());
            Assertions.assertEquals(arrivalListDTOExpected.get(i).getSex(),arrivalListDTO.get(i).getSex());
            Assertions.assertEquals(arrivalListDTOExpected.get(i).getScheduledTime(),arrivalListDTO.get(i).getScheduledTime());
            Assertions.assertEquals(arrivalListDTOExpected.get(i).getPhoneNumber(),arrivalListDTO.get(i).getPhoneNumber());

        }

    }







# 5. Construction (Implementation)

*In this section, it is suggested to provide, if necessary, some evidence that the construction/implementation is in accordance with the previously carried out design. Furthermore, it is recommended to mention/describe the existence of other relevant (e.g. configuration) files and highlight relevant commits.*

## Class CreateVaccineController

        public Object createVaccine(int ID,String name, String brand, VaccineType vaccineType) {

          this.vc=vs.createVaccine(ID,name,brand,vaccineType);
          if(vs.validateVaccineLocally(vc)){
              return vc;
          }
          return false;

        }

        public Object createVaccineAdministrationProcess(int minimumAge, int maximumAge, int numberOfDoses,Vaccine vc) {
          VaccineAdministrationProcess vap=vc.createVaccineAdministrationProcess(minimumAge,maximumAge,numberOfDoses);
          if(vc.validateVaccineAdministrationProcessLocally(vap)){
              return vap;
          }
          return false;

        }

        public Object createDoseAdministration(int doseNumber, float dosage, int timePreviousToCurrentDose, int recoveryTime,VaccineAdministrationProcess vap) {
          DoseAdministration da = vap.createDoseAdministration(doseNumber,dosage,timePreviousToCurrentDose,recoveryTime);
          if(vap.validateDoseAdministrationLocally(da)){
            return da;
          }
          return false;
        }

        public boolean saveVaccine(Vaccine vc){

          return (this.vs).saveVaccine(vc);

        }




## Class Company

        public VaccineStore getVaccineStore() {
          return vs;
        }
    
        
        public VaccineTypeStore getVaccineTypeStore() {
            return vts;
        }

##Class VaccineStore


- A comparator class was added to compare ID's of vaccines, and allow global validation more easily


        public ArrayList<Vaccine> getVaccineStore() {
          return vaccineList;
        }

		public Vaccine createVaccine(int ID,String name, String brand, VaccineType vaccineType) {

          Vaccine vc = new Vaccine(ID,name,brand,vaccineType);
          return vc;

        }

        public boolean validateVaccineLocally(Vaccine vc){
          return vc.validate();
        }

        public boolean addVaccine(Vaccine vc){
          if(validateVaccineGlobally(vc)) {
            return vaccineList.add(vc);
          }else{
            return false;
          }
        }

        public boolean validateVaccineGlobally(Vaccine vc){//to alter to comparison of attributes! THIS IS NOT FINAL
          VaccineIDComparator comparator=new VaccineIDComparator();
          if(!vaccineList.contains(vc)) {
              for (Vaccine vax : vaccineList) {
                  if (comparator.compare(vax, vc) == 0) {
                      return false;
                  }
              }
              return true;
          }else{
              return false;
          }
        }

##Class Vaccine

- Some getter methods were added to allow unit testing

- A toString() function was added to describe the vaccine

- A comparator class was added to compare age groups of vaccine administration processes, and allow global validation more easily


        public Vaccine(Integer ID, String name, String brand, VaccineType vaccineType) {
          this.ID=ID;
          this.name = name;
          this.brand = brand;
          this.vaccineType = vaccineType;
          this.vaccineAdministrationProcessesList=new ArrayList<>();
        }
        
        public boolean validate(){

          try {
              if(this.ID<=0){
                  throw new IllegalArgumentException("The ID is not valid!");
              }
              if (this.name.trim().equals("")) {
                  throw new IllegalArgumentException("The name is not valid!");
              }
              if (this.brand.trim().equals("")) {
                  throw new IllegalArgumentException("The brand is not valid!");
              }
              if (!vaccineType.validate()) {
                  throw new IllegalArgumentException("The vaccine type is not valid!");
              }
  
          }catch(IllegalArgumentException illegalArgumentException){
              return false;
          }
          return true;
    
        }

        public boolean validateVaccineAdministrationProcessLocally(VaccineAdministrationProcess vap){
          return vap.validate();
        }
    
        public VaccineAdministrationProcess createVaccineAdministrationProcess(int minimumAge, int maximumAge,int numberOfDoses) {
    
            VaccineAdministrationProcess vap = new VaccineAdministrationProcess(minimumAge,maximumAge,numberOfDoses);
            return vap;
    
        }
    
        public boolean addVaccineAdministrationProcess(VaccineAdministrationProcess vap){
            VaccineAdministrationProcessAgeComparator comparator=new VaccineAdministrationProcessAgeComparator ();
            if(!vaccineAdministrationProcessesList.contains(vap)) {
                for (VaccineAdministrationProcess vapFromList : vaccineAdministrationProcessesList) {
                    if (comparator.compare(vapFromList, vap) == 0) {
                        return false;
                    }
                }
                return vaccineAdministrationProcessesList.add(vap);
            }else{
                return false;
            }
        }

##Class VaccineAdministrationProcess

- Some getter methods were added to allow unit testing

- A toString() function was added to describe the vaccine administration process


        public VaccineAdministrationProcess(Integer minimumAge,Integer maximumAge,Integer numberOfDoses) {
          this.numberOfDoses = numberOfDoses;
          this.ag = new AgeGroup(minimumAge,maximumAge);
          doseAdministrationList=new ArrayList<>();
        }


        public DoseAdministration createDoseAdministration(int doseNumber, float dosage, int timePreviousToCurrentDose, int recoveryTime) {
            DoseAdministration da = new DoseAdministration(doseNumber,dosage,timePreviousToCurrentDose,recoveryTime);
            return da;
        }
    
        public boolean validateDoseAdministrationLocally(DoseAdministration da){
            return da.validate();
        }
    
         public boolean validate(){
            try {
                if (this.numberOfDoses <= 0) {
                    throw new IllegalArgumentException("The number of doses is not valid!");
                }
                if (!ag.validate()) {
                    throw new IllegalArgumentException("The age group is not valid!");
                }
            }catch(IllegalArgumentException illegalArgumentException){
                return false;
            }
            return true;
        }
        
        public boolean addDoseAdministration(DoseAdministration da){
            return doseAdministrationList.add(da);
        }

##Class DoseAdministration

- Some getter methods were added to allow unit testing

- A toString() function was added to describe the dose administration


        public DoseAdministration(Integer doseNumber, Float dosage, Integer timePreviousToCurrentDose, Integer recoveryTime) {
          this.doseNumber = doseNumber;
          this.dosage = dosage;
          this.timePreviousToCurrentDose = timePreviousToCurrentDose;
          this.recoveryTime = recoveryTime;
        }

    
        public boolean validate() {
    
            try {
                if (this.doseNumber <= 0) {
                    throw new IllegalArgumentException("The dose number is not valid!");
                }
                if (this.dosage <= 0) {
                    throw new IllegalArgumentException("The dosage is not valid!");
                }
                if (this.timePreviousToCurrentDose < 0) {
                    throw new IllegalArgumentException("The time of the previous dose to the current one is not valid!");
                }
                if (this.recoveryTime < 0) {
                    throw new IllegalArgumentException("The recovery time is not valid!");
                }
            } catch (IllegalArgumentException illegalArgumentException) {
                return false;
            }
            return true;
    
        }

*It is also recommended to organize this content by subsections.*

# 6. Integration and Demo

*In this section, it is suggested to describe the efforts made to integrate this functionality with the other features of the system.*
* A new option on the Admin menu was added.
* Utils methods on reading a certain type of variable can now throw exceptions, which are caught in the UI section, to improve UX.
* Used Utils methods to interact with the console and to show and read data.

# 7. Observations

*In this section, it is suggested to present a critical perspective on the developed work, pointing, for example, to other alternatives and or future related work.*

CreateVaccineUI is quite busy and involves many steps, and could be simplified by using A GUI.




