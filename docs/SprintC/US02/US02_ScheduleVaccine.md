# US02 - Schedule Vaccine For a SNS User

## 1. Requirements Engineering


### 1.1. User Story Description

As a receptionist at one vaccination center, I want to schedule a vaccination.
### 1.2. Customer Specifications and Clarifications 

**From the specifications document:**

> "DGS is responsible for the Vaccination Program in Portugal and needs a software application to manage the vaccination process and to **allow SNS users to schedule a vaccine** (...)."
> 
> "To take a vaccine, the SNS user should use the application to schedule his/her vaccination. The user should introduce his/her SNS user number, select the vaccination center, the date, and the time (s)he wants to be vaccinated as well as the type of vaccine to be administered (by default, the system suggests the one related to the ongoing outbreak). Then, the application should check the vaccination center capacity for that day/time and, if possible, confirm that the vaccination is scheduled and inform the user that (s)he should be at the selected vaccination center at the scheduled day and time.  The SNS user may also authorize the DGS to send a SMS message with information about the scheduled appointment. If the user authorizes the sending of the SMS, the application should send an SMS message when the vaccination event is scheduled and registered in the system. Some users (e.g.: older ones) may want to go to a healthcare center to schedule the
vaccine appointment with the help of a receptionists at one vaccination center. "

**From the client clarifications:**

> "Dear students,
>
>In a previous message I said: "The acceptance criteria for US1 and US2 are: a. A SNS user cannot schedule the same vaccine more than once. b. The algorithm should check if the SNS User is within the age and time since the last vaccine".
>
>We can always prepare a system for these two acceptance criteria. Even so, as we are in the final stage of Sprint C, we will drop the acceptance criteria b ( The algorithm should check if the SNS User is within the age and time since the last vaccine). This acceptance criteria will be included in Sprint D.
>
>Best regards,
>Carlos Ferreira"

>"Dear students,
>
>The acceptance criteria for US1 and US2 are: a. A SNS user cannot schedule the same vaccine more than once. b. The algorithm should check if the SNS User is within the age and time since the last vaccine".
>
>This means:
>
>a. At a given moment, the SNS user cannot have more than one vaccine (of a given type) scheduled;
>
>b. The algorithm has to check which vaccine the SNS user took before and check if the conditions (age and time since the last vaccine) are met. If the conditions are met the vaccination event should be scheduled and registered in the system. When scheduling the first dose there is no need to check these conditions.
>
>Best regards,
>Carlos Ferreira"

> Question: We are unsure if it's in this user stories that's asked to implement the "send a SMS message with information about the scheduled appointment" found on the Project Description available in moodle. Could you clarify? 
> 
> Answer: In a previous clarification that I made on this forum, I said: "[The user should receive a] SMS Message to warn of a scheduling [and the message] should include: Date, Time and vaccination center". Teams must record the answers! A file named SMS.txt should be used to receive/record the SMS messages. We will not use a real word service to send SMSs.

>Question: What kind of information should be included in an SMS Message to warn of a scheduling? (for example, the SNS number, center, day, time and vaccine type)?
>
> Answer: Date, Time and vaccination center.
> 
> Dear student,

>Question: "Are we allowed to create an attribute of vaccine type in CMVC object?"
>
>Answer: Please study ESOFT and discuss this issue with ESOFT teachers.
>
>Best regards,
>Carlos Ferreira

>Question:
>"For the US1, the acceptance criteria is: A SNS user cannot schedule the same vaccine more than once. For the US2, the acceptance criteria is: The algorithm should check if the SNS User is within the age and time since the last vaccine.
>[1] Are this acceptance criteria exclusive of each US or are implemented in both?
>[2] To make the development of each US more clear, could you clarify the differences between the two US?"
>
>Answer:
>1- The acceptance criteria for US1 and US2 should be merged. The acceptance criteria por US1 and US2 is: A SNS user cannot schedule the same vaccine more than once. The algorithm should check if the SNS User is within the age and time since the last vaccine."
>
>2- In US1 the actor is the SNS user, in US2 the actor is the receptionist. In US1 the SNS user is already logged in the system and information that is required and that exists in the system should be automatically obtained. In US2 the receptionist should ask the SNS user for the information needed to schedule a vaccination. Information describing the SNS user should be automatically obtained by introducing the SNS user number.
>
>Best regards,
>Carlos Ferreira
### 1.3. Acceptance Criteria

* **AC1:** A SNS user cannot schedule the same vaccine more than once.
* **AC2** The algorithm should check if the SNS User is within the age and time since the last vaccine. (**This criteria was dropped last minute.**)

### 1.4. Found out Dependencies

* "US03 Register an SNS User" because the SNSUser must be registered in the system so the receptionist is able to schedule their vaccine.
* "US09 Register a New Vaccination Center" because vaccination(s) center(s) must be registered in the system so the receptionist can select at which center the SNS User wants to be vaccinated.
* "US12 Register new vaccine type" because the vaccine type must be available in the system so the Receptionist can select which one the SNS User wants to schedule.
* "US13 Register new vaccine" since this has the data to confirm if the SNSUser has the age and the recovery time to take the vaccine that the Receptionist is scheduling. 


### 1.5 Input and Output Data


**Input Data:**
* Typed data:
    * SNSUSer number
    * date
    * time
  
* Selected data:
  * type of vaccine
  * vaccination center
  * Yes (If SNS User authorizes to receive a message) or No (If the SNS User doesn´t authorize toreceive a message)


**Output Data:**

* Confirmation if the schedule of the SNSUser is possible for that center, day and time.
* Asks if the SNSUser authorizes the DGS to send them an SMS message with information about the scheduled appointment.
  *Confirmation of user's choices

### 1.6. System Sequence Diagram (SSD)


![US02_SSD](US02_SSD.svg)


**Other alternatives might exist.**

### 1.7 Other Relevant Remarks

n/a


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 

![US02_MD](US02_MD.svg)

### 2.2. Other Remarks

n/a


## 3. Design - User Story Realization 

### 3.1. Rationale

**SSD - Alternative 1 is adopted.**

| Interaction ID | Question: Which class is responsible for...                                      | Answer                     | Justification (with patterns)                                                                                 |
| :------------- | :---------------------                                                           | :------------              | :----------------------------                                                                                 |
| Step 1         | ...interacting with the SNSUser?                                                 | ScheduleVaccineUI          | Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model. |
|                | ...coordinating the US?                                                          | ScheduleVaccineController  | Controller: This class is responsible for coordinating the system operations beyond the UI.                   |
|                | ...instantiating a new vaccine schedule?                                         | AppointmentStore     | IE: AppointmentStore has all data about schedules.                                                                                                            |
| Step 2         |                                                                                  |                            |                                                                                                               |
| Step 3         | ...verifying if the inputted SNSUser number is correct?                          | SNSUserStore               | IE: SNSUserStore has all data about SNSUsers.                                                                     |
| Step 4         | ...knowing the vaccination centers to show?                                            | VaccinationCenterStore           | IE:VaccinationCenterStore has all vaccination centers.                                                                       |
| Step 5         |             |           |                                                     |
| Step 6         | ...knowing the vaccine types to show?                                      | VaccineTypeStore     | IE: VaccineTypeStore has all vaccination centers.                                                           |
| Step 7         |      ...verifying if is the first time that the user schedules selected vaccine type? |AppointmentStore  |IE: AppointmentStore has all data about schedules.                                                                            |                            |                                                                                                               |
| Step 8         |                                                                                  |                            |                                                                                                               |
| Step 9         | ...verifying if the date is correct?                                             | DateUtils                  | IE: DateUtils knows everything about its format.                                                                  |
|                | ...verifying if it's possible to schedule for this day?                          | AppointmentStore     | IE: AppointmentStore has all schedules.                                                                     |
| Step 10        |                                                                                  |                            |                                                                                                               |
| Step 11        | ...verifying if the time is correct?                                             | TimeUtils                  | IE: TimeUtils knows everything about its format.                                                                  |
|                | ...verifying if it's possible to make a schedule with the selected data?         | AppointmentStore     | IE: AppointmentStore has all schedules.                                                                     |
|                | ...creating a schedule with the data?                                            | Appointment                | IE: Appointment contains all data about itself.                                                                   |
                                                   |
| Step 12        | ...informing the operation success?                                              | ScheduleVaccineUI          | ScheduleVaccineUI interacts with the SNSUser.                                                                 |
| Step 13        | ...saving the info about the receive message authorization?                                      | WantsToReceiveMessageStore | IE: WantsToReceiveMessageStore has all users that want to receive a message.                                      |
| Step 14        |                                       |             |                                       |
| Step 15        | ...confirming and saving the data                                    | AppointmentStore | IE: AppointmentStore has all schedules.                                      |
| Step 16        | ...informing the operation, success?                                             | ScheduleVaccineUI          | ScheduleVaccineUI interacts with the SNSUser.                                                                 |

### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * TimeUtils
 * DateUtils

Other software classes (i.e. Pure Fabrication) identified: 

 * ScheduleVaccineUI  
 * ScheduleVaccineController
 * AppointmentStore
 * VaccineTypeStore
 * WantsToReceiveMessageStore

## 3.2. Sequence Diagram (SD)

![US02_SD](US02_SD.svg)

## 3.3. Class Diagram (CD)

![US02_CD](US02_CD.svg)

# 4. Tests 

**Test 1:** Check that it is not possible to create an instance of the Task class with null values. 

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Task instance = new Task(null, null, null, null, null, null, null);
	}
	

**Test 2:** Check that it is not possible to create an instance of the Task class with a reference containing less than five chars - AC2. 

	@Test(expected = IllegalArgumentException.class)
		public void ensureReferenceMeetsAC2() {
		Category cat = new Category(10, "Category 10");
		
		Task instance = new Task("Ab1", "Task Description", "Informal Data", "Technical Data", 3, 3780, cat);
	}


*It is also recommended to organize this content by subsections.* 

# 5. Construction (Implementation)


## Class CreateTaskController 

		public boolean createTask(String ref, String designation, String informalDesc, 
			String technicalDesc, Integer duration, Double cost, Integer catId)() {
		
			Category cat = this.platform.getCategoryById(catId);
			
			Organization org;
			// ... (omitted)
			
			this.task = org.createTask(ref, designation, informalDesc, technicalDesc, duration, cost, cat);
			
			return (this.task != null);
		}


## Class Organization


		public Task createTask(String ref, String designation, String informalDesc, 
			String technicalDesc, Integer duration, Double cost, Category cat)() {
		
	
			Task task = new Task(ref, designation, informalDesc, technicalDesc, duration, cost, cat);
			if (this.validateTask(task))
				return task;
			return null;
		}



# 6. Integration and Demo 

* A new option on the Employee menu options was added.

* Some demo purposes some tasks are bootstrapped while system starts.


# 7. Observations

Platform and Organization classes are getting too many responsibilities due to IE pattern and, therefore, they are becoming huge and harder to maintain. 

Is there any way to avoid this to happen?





