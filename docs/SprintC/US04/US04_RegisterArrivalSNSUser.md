# US 04 Register the arrival of a SNS user to take the vaccine.

## 1. Requirements Engineering

### 1.1. User Story Description

As a receptionist I want to register the arrival of a SNS user to take the vaccine.

### 1.2. Customer Specifications and Clarifications 

**From the specifications document:**

> A receptionist can register the arrival of a SNS user and send him to a waiting room.

**From the client clarifications:**

> **Question:** What's the capacity of the waiting room?
>  
> **Answer:**  The waiting room of each vaccination center has the capacity to receive all users who take the vaccine on given slot.


***

> **Question:** What is the structure of an SNS number?
>  
> **Answer:** 9 digit number.

### 1.3. Acceptance Criteria

* **AC1:** The SNS number must be in a valid format(9 digits)


* **AC2:** No duplicate entries should be possible for the same SNS user on the same day or vaccine period.


* **AC3:** SNS Users’ list should be presented by order of arrival.


* **AC4:** Vaccination center must exists.


* **AC5:** ArrivalTime must be in a valid format (hh:mm).

* **AC6:** AppointmentDay must be in a valid format (dd-mm-yyyy).

### 1.4. Found out Dependencies

* There is a dependency to "US01 Schedule a vaccine" since at least one SNS user should have scheduled a vaccine to be necessary to do the registration.
* There is a dependency to "US03 Register SNS User" because there must be at least one SNS user.
* There is a dependency to "US09 Register New Vaccination Center" since at least one center must exist for registration to be possible.
* There is a dependency to "US10 Register Employee" as there must be at least one employee (a receptionist) for the registration to be done.


### 1.5 Input and Output Data

**Input Data:**


* Typed data:
    * SNS Number
    * Appointment VaccinationCenter
    * Appointment Day
    * Arrival Time
    

* Selected data:
    * List of scheduled vaccines
    * List of centers

**Output Data:**

* A message stating that registration was successful or not.

### 1.6. System Sequence Diagram (SSD)

**Alternative 1**

![US04_SSD](US04_SSD.svg)

### 1.7 Other Relevant Remarks

N/A


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

![US04_MD](US04_MD.svg)

### 2.2. Other Remarks

N/A


## 3. Design - User Story Realization 

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1  		 |....interacting with the actor?                                             |RegisterArrivalSNSUserUI                   |Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model.|
| 			  	 |...coordinating the US?                                                    |AppointmentController          |Controller|
| 			  	 |...knowing the user's session's vaccination center?                         |EmployeeSession         |IE: EmployeeSession knows information about the user's session|
| 			  	 |...check if the receptionist login is valid?( controls authentications)     |AuthController          |IE: can validate logins of all users( knows all users).|
| 			  	 |...runs the receptionist menu?                                              |ReceptionistUI          |IE: shows and runs the options that the receptionist has in the app|
| Step 2 		 |...asks the receptionist the data?                                                                      |RegisterArrivalSNSUserUI                        |IE: asks all the data.                           |
| Step 3		 |...colect the data entered by the receptionist?				              |RegisterArrivalSNSUserUI                          |IE: colect all the data entered by the receptionist.|
|                |...checks if the user exists?                                                |SNSUserStore| IE: knows all the users. |
| 			  	 |...checks if the user has appointment?                                            |AppointmentStore       |IE: can check appointments.|
| Step 4  		 |...asks the receptionist if the user should go to the Waiting Room?   				                      |RegisterArrivalSNSUserUI                        |IE: asks all the data.                           |
| Step 5  		 |...add the User to the Waiting Room                                                  |VaccinationCenter| IE: Control the entire center.|
| Step 6  		 |...informs the operation (in)sucess						                          |RegisterArrivalSNSUserUI |IE: can send messages to user at any time.|              


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * AppointmentController
 * AppointmentStore
 * ReceptionistUI
 * VaccinationCenter
 * SNSUserStore

 Other software classes (i.e. Pure Fabrication) identified:

 * Appointment
 * EmployeeSession
 * RegisterArrivalSNSUserUI

External software classes:

 * AuthFacade
 * AuthUI
 * AuthController

## 3.2. Sequence Diagram (SD)

*In this section, it is suggested to present an UML dynamic view stating the sequence of domain related software objects' interactions that allows to fulfill the requirement.* 

![US04_SD](US04_SD.svg)

## 3.3. Class Diagram (CD)

*In this section, it is suggested to present an UML static view representing the main domain related software classes that are involved in fulfilling the requirement as well as and their relations, attributes and methods.*

![US04_CD](US04_CD.svg)

# 4. Tests 

*In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of requirements fulfilling.* 

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

**Test 1:** Register an Arrival and Add the User to the WaitingRoom. 

	 public class RegisterArrivalTest {

    @Test
    public void RegisterTheArrival() throws Exception {

        DateUtils birthdate = new DateUtils(2001,01,01);
        DateUtils Appointmentdate = new DateUtils(2022,05,30);
        TimeUtils ArrivalTime = new TimeUtils(13,00);
        AppointmentController controller = new AppointmentController();
        AuthController authController = new AuthController();
       public class RegisterArrivalTest {

    @Test
    public void RegisterTheArrival() throws Exception {

        DateUtils birthdate = new DateUtils(2001,01,01);
        DateUtils Appointmentdate = new DateUtils(2022,05,30);
        TimeUtils ArrivalTime = new TimeUtils(13,00);
        AppointmentController controller = new AppointmentController();
        AuthController authController = new AuthController();
        VaccinationCenter vc = new VaccinationCenter("healthcare center",
                "Centro de Saúde do Barão do Corvo",
                "R. Barão do Corvo 676, 4400-037 Vila Nova de Gaia",
                223747010,
                "usf.arcoprado@arsnorte.min-saude.pt",
                93848374,
                "baraodocorvo.com",
                "09:00", "20:00", 45, 30, 2);

        VaccineType t1 = new VaccineType("1234","Descrição");

        SNSController.getInstance().getSNSUserStore().addNewSNSUser(111111111, "João", birthdate , "joao@gmail.com", 987654321, Gender.MALE, 12345678, "Porto");

        SNSUser user = SNSController.getInstance().getSNSUserStore().get(111111111);

        controller.scheduleAppointment(user.getSnsNumber(), t1, vc, Appointmentdate, ArrivalTime);

        Appointment appointment = AppointmentController.getInstance().getStore().get(user,Appointmentdate, ArrivalTime);

        ArrayList<Arrival> arrivalList=new ArrayList<>();
        WaitingRoom wr=new WaitingRoom(arrivalList);
        wr.add(ArrivalTime,user,appointment);
    }

        VaccineType t1 = new VaccineType("1234","Descrição");

        SNSController.getInstance().getSNSUserStore().addNewSNSUser(111111111, "João", birthdate , "joao@gmail.com", 987654321, Gender.MALE, 12345678, "Porto");

        SNSUser user = SNSController.getInstance().getSNSUserStore().get(111111111);

        controller.scheduleAppointment(user.getSnsNumber(), t1, vc, Appointmentdate, ArrivalTime);

        Appointment appointment = AppointmentController.getInstance().getStore().get(user,Appointmentdate, ArrivalTime);

        ArrayList<Arrival> arrivalList=new ArrayList<>();
        WaitingRoom wr=new WaitingRoom(arrivalList);
        wr.add(ArrivalTime,user,appointment);
    }



# 5. Construction (Implementation)

## Class RegisterArrivalSNSUserUI

    public class RegisterArrivalSNSUserUI implements Runnable {

    /**
     * Run the SNS User Arrival UI Menu.
     */
    @Override
    public void run() {

        String dateString = "";
        DateUtils date;
        String timeString = "";
        TimeUtils time = TimeUtils.setTime("00:00");

       // String arrivalTime ="";
        boolean success = false;
        int snsNumber;
        String option = "";

        do{
             snsNumber = Utils.readIntegerFromConsole("SNS Number: ");
             success = SNSUser.verifyIfSNSUserNumberIsCorrect(snsNumber);

             if (!success) {
                 System.out.println("\nInvalid SNS Number(must have 9 digits)!");
             }

        }while(!success);

        do {
            do {
                dateString = Utils.readLineFromConsole("Write the desired date (dd-mm-yyyy):");
                success = AppointmentController.getInstance().seeIfDateIsCorrect(dateString, "-");
                if (success == false) {
                    System.out.println("\nThis date was incorrectly written, type again.");
                }
            } while (success == false);
            date = AppointmentController.getInstance().setDate(dateString, "-");
            if (AppointmentController.getInstance().confirmIfDateItsNotInThePast(date) == false) {
                System.out.println("\nIt seems that someone is trying to travel in time...\nUnfortunately, we still don't have that kind of technology.\nBut you can always travel to the future ;)\n(But not too far.)");
                success = false;
            }
            if (AppointmentController.getInstance().confirmIfDateItsNotTooInTheFuture(date) == true){
                System.out.println("\nIt seems that you are trying to schedule your appointment to an date where you may be not present. It's better to try another date that's closer to the present.");
                success = false;
            }
        } while (success == false);

        do {
            timeString = Utils.readLineFromConsole("Write the desired time (hh:mm):");
            success = AppointmentController.getInstance().seeIfTimeIsCorrect(timeString);
            if (success == false) {
                System.out.println("\nThis time was incorrectly written, type again.");
            }
            if (success == true) {
                time = AppointmentController.getInstance().setTime(timeString);
                }
        } while (success == false);




       // TimeUtils time = AppointmentController.getInstance().setTime(arrivalTime);

        try{

            AuthController authController = new AuthController();
            VaccinationCenter center = authController.getUserVaccinationCenter();

            SNSUser user = SNSController.getInstance().getSNSUserStore().get(snsNumber);
            Appointment appointment = AppointmentController.get(user,date, time);

            do {
                option = Utils.readLineFromConsole("Do you want to add the User to the Waiting Room? (Y/N)");

            } while (!option.equals("Y") && !option.equals("N") && !option.equals("y") && !option.equals("n"));

            if (option.equals("Y") || option.equals("y")) {
                center.addToWaitingRoom(user, time, appointment);
                System.out.println("Info: User Arrival was successfully registered and was placed in the waiting room.");
            }else{
                throw new NoWaitingRoomException("Info:The user has not been placed in the waiting room.");
            }

        }catch (Exception e){
            System.out.println("Error: " + e.getMessage());
            System.out.println("\nInfo: User Arrival was not successfully registered.");
        }

    }
}

## Class AppointmentController

    public class AppointmentController {

    private AppointmentStore appointmentStore;

    /**
     * Appointment Controller's empty constructor.
     */
    public AppointmentController()
    {
        this.appointmentStore = new AppointmentStore();
    }

    public static boolean hasAppointment(int snsNumber, DateUtils date, TimeUtils time) {
        AppointmentStore appointmentStore = App.getInstance().getCompany().getAppointmentStore();

        ZoneId defaultZoneId = ZoneId.systemDefault();

        return appointmentStore.hasAppointment(snsNumber, date, time);
    }

    public static Appointment get(SNSUser user, DateUtils date, TimeUtils time) throws AppointmentNotFoundException
    {

        return App.getInstance().getCompany().getAppointmentStore().get(user, date, time);

    }


    /**
     * Get the Appointment Store
     * @return Returns the Appointment Store
     */
    public AppointmentStore getStore() {
        return this.appointmentStore;
    }

    /**
     * Appointment Controller singleton.
     *
     * Extracted from https://www.javaworld.com/article/2073352/core-java/core-java-simply-singleton.html?page=2
     */
    private static AppointmentController singleton = null;
    public static AppointmentController getInstance()
    {
        if(singleton == null)
        {
            synchronized(AppointmentController.class)
            {
                singleton = new AppointmentController();
            }
        }
        return singleton;
    }





    //ANNA

    /**
     * To confirm if date is in correct format
     * @param dateString date provided by the user
     * @return true if user wrote the date correctly
     */
    public boolean seeIfDateIsCorrect(String dateString){
        boolean isCorrect= false;

        isCorrect = DateUtils.seeIfItsPossibleToConvertStringToDateUtils(dateString, "/", 3);

        return isCorrect;
    }

    /**
     * Sets date provided by the user as DateUtils
     * @param dateString date provided by the user
     * @return dateString in object DateUtils
     */
    public DateUtils setDate(String dateString){
        DateUtils date = DateUtils.setDate(dateString);
        return date;
    }


    /**
     * Sets time provided by the user as TimeUtils
     * @param timeString time provided by the user
     * @return timeString in object TimeUtils
     */
    public TimeUtils setTime(String timeString){
        TimeUtils time = TimeUtils.setTime(timeString);
        return time;
    }


    /**
     * MethoD to confirm if there are registered vaccine types in the system
     * @return true if there is at least one vaccine in the system
     */
    public boolean vaccineTypeStoreExists(){
        boolean exists = false;

        ArrayList<VaccineType> vaccineTypeStore = VaccineTypeController.getInstance().getStore().getVaccineTypeList();
        if (vaccineTypeStore.size() != 0) {
            exists = true;
        }

        return exists;
    }

    /**
     * MethoD to confirm if there are registered vaccine types in the system
     * @return true if there is at least one vaccine in the system
     */
    public boolean vaccinationCentersStoreExists(){
        boolean exists = false;

        List<VaccinationCenter> allVC = App.getInstance().getCompany().getVaccinationCenterStore().getAllVaccinationCenters();
        if (allVC.size() != 0) {
            exists = true;
        }

        return exists;
    }

    /**
     * Method to verify if SNS number given by the user has the correct structure
     * @param SNSNumber SNS number given by the user
     * @return true if it's in correct format
     */
    public boolean verifySNSUserNumberFormat(int SNSNumber){
        return SNSUser.verifyIfSNSUserNumberIsCorrect(SNSNumber);
    }

    /**
     * Method to see if the given SNS given number matches with the SNS number associated with the email
     * @param email that is logged in
     * @param snsUserNumber SNS number given by the user
     * @return true if associated SNS number associated to the email matches the given SNS number
     */
    public boolean searchSNSUserNumber(Email email, int snsUserNumber){
        return SNSController.getInstance().getSNSUserStore().confirmSNSUserNumberByEmail(email, snsUserNumber);
    }








