# US 11 - Get a list of Employees with a given function/role

## 1. Requirements Engineering

### 1.1. User Story Description

*As an administrator, I want to get a list of Employees with a given function/role.*

### 1.2. Customer Specifications and Clarifications

*(Insert here any related specification and/or clarification provided by the client together with **your interpretation**. When possible, provide a link to such specifications/clarifications).*

**From the specifications document:**

>	*”An Administrator is responsible for properly configuring and managing the core information (e.g.: type of vaccines, vaccines, vaccination centers, employees) required for this application to be operated daily by SNS users, nurses, receptionists, etc.”*


**From the client clarifications:**

> **Question:** Should we give the user the possibility of listing employees from more than 1 role?
> [link to Moodle](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=15711#p20186)
>  
> **Answer:** *The Administrator should select a given role and all employees associated with the selected role should be listed.*

-

> **Question:** Should the list be sorted in any specific way(for example alphabetical name order) or should we give the option to sort by other characteristics?
> [link to Moodle](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=15711#p20186)
>  
> **Answer:** *For now I just want to get a list of employees without considering the order of presentation.*

-

> **Question:** Should the administrator type the role he wants to see the list or select it from a list of existent roles?
> [link to Moodle](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=15744#p20238)
>
> **Answer:** *to be clarified*

-

> **Question:** The list of employees to be got by the admin is per vaccination center, isn't it?
> [link to Moodle](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=15752#p20246)
>
> **Answer:** *to be clarified*

-
### 1.3. Acceptance Criteria

*(Insert here the client acceptance criteria.)*

There are no acceptance criteria for now.

### 1.4. Found out Dependencies

* There is a dependency to "US10 Register Employees" since employees must exist to get a list of Employees with a given function/role.

### 1.5 Input and Output Data

**Selected Data:**

* role wanted
	
**Output Data:**

* list of employees associated with the given role

### 1.6. System Sequence Diagram (SSD)

*(Insert here a SSD depicting the envisioned Actor-System interactions and throughout which data is inputted and outputted to fulfill the requirement. All interactions must be numbered.)*

![US11_SSD](US11_SSD.svg)


### 1.7 Other Relevant Remarks

*(Use this section to capture other relevant information that is related with this US such as (i) special requirements ; (ii) data and/or technology variations; (iii) how often this US is held.)*


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt
*(In this section, it is suggested to present an excerpt of the domain model that is seen as relevant to fulfill this requirement.)*

![US11_MD](US11_MD.svg)

### 2.2. Other Remarks

*(Use this section to capture some aditional notes/remarks that must be taken into consideration into the design activity. In some case, it might be usefull to add other analysis artifacts (e.g. activity or state diagrams).)*



## 3. Design - User Story Realization

### 3.1. Rationale

**(The rationale grounds on the SSD interactions and the identified input/output data.)**

| Interaction ID | Question: Which class is responsible for...        | Answer             | Justification (with patterns)                                                                                 |
| :------------- | :---------------------                             | :------------      | :----------------------------                                                                                 |
| Step 1         | ... interacting with the actor?                    | GetEmployeeListUI  | Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model. |
|                | ... coordinating the US?                           | EmployeeController | Controller                                                                                                    |
|                | ... having all employee roles?                     | EmployeStore       | EmployeeStore knows all employee roles.                                                                |
| Step 2         |                                                    |                    |                                                                                                               |
| Step 3         | ... instantiating a new list of Employees by role? | EmployeeStore      | EmployeeStore has all employees.                                                                              |
| Step 4         |                                                    |                    |                                                                                                               |

### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are:

* EmployeeStore

Other software classes (i.e. Pure Fabrication) identified:
* GetEmployeeListUI
* EmployeeController

## 3.2. Sequence Diagram (SD)

*(In this section, it is suggested to present an UML dynamic view stating the sequence of domain related software objects' interactions that allows to fulfill the requirement.)*

![US11_SD](US11_SD.svg)

## 3.3. Class Diagram (CD)

*(In this section, it is suggested to present an UML static view representing the main domain related software classes that are involved in fulfilling the requirement as well as and their relations, attributes and methods.)*

![US11_CD](US11_CD.svg)

# 4. Tests
*In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of requirements fulfilling.*

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

**Test 1:** Check that it is not possible to create an instance of the Example class with null values.

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

*It is also recommended to organize this content by subsections.*

# 5. Construction (Implementation)

*In this section, it is suggested to provide, if necessary, some evidence that the construction/implementation is in accordance with the previously carried out design. Furthermore, it is recommeded to mention/describe the existence of other relevant (e.g. configuration) files and highlight relevant commits.*

*It is also recommended to organize this content by subsections.*

# 6. Integration and Demo

*In this section, it is suggested to describe the efforts made to integrate this functionality with the other features of the system.*


# 7. Observations

*In this section, it is suggested to present a critical perspective on the developed work, pointing, for example, to other alternatives and or future related work.*
