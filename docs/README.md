# Integrating Project for the 2nd Semester of LEI-ISEP 2021-22

# 1. Team Members

The teams consists of students identified in the following table.

| Student Number | Name                         |
|-- ------------ | ---------------------------- |
| **1211274**    | Alexandra Ipatova            |
| **1211313**    | Anna Vasylyshyna             |
| **1210787**    | Cátia Remelgado              |
| **1191666**    | Fernando Silva               |
| **1211334**    | João Miguel                  |

# 2. Task Distribution ###

Throughout the project's development period, the distribution of _tasks / requirements / features_ by the team members
was carried out as described in the following table.

**Keep this table must always up-to-date.**

| Task                          | [Sprint A](SprintA/README.md)   | [Sprint B](SprintB/README.md)                     | [Sprint C](SprintC/README.md)      | [Sprint D](SprintD/README.md)   |
|------------------------------ | ------------------------------- | ------------------------------------------------- | ---------------------------------- | ------------------------------- |
| Glossary                      | [all](SprintA/Glossary.md)      | [all](SprintB/Glossary.md)                        | [all](SprintC/Glossary.md)         | [all](SprintD/Glossary.md)      |
| Use Case Diagram (UCD)        | [all](SprintA/UCD.md)           | [all](SprintB/UCD/UCD.md)                         | [all](SprintC/UCD.md)              | [all](SprintD/UCD.md)           |
| Supplementary Specification   | [all](SprintA/FURPS.md)         | [all](SprintB/FURPS.md)                           | [all](SprintC/FURPS.md)            | [all](SprintD/FURPS.md)         |
| Domain Model                  | [all](SprintA/DM.md)            | [all](SprintB/DM/DM.md)                           | [all](SprintC/DM.md)               | [all](SprintD/DM.md)            |
| US 01 (SDP Activities)        |                                 |                                                   | [1211313](SprintC/US01/US01.md)    |                                 |
| US 02 (SDP Activities)        |                                 |                                                   | [1191666](SprintC/US02/US02.md)    |                                 |
| US 03 (SDP Activities)        |                                 | [1211334](SprintB/US03/US03.md)                   |                                    |                                 |
| US 04 (SDP Activities)        |                                 |                                                   | [1211334](SprintC/US04/US04.md)    |                                 |
| US 05 (SDP Activities)        |                                 |                                                   | [1211274](SprintC/US05/US05.md)    |                                 |
| US 06 (SDP Activities)        |                                 |                                                   |                                    | [1191666](SprintD/US06/US6.md)  |
| US 08 (SDP Activities)        |                                 |                                                   |                                    | [1211334](SprintD/US08/US08.md) |
| US 09 (SDP Activities)        |                                 | [1210787](SprintB/US09/US09.md)                   |                                    |                                 |
| US 10 (SDP Activities)        |                                 | [1191666](SprintB/US10/US10.md)                   |                                    |                                 |
| US 11 (SDP Activities)        |                                 | [1211313](SprintB/US11/US11_GetEmployeeList.md)   |                                    |                                 |
| US 12 (SDP Activities)        |                                 | [1211334](SprintB/US12/US12.md)                   |                                    |                                 |
| US 13 (SDP Activities)        |                                 | [1211274](SprintB/US13/US13.md)                   |                                    |                                 |
| US 14  (SDP Activities)       |                                 |                                                   | [1210787](SprintC/US14/US14.md)    |                                 |
| US 15  (SDP Activities)       |                                 |                                                   |                                    | [1210787](SprintD/US15/US15.md) |
| US 16  (SDP Activities)       |                                 |                                                   |                                    | [1211274](SprintD/US16/US16.md) |
| US 17  (SDP Activities)       |                                 |                                                   |                                    | [1211313](SprintD/US17/US17.md) |

* The US's which contain MATCP and MDISC theorical contents are done as a group.
