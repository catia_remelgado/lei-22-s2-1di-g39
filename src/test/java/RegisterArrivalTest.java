import app.controller.App;
import app.controller.AppointmentController;
import app.controller.AuthController;
import app.controller.SNSController;
import app.domain.model.*;
import app.domain.utils.DateUtils;
import app.domain.utils.TimeUtils;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class RegisterArrivalTest {

    @Test
    public void RegisterTheArrival() throws Exception {

        DateUtils birthdate = new DateUtils(2001,01,01);
        DateUtils Appointmentdate = new DateUtils(2022,05,30);
        TimeUtils ArrivalTime = new TimeUtils(13,00);
        AppointmentController controller = new AppointmentController();
        AuthController authController = new AuthController();
        VaccinationCenter vc = new VaccinationCenter();

        VaccineType t1 = new VaccineType("1234","Descrição");

        App.getInstance().getCompany().getSnsUserStore().addNewSNSUser(111111111, "João", birthdate , "joao@gmail.com", 987654321, Gender.MALE, 12345678, "Porto");

        SNSUser user = App.getInstance().getCompany().getSnsUserStore().get(111111111);

        controller.scheduleAppointment(user.getSnsNumber(), t1, vc, Appointmentdate, ArrivalTime);

        Appointment appointment = AppointmentController.getInstance().getStore().get(user,Appointmentdate, ArrivalTime);

        ArrayList<Arrival> arrivalList=new ArrayList<>();
        WaitingRoom wr=new WaitingRoom(arrivalList);
        wr.add(ArrivalTime,user,appointment);
    }
}
