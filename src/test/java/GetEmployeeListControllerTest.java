import app.domain.model.EmployeeRolesList;
import app.domain.store.EmployeeStore;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class GetEmployeeListControllerTest {

    @Test
    void showRolesSelectionList() {

        ArrayList<EmployeeRolesList> roles= new ArrayList<EmployeeRolesList>();
        roles.add(EmployeeRolesList.RECEPTIONIST);
        roles.add(EmployeeRolesList.NURSE);
        roles.add(EmployeeRolesList.CENTER_COORDINATOR);

        assertEquals(new EmployeeStore().getEmployeeRolesList(), roles);
    }

}