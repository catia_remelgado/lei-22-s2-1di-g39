import app.controller.App;
import app.controller.AppointmentController;
import app.controller.CreateVaccineController;
import app.controller.RecordVaccineAdministrationController;
import app.domain.model.*;
import app.domain.store.VaccinationCenterStore;
import app.domain.store.VaccineStore;
import app.domain.utils.DateUtils;
import app.domain.utils.TimeUtils;
import org.junit.jupiter.api.Test;

public class TestRecordVaccineAdmistration {


    @Test
    public void RecordVaccineAdministration(){

        String lotNumber = "21C15-16";

        try {
            VaccinationCenter vc = new VaccinationCenter("healthcare center",
                    "Centro de Saúde do Barão do Corvo",
                    "R. Barão do Corvo ",
                    676, "4400-037", " Vila Nova de Gaia",
                    223747010,
                    "usf.arcoprado@arsnorte.min-saude.pt",
                    93848374,
                    "baraodocorvo.com",
                    "09:00", "20:00", 45, 30, 2);

            App app = App.getInstance();
            Company company = app.getCompany();
            VaccinationCenterStore createVaccinationCenterController = company.createVaccinationCenter();
            createVaccinationCenterController.addVaccinationCenter(vc);


            //            CreateVaccinationCenterController.createVaccinationCenter("aaaa","aa","rua",10,"4000-000","porto",123123123,"centro@gmail.com",123123123,"https://aaa.com","10:00","20:00",120,12,10);
            App.getInstance().getCompany().getSnsUserStore().addNewSNSUser(223456789, "João", DateUtils.setDate("01-01-2003", "-"), "joao@gmail.com", 123123123, Gender.MALE, 11231234, "porto");
            SNSUser snsuser = App.getInstance().getCompany().getSnsUserStore().get(223456789);
            App.getInstance().getCompany().getVaccineTypeStore().addVaccineType("1234", "aaaa");
            VaccineType type = App.getInstance().getCompany().getVaccineTypeStore().getVaccineTypeList().get(0);


            VaccineStore vaccineStore = CreateVaccineController.getInstance().getVaccineStore();
            Vaccine vaccine= new Vaccine(1234,"Vaccine Name","testeBrand","rna",type);
            VaccineAdministrationProcess vaccineAdministrationProcess = new VaccineAdministrationProcess(10,21,2);
            vaccine.addVaccineAdministrationProcess(vaccineAdministrationProcess);
            vaccineStore.saveVaccine(vaccine);
            DoseAdministration doseAdministrationProcess = new DoseAdministration(1,0.2f,180,30);
            vaccineAdministrationProcess.addDoseAdministration(doseAdministrationProcess);
            VaccinationCenter center = App.getInstance().getCompany().getVaccinationCenterStore().getAllVaccinationCenters().get(0);
            AppointmentController.getInstance().scheduleAppointment(223456789, type, center, DateUtils.setDate("01-01-2002", "-"), TimeUtils.setTime("11:00"));
            Appointment ap = App.getInstance().getCompany().getAppointmentStore().get(snsuser, DateUtils.setDate("01-01-2002", "-"), TimeUtils.setTime("11:00"));
            center.addToWaitingRoom(snsuser, TimeUtils.setTime("11:00"), ap);

             VaccineShot vaccineShot = new VaccineShot(vaccine,DateUtils.setDate("01-01-2003","-"));
              snsuser.getTakenVaccines().add(0,vaccineShot);



        } catch (Exception exception) {
        }
        }
    }



