import app.controller.ConsultWaitingRoomController;
import app.domain.dto.ArrivalDTO;
import app.domain.model.*;
import app.domain.utils.DateUtils;
import app.domain.utils.TimeUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * testing center performance components
 *
 * @author alexandraipatova
 */
public class TestCenterPerformance {

    @Test
    public void TestDifferenceList() throws IOException {

        //Setting dummy vaccination center
        VaccinationCenter vc = new VaccinationCenter("healthcare center",
                "Centro de Saúde do Barão do Corvo",
                "R. Barão do Corvo ",
                676, "4400-037", " Vila Nova de Gaia",
                223747010,
                "usf.arcoprado@arsnorte.min-saude.pt",
                93848374,
                "baraodocorvo.com", "08:00", "10:00", 45, 30, 2);
        //Setting dummy vaccine administrations
        VaccineAdministration va1 = new VaccineAdministration(
                161593121,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(8, 1),
                new TimeUtils(8),
                new TimeUtils(8, 17),
                new TimeUtils(9, 25));
        VaccineAdministration va2 = new VaccineAdministration(
                161593120,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(8, 24),
                new TimeUtils(8),
                new TimeUtils(9, 11),
                new TimeUtils(9, 43));

        VaccineAdministration va3 = new VaccineAdministration(
                161593121,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(8, 45),
                new TimeUtils(9, 7),
                new TimeUtils(9, 17),
                new TimeUtils(9, 25));
        VaccineAdministration va4 = new VaccineAdministration(
                161593121,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(9, 20),
                new TimeUtils(9, 21),
                new TimeUtils(9, 22),
                new TimeUtils(9, 40));
        VaccineAdministration va5 = new VaccineAdministration(
                161593121,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(9, 20),
                new TimeUtils(9, 21),
                new TimeUtils(9, 22),
                new TimeUtils(9, 40));

        VaccineAdministration va6 = new VaccineAdministration(
                161593121,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(9, 20),
                new TimeUtils(9, 21),
                new TimeUtils(9, 22),
                new TimeUtils(9, 40));

        //creating dummy vaccine administration list
        List<VaccineAdministration> vaList = new ArrayList<>();
        vaList.add(va1);
        vaList.add(va2);
        vaList.add(va3);
        vaList.add(va4);
        vaList.add(va5);
        vaList.add(va5);

        //creating expected difference list result
        List<Integer> expectedList = new ArrayList<>();

        expectedList.add(1); //8:00 - 8:19:59
        expectedList.add(1); //8:20 - 8:39:59
        expectedList.add(1); //8:40 - 8:59:59
        expectedList.add(0); //9:00 - 9:19:59
        expectedList.add(1); //9:20 - 9:39:59
        expectedList.add(-4); //9:40 - 9:59:59

        //calculating actual difference list
        CenterPerformance cp = new CenterPerformance(vc, vaList, 20);

        cp.evaluatePerformance();
        //asserting values
        List<Integer> calculatedList = cp.getDifferenceList();
        for (int i = 0; i < expectedList.size(); i++) {
            Assertions.assertEquals(expectedList.get(i), calculatedList.get(i));

        }
    }
/*
    @Test
    public void TestDifferenceListUnEvenDivision() {

        //Setting dummy vaccination center
        VaccinationCenter vc = new VaccinationCenter("healthcare center",
                "Centro de Saúde do Barão do Corvo",
                "R. Barão do Corvo ",
                676, "4400-037", " Vila Nova de Gaia",
                223747010,
                "usf.arcoprado@arsnorte.min-saude.pt",
                93848374,
                "baraodocorvo.com", "08:00", "10:30", 45, 30, 2);

        //Setting dummy vaccine administrations
        VaccineAdministration va1 = new VaccineAdministration(
                161593121,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(8, 1),
                new TimeUtils(8),
                new TimeUtils(8, 17),
                new TimeUtils(9, 25));
        VaccineAdministration va2 = new VaccineAdministration(
                161593120,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(8, 24),
                new TimeUtils(8),
                new TimeUtils(9, 11),
                new TimeUtils(9, 43));

        VaccineAdministration va3 = new VaccineAdministration(
                161593121,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(8, 45),
                new TimeUtils(9, 7),
                new TimeUtils(9, 17),
                new TimeUtils(9, 25));
//creating dummy vaccine administration list
        List<VaccineAdministration> vaList = new ArrayList<>();
        vaList.add(va1);
        vaList.add(va2);
        vaList.add(va3);

        //creating expected difference list result
        List<Integer> expectedList = new ArrayList<>();

        expectedList.add(2); //8:00 - 8:26:59
        expectedList.add(1); //8:27 - 8:53:59
        expectedList.add(0); //8:54 - 9:22:59
        expectedList.add(-3); //9:23 - 9:49:59
        expectedList.add(0); //9:50 - 10:15:59

        //calculating actual difference list
        CenterPerformance cp = new CenterPerformance(vc, vaList, 27);

        //asserting values
        List<Integer> calculatedList = cp.getDifferenceList();
        for (int i = 0; i < expectedList.size(); i++) {
            Assertions.assertEquals(expectedList.get(i), calculatedList.get(i));

        }
    }*/

    @Test
    public void TestMaxSumSublist() throws IOException {

        //Setting dummy vaccination center
        VaccinationCenter vc = new VaccinationCenter("healthcare center",
                "Centro de Saúde do Barão do Corvo",
                "R. Barão do Corvo ",
                676, "4400-037", " Vila Nova de Gaia",
                223747010,
                "usf.arcoprado@arsnorte.min-saude.pt",
                93848374,
                "baraodocorvo.com", "08:00", "10:00", 45, 30, 2);
        //Setting dummy vaccine administrations
        VaccineAdministration va1 = new VaccineAdministration(
                161593121,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(8, 1),
                new TimeUtils(8),
                new TimeUtils(8, 17),
                new TimeUtils(9, 25));
        VaccineAdministration va2 = new VaccineAdministration(
                161593120,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(8, 24),
                new TimeUtils(8),
                new TimeUtils(9, 11),
                new TimeUtils(9, 43));

        VaccineAdministration va3 = new VaccineAdministration(
                161593121,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(8, 45),
                new TimeUtils(9, 7),
                new TimeUtils(9, 17),
                new TimeUtils(9, 25));

        //creating dummy vaccine administration list
        List<VaccineAdministration> vaList = new ArrayList<>();
        vaList.add(va1);
        vaList.add(va2);
        vaList.add(va3);

        //creating expected difference list result
        List<Integer> expectedList = new ArrayList<>();

        expectedList.add(1); //8:00 - 8:19:59
        expectedList.add(1); //8:20 - 8:39:59
        expectedList.add(1); //8:40 - 8:59:59
        expectedList.add(0); //9:00 - 9:19:59
        expectedList.add(-2); //9:20 - 9:39:59
        expectedList.add(-1); //9:40 - 9:59:59

        //calculating actual difference list
        CenterPerformance cp = new CenterPerformance(vc, vaList, 20);


        cp.evaluatePerformance();

        //setting expected values
        List<Integer> expectedSubList= new ArrayList<>();
        expectedSubList.add(1);
        expectedSubList.add(1);
        expectedSubList.add(1);

        int expectedMaxSum=3;
        TimeUtils expectedStartTime=new TimeUtils(8,0,0);
        TimeUtils expectedEndTime=new TimeUtils(8,40,0);

        //asserting values
        Assertions.assertEquals(expectedMaxSum,cp.getMaxSum());
        Assertions.assertEquals(expectedStartTime,cp.getStartTime());
        Assertions.assertEquals(expectedEndTime,cp.getEndTime());
        for (int i = 0; i < expectedSubList.size(); i++) {
            Assertions.assertEquals(expectedSubList.get(i), cp.getMaxSumSubList().get(i));

        }
    }
}

