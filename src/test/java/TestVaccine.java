import app.controller.CreateVaccineController;
import app.domain.model.Vaccine;
import app.domain.store.VaccineStore;
import app.domain.model.VaccineType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestVaccine {

    /**
     * tests to see  when ID is null, the controller function returns the adequate value
     */
    @Test
    public void TestVaccineIDZero() {

        //Setting variables
        CreateVaccineController controller = new CreateVaccineController();
        VaccineType vt=new VaccineType("12345","COVID-19");
        Object vc;
        boolean expectedRes = false;
        vc = controller.createVaccine(0, "Cominarty", "Pfizer","Messenger RNA",vt);
        //Asserting data
        Assertions.assertEquals(expectedRes, vc);
    }

    /**
     * tests to see when the name is empty, the controller function returns the adequate value
     */
    @Test
    public void TestVaccineNameEmpty() {

        //Setting variables
        CreateVaccineController controller = new CreateVaccineController();
        VaccineType vt=new VaccineType("12345","COVID-19");
        Object vc;
        boolean expectedRes = false;
        vc = controller.createVaccine(1, "   ", "Pfizer","Messenger RNA",vt);
        //Asserting data
        Assertions.assertEquals(expectedRes, vc);
    }

    /**
     * tests to see when the brand is empty, the controller function returns the adequate value
     */
    @Test
    public void TestVaccineBrandEmpty() {
        //Setting variables
        CreateVaccineController controller = new CreateVaccineController();
        VaccineType vt=new VaccineType("12345","COVID-19");
        Object vc;
        boolean expectedRes = false;
        vc = controller.createVaccine(1, "Cominarty", "  ","Messenger RNA",vt);

        //Asserting data
        Assertions.assertEquals(expectedRes, vc);
    }

    /**
     * tests to see when the technology is empty, the controller function returns the adequate value
     */
    @Test
    public void TestVaccineTechnologyEmpty() {
        //Setting variables
        CreateVaccineController controller = new CreateVaccineController();
        VaccineType vt=new VaccineType("12345","COVID-19");
        Object vc;
        boolean expectedRes = false;
        vc = controller.createVaccine(1, "Cominarty", "Pfizer"," ",vt);

        //Asserting data
        Assertions.assertEquals(expectedRes, vc);
    }

    /**
     * tests to see if the vaccine that is created is valid, if the controller function returns an adequate value
     */
    @Test
    public void TestVaccineRegular() {

        //Setting variables
        CreateVaccineController controller = new CreateVaccineController();
        VaccineType vt=new VaccineType("12345","COVID-19");
        Vaccine vc = (Vaccine) controller.createVaccine(1, "Cominarty", "Pfizer","Messenger RNA",vt);
        Vaccine vcExpected = new Vaccine(1,"Cominarty","Pfizer","Messenger RNA",vt);
        //Asserting data
        Assertions.assertEquals(vcExpected.getID(), vc.getID());
        Assertions.assertEquals(vcExpected.getName(), vc.getName());
        Assertions.assertEquals(vcExpected.getBrand(), vc.getBrand());
        Assertions.assertEquals(vcExpected.getVaccineType(), vc.getVaccineType());
    }

    /**
     * tests to see when the vaccine administration process is a duplicate, if the controller function returns an adequate value
     */
    @Test
    public void TestVaccineDuplicate() {

        //Setting variables
        CreateVaccineController controller = new CreateVaccineController();
        VaccineType vt=new VaccineType("12345","COVID-19");
        VaccineStore vs= new VaccineStore();
        Vaccine vc = (Vaccine) controller.createVaccine(1, "Cominarty", "Pfizer","Messenger RNA",vt);
        boolean flagNoRepeat= controller.saveVaccine(vc);
        boolean expectedFlagNoRepeat=true;
        Vaccine vcRepeat = (Vaccine) controller.createVaccine(1, "Cominarty", "Pfizer","Messenger RNA",vt);
        boolean flagRepeat= controller.saveVaccine(vcRepeat);
        boolean expectedFlagRepeat=false;
        //Asserting data
        Assertions.assertEquals(expectedFlagNoRepeat, flagNoRepeat);
        Assertions.assertEquals(expectedFlagRepeat, flagRepeat);
    }

    /**
     * test that the vaccine cannot be set with null values
     */
    @Test
    public void ensureNullIsNotAllowed(){
        Assertions.assertThrows(NullPointerException.class, () -> {
            Vaccine vc = new Vaccine(null, null, null, null,null);
        });
    }

}
