import app.controller.App;
import app.controller.VaccineTypeController;
import app.domain.model.VaccineType;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VaccineTypeControllerTest {


    @Test
    public void VaccineTypeCreation() {


        App.getInstance().getCompany().getVaccineTypeStore().addVaccineType("1234", "Descrição");

        Assertions.assertEquals(App.getInstance().getCompany().getVaccineTypeStore().getVaccineTypeList().size(), 1);
        Assertions.assertEquals(App.getInstance().getCompany().getVaccineTypeStore().getVaccineTypeList().get(0).getCode(), "1234");
        Assertions.assertEquals(App.getInstance().getCompany().getVaccineTypeStore().getVaccineTypeList().get(0).getDesignation(), "Descrição");

    }

    @Test
    public void VaccineTypeCreationErrorCodeBlank() {



        try {
            App.getInstance().getCompany().getVaccineTypeStore().addVaccineType("", "Descrição");
        } catch (Exception exception) {
            Assertions.assertEquals(exception.getMessage(), "Code cannot be blank.");
        }
    }

    @Test
    public void VaccineTypeCreationErrorCodeSize1() {


        try {
            App.getInstance().getCompany().getVaccineTypeStore().addVaccineType("123", "Descrição");
        } catch (Exception exception) {
            Assertions.assertEquals(exception.getMessage(), "Code must have 4 to 8 chars.");
        }
    }

    @Test
    public void VaccineTypeCreationErrorCodeSize2() {


        try {
            App.getInstance().getCompany().getVaccineTypeStore().addVaccineType("123456789", "Descrição");
        } catch (Exception exception) {
            Assertions.assertEquals(exception.getMessage(), "Code must have 4 to 8 chars.");
        }

    }

    @Test
    public void VaccineTypeCreationErrorDesignationBlank() {


        try {
            App.getInstance().getCompany().getVaccineTypeStore().addVaccineType("1234", "");
        } catch (Exception exception) {
            Assertions.assertEquals(exception.getMessage(), "Designation cannot be blank.");
        }

    }

    @Test
    public void VaccineTypeCreationErrorDesignationSize() {
        

        try {
            App.getInstance().getCompany().getVaccineTypeStore().addVaccineType("1234", "dkjagkfdjgfdshgkjfhdskljghldfkhgkfdshglhdflkfgjhfdkjhglkdfhglkjsdfhglkfdhlgkjhfdlsgkfsdkhgjsfgsdf");
        } catch (Exception exception) {
            Assertions.assertEquals(exception.getMessage(), "Designation cannot have more than 40 chars.");
        }

    }

}