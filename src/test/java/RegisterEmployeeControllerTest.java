import app.controller.RegisterEmployeeController;
import app.domain.model.Employee;
import app.domain.model.EmployeeRolesList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class RegisterEmployeeControllerTest {

    @Test
    void getArrayWithAllEmployees() {
        RegisterEmployeeController controller = new RegisterEmployeeController();
        controller.addEmployee(EmployeeRolesList.NURSE, "Bea", "Castelinho", new Email("Bea@gm.gm"), 123456789, 123456789);
        controller.addEmployee(EmployeeRolesList.NURSE, "Van", "Compo", new Email("vn@gm.gm"), 174258369, 987654321);

        ArrayList<Employee> wanted = new ArrayList<>();
        wanted = controller.getArrayWithAllEmployees();
        assertEquals(wanted, controller.getArrayWithAllEmployees());
    }

}