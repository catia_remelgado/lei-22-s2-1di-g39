import app.controller.ConsultWaitingRoomController;
import app.domain.dto.ArrivalDTO;
import app.domain.dto.WaitingRoomDTO;
import app.domain.model.*;
import app.domain.utils.DateUtils;
import app.domain.utils.TimeUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Testing consult waiting room components
 * @author alexandraipatova
 */
public class TestConsultWaitingRoom {



    @Test
    public void TestConsultWaitingRoomRegular(){

        //Setting dummy vaccination center
        VaccinationCenter vc = new VaccinationCenter("healthcare center",
                "Centro de Saúde do Barão do Corvo",
                "R. Barão do Corvo ",
                676, "4400-037"," Vila Nova de Gaia",
                223747010,
                "usf.arcoprado@arsnorte.min-saude.pt",
                93848374,
                "baraodocorvo.com",
                "09:00", "20:00", 45, 30, 2);

        //Seting expected result
        ArrayList<Arrival> arrivalList=new ArrayList<>();
        ArrivalDTO ar1=new ArrivalDTO(new TimeUtils(12,43,15),123456788,"João Ratão",new DateUtils(1997,12,13),"MALE",new TimeUtils(13,0,0),738273849);
        ArrivalDTO ar2=new ArrivalDTO(new TimeUtils(15,5,37),738273849,"Maria Joaninha",new DateUtils(2005,7,1),"FEMALE",new TimeUtils(15,30,0),273849365);
        ArrivalDTO ar3=new ArrivalDTO(new TimeUtils(16,58,23),829384028,"Carochinha",new DateUtils(1967,9,29),"FEMALE",new TimeUtils(17,0,0),374859376);
        List<ArrivalDTO> arrivalListDTOExpected=new ArrayList<>();
        arrivalListDTOExpected.add(ar1);
        arrivalListDTOExpected.add(ar2);
        arrivalListDTOExpected.add(ar3);



        //Setting converted result
        ConsultWaitingRoomController ctrl = new ConsultWaitingRoomController();

        Arrival arr1=new Arrival(
                new TimeUtils(12,43,15),
                new SNSUser(
                        123456788,
                        "João Ratão",
                        new DateUtils(1997,12,13),
                        new Email("joaoratao@mail.com"),
                        738273849,
                        Gender.MALE,
                        172839489,
                        "wfdnjkkjnsjsd"),
                new Appointment(
                        293849589,
                        DateUtils.currentDate(),
                        new TimeUtils(13,0,0)));

        Arrival arr2=new Arrival(
                new TimeUtils(15,5,37),
                new SNSUser(
                        738273849,
                        "Maria Joaninha",
                        new DateUtils(2005,7,1),
                        new Email("mariajoaninha@mail.com"),
                        273849365,
                        Gender.FEMALE,
                        172839409,
                        "wfdnjkkjnskjsd"),
                new Appointment(
                        738273849,
                        DateUtils.currentDate(),
                        new TimeUtils(15,30,0)));

        Arrival arr3=new Arrival(
                new TimeUtils(16,58,23),
                new SNSUser(
                        829384028,
                        "Carochinha",
                        new DateUtils(1967,9,29),
                        new Email("carochinha@mail.com"),
                        374859376,
                        Gender.FEMALE,
                        152839409,
                        "wfdnjkkjnjsd"),
                new Appointment(
                        829384028,
                        DateUtils.currentDate(),
                        new TimeUtils(17,0,0)));

        arrivalList.add(arr1);
        arrivalList.add(arr2);
        arrivalList.add(arr3);

        WaitingRoom wr=new WaitingRoom(arrivalList);

        vc.setWaitingRoom(wr);

        List<ArrivalDTO> arrivalListDTO=ctrl.getWaitingRoom(vc);
        for (int i = 0; i < arrivalListDTOExpected.size() ; i++) {
            Assertions.assertEquals(arrivalListDTOExpected.get(i).getArrivalTime(),arrivalListDTO.get(i).getArrivalTime());
            Assertions.assertEquals(arrivalListDTOExpected.get(i).getSNSNumber(),arrivalListDTO.get(i).getSNSNumber());
            Assertions.assertEquals(arrivalListDTOExpected.get(i).getName(),arrivalListDTO.get(i).getName());
            Assertions.assertEquals(arrivalListDTOExpected.get(i).getBirthDate(),arrivalListDTO.get(i).getBirthDate());
            Assertions.assertEquals(arrivalListDTOExpected.get(i).getSex(),arrivalListDTO.get(i).getSex());
            Assertions.assertEquals(arrivalListDTOExpected.get(i).getScheduledTime(),arrivalListDTO.get(i).getScheduledTime());
            Assertions.assertEquals(arrivalListDTOExpected.get(i).getPhoneNumber(),arrivalListDTO.get(i).getPhoneNumber());

        }

    }

}
