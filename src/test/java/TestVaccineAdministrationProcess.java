import app.controller.CreateVaccineController;
import app.domain.model.Vaccine;
import app.domain.model.VaccineAdministrationProcess;
import app.domain.model.VaccineType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestVaccineAdministrationProcess {

    /**
     * tests to see  when number of doses is null, the controller function returns the adequate value
     */
    @Test
    public void TestVaccineAdministrationProcessNumberOfDosesZero() {
        //Setting variables
        CreateVaccineController controller = new CreateVaccineController();
        VaccineType vt=new VaccineType("12345","COVID-19");
        Vaccine vc = new Vaccine(2, "Cominarty", "Pfizer","Messenger RNA",vt);
        Object vap;
        boolean expectedRes = false;
        vap = controller.createVaccineAdministrationProcess(10, 20, 0, vc);

        //Asserting data
        Assertions.assertEquals(expectedRes, vap);
    }

    /**
     * tests to see when the minimum age is less than zero, the controller function returns the adequate value
     */
    @Test
    public void TestVaccineAdministrationProcessMinimumAgeLessThanZero() {
        //Setting variables
        CreateVaccineController controller = new CreateVaccineController();
        VaccineType vt=new VaccineType("12345","COVID-19");
        Vaccine vc = new Vaccine(2, "Cominarty", "Pfizer","Messenger RNA",vt);
        Object vap;
        boolean expectedRes = false;
        vap = controller.createVaccineAdministrationProcess(-1, 20, 1, vc);

        //Asserting data
        Assertions.assertEquals(expectedRes, vap);
    }

    /**
     * tests to see when the minimum age is unrealistically large, the controller function returns the adequate value
     */
    @Test
    public void TestVaccineAdministrationProcessMinimumAgeVeryLarge() {
        //Setting variables
        CreateVaccineController controller = new CreateVaccineController();
        VaccineType vt=new VaccineType("12345","COVID-19");
        Vaccine vc = new Vaccine(2, "Cominarty", "Pfizer","Messenger RNA",vt);
        Object vap;
        boolean expectedRes = false;
        vap = controller.createVaccineAdministrationProcess(150, 20, 1, vc);

        //Asserting data
        Assertions.assertEquals(expectedRes, vap);
    }
    /**
     * tests to see when the maximum age is less than zero, the controller function returns the adequate value
     */
    @Test
    public void TestVaccineAdministrationProcessMaximumAgeLessThanZero() {

        //Setting variables
        CreateVaccineController controller = new CreateVaccineController();
        VaccineType vt=new VaccineType("12345","COVID-19");
        Vaccine vc = new Vaccine(2, "Cominarty", "Pfizer","Messenger RNA",vt);
        Object vap;
        boolean expectedRes = false;
        vap = controller.createVaccineAdministrationProcess(0, -1, 1, vc);

        //Asserting data
        Assertions.assertEquals(expectedRes, vap);
    }
    /**
     * tests to see when the maximum age is unrealistically large, the controller function returns the adequate value
     */
    @Test
    public void TestVaccineAdministrationProcessMaximumAgeVeryLarge() {

        //Setting variables
        CreateVaccineController controller = new CreateVaccineController();
        VaccineType vt=new VaccineType("12345","COVID-19");
        Vaccine vc = new Vaccine(2, "Cominarty", "Pfizer","Messenger RNA",vt);
        Object vap;
        boolean expectedRes = false;
        vap = controller.createVaccineAdministrationProcess(0, 141, 1, vc);

        //Asserting data
        Assertions.assertEquals(expectedRes, vap);
    }

    /**
     * tests to see when the maximum age is less than the minimum age, the controller function returns the adequate value
     */
    @Test
    public void TestVaccineAdministrationProcessMaximumAgeLessThanMinimumAge() {

        //Setting variables
        CreateVaccineController controller = new CreateVaccineController();
        VaccineType vt=new VaccineType("12345","COVID-19");
        Vaccine vc = new Vaccine(2, "Cominarty", "Pfizer","Messenger RNA",vt);
        Object vap;
        boolean expectedRes = false;
        vap = controller.createVaccineAdministrationProcess(30, 17, 1, vc);

        //Asserting data
        Assertions.assertEquals(expectedRes, vap);
    }

    /**
     * tests to see if the vaccine administration process that is created is valid, if the controller function returns an adequate value
     */
    @Test
    public void TestVaccineAdministrationProcessRegular() {
        //Setting variables
        CreateVaccineController controller = new CreateVaccineController();
        VaccineType vt=new VaccineType("12345","COVID-19");
        Vaccine vc = new Vaccine(2, "Cominarty", "Pfizer","Messenger RNA",vt);
        VaccineAdministrationProcess vap;
        VaccineAdministrationProcess expectedRes = new VaccineAdministrationProcess(1, 30, 31);
        vap = (VaccineAdministrationProcess) controller.createVaccineAdministrationProcess(1, 30, 31, vc);

        //Asserting data
        Assertions.assertEquals(expectedRes.getNumberOfDoses(), vap.getNumberOfDoses());
        Assertions.assertEquals(expectedRes.getAg().getMinimumAge(), vap.getAg().getMinimumAge());
        Assertions.assertEquals(expectedRes.getAg().getMaximumAge(), vap.getAg().getMaximumAge());
    }

    /**
     * tests to see when the vaccine administration process is a duplicate, if the controller function returns an adequate value
     */
    @Test
    public void TestVaccineAdministrationProcessDuplicate() {
        //Setting variables
        CreateVaccineController controller = new CreateVaccineController();
        VaccineType vt=new VaccineType("12345","COVID-19");
        Vaccine vc = new Vaccine(2, "Cominarty", "Pfizer","Messenger RNA",vt);
        VaccineAdministrationProcess vap = (VaccineAdministrationProcess) controller.createVaccineAdministrationProcess(1, 30, 31, vc);
        boolean flagNoRepeat=vc.addVaccineAdministrationProcess(vap);
        boolean expectedFlagNoRepeat=true;
        VaccineAdministrationProcess vapRepeated = (VaccineAdministrationProcess) controller.createVaccineAdministrationProcess(1, 30, 2, vc);
        boolean flagRepeat=vc.addVaccineAdministrationProcess(vapRepeated);
        boolean expectedFlagRepeat=false;

        //Asserting data
        Assertions.assertEquals(expectedFlagNoRepeat, flagNoRepeat);
        Assertions.assertEquals(expectedFlagRepeat, flagRepeat);
    }

    /**
     * test that the vaccine administration process cannot be set with null values
     */
    @Test
    public void ensureNullIsNotAllowed(){
        Assertions.assertThrows(NullPointerException.class, () -> {
            VaccineAdministrationProcess vc = new VaccineAdministrationProcess(null,null,null);
        });
    }

}
