import app.controller.RegisterEmployeeController;
import app.domain.model.EmployeeRolesList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pt.isep.lei.esoft.auth.domain.model.Email;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    public void EmployeeIsAdded(){

        RegisterEmployeeController controller = new RegisterEmployeeController();

        controller.addEmployee(EmployeeRolesList.NURSE, "Bea", "Castelinho", new Email("Bea@gm.gm"), 123456789, 123456789);

        Assertions.assertEquals(controller.getArrayWithAllEmployees().size(), 1);
        Assertions.assertEquals(controller.getArrayWithAllEmployees().get(0).getRole(), EmployeeRolesList.NURSE);
        Assertions.assertEquals(controller.getArrayWithAllEmployees().get(0).getCCNumber(), 123456789);
    }

    @Test
    void testToString() {
        RegisterEmployeeController controller = new RegisterEmployeeController();
        controller.addEmployee(EmployeeRolesList.NURSE, "Bea", "Castelinho", new Email("Bea@gm.gm"), 123456789, 123456789);

        Assertions.assertEquals(controller.getArrayWithAllEmployees().get(0).toString(), "Employee:" +
                " name='Bea'"+
                ", role=NURSE" +
                ", adress='Castelinho'" +
                ", number=123456789" +
                ", email=Bea@gm.gm" +
                ", CCNUmber=123456789");
    }

    @Test
    void getCCNumber() {
        RegisterEmployeeController controller = new RegisterEmployeeController();
        controller.addEmployee(EmployeeRolesList.NURSE, "Bea", "Castelinho", new Email("Bea@gm.gm"), 123456789, 123456789);

        Assertions.assertEquals(controller.getArrayWithAllEmployees().get(0).getCCNumber(), 123456789);
    }

    @Test
    void getRole() {
        RegisterEmployeeController controller = new RegisterEmployeeController();
        controller.addEmployee(EmployeeRolesList.NURSE, "Bea", "Castelinho", new Email("Bea@gm.gm"), 123456789, 123456789);

        Assertions.assertEquals(controller.getArrayWithAllEmployees().get(0).getRole(), EmployeeRolesList.NURSE);
    }
}