package app.domain.utils;

import org.junit.jupiter.api.Test;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.*;

class TimeUtilsTest {

    @Test
    void addOrTakeMinutes() throws ParseException {
        String time = "12:30";
        int amount = -30;

        TimeUtils expected = new TimeUtils(12, 0, 0);
        TimeUtils real = TimeUtils.setTime(time);
        real.addOrTakeMinutes(time, amount);

        assertEquals(expected, real);
    }


}