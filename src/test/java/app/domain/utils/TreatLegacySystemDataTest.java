package app.domain.utils;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TreatLegacySystemDataTest {

    @Test
    void treatSystemLegacyData() {
        List<List<String>> seeIfIsParsable = new ArrayList<>();

        //invalid sns user number
        List<String> item1 = new ArrayList<String>(
                Arrays.asList("11a", "dcswd", "wed", "desdcfse", "fed 8:00",
                        "20/05/2022 8:22", "20/05/2022 8:22", "20/05/2022 8:22"));

        //"invalid" date
        List<String> item2 = new ArrayList<String>(
                Arrays.asList("11", "dcswd", "wed", "desdcfse", "05/05/2022 8:00",
                        "20/05/2022 8:22", "20/05/2022 8:22", "20/05/2022 8:22"));

        //valid (it may be not valid for the object, but everything here it's parsable to object's attributes)
        List<String> item3 = new ArrayList<String>(
                Arrays.asList("11", "dcswd", "wed", "desdcfse", "05/05/2022 8:00",
                        "05/05/2022 8:22", "05/05/2022 8:22", "05/05/2022 8:22"));

        //invalid time
        List<String> item4 = new ArrayList<String>(
                Arrays.asList("11", "dcswd", "wed", "desdcfse", "05/05/2022 8:00",
                        "05/05/2022 8:22", "05/05/2022 8", "05/05/2022 8:22"));

        //invalid date and time
        List<String> item5 = new ArrayList<String>(
                Arrays.asList("11", "dcswd", "wed", "desdcfse", "05/05/2022 8:00",
                        "05/05/2022 8:22", "  ", "05/05/2022 8:22"));

        //invalid one of the fields has information a mais
        List<String> item6 = new ArrayList<String>(
                Arrays.asList("11", "dcswd", "wed", "desdcfse", "05/05/2022 8:00",
                        "05/05/2022 5 8:22", "  ", "05/05/2022 8:22"));

        seeIfIsParsable.add(item1);
        seeIfIsParsable.add(item2);
        seeIfIsParsable.add(item3);
        seeIfIsParsable.add(item4);
        seeIfIsParsable.add(item5);
        seeIfIsParsable.add(item6);

        List<List<String>> expected = new ArrayList<>();

        expected.add(item3);

        List<List<String>> real = new TreatLegacySystemData().treatSystemLegacyData(seeIfIsParsable);

        assertEquals(expected, real);
    }

    @Test
    void seeIfDateAndTimeAreValid() {
        List<List<String>> seeIfIsParsable = new ArrayList<>();

        //invalid sns user number
        List<String> item1 = new ArrayList<String>(
                Arrays.asList("11a", "dcswd", "wed", "desdcfse", "fed 8:00",
                        "20/05/2022 8:22", "20/05/2022 8:22", "20/05/2022 8:22"));

        //"invalid" date
        List<String> item2 = new ArrayList<String>(
                Arrays.asList("11", "dcswd", "wed", "desdcfse", "05/05/2022 8:00",
                        "20/05/2022 8:22", "20/05/2022 8:22", "20/05/2022 8:22"));

        //valid (it may be not valid for the object, but everything here it's parsable to object's attributes)
        List<String> item3 = new ArrayList<String>(
                Arrays.asList("11", "dcswd", "wed", "desdcfse", "05/05/2022 8:00",
                        "05/05/2022 8:22", "05/05/2022 8:22", "05/05/2022 8:22"));

        //invalid time
        List<String> item4 = new ArrayList<String>(
                Arrays.asList("11", "dcswd", "wed", "desdcfse", "05/05/2022 8:00",
                        "05/05/2022 8:22", "05/05/2022 8", "05/05/2022 8:22"));

        //invalid date and time
        List<String> item5 = new ArrayList<String>(
                Arrays.asList("11", "dcswd", "wed", "desdcfse", "05/05/2022 8:00",
                        "05/05/2022 8:22", "  ", "05/05/2022 8:22"));

        assertEquals(false, new TreatLegacySystemData().seeIfDateAndTimeAreValid(item1));
        assertEquals(false, new TreatLegacySystemData().seeIfDateAndTimeAreValid(item2));
        assertEquals(true, new TreatLegacySystemData().seeIfDateAndTimeAreValid(item3));
        assertEquals(false, new TreatLegacySystemData().seeIfDateAndTimeAreValid(item4));
        assertEquals(false, new TreatLegacySystemData().seeIfDateAndTimeAreValid(item5));
    }

    @Test
    void treatToMakeDTOOfVaccineAdministration() {

        List<String> item1 = new ArrayList<String>(
                Arrays.asList("123456789", "vacina", "primeira", "21345-77", "05/05/2022 8:00",
                        "05/05/2022 8:22", "05/05/2022 8:22", "05/05/2022 8:22"));

        List<String> item2 = new ArrayList<String>(
                Arrays.asList("11", "dcswd", "wed", "desdcfse", "05/05/2022 8:00",
                        "05/05/2022 8:22", "05/05/2022 8:22", "05/05/2022 8:22"));

        List<String> item3 = new ArrayList<String>(
                Arrays.asList("111111111", "dcswd", "wed", "desdcfse", "05/05/2022 8:00",
                        "05/05/2022 8:22", "05/05/2022 8:22", "05/05/2022 8:22"));

        List<List<String>> seeIfIsParsable = new ArrayList<>();

        seeIfIsParsable.add(item1);
        seeIfIsParsable.add(item2);
        seeIfIsParsable.add(item3);

        List<String> expected1 = new ArrayList<>(
                Arrays.asList("123456789", "vacina", "primeira", "21345-77",
                        "05/05/2022", "8:22", "8:00", "8:22", "8:22"));

        List<String> expected2 = new ArrayList<>(
                Arrays.asList("11", "dcswd", "wed", "desdcfse", "05/05/2022",
                        "8:22", "8:00", "8:22", "8:22"));

        List<String> expected3 = new ArrayList<>(
                Arrays.asList("111111111", "dcswd", "wed", "desdcfse", "05/05/2022",
                        "8:22", "8:00", "8:22", "8:22"));

        List<List<String>> finalExpected = new ArrayList<>();

        finalExpected.add(expected1);
        finalExpected.add(expected2);
        finalExpected.add(expected3);

        List<List<String>> real = new TreatLegacySystemData().treatToMakeDTOOfVaccineAdministration(seeIfIsParsable);

        assertEquals(finalExpected, real);

    }

    @Test
    void lineToAdd() {

        List<String> item1 = new ArrayList<String>(
                Arrays.asList("123456789", "vacina", "primeira", "21345-77", "05/05/2022 8:00",
                        "05/05/2022 8:22", "05/05/2022 8:22", "05/05/2022 8:22"));

        List<String> item2 = new ArrayList<String>(
                Arrays.asList("11", "dcswd", "wed", "desdcfse", "05/05/2022 8:00",
                        "05/05/2022 8:22", "05/05/2022 8:22", "05/05/2022 8:22"));

        List<String> item3 = new ArrayList<String>(
                Arrays.asList("111111111", "dcswd", "wed", "desdcfse", "05/05/2022 8:00",
                        "05/05/2022 8:22", "05/05/2022 8:22", "05/05/2022 8:22"));

        List<String> line1 = new ArrayList<>();

        line1.add(item1.get(0));
        line1.add(item1.get(1));
        line1.add(item1.get(2));
        line1.add(item1.get(3));
        line1.add(new TreatLegacySystemData().getDate(item1.get(6)));
        line1.add(new TreatLegacySystemData().getTime(item1.get(5)));
        line1.add(new TreatLegacySystemData().getTime(item1.get(4)));
        line1.add(new TreatLegacySystemData().getTime(item1.get(6)));
        line1.add(new TreatLegacySystemData().getTime(item1.get(7)));


        List<String> line2 = new ArrayList<>();

        line2.add(item2.get(0));
        line2.add(item2.get(1));
        line2.add(item2.get(2));
        line2.add(item2.get(3));
        line2.add(new TreatLegacySystemData().getDate(item2.get(6)));
        line2.add(new TreatLegacySystemData().getTime(item2.get(5)));
        line2.add(new TreatLegacySystemData().getTime(item2.get(4)));
        line2.add(new TreatLegacySystemData().getTime(item2.get(6)));
        line2.add(new TreatLegacySystemData().getTime(item2.get(7)));


        List<String> line3 = new ArrayList<>();

        line3.add(item3.get(0));
        line3.add(item3.get(1));
        line3.add(item3.get(2));
        line3.add(item3.get(3));
        line3.add(new TreatLegacySystemData().getDate(item3.get(6)));
        line3.add(new TreatLegacySystemData().getTime(item3.get(5)));
        line3.add(new TreatLegacySystemData().getTime(item3.get(4)));
        line3.add(new TreatLegacySystemData().getTime(item3.get(6)));
        line3.add(new TreatLegacySystemData().getTime(item3.get(7)));


        List<String> expected1 = new ArrayList<>(
                Arrays.asList("123456789", "vacina", "primeira", "21345-77",
                        "05/05/2022", "8:22", "8:00", "8:22", "8:22"));

        List<String> expected2 = new ArrayList<>(
                Arrays.asList("11", "dcswd", "wed", "desdcfse", "05/05/2022",
                        "8:22", "8:00", "8:22", "8:22"));

        List<String> expected3 = new ArrayList<>(
                Arrays.asList("111111111", "dcswd", "wed", "desdcfse", "05/05/2022",
                        "8:22", "8:00", "8:22", "8:22"));

        assertEquals(line1,expected1);
        assertEquals(line2, expected2);
        assertEquals(line3, expected3);
    }

    @Test
    void getDate() {

        String date1 = "05/05/2022 8:00";
        String date2 = "02-02-2022 20:00";

        String expected1 = "05/05/2022";
        String expected2 = "02-02-2022";

        String real1 = new TreatLegacySystemData().getDate(date1);
        String real2 = new TreatLegacySystemData().getDate(date2);

        assertEquals(expected1, real1);
        assertEquals(expected2, real2);
    }

    @Test
    void getTime() {
        String time1 = "05/05/2022 8:00";
        String time2 = "02-02-2022 20:00";

        String expected1 = "8:00";
        String expected2 = "20:00";

        String real1 = new TreatLegacySystemData().getTime(time1);
        String real2 = new TreatLegacySystemData().getTime(time2);

        assertEquals(expected1, real1);
        assertEquals(expected2, real2);
    }
}