package app.domain.utils;

import org.junit.jupiter.api.Test;

import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.*;

class DateUtilsTest {

    @Test
    void confirmIfDateItsTooInTheFuture() {
        DateUtils date = new DateUtils(20000000, 12, 12);

        boolean real = DateUtils.confirmIfDateItsTooInTheFuture(date);
        assertEquals(false, real);
    }

    @Test
    void confirmIfDateItsNotPast() {
        DateUtils date = new DateUtils(200, 12, 12);
        boolean real = DateUtils.confirmIfDateItsNotPast(date);
        assertEquals(false, real);
    }
}