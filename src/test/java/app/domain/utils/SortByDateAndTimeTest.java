package app.domain.utils;

import app.domain.model.VaccineAdministration;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SortByDateAndTimeTest {

    @Test
    void bubbleSortForArrivalTime() {
        VaccineAdministration vacAdmin1 = new VaccineAdministration(123456789, "Help",
                1, "21345-22", new DateUtils(2022,05,15),
                new TimeUtils(10, 15), new TimeUtils(10, 10),
                new TimeUtils(10, 20), new TimeUtils(11, 0));

        VaccineAdministration vacAdmin2 = new VaccineAdministration(123456789, "Help",
                1, "21345-22", new DateUtils(2022,05,15),
                new TimeUtils(10, 30), new TimeUtils(10, 25),
                new TimeUtils(10, 40), new TimeUtils(11, 10));

        VaccineAdministration vacAdmin3 = new VaccineAdministration(123456789, "Help",
                1, "21345-22", new DateUtils(2022,05,15),
                new TimeUtils(10, 00), new TimeUtils(10, 25),
                new TimeUtils(10, 40), new TimeUtils(11, 15));

        VaccineAdministration vacAdmin4 = new VaccineAdministration(123456789, "Help",
                1, "21345-22", new DateUtils(2022,05,16),
                new TimeUtils(10, 00), new TimeUtils(10, 25),
                new TimeUtils(10, 40), new TimeUtils(11, 15));

        VaccineAdministration vacAdmin5 = new VaccineAdministration(123456789, "Help",
                1, "21345-22", new DateUtils(2022,05,10),
                new TimeUtils(10, 00), new TimeUtils(10, 25),
                new TimeUtils(10, 40), new TimeUtils(11, 15));

        VaccineAdministration vacAdmin6 = new VaccineAdministration(123456789, "Help",
                1, "21345-22", new DateUtils(2022,05,17),
                new TimeUtils(10, 00), new TimeUtils(10, 25),
                new TimeUtils(10, 40), new TimeUtils(11, 15));

        List<VaccineAdministration> expected = new ArrayList<>();
        expected.add(vacAdmin5);
        expected.add(vacAdmin3);
        expected.add(vacAdmin1);
        expected.add(vacAdmin2);
        expected.add(vacAdmin4);
        expected.add(vacAdmin6);

        List<String> expectedInStrings = new ArrayList<>();
        expectedInStrings.add(expected.get(0).getAdministrationDate().toAnoMesDiaString() +"  "+ expected.get(0).getArrivalTime().toStringHHMM());
        expectedInStrings.add(expected.get(1).getAdministrationDate().toAnoMesDiaString() +"  "+ expected.get(1).getArrivalTime().toStringHHMM());
        expectedInStrings.add(expected.get(2).getAdministrationDate().toAnoMesDiaString() +"  "+ expected.get(2).getArrivalTime().toStringHHMM());
        expectedInStrings.add(expected.get(3).getAdministrationDate().toAnoMesDiaString() +"  "+ expected.get(3).getArrivalTime().toStringHHMM());
        expectedInStrings.add(expected.get(4).getAdministrationDate().toAnoMesDiaString() +"  "+ expected.get(4).getArrivalTime().toStringHHMM());
        expectedInStrings.add(expected.get(5).getAdministrationDate().toAnoMesDiaString() +"  "+ expected.get(5).getArrivalTime().toStringHHMM());

        List<VaccineAdministration> real = new ArrayList<>();
        real.add(vacAdmin1);
        real.add(vacAdmin2);
        real.add(vacAdmin3);
        real.add(vacAdmin4);
        real.add(vacAdmin6);
        real.add(vacAdmin5);

        SortByDateAndTime.bubbleSortForArrivalTime(real);

        List<String> realInStrings = new ArrayList<>();
        realInStrings.add(real.get(0).getAdministrationDate().toAnoMesDiaString() +"  "+ real.get(0).getArrivalTime().toStringHHMM());
        realInStrings.add(real.get(1).getAdministrationDate().toAnoMesDiaString() +"  "+ real.get(1).getArrivalTime().toStringHHMM());
        realInStrings.add(real.get(2).getAdministrationDate().toAnoMesDiaString() +"  "+ real.get(2).getArrivalTime().toStringHHMM());
        realInStrings.add(real.get(3).getAdministrationDate().toAnoMesDiaString() +"  "+ real.get(3).getArrivalTime().toStringHHMM());
        realInStrings.add(real.get(4).getAdministrationDate().toAnoMesDiaString() +"  "+ real.get(4).getArrivalTime().toStringHHMM());
        realInStrings.add(real.get(5).getAdministrationDate().toAnoMesDiaString() +"  "+ real.get(5).getArrivalTime().toStringHHMM());


        //assertEquals(expected, real);
        assertEquals(expectedInStrings, realInStrings);
    }

    @Test
    void bubbleSortForLeavingTime() {
        VaccineAdministration vacAdmin1 = new VaccineAdministration(123456789, "Help",
                1, "21345-22", new DateUtils(2022,05,15),
                new TimeUtils(10, 15), new TimeUtils(10, 10),
                new TimeUtils(10, 20), new TimeUtils(11, 0));

        VaccineAdministration vacAdmin2 = new VaccineAdministration(123456789, "Help",
                1, "21345-22", new DateUtils(2022,05,15),
                new TimeUtils(10, 30), new TimeUtils(10, 25),
                new TimeUtils(10, 40), new TimeUtils(11, 10));

        VaccineAdministration vacAdmin3 = new VaccineAdministration(123456789, "Help",
                1, "21345-22", new DateUtils(2022,05,15),
                new TimeUtils(10, 00), new TimeUtils(10, 25),
                new TimeUtils(10, 40), new TimeUtils(11, 15));

        VaccineAdministration vacAdmin4 = new VaccineAdministration(123456789, "Help",
                1, "21345-22", new DateUtils(2022,05,16),
                new TimeUtils(10, 00), new TimeUtils(10, 25),
                new TimeUtils(10, 40), new TimeUtils(11, 15));

        VaccineAdministration vacAdmin5 = new VaccineAdministration(123456789, "Help",
                1, "21345-22", new DateUtils(2022,05,10),
                new TimeUtils(10, 00), new TimeUtils(10, 25),
                new TimeUtils(10, 40), new TimeUtils(11, 15));

        VaccineAdministration vacAdmin6 = new VaccineAdministration(123456789, "Help",
                1, "21345-22", new DateUtils(2022,05,17),
                new TimeUtils(10, 00), new TimeUtils(10, 25),
                new TimeUtils(10, 40), new TimeUtils(11, 15));

        List<VaccineAdministration> expected = new ArrayList<>();
        expected.add(vacAdmin5);
        expected.add(vacAdmin1);
        expected.add(vacAdmin2);
        expected.add(vacAdmin3);
        expected.add(vacAdmin4);
        expected.add(vacAdmin6);

        List<String> expectedInStrings = new ArrayList<>();
        expectedInStrings.add(expected.get(0).getAdministrationDate().toAnoMesDiaString() +"  "+ expected.get(0).getLeavingTime().toStringHHMM());
        expectedInStrings.add(expected.get(1).getAdministrationDate().toAnoMesDiaString() +"  "+ expected.get(1).getLeavingTime().toStringHHMM());
        expectedInStrings.add(expected.get(2).getAdministrationDate().toAnoMesDiaString() +"  "+ expected.get(2).getLeavingTime().toStringHHMM());
        expectedInStrings.add(expected.get(3).getAdministrationDate().toAnoMesDiaString() +"  "+ expected.get(3).getLeavingTime().toStringHHMM());
        expectedInStrings.add(expected.get(4).getAdministrationDate().toAnoMesDiaString() +"  "+ expected.get(4).getLeavingTime().toStringHHMM());
        expectedInStrings.add(expected.get(5).getAdministrationDate().toAnoMesDiaString() +"  "+ expected.get(5).getLeavingTime().toStringHHMM());

        List<VaccineAdministration> real = new ArrayList<>();
        real.add(vacAdmin3);
        real.add(vacAdmin4);
        real.add(vacAdmin6);
        real.add(vacAdmin5);
        real.add(vacAdmin2);
        real.add(vacAdmin1);

        SortByDateAndTime.bubbleSortForLeavingTime(real);

        List<String> realInStrings = new ArrayList<>();
        realInStrings.add(real.get(0).getAdministrationDate().toAnoMesDiaString() +"  "+ real.get(0).getLeavingTime().toStringHHMM());
        realInStrings.add(real.get(1).getAdministrationDate().toAnoMesDiaString() +"  "+ real.get(1).getLeavingTime().toStringHHMM());
        realInStrings.add(real.get(2).getAdministrationDate().toAnoMesDiaString() +"  "+ real.get(2).getLeavingTime().toStringHHMM());
        realInStrings.add(real.get(3).getAdministrationDate().toAnoMesDiaString() +"  "+ real.get(3).getLeavingTime().toStringHHMM());
        realInStrings.add(real.get(4).getAdministrationDate().toAnoMesDiaString() +"  "+ real.get(4).getLeavingTime().toStringHHMM());
        realInStrings.add(real.get(5).getAdministrationDate().toAnoMesDiaString() +"  "+ real.get(5).getLeavingTime().toStringHHMM());

        // assertEquals(expected, real);
        assertEquals(expectedInStrings, realInStrings);
    }

    @Test
    void quickSortLeavingTime() {
        VaccineAdministration vacAdmin1 = new VaccineAdministration(123456789, "Help",
                1, "21345-22", new DateUtils(2022,05,15),
                new TimeUtils(10, 15), new TimeUtils(10, 10),
                new TimeUtils(10, 20), new TimeUtils(11, 0));

        VaccineAdministration vacAdmin2 = new VaccineAdministration(123456789, "Help",
                1, "21345-22", new DateUtils(2022,05,15),
                new TimeUtils(10, 30), new TimeUtils(10, 25),
                new TimeUtils(10, 40), new TimeUtils(11, 10));

        VaccineAdministration vacAdmin3 = new VaccineAdministration(123456789, "Help",
                1, "21345-22", new DateUtils(2022,05,15),
                new TimeUtils(10, 00), new TimeUtils(10, 25),
                new TimeUtils(10, 40), new TimeUtils(11, 15));

        VaccineAdministration vacAdmin4 = new VaccineAdministration(123456789, "Help",
                1, "21345-22", new DateUtils(2022,05,16),
                new TimeUtils(10, 00), new TimeUtils(10, 25),
                new TimeUtils(10, 40), new TimeUtils(11, 15));

        VaccineAdministration vacAdmin5 = new VaccineAdministration(123456789, "Help",
                1, "21345-22", new DateUtils(2022,05,10),
                new TimeUtils(10, 00), new TimeUtils(10, 25),
                new TimeUtils(10, 40), new TimeUtils(11, 15));

        VaccineAdministration vacAdmin6 = new VaccineAdministration(123456789, "Help",
                1, "21345-22", new DateUtils(2022,05,17),
                new TimeUtils(10, 00), new TimeUtils(10, 25),
                new TimeUtils(10, 40), new TimeUtils(11, 15));

        List<VaccineAdministration> expected = new ArrayList<>();
        expected.add(vacAdmin5);
        expected.add(vacAdmin3);
        expected.add(vacAdmin1);
        expected.add(vacAdmin2);
        expected.add(vacAdmin4);
        expected.add(vacAdmin6);

        List<String> expectedInStrings = new ArrayList<>();
        expectedInStrings.add(expected.get(0).getAdministrationDate().toAnoMesDiaString() +"  "+ expected.get(0).getArrivalTime().toStringHHMM());
        expectedInStrings.add(expected.get(1).getAdministrationDate().toAnoMesDiaString() +"  "+ expected.get(1).getArrivalTime().toStringHHMM());
        expectedInStrings.add(expected.get(2).getAdministrationDate().toAnoMesDiaString() +"  "+ expected.get(2).getArrivalTime().toStringHHMM());
        expectedInStrings.add(expected.get(3).getAdministrationDate().toAnoMesDiaString() +"  "+ expected.get(3).getArrivalTime().toStringHHMM());
        expectedInStrings.add(expected.get(4).getAdministrationDate().toAnoMesDiaString() +"  "+ expected.get(4).getArrivalTime().toStringHHMM());
        expectedInStrings.add(expected.get(5).getAdministrationDate().toAnoMesDiaString() +"  "+ expected.get(5).getArrivalTime().toStringHHMM());

        List<VaccineAdministration> real = new ArrayList<>();
        real.add(vacAdmin6);
        real.add(vacAdmin1);
        real.add(vacAdmin2);
        real.add(vacAdmin3);
        real.add(vacAdmin4);
        real.add(vacAdmin5);

        SortByDateAndTime.quickSortArrivalTime(real, 0, real.size()-1);

        List<String> realInStrings = new ArrayList<>();
        realInStrings.add(real.get(0).getAdministrationDate().toAnoMesDiaString() +"  "+ real.get(0).getArrivalTime().toStringHHMM());
        realInStrings.add(real.get(1).getAdministrationDate().toAnoMesDiaString() +"  "+ real.get(1).getArrivalTime().toStringHHMM());
        realInStrings.add(real.get(2).getAdministrationDate().toAnoMesDiaString() +"  "+ real.get(2).getArrivalTime().toStringHHMM());
        realInStrings.add(real.get(3).getAdministrationDate().toAnoMesDiaString() +"  "+ real.get(3).getArrivalTime().toStringHHMM());
        realInStrings.add(real.get(4).getAdministrationDate().toAnoMesDiaString() +"  "+ real.get(4).getArrivalTime().toStringHHMM());
        realInStrings.add(real.get(5).getAdministrationDate().toAnoMesDiaString() +"  "+ real.get(5).getArrivalTime().toStringHHMM());



        //assertEquals(expected, real);
        assertEquals(expectedInStrings, realInStrings);
    }

    @Test
    void partitionArrivalTime() {
        VaccineAdministration vacAdmin1 = new VaccineAdministration(123456789, "Help",
                1, "21345-22", new DateUtils(2022,05,15),
                new TimeUtils(10, 15), new TimeUtils(10, 10),
                new TimeUtils(10, 20), new TimeUtils(11, 0));

        VaccineAdministration vacAdmin2 = new VaccineAdministration(123456789, "Help",
                1, "21345-22", new DateUtils(2022,05,15),
                new TimeUtils(10, 30), new TimeUtils(10, 25),
                new TimeUtils(10, 40), new TimeUtils(11, 10));

        VaccineAdministration vacAdmin3 = new VaccineAdministration(123456789, "Help",
                1, "21345-22", new DateUtils(2022,05,15),
                new TimeUtils(10, 00), new TimeUtils(10, 25),
                new TimeUtils(10, 40), new TimeUtils(11, 15));

        VaccineAdministration vacAdmin4 = new VaccineAdministration(123456789, "Help",
                1, "21345-22", new DateUtils(2022,05,16),
                new TimeUtils(10, 00), new TimeUtils(10, 25),
                new TimeUtils(10, 40), new TimeUtils(11, 15));

        VaccineAdministration vacAdmin5 = new VaccineAdministration(123456789, "Help",
                1, "21345-22", new DateUtils(2022,05,10),
                new TimeUtils(10, 00), new TimeUtils(10, 25),
                new TimeUtils(10, 40), new TimeUtils(11, 15));

        VaccineAdministration vacAdmin6 = new VaccineAdministration(123456789, "Help",
                1, "21345-22", new DateUtils(2022,05,17),
                new TimeUtils(10, 00), new TimeUtils(10, 25),
                new TimeUtils(10, 40), new TimeUtils(11, 15));

        List<VaccineAdministration> expected = new ArrayList<>();
        expected.add(vacAdmin5);
        expected.add(vacAdmin1);
        expected.add(vacAdmin2);
        expected.add(vacAdmin3);
        expected.add(vacAdmin4);
        expected.add(vacAdmin6);

        List<String> expectedInStrings = new ArrayList<>();
        expectedInStrings.add(expected.get(0).getAdministrationDate().toAnoMesDiaString() +"  "+ expected.get(0).getLeavingTime().toStringHHMM());
        expectedInStrings.add(expected.get(1).getAdministrationDate().toAnoMesDiaString() +"  "+ expected.get(1).getLeavingTime().toStringHHMM());
        expectedInStrings.add(expected.get(2).getAdministrationDate().toAnoMesDiaString() +"  "+ expected.get(2).getLeavingTime().toStringHHMM());
        expectedInStrings.add(expected.get(3).getAdministrationDate().toAnoMesDiaString() +"  "+ expected.get(3).getLeavingTime().toStringHHMM());
        expectedInStrings.add(expected.get(4).getAdministrationDate().toAnoMesDiaString() +"  "+ expected.get(4).getLeavingTime().toStringHHMM());
        expectedInStrings.add(expected.get(5).getAdministrationDate().toAnoMesDiaString() +"  "+ expected.get(5).getLeavingTime().toStringHHMM());

        List<VaccineAdministration> real = new ArrayList<>();
        real.add(vacAdmin3);
        real.add(vacAdmin6);
        real.add(vacAdmin4);
        real.add(vacAdmin5);
        real.add(vacAdmin2);
        real.add(vacAdmin1);

        SortByDateAndTime.quickSortLeavingTime(real, 0, (real.size()-1));


        List<String> realInStrings = new ArrayList<>();
        realInStrings.add(real.get(0).getAdministrationDate().toAnoMesDiaString() +"  "+ real.get(0).getLeavingTime().toStringHHMM());
        realInStrings.add(real.get(1).getAdministrationDate().toAnoMesDiaString() +"  "+ real.get(1).getLeavingTime().toStringHHMM());
        realInStrings.add(real.get(2).getAdministrationDate().toAnoMesDiaString() +"  "+ real.get(2).getLeavingTime().toStringHHMM());
        realInStrings.add(real.get(3).getAdministrationDate().toAnoMesDiaString() +"  "+ real.get(3).getLeavingTime().toStringHHMM());
        realInStrings.add(real.get(4).getAdministrationDate().toAnoMesDiaString() +"  "+ real.get(4).getLeavingTime().toStringHHMM());
        realInStrings.add(real.get(5).getAdministrationDate().toAnoMesDiaString() +"  "+ real.get(5).getLeavingTime().toStringHHMM());

        // assertEquals(expected, real);
        assertEquals(expectedInStrings, realInStrings);
    }
}