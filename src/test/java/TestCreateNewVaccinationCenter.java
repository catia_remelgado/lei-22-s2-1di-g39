import app.domain.model.VaccinationCenter;
import org.junit.jupiter.api.Test;

public class TestCreateNewVaccinationCenter {

    VaccinationCenter vc = new VaccinationCenter();

    /**
     * Tests for the name
     */
    @Test
    public void testEmptyName() {
        String name = "";
        vc.setName(name);
    }

    @Test
    public void moreThan20CharacterName() {
        String name = "12345678901234567890";
        vc.setName(name);
    }

    @Test
    public void lessThan10CharacterName() {
        String name = "1234567890";
        vc.setName(name);
    }

    /**
     * Tests for the type
     */
    @Test
    public void emptyType() {
        String type = "";
        vc.setType(type);
    }

    @Test
    public void moreThan30() {
        String type = "123456789012345678901234567890";
        vc.setType(type);
    }

    @Test
    public void lessThan16() {
        String type = "1234567890123456";
        vc.setType(type);
    }

    /**
     * Tests for the address
     */
    /**
    @Test
    public void emptyAddress() {
        String address = "";
        vc.setAddress(address);
    }

    @Test
    public void lessThan4Character() {
        String address = "1234";
        vc.setAddress(address);
    }

    @Test
    public void moreThan40Character() {
        String address = "1234567890123456789012345678901234567890";
        vc.setAddress(address);
    }
*/
    /**
     * Tests for the phone
     */
    @Test
    public void emptyPhone() {
        int phone = 0;
        vc.setPhone(phone);
    }

    @Test
    public void lessThan9Digits() {
        int phone = 123456789;
        vc.setPhone(phone);
    }

    @Test
    public void moreThan9Digits() {
        int phone = 123456789;
        vc.setPhone(phone);
    }

    /**
     * Tests for the email
     */
    @Test
    public void emptyEmail() {
        String email = "";
        vc.setEmail(email);
    }

    @Test
    public void lessThan11Characters() {
        String email = "12345678901";
        vc.setEmail(email);
    }

    /**
     * Tests for the Fax number
     */
    @Test
    public void emptyFax() {
        int faxNumber = 0;
        vc.setFaxNumber(faxNumber);
    }

    @Test
    public void lessThan10Digits() {
        int faxNumber = 1234567890;
        vc.setFaxNumber(faxNumber);
    }

    @Test
    public void moreThan10Digits() {
        int faxNumber = 1234567890;
        vc.setFaxNumber(faxNumber);
    }

    /**
     * Tests for the website
     */
    @Test
    public void emptyWebsite() {
        String website = "";
        vc.setWebsite(website);
    }

    /**
     * open hour
     */
    @Test
    public void emptyOpenHour(){
        String openHour = "";
        vc.setOpenHour(openHour);
    }
    /**
     * close hour
     */
    @Test
    public void emptyCloseHour(){
        String closeHour = "";
        vc.setCloseHour(closeHour);
    }

    @Test
    public void vaccinesPerSlot(){
       int vaccinesPerSlot = 0;
       vc.setVaccinePerSlot(vaccinesPerSlot);
    }
}
