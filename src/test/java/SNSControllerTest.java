import app.controller.App;
import app.controller.SNSController;

import app.domain.model.Gender;
import app.domain.utils.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;


class SNSControllerTest {


    @Test
    public void SNSUserCreation() {

        SNSController controller = new SNSController();
        Date birthdate = new Date(01-01-2001);

        App.getInstance().getCompany().getSnsUserStore().addNewSNSUser(111111111, "João", DateUtils.dateToDateUtils(birthdate), "joao@gmail.com", 987654321, Gender.MALE, 12345678, "Porto");

    }

    @Test
    public void SNSUserCreationWithInvalidEmail() {

        SNSController controller = new SNSController();
        Date birthdate = new Date(01-01-2001);

        try {
            App.getInstance().getCompany().getSnsUserStore().addNewSNSUser(111111111, "João", DateUtils.dateToDateUtils(birthdate), "joaogmail.com", 987654321, Gender.MALE, 12345678, "Porto");
        } catch (Exception exception) {
            Assertions.assertEquals(exception.getMessage(), "Warning: Invalid Email Address (joaogmail.com)");
        }
    }

    @Test
    public void SNSUserCreationWithInvalidSNSnumber() {

        SNSController controller = new SNSController();
        Date birthdate = new Date(01-01-2001);

        try {
            App.getInstance().getCompany().getSnsUserStore().addNewSNSUser(1111111111, "João", DateUtils.dateToDateUtils(birthdate), "joao@gmail.com", 987654321, Gender.MALE, 12345678, "Porto");
        } catch (Exception exception) {
            Assertions.assertEquals(exception.getMessage(), "Invalid SNS number.");
        }
    }

    @Test
    public void SNSUserCreationWithNameBlank() {

        SNSController controller = new SNSController();
        Date birthdate = new Date(01-01-2001);

        try {
            App.getInstance().getCompany().getSnsUserStore().addNewSNSUser(111111111, " ", DateUtils.dateToDateUtils(birthdate), "joao@gmail.com", 987654321, Gender.MALE, 12345678, "Porto");
        } catch (Exception exception) {
            Assertions.assertEquals(exception.getMessage(), "Name is null.");
        }
    }

    @Test
    public void SNSUsersCreationWithTheSameSNSNumber() {

        SNSController controller = new SNSController();
        Date birthdate = new Date(01-01-2001);

        try {
            App.getInstance().getCompany().getSnsUserStore().addNewSNSUser(111111111, "João", DateUtils.dateToDateUtils(birthdate), "joao@gmail.com", 987654321, Gender.MALE, 12345678, "Porto");
            App.getInstance().getCompany().getSnsUserStore().addNewSNSUser(111111111, "Diogo", DateUtils.dateToDateUtils(birthdate), "diogo@gmail.com", 987654320, Gender.MALE, 12345677, "Porto");
        } catch (Exception exception) {
            Assertions.assertEquals(exception.getMessage(), "Warning: User 111111111 already exists.");
        }
    }

    @Test
    public void SNSUsersCreationWithTheSameEmail() {

        SNSController controller = new SNSController();
        Date birthdate = new Date(01-01-2001);

        try {
            App.getInstance().getCompany().getSnsUserStore().addNewSNSUser(111111111, "João", DateUtils.dateToDateUtils(birthdate), "joao@gmail.com", 987654321, Gender.MALE, 12345678, "Porto");
            App.getInstance().getCompany().getSnsUserStore().addNewSNSUser(111111112, "Diogo", DateUtils.dateToDateUtils(birthdate), "joao@gmail.com", 987654320, Gender.MALE, 12345677, "Porto");
        } catch (Exception exception) {
            Assertions.assertEquals(exception.getMessage(), "Warning: User joao@gmail.com already exists.");
        }
    }
    @Test
    public void SNSUsersCreationWithTheCCnumber() {

        SNSController controller = new SNSController();
        Date birthdate = new Date(01-01-2001);

        try {
            App.getInstance().getCompany().getSnsUserStore().addNewSNSUser(111111111, "João", DateUtils.dateToDateUtils(birthdate), "joao@gmail.com", 987654321, Gender.MALE, 12345678, "Porto");
            App.getInstance().getCompany().getSnsUserStore().addNewSNSUser(111111112, "Diogo", DateUtils.dateToDateUtils(birthdate), "diogo@gmail.com", 987654320, Gender.MALE, 12345678, "Porto");
        } catch (Exception exception) {
            Assertions.assertEquals(exception.getMessage(), "Warning: User 12345678 already exists.");
        }
    }
}