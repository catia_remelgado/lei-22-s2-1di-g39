import app.controller.App;
import app.controller.AppointmentController;
import app.controller.VaccineTypeController;
import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineType;
import app.domain.store.VaccineTypeStore;
import app.domain.utils.DateUtils;
import app.domain.utils.TimeUtils;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AppointmentControllerTest {

    @Test
    void seeIfDateIsCorrect() {
        String date = "12/12/2002";
        String separator = "-";

        boolean first = new AppointmentController().seeIfDateIsCorrect(date, separator);

        String date2 = "12-14-2002";
        boolean second = new AppointmentController().seeIfDateIsCorrect(date2, separator);

        String date3 = "12-12-2022";
        boolean third = new AppointmentController().seeIfDateIsCorrect(date3, separator);

        String date4 = "12-12-2000";
        boolean fourth = new AppointmentController().seeIfDateIsCorrect(date4, separator);

        assertEquals(false, first);
        assertEquals(false, second);
        assertEquals(true, third);
        assertEquals(false, fourth);
    }

    @Test
    void setDate() {
        String separator1 = "-";
        String separator2 = "/";

        String date1 = "12/12/2002";
        DateUtils expected1 = new DateUtils(2002, 12, 12);
        DateUtils real1 = new AppointmentController().setDate(date1,separator2);

        String date2 = "12-12-2002";
        DateUtils expected2 = new DateUtils(2002, 12, 12);
        DateUtils real2 = new AppointmentController().setDate(date2,separator1);

        assertEquals(expected1, real1);
        assertEquals(expected2, real2);

    }

    @Test
    void setTime() {
        String stringTime1 = "12:00";
        TimeUtils expected1 = new TimeUtils(12, 00, 00);
        TimeUtils real1 = new AppointmentController().setTime(stringTime1);

        assertEquals(expected1, real1);
    }

    @Test
    void vaccineTypeStoreExists() {
        ArrayList<VaccineType> vaccineTypeStore1= new VaccineTypeController().getInstance().getStore().getVaccineTypeList();
        int expectedsize1 = 0;
        boolean exp1 = new AppointmentController().vaccineTypeStoreExists();
        assertEquals(expectedsize1, vaccineTypeStore1.size());
        assertEquals(false, exp1);

    }

    @Test
    void vaccinationCentersStoreExists() {
        List<VaccinationCenter> allVC = App.getInstance().getCompany().getVaccinationCenterStore().getAllVaccinationCenters();
        int expectedsize1 = 0;
        boolean exp1 = new AppointmentController().vaccineTypeStoreExists();
        assertEquals(expectedsize1, allVC.size());
        assertEquals(false, exp1);
    }

    @Test
    void verifySNSUserNumberFormat() {
        int firstN = 123;
        int secondN = 123456789;

        boolean first = new AppointmentController().verifySNSUserNumberFormat(firstN);
        boolean second = new AppointmentController().verifySNSUserNumberFormat(secondN);

        assertEquals(false, first);
        assertEquals(true, second);
    }

    @Test
    void seeIfTimeIsCorrect() {
        String time1 = "12:12:00";
        String time2 = "12:12";

        boolean real1 = new AppointmentController().seeIfTimeIsCorrect(time1);
        boolean real2 = new AppointmentController().seeIfTimeIsCorrect(time2);

        assertEquals(false, real1);
        assertEquals(true, real2);
    }
}