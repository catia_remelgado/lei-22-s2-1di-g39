import app.controller.CreateVaccineController;
import app.domain.model.DoseAdministration;
import app.domain.model.VaccineAdministrationProcess;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestDoseAdministration {

    /**
     * tests to see  when dosage is null, the controller function returns the adequate value
     */
    @Test
    public void TestDoseAdministrationDosageZero() {
        //Setting variables
        CreateVaccineController controller = new CreateVaccineController();
        VaccineAdministrationProcess vap = new VaccineAdministrationProcess(2, 14, 27);
        Object da;
        boolean expectedRes = false;
        da = controller.createDoseAdministration(1, 0, 30, 30, vap);

        //Asserting data
        Assertions.assertEquals(expectedRes, da);
    }

    /**
     * tests to see when TimePreviousToCurrentDose is less than zero, the controller function returns the adequate value
     */
    @Test
    public void TestDoseAdministrationTimePreviousToCurrentDoseLessThanZero() {
        //Setting variables
        CreateVaccineController controller = new CreateVaccineController();
        VaccineAdministrationProcess vap = new VaccineAdministrationProcess(2, 14, 27);
        Object da;
        boolean expectedRes = false;
        da = controller.createDoseAdministration(1, 30, -2, 30, vap);

        //Asserting data
        Assertions.assertEquals(expectedRes, da);
    }

    /**
     * tests to see when RecoveryTime is less than zero, the controller function returns the adequate value
     */
    @Test
    public void TestDoseAdministrationRecoveryTimeLessThanZero() {
        //Setting variables
        CreateVaccineController controller = new CreateVaccineController();
        VaccineAdministrationProcess vap = new VaccineAdministrationProcess(2, 14, 27);
        Object da;
        boolean expectedRes = false;
        da = controller.createDoseAdministration(1, 30, 30, -3, vap);

        //Asserting data
        Assertions.assertEquals(expectedRes, da);
    }

    /**
     * tests to see if the dose administration tha is created is valid, if the controller function returns an adequate value
     */
    @Test
    public void TestDoseAdministrationRegular() {

        //Setting variables
        CreateVaccineController controller = new CreateVaccineController();
        VaccineAdministrationProcess vap = new VaccineAdministrationProcess(2, 14, 27);
        DoseAdministration da;
        DoseAdministration expectedRes = new DoseAdministration(1, 30.0f, 30, 30);
        da = (DoseAdministration) controller.createDoseAdministration(1, 30.0f, 30, 30, vap);

        //Asserting data
        Assertions.assertEquals(expectedRes.getDoseNumber(), da.getDoseNumber());
        Assertions.assertEquals(expectedRes.getDosage(), da.getDosage());
        Assertions.assertEquals(expectedRes.getTimePreviousToCurrentDose(), da.getTimePreviousToCurrentDose());
        Assertions.assertEquals(expectedRes.getRecoveryTime(), da.getRecoveryTime());
    }

    /**
     * test that the dose administration cannot be set with null values
     */
    @Test
    public void ensureNullIsNotAllowed(){
        Assertions.assertThrows(NullPointerException.class, () -> {
            DoseAdministration da = new DoseAdministration(null,null,null,null);
        });
    }
}
