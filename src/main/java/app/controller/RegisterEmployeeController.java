package app.controller;

import app.domain.model.Employee;
import app.domain.model.EmployeeRolesList;
import app.domain.store.EmployeeStore;
import app.ui.console.utils.Utils;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.util.ArrayList;

public class RegisterEmployeeController {

    private EmployeeStore es = App.getInstance().getCompany().getEmployeeStore();

   /* public RegisterEmployeeController() {
        this.es = new EmployeeStore();
    }*/
/*
    public boolean verifyIfIsUnique(Employee emp) {
        if (es.getEmployeeStore() != null) {
            return es.seeIfEmployeeIsUnique(emp);
        }
        return true;
    }
*/

    public boolean verifyIfItsNewCCNumber(int CCNumber){
        for (int i = 0; i < es.getEmployeeStore().size(); i++ ){
            if (es.getEmployeeStore().get(i).getCCNumber()==CCNumber){
                return false;
            }
        }
        return true;
    }

    /**
     * adds employee to the store
     * @param role role of the new employee
     * @param name name of the new employee
     * @param adress adress of the new employee
     * @param email email of the new employee
     * @param phoneNumber phone number of the new employee
     * @param CCNumber citizen card number of the new employee
     */
    public Employee addEmployee(EmployeeRolesList role, String name, String adress, Email email, int phoneNumber, int CCNumber) {
        try {
            return App.getInstance().getCompany().getEmployeeStore().addNewEmployee(role, name, adress, email, phoneNumber, CCNumber);
        } catch (NullPointerException e) {
            System.out.println("Null pointer exception we can't handle this :(");
        }

        return null;

    }


    //due to some changes, this method it's not used anymore
    /**
     * method to verify if a given phone number has nine digits (portuguese format)
     *
     * @param number phone number given
     * @return valid phone number
     */
   /* public boolean seeIfValidNumber(int number) {
        boolean valid = false;
        while (number < 100000000 || number > 999999999) {
            System.out.println("\nInvalid number, it must have 9 digit.");
            number = Utils.readIntegerFromConsole("Please insert a valid number:");
            valid = true;
        }
        return valid;
    }
*/

    /**
     * method to verify if a given string is valid (not null or empty)
     *
     * @param string     given string
     * @param wantedData what the given string represents
     * @return valid string
     */
    public String seeIfStringIsValid(String string, String wantedData) {
        while (verifyIfStringItsNotEmptyOrNull(string)) {
            System.out.println("The " + wantedData + " can't be blank.");
            string = Utils.readLineFromConsole("Please insert " + wantedData + ":");
        }
        return string;
    }


    /**
     * method to verify if a given string is valid (not null or empty)
     *
     * @param string string to be verified
     * @return true if string is null or empty
     */

    public boolean verifyIfStringItsNotEmptyOrNull(String string) {
        if (string.equals("")) {
            return true;
        } else if (string.equals(null)) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * This ensures that is created only one object of Register Employee Controller
     * https://sourcemaking.com/design_patterns/singleton
     * Extracted from https://www.javaworld.com/article/2073352/core-java/core-java-simply-singleton.html?page=2
     */
    private static RegisterEmployeeController singleton = null;

    public static RegisterEmployeeController getInstance() {
        if (singleton == null) {
            synchronized (RegisterEmployeeController.class) {
                singleton = new RegisterEmployeeController();
            }
        }
        return singleton;
    }


    /**
     * @return list with all registered employees
     */
    public ArrayList<Employee> getArrayWithAllEmployees() {
        return this.es.getEmployeeStore();
    }


    /**
     * Method to verify if it's possible to convert the given string email to an Email object
     *
     * @param mailString given email in string format
     * @return is it's possible or not to convert email string to Email (true = possible, false = impossible)
     */
    public boolean seeIfItsPossibleToConvertStringToEmail(String mailString) {
        boolean succes = false;
        try {
            Email email = new Email(mailString);
            succes = true;
        } catch (IllegalArgumentException e) {
            succes = false;
        }
        return succes;
    }


    /**
     * method to verify and handle the exception to see if the number input it's actually an integer
     * @param number variable that represents the number
     * @return an integer
     */
    public int seeIfItsANumber(int number, String prompt) {
        boolean succes = false;
        do {
            try {
                number = Utils.readIntegerFromConsole(prompt);
                succes = true;
            } catch (IllegalArgumentException e) {
                System.out.println("Invalid, please insert just one INTEGER. Try again.");
                succes = false;
            }
        } while (succes == false);

        return number;
    }

}


