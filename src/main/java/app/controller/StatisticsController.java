package app.controller;

import app.controller.App;
import app.domain.model.*;
import app.domain.store.SNSUserStore;
import app.domain.store.VaccinationCenterStore;
import app.domain.store.VaccineStore;
import app.domain.utils.DateUtils;

import java.util.Date;
import java.util.List;

/**
 * Represents the vaccine center controller.
 *
 */
public class StatisticsController {

    /**
     * Represents the company.
     */
    private Company company;

    /**
     * Represents the vaccine admnistration store.
     */
    private VaccineAdministration vaccineAdministration;

    /**
     * Represents the vaccine.
     */
    private VaccinationCenterStore vaccinationCenterStore;

    /**
     * Represents the vaccine store.
     */
    private VaccineStore vaccineStore;

    /**
     * Represents the SNS user store.
     */
    private SNSUserStore snsUserStore;

    /**
     * Represents the vaccination statistics.
     */
    private Statistics statistics;

    /**
     * Represents the vaccination center.
     */
    private VaccinationCenter vaccinationCenter;

    /**
     * Builds an instance of VaccinationStatisticsController.
     */
    public StatisticsController(int vcId) {
        this(App.getInstance().getCompany());
        this.vaccinationCenterStore = company.getVaccinationCenterStore();
        vaccinationCenter = vaccinationCenterStore.getVaccinationCenter(vcId);
    }

    /**
     * Builds an instance of vaccinationStatisticsController
     *
     * @param company represents the company.
     */
    public StatisticsController(Company company) {
        this.company = company;
        this.statistics = null;
    }

    /**
     * This function gets a list of the fully vaccinated people in an interval.
     *
     * @param date1 the first date in the interval.
     * @param date2 the second date in the interval.
     * @return a list of integers.
     */
    public List<Integer> totalNumberFullyVaccinatedInInterval(DateUtils date1, DateUtils date2) {
        this.snsUserStore = this.company.getSnsUserStore();
        List<SNSUser> suList = this.snsUserStore.getSnsUserList();

        this.vaccineStore = this.company.getVaccineStore();
        List<Vaccine> vList = this.vaccineStore.getVaccineList();

        this.vaccineAdministration = this.company.getVaccineAdministration();
        return this.vaccineAdministration.totalNumberFullyVaccinatedInInterval(suList, vList, date1, date2, vaccinationCenter);
    }
}
