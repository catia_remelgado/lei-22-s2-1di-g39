package app.controller;

import app.domain.dto.ArrivalDTO;
import app.domain.model.VaccinationCenter;
import app.domain.model.WaitingRoom;
import app.domain.dto.WaitingRoomDTO;
import app.domain.mapper.WaitingRoomMapper;
import app.ui.console.EmployeeSession;

import java.util.ArrayList;
import java.util.List;

/**
 * controls the ConsultWaitingRoomUI
 * @author alexandraipatova
 */
public class ConsultWaitingRoomController {

    /**
     * returns waiting room dto with necessary information
     * @param vc vaccination center that contains our wanted waiting room list
     * @return waiting room dto that contains necessary information
     */
    public List<ArrivalDTO> getWaitingRoom(VaccinationCenter vc){
        WaitingRoom wr=vc.getWaitingRoom();
        WaitingRoomMapper waitingRoomMapper=new WaitingRoomMapper();
        return waitingRoomMapper.waitingRoomToDTO(wr);
    }

    /**
     * returns the employee session vaccination center
     * @return employee session vaccination center
     */
    public VaccinationCenter getSessionVaccinationCenter(){
        return EmployeeSession.getVaccinationCenter();
    }
}
