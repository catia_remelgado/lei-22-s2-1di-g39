package app.controller;

import app.domain.exceptions.UserAlreadyExistsException;
import app.domain.exceptions.UserUnknownException;
import app.domain.model.*;
import app.domain.store.VaccineStore;
import app.domain.utils.DateUtils;
import app.domain.utils.TimeUtils;
import app.ui.console.EmployeeSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author João Miguel
 */
public class RecordVaccineAdministrationController {

    /**
     * The Vaccine Store object.
     */
    private VaccineStore vaccineStore;

    /**
     * Get the Vaccine Store
     * @return returns the Vaccine Store
     */
    public VaccineStore getVaccineStore() {
        return this.vaccineStore;
    }

    /**
     *  Method to get the Waiting Room
     * @return returns the center Waiting Room
     */
    public WaitingRoom getWaitingRoom(){
        VaccinationCenter center = EmployeeSession.getVaccinationCenter();
        return center.getWaitingRoom();
    }

    /**
     *  Method to get the Recovery Room
     * @return returns the center Recovery Room
     */
    public RecoveryRoom getRecoveryRoom(){
        VaccinationCenter center = EmployeeSession.getVaccinationCenter();
        return center.getRecoveryRoom();
    }

    /**
     *  Method to get the Users in the Waiting Room
     * @return returns one HashMap with snsNumbers and users
     */
    public HashMap<Integer,SNSUser> getSNSUsersWaiting() {
        WaitingRoom waitingRoom = this.getWaitingRoom();
        HashMap<Integer,SNSUser> snsUserList = new HashMap<>();

        for (Arrival arrival : waitingRoom.getArrivalList()) {
            try {
                int n = arrival.getSNSNumber();;
                snsUserList.put(n, App.getInstance().getCompany().getSnsUserStore().get(n));
            } catch (UserUnknownException exception) {

            }
        }

        return snsUserList;
    }

    /**
     * Method to get the Vaccination List of an User
     * @param user One SNS User
     * @return returns the Vaccination List of an User
     */
    public List<Vaccine> getVaccinationList(SNSUser user) {
        List<Vaccine> result = new ArrayList<>();

        // Check if it is the first vac.
        List<VaccineShot> takenVaccines = user.getTakenVaccines();


        int listSize = takenVaccines.size();

        if (listSize > 0) {
            Vaccine vaccine = takenVaccines.get(0).getVaccine();

            List<VaccineAdministrationProcess> processes = vaccine.getVaccineAdministrationProcess();

            boolean addToResult = false;

            for (VaccineAdministrationProcess process : processes) {
                if (process.validateAdministration(user.getAge(), listSize, DateUtils.currentDate().difference(takenVaccines.get(listSize-1).getDate()))) {
                    addToResult = true;
                    break;
                }
            }

            if (addToResult) {
                result.add(vaccine);
            }
        }

        if (takenVaccines.size() == 0) {

            List<Vaccine> vaccineList = App.getInstance().getCompany().getVaccineStore().getVaccineList();

            for (Vaccine vaccine : vaccineList) {

                List<VaccineAdministrationProcess> processes = vaccine.getVaccineAdministrationProcess();

                boolean addToResult = false;


                for (VaccineAdministrationProcess process : processes) {
                    if (process.validateAdministration(user.getAge(), 1, 0)) {
                        addToResult = true;
                        break;
                    }
                }

                if (addToResult) {
                    result.add(vaccine);
                }
            }
        }

        return result;
    }


    /**
     * Method to add a SNS User to the Recovery Room
     * @param user The SNS User
     */
    public void AddRecoveryRoom(SNSUser user) throws UserAlreadyExistsException {

        RecoveryRoom recoveryRoom = this.getRecoveryRoom();

        recoveryRoom.add(user);

    }

    /**
     * Method to give a VaccineShot to a user and add to the VaccineAdministration
     * @param user The SNS User
     * @param vaccine The Vaccine administered
     * @param lotNumber The Vaccine Lot number
     */
    public void VaccineShot(SNSUser user, Vaccine vaccine,String lotNumber){

        WaitingRoom waitingRoom = this.getWaitingRoom();
        Arrival arrival = waitingRoom.remove(user);

        VaccineShot shot = new VaccineShot(vaccine, DateUtils.currentDate());

        user.getTakenVaccines().add(shot);

        int snsNumber = user.getSnsNumber();
        String vaccineName = vaccine.getName();
        int doseNumber = user.getTakenVaccines().size();
        DateUtils administrationDate = shot.getDate();
        TimeUtils arrivalTime = arrival.getArrivalTime();
        TimeUtils scheduledTime = arrival.getScheduledTime();
        TimeUtils administrationTime=TimeUtils.currentTime();
        TimeUtils leavingTime = TimeUtils.currentTime().addTime(new TimeUtils(0, vaccine.getVaccineAdministrationProcess().get(0).getDoseAdministration(doseNumber).getRecoveryTime()));


        VaccineAdministration vaccineAdministration = new VaccineAdministration(snsNumber, vaccineName, doseNumber, lotNumber, administrationDate, arrivalTime, scheduledTime, administrationTime, leavingTime);

        EmployeeSession.getVaccinationCenter().addObjectToVAList(vaccineAdministration);

    }

    /**
     * Method to check if the Lot number is correct
     * @param lotNumber The vaccine Lot number
     * @return returns the true (if the Lot number is in a valid format) or false otherwise
     */
    public boolean verifyIflotNumberIsCorrect(String lotNumber) {

        // ^[a-z,A-Z,0-9]{5}-\d{2}$

        char[] array = lotNumber.toCharArray();

        if (array.length != 8) {
            return false;
        }

        for (int i = 0; i < 5; i++) {
            if (!isAlpha(array[i]) && !isDigit(array[i])) {
                return false;
            }
        }

        if (array[5] != '-') {
            return false;
        }

        for (int i = 6; i <  8; i++) {
            if (!isDigit(array[i])) {
                return false;
            }
        }

        return true;
    }
    /**
     * Method to see if a character is an alphanumeric character
     * @param c The character
     * @return Return true (if the character is a alphanumeric character) or false otherwise
     */
    private boolean isAlpha(char c) {
        return c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z';
    }

    /**
     * Method to see if a character is a digit
     * @param c The character
     * @return Return true (if the character is a digit) or false otherwise
     */
    private boolean isDigit(char c) {
        return c >= '0' && c <= '9';
    }

    /**
     * Method to remove a SNS User from the Center
     * @param snsUser The SNS User
     */
    public void leaveCenter(SNSUser snsUser) {
        RecoveryRoom recoveryRoom = this.getRecoveryRoom();
        Recovery recovery = recoveryRoom.remove(snsUser);

    }

}
