package app.controller;


import app.domain.model.VaccineType;
import app.domain.store.VaccineTypeStore;

/**
 * @author João Miguel
 */

public class VaccineTypeController {


    /**
     * The Vaccine Type Store object.
     */
    private VaccineTypeStore store = App.getInstance().getCompany().getVaccineTypeStore();

    /**
     * Vaccine Type Controller's empty constructor.
     */

    public VaccineTypeController() {
        this.store = new VaccineTypeStore();
    }

    /**
     * Get the Vaccine Type Store
     * @return returns the Vaccine Type Store
     */
    public VaccineTypeStore getStore() {
        return this.store;
    }

    /**
     * Add a new Vaccine Type (adding it to the vaccine type store)
     * @param code The Vaccine Type's code
     * @param designation The Vaccine Type's designation
     * @return return the addition of the user to the store
     */
    public boolean addVaccineType(String code, String designation) {
        return this.store.addVaccineType(new VaccineType(code, designation));
    }

    /**
     * Vaccine Type Controller singleton.
     *
     * Extracted from https://www.javaworld.com/article/2073352/core-java/core-java-simply-singleton.html?page=2
     */
    private static VaccineTypeController singleton = null;
    public static VaccineTypeController getInstance()
    {
        if(singleton == null)
        {
            synchronized(VaccineTypeController.class)
            {
                singleton = new VaccineTypeController();
            }
        }
        return singleton;
    }




}
