package app.controller;

import app.domain.model.Company;
import app.domain.model.VaccinationCenter;
import pt.isep.lei.esoft.auth.AuthFacade;
import pt.isep.lei.esoft.auth.UserSession;
import pt.isep.lei.esoft.auth.domain.model.User;
import pt.isep.lei.esoft.auth.mappers.dto.UserDTO;
import pt.isep.lei.esoft.auth.mappers.dto.UserRoleDTO;

import java.util.List;
import java.util.Optional;

/**
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class AuthController {

    private App app;

    public AuthController() {
        this.app = App.getInstance();
    }

    public boolean doLogin(String email, String pwd) {
        try {
            return this.app.doLogin(email, pwd);
        } catch (IllegalArgumentException ex) {
            return false;
        }
    }

    public List<UserRoleDTO> getUserRoles() {
        if (this.app.getCurrentUserSession().isLoggedIn()) {
            return this.app.getCurrentUserSession().getUserRoles();
        }
        return null;
    }

    public void doLogout() {
        this.app.doLogout();
    }

    public boolean setUserVaccinationCenter(VaccinationCenter center) {

        if (this.app.getCurrentUserSession().isLoggedIn()) {
            UserSession session = this.app.getCurrentUserSession();
            Company company = this.app.getCompany();
            Optional<UserDTO> optional = company.getAuthFacade().getUser(session.getUserId().toString());

            if (optional.isPresent()) {
                company.setUserVaccinationCenter(optional.get(), center);
                return true;
            }
        }

        return false;
    }

    public VaccinationCenter getUserVaccinationCenter() {
        if (this.app.getCurrentUserSession().isLoggedIn()) {
            UserSession session = this.app.getCurrentUserSession();
            Company company = this.app.getCompany();
            Optional<UserDTO> optional = company.getAuthFacade().getUser(session.getUserId().getEmail());

            VaccinationCenter aux = company.getUserVaccinationCenter(optional.get());

            return aux;
        }

        return null;
    }

    public boolean userHasVaccinationCenter() {
        return this.getUserVaccinationCenter() != null;
    }




}
