package app.controller;


import app.domain.store.SNSUserStore;

/**
 * @author João Miguel
 */
public class SNSController {


    /**
     * SNS Controller's empty constructor.
     */
    public SNSController()
    {

    }

    /**
     * SNS Controller singleton.
     *
     * Extracted from https://www.javaworld.com/article/2073352/core-java/core-java-simply-singleton.html?page=2
     */
    private static SNSController singleton = null;
    public static SNSController getInstance()
    {
        if(singleton == null)
        {
            synchronized(SNSController.class)
            {
                singleton = new SNSController();
            }
        }
        return singleton;
    }


}
