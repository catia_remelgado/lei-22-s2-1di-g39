package app.controller;

import app.domain.model.LoadFromFileCSV;
import app.domain.utils.FileHelper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class LoadUsersFromFileController {

    /**
     * redirects to the FileHelper class to save a given file to the application
     * @param path path of file
     * @param newFileName new file name in application for the file
     * @return file that was saved in the application
     * @throws IOException occurs when the file is not saved
     */
    public File saveFile(String path, String newFileName) throws IOException {
        return FileHelper.saveFile(path,newFileName);

    }

    public void loadUsersFromFile(File file) throws FileNotFoundException {
        LoadFromFileCSV csvLoader=new LoadFromFileCSV();
        csvLoader.insertUsersFromFileData(csvLoader.loadFromFile(file));
    }



}
