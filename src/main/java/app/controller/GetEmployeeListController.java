package app.controller;

import app.domain.model.Employee;
import app.domain.model.EmployeeRolesList;
import app.domain.store.EmployeeStore;
import app.ui.console.utils.Utils;

import java.util.ArrayList;

public class GetEmployeeListController {

    /**
     * List of all roles available in the system
     */
    private ArrayList<EmployeeRolesList> allRoles = new EmployeeStore().getEmployeeRolesList();

    /**
     * Shows all roles available in the system
     */
    public int showRolesSelectionList() {
        int options = 0;
        options = Utils.showAndSelectIndex(allRoles, "Select a role to filter:");
        return options;
    }

    /**
     * To make the list just with the employees with a specific role
     * @param option represents the position of the wanted role in the Enum list of roles
     * @return list with the employees of a certain role
     */
    public ArrayList<Employee> makeListOfEmployeesWithWantedRole(int option){
        EmployeeRolesList wantedRole = new EmployeeStore().convertOptionToRole(option);
        //ArrayList<Employee> allEmployees = RegisterEmployeeController.getInstance().getArrayWithAllEmployees();
        ArrayList<Employee> allEmployees = App.getInstance().getCompany().getEmployeeStore().getEmployeeStore();
        ArrayList<Employee> employeesWithWantedRole = getListOfEmployeesByRole(wantedRole, allEmployees);

        return employeesWithWantedRole;
    }

    /**
     * method to filter employees by wanted role
     *
     * @param role role choosen by the admin to see all the employees with that role
     * @return array list with all the employees with the chossen role
     */
    public ArrayList<Employee> getListOfEmployeesByRole(EmployeeRolesList role, ArrayList<Employee> allEmployees) {
        ArrayList<Employee> employeesByRole = new ArrayList<Employee>();

        for (int i = 0; i < allEmployees.size(); i++) {
            if ((allEmployees.get(i)).getRole() == role) {
                employeesByRole.add(allEmployees.get(i));
            }
        }
        return employeesByRole;

    }

    /**
     * This ensures that is created only one object of this controller
     * https://sourcemaking.com/design_patterns/singleton
     * Extracted from https://www.javaworld.com/article/2073352/core-java/core-java-simply-singleton.html?page=2
     */
    private static GetEmployeeListController singleton = null;

    public static GetEmployeeListController getInstance() {
        if (singleton == null) {
            synchronized (GetEmployeeListController.class) {
                singleton = new GetEmployeeListController();
            }
        }
        return singleton;
    }
}
