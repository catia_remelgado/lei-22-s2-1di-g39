package app.controller;

import app.domain.model.Vaccine;
import app.domain.store.VaccineStore;
import app.domain.model.VaccineType;
import app.domain.model.VaccineAdministrationProcess;
import app.domain.model.DoseAdministration;

/**
 * @author alexandraipatova
 */
public class CreateVaccineController {

    /**
     * The vaccine store used by the company
     */
    private VaccineStore vs=App.getInstance().getCompany().getVaccineStore();

    /**
     *Any vaccine that is created
     */
    private Vaccine vc;

    public VaccineStore getVaccineStore() {
        return this.vs;
    }


    /**
     * creates a vaccine to add to the vaccine list
     * @param name name of the vaccine
     * @param brand brand of the vaccine
     * @param vaccineType vaccine type of the vaccine
     * @return vaccine object created
     */
    public Object createVaccine(int ID,String name, String brand,String technology, VaccineType vaccineType) {

        this.vc=vs.createVaccine(ID,name,brand,technology,vaccineType);
        if(vs.validateVaccineLocally(vc)){
            return vc;
        }
        return false;

    }

    /**
     *the creator method for a vaccine administration process
     * @param minimumAge minimum age of an age group
     * @param maximumAge maximum age of an age group
     * @param numberOfDoses number of doses of the vaccine administration process
     * @param vc created vaccine
     * @return VaccineAdministrationProcess that was created
     */
    public Object createVaccineAdministrationProcess(int minimumAge, int maximumAge, int numberOfDoses,Vaccine vc) {
        VaccineAdministrationProcess vap=vc.createVaccineAdministrationProcess(minimumAge,maximumAge,numberOfDoses);
        if(vc.validateVaccineAdministrationProcessLocally(vap)){
            return vap;
        }
        return false;

    }

    /**
     * creates a dose administration of the vaccine administration process
     * @param doseNumber the number of the dose (e.g. 1st/2nd)
     * @param dosage quantity of vaccine solution in ml of this dose administration
     * @param timePreviousToCurrentDose time spent in days from the previous dose to the current dose of this dose administration
     * @param recoveryTime time in minutes to recover from the dose administration and to watch for potential adverse reactions
     * @param vap creted vaccine administration process
     * @return returns a true or false value depending if the dose administration was successfully created
     */
    public Object createDoseAdministration(int doseNumber, float dosage, int timePreviousToCurrentDose, int recoveryTime,VaccineAdministrationProcess vap) {
        DoseAdministration da = vap.createDoseAdministration(doseNumber,dosage,timePreviousToCurrentDose,recoveryTime);
        if(vap.validateDoseAdministrationLocally(da)){
            return da;
        }
        return false;
    }

    /**
     * saving a vaccine (adding it to the vaccine store)
     * @param vc the vaccine that will be saved
     * @return true if the vaccine was saved sucessfully to the store
     */
    public boolean saveVaccine(Vaccine vc){

        return (this.vs).saveVaccine(vc);

    }

    /**
     * method to get the unique instance of the controller
     * How to navigate the deceptively simple Singleton pattern | InfoWorld. (n.d.). Retrieved May 29, 2022, from https://www.infoworld.com/article/2073352/core-java-simply-singleton.html?page=2
     */
    private static CreateVaccineController singleton = null;
    public static CreateVaccineController getInstance() {
        if(singleton == null) {
            synchronized(CreateVaccineController.class) {
                singleton = new CreateVaccineController();
            }
        }
        return singleton;
    }

}
