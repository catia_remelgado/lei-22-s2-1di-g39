package app.controller;

import app.domain.exceptions.NotAWholeDivisionException;
import app.domain.model.CenterPerformance;
import app.domain.model.VaccinationCenter;
import app.domain.utils.DateUtils;

import java.io.IOException;

public class EvaluateCenterPerformanceController {

    /**
     * get the vaccination center of the currently logged in center coordinator
     * @return vaccination center of the currently logged in center coordinator
     */
    public VaccinationCenter getVaccinationCenterOfCoordinator(){
        return App.getInstance().getCompany().getVaccinationCenterStore().getVaccinationCenterOfCoordinator();
    }

    /**
     * redirects to the method of verification of time interval
     * @param vc vaccination center to evaluate
     * @param m time interval in which to analyze performance
     */
    public void verifyTimeIntervalForPerformance(VaccinationCenter vc, int m) throws NotAWholeDivisionException {
        vc.verifyTimeIntervalForPerformance(m);
    }

    /**
     * redirects to the method of evaluation of center performance
     * @param vc vaccination center to evaluate
     * @param day day which will be analyzed
     * @param m time interval in which to analyze performance
     * @return center performance object with all data
     */
    public CenterPerformance evaluateCenterPerformance(VaccinationCenter vc, DateUtils day, int m) throws IOException {
        return vc.evaluateCenterPerformance(day,m);
    }
}
