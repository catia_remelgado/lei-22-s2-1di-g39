package app.controller;

import app.domain.dto.VaccineAdministrationDTO;
import app.domain.mapper.VaccineAdministrationMapper;
import app.domain.model.SNSUser;
import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineAdministration;
import app.domain.store.SNSUserStore;
import app.domain.store.VaccineStore;
import app.domain.utils.GetPropertyValues;
import app.domain.utils.SortByDateAndTime;
import app.domain.utils.TreatLegacySystemData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ImportLegacyV2Controller {



    /**
     * Method to get the vaccination center of the currently logged in center coordinator
     * @return vaccination center of the currently logged in center coordinator
     */
    public VaccinationCenter getVaccinationCenterOfCoordinator(){
        return App.getInstance().getCompany().getVaccinationCenterStore().getVaccinationCenterOfCoordinator();
    }

    /**
     * Method to treat the list of list of the csv file contents
     * @param parsableCSVFileContents list of lists of the csv file contents that has parsable data to dto
     * @param vaccinationCenter the vaccination center of the center coordinator
     * @param typeOfSort the type of sort that the center coordinator wants (arrival or leaving time)
     * @return all treated information necessary to show to the center coordinator
     * @throws IOException
     */
    public List<List<String>> treatRawData(List<List<String>> parsableCSVFileContents, VaccinationCenter vaccinationCenter, String typeOfSort) throws IOException {
        List<List<String>> treatToMakeDTO = treatToMakeDTO(parsableCSVFileContents);
        List<VaccineAdministrationDTO> infoInDTO = csvFileContentsToDTO(treatToMakeDTO);
        List<VaccineAdministration> dtoToObject = toObject(infoInDTO);
        List<VaccineAdministration> withValidUsers = searchValidUsers(dtoToObject);
        List<VaccineAdministration> withValidLotNumber = verifyIfLotNumbersAreInTheCorrectFormat(withValidUsers);
        List<VaccineAdministration> withValidVaccine = verifyIfVaccineNameExists(withValidLotNumber);
        addObjectsThatDontExist(withValidVaccine, vaccinationCenter);
        sortList(withValidVaccine, typeOfSort);
        List<VaccineAdministrationDTO> dataToUI= vaccineAdministrationsToDTO(withValidVaccine);
        List<List<String>> toTableView = new ArrayList<>();
        getLastData(toTableView, dataToUI);
        return toTableView;
    }

    /**
     * Method treat the csv file contents to get only wanted atributes to do the dto
     * @param parsableCSVFileContents list of lists of the parsable lines of the csv file to do the dto
     * @return treated list of list to do the dto
     */
    public List<List<String>> treatToMakeDTO(List<List<String>> parsableCSVFileContents){
        return new TreatLegacySystemData().treatToMakeDTOOfVaccineAdministration(parsableCSVFileContents);
    }

    /**
     * Method to crete a list of vaccine administration dto (from the previous treated data)
     * @param readyToBeDTO valid list of list to transform to dto
     * @return returns a list that is a dto
     */
    public List<VaccineAdministrationDTO> csvFileContentsToDTO(List<List<String>> readyToBeDTO) {
        List<VaccineAdministrationDTO> infoInDTO = new VaccineAdministrationMapper().transformListOfListsToDTO(readyToBeDTO);
        return infoInDTO;
    }

    /**
     * Method to transform dto information into an object
     * @param infoInDTO dto list to transform to object
     * @return list of objects
     */
    public List<VaccineAdministration> toObject(List<VaccineAdministrationDTO> infoInDTO) {
        List<VaccineAdministration> objects = new VaccineAdministrationMapper().transformListOfDTOInVaccineAdmin(infoInDTO);
        return objects;
    }

    /**
     * Method to verify if all data passed to object has valid sns numbers and if has sns user that are in the system
     * @param objects list of objects
     * @return list with valid users
     */
    public List<VaccineAdministration> searchValidUsers(List<VaccineAdministration> objects) {
        List<VaccineAdministration> withValidUsers = new ArrayList<>();
        for (VaccineAdministration vaccineAdministration : objects) {
            if (SNSUser.verifyIfSNSUserNumberIsCorrect(vaccineAdministration.getSnsNumber()) == true) {
                    if (App.getInstance().getCompany().getSnsUserStore().confirmIfUserExists(vaccineAdministration.getSnsNumber())) {
                    withValidUsers.add(vaccineAdministration);
                }
            }
        }
        return withValidUsers;
    }

    /**
     * Method to verify if a lot number is correct
     * @param objects list of objects
     * @return list with valid lot numbers
     */
    public List<VaccineAdministration> verifyIfLotNumbersAreInTheCorrectFormat(List<VaccineAdministration> objects) {
        List<VaccineAdministration> withValidLotNumber = new ArrayList<>();
        for (VaccineAdministration vaccineAdministration : objects) {
            if (VaccineAdministration.confirmLotNumber(vaccineAdministration.getLotNumber()) == true) {
                withValidLotNumber.add(vaccineAdministration);
            }
        }
        return withValidLotNumber;
    }

    /**
     * Method to verify if a vaccine name in each object of the objects list exists
     * @param objects list of objects
     * @return list with valid lot numbers
     */
    public List<VaccineAdministration> verifyIfVaccineNameExists(List<VaccineAdministration> objects) {
        List<VaccineAdministration> withValidVaccineName = new ArrayList<>();
        for (VaccineAdministration vaccineAdministration : objects) {
            if (App.getInstance().getCompany().getVaccineStore().vaccineIsRegistered(vaccineAdministration.getVaccineName()) == true) {
                withValidVaccineName.add(vaccineAdministration);
            }
        }
        return withValidVaccineName;
    }

    /**
     * Method to add to the vaccine administration list of the vaccination center objects that are new to the list
     * @param objects list of valid vaccine administrations
     * @param vaccinationCenter the vaccination center of the administrations
     */
    public void addObjectsThatDontExist(List<VaccineAdministration> objects, VaccinationCenter vaccinationCenter) {
        for (int i = 0; i < objects.size(); i++) {
            vaccinationCenter.verifyRepeatedObjects(objects.get(i));
        }
    }

    /**
     * Method to transform vaccine administrations into dto vaccine administrations
     * @param toBeDTO list of objects to turn into dto
     * @return list of dto
     */
    public List<VaccineAdministrationDTO> vaccineAdministrationsToDTO(List<VaccineAdministration> toBeDTO) {
        List<VaccineAdministrationDTO> infoInDTO = new VaccineAdministrationMapper().transformListOfVaccineAdministrationsToDTO(toBeDTO);
        return infoInDTO;
    }

    /**
     * Method to sort a vaccine administration list
     * @param finalDataFromFile list of objects to be sorted
     * @param typeOfSort type of sort chosed in the combo box (arrival or leaving time)
     * @throws IOException
     */
    public void sortList(List<VaccineAdministration> finalDataFromFile, String typeOfSort) throws IOException {
        GetPropertyValues getProps = new GetPropertyValues();
        String algorithm = getProps.getPropValues("Sort");

        long startTime = System.currentTimeMillis();

       if (algorithm.equalsIgnoreCase("Bubble sort")) {
            SortByDateAndTime.bubbleSort(finalDataFromFile, typeOfSort);
        } else if (algorithm.equalsIgnoreCase("Quick sort")) {
            SortByDateAndTime.quickSort(finalDataFromFile, typeOfSort);
        }
       long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println(elapsedTime + " milliseconds");
        System.out.println("This sort " + algorithm + " had this execution time in seconds: "+(double) elapsedTime / 1_000+" s");
    }


    /**
     * Method to get last data to show to the center coordinator
     * @param toTableView information to show to the table view
     * @param dtos list of dto to show
     */
    public void getLastData( List<List<String>> toTableView, List<VaccineAdministrationDTO> dtos){
        for (VaccineAdministrationDTO dto : dtos){
            toTableView.add(addList(dto));
        }
    }

    /**
     * Method parse the dto attributes to string and get the last two needed attributes to show to the center coordinator
     * @param dto dto to parse to string
     * @return line to add to the table view list of lists
     */
    public List<String> addList(VaccineAdministrationDTO dto){
        SNSUserStore uStore = App.getInstance().getCompany().getSnsUserStore();
        VaccineStore vacStore = App.getInstance().getCompany().getVaccineStore();
        List<String> toAdd = new ArrayList<>();
        toAdd.add(uStore.getSNSUSerNameBySNSNumber(dto.getSnsNumber()));
        toAdd.add(String.valueOf(dto.getSnsNumber()));
        toAdd.add(dto.getVaccineName());
        toAdd.add(vacStore.getVaccineDesignationByVaccineName(dto.getVaccineName()));
        toAdd.add(String.valueOf(dto.getDoseNumber()));
        toAdd.add(dto.getLotNumber());
        toAdd.add(dto.getAdministrationDate().toDiaMesAnoString());
        toAdd.add(dto.getScheduledTime().toStringHHMM());
        toAdd.add(dto.getArrivalTime().toStringHHMM());
        toAdd.add(dto.getAdministrationTime().toStringHHMM());
        toAdd.add(dto.getLeavingTime().toStringHHMM());
        return toAdd;
    }



    /**
     * Legacy System Controller singleton.
     *
     * Extracted from https://www.javaworld.com/article/2073352/core-java/core-java-simply-singleton.html?page=2
     */
    private static ImportLegacyV2Controller singleton = null;
    public static ImportLegacyV2Controller getInstance()
    {
        if(singleton == null)
        {
            synchronized(ImportLegacyV2Controller.class)
            {
                singleton = new ImportLegacyV2Controller();
            }
        }
        return singleton;
    }
}
