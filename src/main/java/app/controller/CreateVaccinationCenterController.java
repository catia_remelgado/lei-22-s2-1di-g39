package app.controller;

import app.domain.model.VaccinationCenter;
import app.domain.model.WaitingRoom;
import app.domain.store.VaccinationCenterStore;

/**
 * @author catiaRemelgado
 */
public class CreateVaccinationCenterController {


    public CreateVaccinationCenterController(App app) { this.app = app;
    }

    public static boolean createVaccinationCenter(String type, String name, String street, int doorNumber, String ZIPcode, String city, int phone, String email,
                                                  int faxNumber, String website, String openHour, String closeHour,
                                                  int slotDuration, int vaccinePerSlot, int capacity) {

        if (!validateData(type, name,  street,  doorNumber,  ZIPcode,  city, phone, email, faxNumber, website, openHour
                , closeHour, slotDuration, vaccinePerSlot, capacity)) {
            return false;
        }

        VaccinationCenter vc = new VaccinationCenter(type, name,  street,  doorNumber,  ZIPcode,  city, phone, email, faxNumber, website, openHour
                , closeHour, slotDuration, vaccinePerSlot, capacity);

        VaccinationCenterStore store = App.getInstance().getCompany().getVaccinationCenterStore();

        return store.addVaccinationCenter(vc);
    }

    private static boolean validateData(String type, String name, String street, int doorNumber, String ZIPcode, String city, int phone, String email, int faxNumber, String website, String openHour
            , String closeHour, int slotDuration, int vaccinePerSlot, int capacity) {
        return true;
    }

    private VaccinationCenterStore store;
    private App app;

    public boolean validatePhoneNumber(int phone) {
        store = app.getCompany().getVaccinationCenterStore();
        return store.validatePhoneNumber(phone);
    }

    public boolean validateFaxNumber(int faxNumber) {
        store = app.getCompany().getVaccinationCenterStore();
        return store.validateFaxNumber(faxNumber);
    }

    public boolean validateOpenAndCloseHour(String openHour, String closeHour){
        store = app.getCompany().getVaccinationCenterStore();
        return  store.validateTime(openHour , closeHour);
    }

    public boolean validateZIPCode(String zipCode){
        store = app.getCompany().getVaccinationCenterStore();
        return store.validateZIPCode(zipCode);
    }

    public boolean validateStreet(String street){
        store = app.getCompany().getVaccinationCenterStore();
        return store.validateStreet(street);
    }
    public boolean validateCity(String city){
        store = app.getCompany().getVaccinationCenterStore();
        return store.validateCity(city);
    }

    public boolean validateEmail(String email) {
        store = app.getCompany().getVaccinationCenterStore();
        return store.validateEmail(email);
    }

    public boolean validateWebsite(String website) {
        store = app.getCompany().getVaccinationCenterStore();
        return store.validateWebsite(website);
    }

    public boolean validateName(String name) {
        store = app.getCompany().getVaccinationCenterStore();
        return store.validateName(name);
    }
}
