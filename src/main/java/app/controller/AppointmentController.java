package app.controller;

import app.domain.exceptions.AppointmentNotFoundException;
import app.domain.model.*;
import app.domain.store.AppointmentStore;
import app.domain.utils.DateUtils;
import app.domain.utils.TimeUtils;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.text.ParseException;
import java.util.ArrayList;
import java.time.ZoneId;
import java.util.List;

/**
 * @author João Miguel
 */
public class AppointmentController {

    /**
     * The Appointment Store object.
     */
    private AppointmentStore appointmentStore;
            //= App.getInstance().getCompany().getAppointmentStore();

    /**
     * Appointment Controller's empty constructor.
     */
    public AppointmentController()
    {
        this.appointmentStore = new AppointmentStore();
    }

    public static boolean hasAppointment(int snsNumber, DateUtils date, TimeUtils time) {
        AppointmentStore appointmentStore = App.getInstance().getCompany().getAppointmentStore();

        ZoneId defaultZoneId = ZoneId.systemDefault();

        return appointmentStore.hasAppointment(snsNumber, date, time);
    }

    public static Appointment get(SNSUser user, DateUtils date, TimeUtils time) throws AppointmentNotFoundException
    {

        return App.getInstance().getCompany().getAppointmentStore().get(user, date, time);

    }


    /**
     * Get the Appointment Store
     * @return Returns the Appointment Store
     */
    public AppointmentStore getStore() {
        return this.appointmentStore;
    }

    /**
     * Appointment Controller singleton.
     *
     * Extracted from https://www.javaworld.com/article/2073352/core-java/core-java-simply-singleton.html?page=2
     * How to navigate the deceptively simple Singleton pattern | InfoWorld. (n.d.). Retrieved May 29, 2022, from https://www.infoworld.com/article/2073352/core-java-simply-singleton.html?page=2
     */
    private static AppointmentController singleton = null;
    public static AppointmentController getInstance()
    {
        if(singleton == null)
        {
            synchronized(AppointmentController.class)
            {
                singleton = new AppointmentController();
            }
        }
        return singleton;
    }





    //ANNA

    /**
     * To confirm if date is in correct format
     * @param dateString date provided by the user
     * @return true if user wrote the date correctly
     */
    public boolean seeIfDateIsCorrect(String dateString, String separator){
        boolean isCorrect= false;

        isCorrect = DateUtils.seeIfItsPossibleToConvertStringToDateUtils(dateString, separator, 3);

        return isCorrect;
    }


    /**
     * Sets date provided by the user as DateUtils
     * @param dateString date provided by the user
     * @return dateString in object DateUtils
     */
    public DateUtils setDate(String dateString, String separator){
        DateUtils date = DateUtils.setDate(dateString,separator);
        return date;
    }


    /**
     * Sets time provided by the user as TimeUtils
     * @param timeString time provided by the user
     * @return timeString in object TimeUtils
     */
    public TimeUtils setTime(String timeString){
        TimeUtils time = TimeUtils.setTime(timeString);
        return time;
    }


    /**
     * MethoD to confirm if there are registered vaccine types in the system
     * @return true if there is at least one vaccine in the system
     */
    public boolean vaccineTypeStoreExists(){
        boolean exists = false;

        ArrayList<VaccineType> vaccineTypeStore = App.getInstance().getCompany().getVaccineTypeStore().getVaccineTypeList();
        if (vaccineTypeStore.size() != 0) {
            exists = true;
        }

        return exists;
    }

    /**
     * MethoD to confirm if there are registered vaccine types in the system
     * @return true if there is at least one vaccine in the system
     */
    public boolean vaccinationCentersStoreExists(){
        boolean exists = false;

        List<VaccinationCenter> allVC = App.getInstance().getCompany().getVaccinationCenterStore().getAllVaccinationCenters();
        if (allVC.size() != 0) {
            exists = true;
        }

        return exists;
    }

    /**
     * Method to verify if SNS number given by the user has the correct structure
     * @param SNSNumber SNS number given by the user
     * @return true if it's in correct format
     */
    public boolean verifySNSUserNumberFormat(int SNSNumber){
        return SNSUser.verifyIfSNSUserNumberIsCorrect(SNSNumber);
    }

    public boolean verifyIfSNSUserNumberExists(int snsNumber){
        return  App.getInstance().getCompany().getSnsUserStore().confirmIfUserExists(snsNumber);
    }

    /**
     * Method to see if the given SNS given number matches with the SNS number associated with the email
     * @param email that is logged in
     * @param snsUserNumber SNS number given by the user
     * @return true if associated SNS number associated to the email matches the given SNS number
     */
    public boolean searchSNSUserNumber(Email email, int snsUserNumber){
        return App.getInstance().getCompany().getSnsUserStore().confirmSNSUserNumberByEmail(email, snsUserNumber);
    }

    /**
     * Method to verify if it will be the first time that the user will take this vaccine type
     * @param vaccineType choosed vaccine type to be taken by the user
     * @param snsNumber user's sns number
     * @return false if it's not the first time that this vaccine type is scheduled
     */
    public boolean confirmIfItsFirstSchedule(VaccineType vaccineType, int snsNumber){
        return  App.getInstance().getCompany().getAppointmentStore().confirmIfItsNotRepeatedVaccineType(vaccineType, snsNumber);
        //return true;
    }


    /**
     * verifies if the wanted time by the user is available
     * @param time time that the user wants
     * @param vaccinationCenter vaccination center chosed by the user
     * @return true if typed time is between the working hour of the center
     * @throws ParseException
     */
    public boolean verifyIfTimeIsInsideTheWorkingSchedule(TimeUtils time, VaccinationCenter vaccinationCenter) throws ParseException {
        return vaccinationCenter.verifyIfTimeIsInsideWorkingSchedule(time);
    }

    /**
     * verify if it's possible to schedule a vaccine to the user in the desired vaccination center, day and time
     * @param vaccinationCenter
     * @param date
     * @param time
     * @return
     * @throws ParseException
     */
    public boolean verifyIfItsPossibleToSchedule(VaccinationCenter vaccinationCenter, DateUtils date, TimeUtils time) throws ParseException {
        TimeUtils openingHours = new TimeUtils(TimeUtils.setTime(vaccinationCenter.getOpenHour()));
        TimeUtils closingHours = new TimeUtils(TimeUtils.setTime(vaccinationCenter.getCloseHour()));
         return  App.getInstance().getCompany().getAppointmentStore().verifyIfItsPossibleToSchedule(vaccinationCenter, date, time);
    }

    /**
     * Method to add an appointment to the appointment store
     * @param snsUserNumber the sns number (person) associated to the appointment
     * @param vaccineType the vaccine type that will be taken
     * @param vaccinationCenter the vaccination center where will occure the vaccination
     * @param date date for which the vaccine was scheduled
     * @param time time for which the vaccine was scheduled
     */
    public void scheduleAppointment(int snsUserNumber, VaccineType vaccineType, VaccinationCenter vaccinationCenter, DateUtils date, TimeUtils time){
        App.getInstance().getCompany().getAppointmentStore().addAppointmentToAppointmentStore(snsUserNumber, vaccineType, vaccinationCenter, date, time);
    }

    /**
     * Method to confirm if a givven date it's not in the past
     * @param date inputed date by the user
     * @return false if date it's past (false if it's not today's date or it's not future)
     */
    public boolean confirmIfDateItsNotInThePast(DateUtils date){
        return DateUtils.confirmIfDateItsNotPast(date);
    }

    public boolean confirmIfDateItsNotTooInTheFuture(DateUtils date){
        return DateUtils.confirmIfDateItsTooInTheFuture(date);
    }

    /**
     * To confirm if time is in correct format
     * @param timeString time provided by the user
     * @return true if user wrote the time correctly
     */
    public boolean seeIfTimeIsCorrect(String timeString){
        boolean isCorrect= false;

        isCorrect = TimeUtils.seeIfItsPossibleToConvertStringToTimeUtils(timeString, ":", 2);
        if (isCorrect == false) {
            System.out.println("\nThis time was incorrectly written, type again.");
        }

        return isCorrect;
    }

    public Appointment verifyIfTheresAnAppointment(SNSUser user, DateUtils date, TimeUtils time) throws AppointmentNotFoundException {
       return  App.getInstance().getCompany().getAppointmentStore().verifyIfThereIsASchedule(user,  date,  time);
    }
}
