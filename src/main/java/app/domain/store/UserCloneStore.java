package app.domain.store;

import app.domain.model.UserClone;
import app.domain.model.Vaccine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UserCloneStore implements Serializable {

    private List<UserClone> userCloneStore;
    long serialVersionUID;

    public UserCloneStore() {
        this.userCloneStore = new ArrayList<>();
    }

    public List<UserClone> getUserCloneList() {
        return userCloneStore;
    }

    /**
     * adds vaccine to the vaccine store
     * @param uc user clone to be added
     * @return true if sucessfully added
     */
    public boolean addUser(UserClone uc){
            return userCloneStore.add(uc);
    }
}
