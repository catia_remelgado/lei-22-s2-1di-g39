package app.domain.store;

import app.controller.App;
import app.domain.model.Employee;
import app.domain.model.EmployeeRolesList;
import app.domain.shared.Constants;
import app.ui.console.utils.Utils;
import pt.isep.lei.esoft.auth.AuthFacade;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * class that represents all the employees
 */
public class EmployeeStore implements Serializable {

    /**
     * has all the employees registered
     */
    private ArrayList<Employee> allEmployeesList;

    public EmployeeStore() {
        this.allEmployeesList = new ArrayList<Employee>();
    }

    /**
     * @return all employees in the store
     */
    public ArrayList<Employee> getEmployeeStore() {
        return allEmployeesList;
    }


    /**
     * Sees if an employee to be registered has the same ccnumber of some employee that's already registered
     * @param emp employee that might be registered
     * @return true if the citizen card number it's not registered in the system
     */
    public boolean seeIfEmployeeIsUnique(Employee emp) {
        for (Employee emp2: allEmployeesList){
            if (emp2.getCCNumber()==emp.getCCNumber()){
                return false;
            }
        }
        return true;
    }


    /**
     * @return array with all available roles for employees
     */
    public ArrayList<EmployeeRolesList> getEmployeeRolesList() {
        ArrayList<EmployeeRolesList> allRoles = new ArrayList<EmployeeRolesList>(EnumSet.allOf(EmployeeRolesList.class));
        return allRoles;
    }



    /**
     * @param option number of the position of the role choosen by the admin
     * @return role in the position of wanted option
     */
    public EmployeeRolesList convertOptionToRole(int option) {
        return getEmployeeRolesList().get(option);
    }


    /**
     * Adds the employee to the store
     * @param role
     * @param name
     * @param adress
     * @param email
     * @param phoneNumber
     * @param CCNumber
     */
    public Employee addNewEmployee(EmployeeRolesList role, String name, String adress, Email email, int phoneNumber, int CCNumber){
        Employee emp=new Employee(role, name, adress, email, phoneNumber, CCNumber);
        this.allEmployeesList.add(emp);

        AuthFacade authFacade = App.getInstance().getCompany().getAuthFacade();

        if (role.equals(EmployeeRolesList.NURSE)) {
            authFacade.addUserWithRole(name, email.toString(), "123456", Constants.ROLE_NURSE);
        } else if (role.equals(EmployeeRolesList.CENTER_COORDINATOR)) {
            authFacade.addUserWithRole(name, email.toString(), "123456", Constants.ROLE_CENTER_COORDINATOR);
        } else if (role.equals(EmployeeRolesList.RECEPTIONIST)) {
            authFacade.addUserWithRole(name, email.toString(), "123456", Constants.ROLE_RECEPTIONIST);
        }

        return emp;


    }













    //Never used
    /**
     * registers employee to add to the employee list
     *
     * @param role        role of the employee
     * @param name        name of the employee
     * @param adress      adress of the employee
     * @param email       email of the employee
     * @param phoneNumber employee's phone number
     * @return employee registered
     */
   /* public Employee registerEmployee(EmployeeRolesList role, String name, String adress, Email email, int phoneNumber, int CCNumber) {
        Employee emp = new Employee(role, name, adress, email, phoneNumber, CCNumber);
        return emp;
    }*/
}
