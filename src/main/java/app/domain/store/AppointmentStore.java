package app.domain.store;


import app.domain.model.Appointment;
import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineType;
import app.domain.exceptions.AppointmentNotFoundException;
import app.domain.model.SNSUser;
import app.domain.utils.DateUtils;
import app.domain.utils.TimeUtils;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author João Miguel
 */
public class AppointmentStore implements Serializable {

    /**
     * represents the various Appointments
     */
    private List<Appointment> appointmentList;

    /**
     * Appointment empty constructor.
     */
    public AppointmentStore() {
        this.appointmentList = new ArrayList<>();
    }

    /**
     * Checks if the appointment already exists in the system or not.
     *
     * @param snsNumber The SNS number of the user
     * @param date      The Appointment Date
     * @return Returns true or false whether the appointment exists or not
     */

    public boolean hasAppointment(int snsNumber, DateUtils date, TimeUtils time) {

        for (Appointment appointment : this.appointmentList) {

            if (appointment.getSnsNumber() == snsNumber && appointment.getDate().equals(date) && appointment.getTime() == time) {
                return true;
            }

        }

        return false;
    }

    /**
     * Get the Appointment.
     *
     * @param user The SNS User
     * @param date The Date of the appointment
     * @param time The Appointment Time
     * @return returns the appointment.
     * @throws AppointmentNotFoundException
     */


    public Appointment get(SNSUser user, DateUtils date, TimeUtils time) throws AppointmentNotFoundException {
        //for (Appointment appointment : AppointmentController.getInstance().getStore().appointmentList) {
        for (Appointment appointment : this.appointmentList) {
            if ((appointment.getTime().toString()).equalsIgnoreCase(time.toString()) && (appointment.getDate().toString()).equalsIgnoreCase(date.toString()) && (appointment.getSnsNumber()) == user.getSnsNumber()) {
                return appointment;
            }
        }
        throw new AppointmentNotFoundException("Appointment to " + user.getSnsNumber() + " at " + date + " and " + time + " not found.");
    }
    //ANNA


    public Appointment verifyIfThereIsASchedule(SNSUser user, DateUtils date, TimeUtils time) throws AppointmentNotFoundException {
        List<Appointment> appointmentsByDay = doForLoopByDayString(date, this.appointmentList);
        List<Appointment> appointmentsByTime = doForLoopByTimeString(time, appointmentsByDay);
        List<Appointment> appointmentsBySNSUser = doForLoopBySNSUser(user.getSnsNumber(), appointmentsByTime);

        if (appointmentsBySNSUser.size() == 1) {
            return appointmentsBySNSUser.get(0);
        }
        throw new AppointmentNotFoundException("Appointment to " + user.getSnsNumber() + " at " + date + " and " + time + " not found.");
    }


    /**
     * Method to get the appointment store without the vaccination center and vaccine type associated
     *
     * @param appointments list of appointments with three atributes
     * @return appointments lis with the sns number, date and time
     */
    public List<Appointment> getJustThreeAtributes(List<Appointment> appointments) {
        List<Appointment> appointmentsCopy = new ArrayList<Appointment>();
        for (int i = 0; i < appointments.size(); i++) {
            appointmentsCopy.add(new Appointment(appointments.get(i).getSnsNumber(), appointments.get(i).getDate(), appointments.get(i).getTime()));
        }
        return appointmentsCopy;
    }

    /**
     * Method to verify if it will be the first time that the user will take this vaccine type
     *
     * @param vaccineType chosed vaccine type to be taken by the user
     * @param snsNumber   user's sns number
     * @return false if it's not the first time that this vaccine type is scheduled
     */
    public boolean confirmIfItsNotRepeatedVaccineType(VaccineType vaccineType, int snsNumber) {
        int i = 0;
        boolean found = false;
        //VaccineType vaccineType1= new VaccineType("404", "Not_Found");
        VaccineType vaccineType1;

        if (appointmentList.size() != 0) {
            do {
                if (snsNumber == (appointmentList.get(i).getSnsNumber())) {
                    found = true;
                }
                i++;
            } while (found == false && i < this.appointmentList.size());

            if (found == true) {
                vaccineType1 = this.appointmentList.get(i - 1).getVaccineType();
                if (vaccineType1 == vaccineType) {
                    return false;
                }
            }
        }
        return true;
    }


    /**
     * Method to verify if it's possible to schedule a vaccine to the user in the desired vaccination center, day and time
     *
     * @param vaccinationCenter the vaccination center where the user wants to take the vaccine
     * @param date              the day that the user pretend to take the vaccine
     * @param time              time where the user wants to take the vaccine
     * @return true if it's possible to schedule the vaccine to the desired day
     * int snsUserNumber, VaccineType vaccineType,
     */
    public boolean verifyIfItsPossibleToSchedule(VaccinationCenter vaccinationCenter, DateUtils date, TimeUtils time) throws ParseException {
        boolean possible = false;
        //Appointment appointment = new Appointment(snsUserNumber, vaccineType, vaccinationCenter, DateUtils.dateUtilsToDate(date), time);
        List<Appointment> appointmentListByCenter = doForLoopByVaccinationCenterString(vaccinationCenter, this.appointmentList);
        List<Appointment> appointmentListByVaccinationCenterAndDay = doForLoopByDayString(date, appointmentListByCenter);

        if (appointmentListByVaccinationCenterAndDay.size() == (vaccinationCenter.getVaccinePerSlot())) {
            return false;
        } else {
            return verifyIfTimeOverlapsAnotherAppointment(appointmentListByVaccinationCenterAndDay, time, vaccinationCenter.getSlotDuration(), new TimeUtils(TimeUtils.setTime(vaccinationCenter.getOpenHour())), new TimeUtils(TimeUtils.setTime(vaccinationCenter.getCloseHour())));
        }
    }


    /**
     * Method to a list with appointments by one vaccination center
     *
     * @param criteria        chosen vaccination center to filter the list with appointments
     * @param dataToBeTreated list with the appointments
     * @return list that contains appointments of a specific vaccination center
     */
    public List<Appointment> doForLoopByVaccinationCenter(VaccinationCenter criteria, List<Appointment> dataToBeTreated) {
        List<Appointment> appointmentListByCriteria = new ArrayList<Appointment>();

        for (int i = 0; i < dataToBeTreated.size(); i++) {
            if (dataToBeTreated.get(i).getVaccinationCenter() == criteria) {
                appointmentListByCriteria.add(dataToBeTreated.get(i));
            }
        }
        return appointmentListByCriteria;
    }


    public List<Appointment> doForLoopByVaccinationCenterString(VaccinationCenter criteria, List<Appointment> dataToBeTreated) {
        List<Appointment> appointmentListByCriteria = new ArrayList<Appointment>();

        for (int i = 0; i < dataToBeTreated.size(); i++) {
            if (dataToBeTreated.get(i).getVaccinationCenter().toString() == criteria.toString()) {
                appointmentListByCriteria.add(dataToBeTreated.get(i));
            }
        }
        return appointmentListByCriteria;
    }

    public List<Appointment> doForLoopByTime(TimeUtils criteria, List<Appointment> dataToBeTreated) {
        List<Appointment> appointmentListByCriteria = new ArrayList<Appointment>();

        for (int i = 0; i < dataToBeTreated.size(); i++) {
            if (dataToBeTreated.get(i).getTime() == criteria) {
                appointmentListByCriteria.add(dataToBeTreated.get(i));
            }
        }
        return appointmentListByCriteria;
    }


    public List<Appointment> doForLoopByTimeString(TimeUtils criteria, List<Appointment> dataToBeTreated) {
        List<Appointment> appointmentListByCriteria = new ArrayList<Appointment>();

        for (int i = 0; i < dataToBeTreated.size(); i++) {
            if (dataToBeTreated.get(i).getTime().toString().equalsIgnoreCase(criteria.toString())) {
                appointmentListByCriteria.add(dataToBeTreated.get(i));
            }
        }
        return appointmentListByCriteria;
    }

    /**
     * Method to filter Appointment Store by a specific day
     *
     * @param criteria        choosen day to filter the list with the appointments
     * @param dataToBeTreated list with the appointments
     * @return list that contains appointments scheduled for a specific day
     */
    public List<Appointment> doForLoopByDay(DateUtils criteria, List<Appointment> dataToBeTreated) {
        List<Appointment> appointmentListByCriteria = new ArrayList<Appointment>();

        for (int i = 0; i < dataToBeTreated.size(); i++) {
            if (dataToBeTreated.get(i).getDate() == criteria) {
                appointmentListByCriteria.add(dataToBeTreated.get(i));
            }
        }
        return appointmentListByCriteria;
    }


    public List<Appointment> doForLoopByDayString(DateUtils criteria, List<Appointment> dataToBeTreated) {
        List<Appointment> appointmentListByCriteria = new ArrayList<Appointment>();

        for (int i = 0; i < dataToBeTreated.size(); i++) {
            if (dataToBeTreated.get(i).getDate().toString().equalsIgnoreCase(criteria.toString())) {
                appointmentListByCriteria.add(dataToBeTreated.get(i));
            }
        }
        return appointmentListByCriteria;
    }

    public List<Appointment> doForLoopBySNSUser(int criteria, List<app.domain.model.Appointment> dataToBeTreated) {
        List<app.domain.model.Appointment> appointmentListByCriteria = new ArrayList<app.domain.model.Appointment>();

        for (int i = 0; i < dataToBeTreated.size(); i++) {
            if (dataToBeTreated.get(i).getSnsNumber() == criteria) {
                appointmentListByCriteria.add(dataToBeTreated.get(i));
            }
        }
        return appointmentListByCriteria;
    }

    /**
     * Method to verify if the time that the user is trying to schedule is available (does not overlaps with other schedule because of the slot duration)
     *
     * @param list         list with appointments (from a specific vaccination center and a specific day, in the case for US01) to be verified
     * @param time         time to which user pretends to schedule the vaccine
     * @param slotDuration slot duration of the specific vaccination center
     * @param opening      opening hour of the vaccination center
     * @param closing      closing hour of the vaccination center
     * @return true if it's possible to schedule an appointment for the desired day by the user
     */
   /* public boolean verifyIfTimeOverlapsAnotherAppointment(List<Appointment> list, TimeUtils time, int slotDuration, TimeUtils opening, TimeUtils closing) throws ParseException {
        if (list.size() == 0) {
            return true;
        } else if (list.size() == 1) {
            if (time.isBigger(list.get(0).getTime()) && time.isBiggerOrEquals((list.get(0).getTime().addOrTakeMinutes(list.get(0).getTime().toStringHHMM(), slotDuration)))) {
                return true;
            }
            if ((list.get(0).getTime()).isBigger(time) && ((time.addOrTakeMinutes(time.toStringHHMM(), slotDuration)).isBiggerOrEquals(list.get(0).getTime()))) {
                return true;
            }
        } else if (list.size() >= 2) {
            if ((list.get(0).getTime()).isBigger(time) && ((list.get(0).getTime()).isBiggerOrEquals(time.addOrTakeMinutes(time.toStringHHMM(), slotDuration)))) {
                return true;
            }
            if (time.isBigger(list.get(list.size() - 1).getTime()) && time.isBiggerOrEquals((list.get(list.size() - 1).getTime().addOrTakeMinutes(list.get(list.size() - 1).getTime().toStringHHMM(), slotDuration)))) {
                return true;
            }

            boolean itsPossible = true;
            int i = 0;
            do {
                if ((time.isBiggerOrEquals(list.get(i).getTime())) && (list.get(i + 1).getTime().isBiggerOrEquals(time))) {
                    itsPossible = true;
                } else {
                    itsPossible = false;
                }
                i++;
            } while (itsPossible == true && i < list.size());
            if (itsPossible == false) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    } */
    public boolean verifyIfTimeOverlapsAnotherAppointment(List<Appointment> list, TimeUtils time, int slotDuration, TimeUtils opening, TimeUtils closing) throws ParseException {
      /*  List<Appointment> copy = copyList(list);
        boolean itsPossible = true;
        int i = 0;
        if (copy.size()!=0) {
            do {
                if (time.compareTo(copy.get(i).getTime().addOrTakeMinutes(copy.get(i).getTime().toStringHHMM(), slotDuration)) == 1 || time.compareTo(copy.get(i).getTime()) == -1) {
                    itsPossible = true;
                } else {
                    itsPossible = false;
                }
                i++;
            } while (itsPossible == true && i < copy.size());
        }
        if (itsPossible == false) {
            return false;
        } else {*/
        return true;
    }
/*
        if (copy.size() == 0) {
            return true;
        } else if (copy.size() == 1) {
            if ((time.isBigger(copy.get(0).getTime())) && (time.isBiggerOrEquals((copy.get(0).getTime().addOrTakeMinutes(copy.get(0).getTime().toStringHHMM(), slotDuration))))) {
                return true;
                // return false;
            }
            if (((copy.get(0).getTime()).isBigger(time)) && (((time.addOrTakeMinutes(time.toStringHHMM(), slotDuration)).isBiggerOrEquals(copy.get(0).getTime())))) {
                return true;
                // return false;
            }
        } else if (copy.size() >= 2) {
            if (((copy.get(0).getTime()).isBigger(time)) && (((copy.get(0).getTime()).isBiggerOrEquals(time.addOrTakeMinutes(time.toStringHHMM(), slotDuration))))) {
                return true;
                //return false;
            }
            if ((time.isBigger(copy.get(copy.size() - 1).getTime())) && (time.isBiggerOrEquals((copy.get(copy.size() - 1).getTime().addOrTakeMinutes(copy.get(copy.size() - 1).getTime().toStringHHMM(), slotDuration))))) {
                return true;
                // return false;
            }

            boolean itsPossible = true;
            int i = 0;
            do {
                if (!((time.isBiggerOrEquals(copy.get(i).getTime()))) && !((copy.get(i + 1).getTime().isBiggerOrEquals(time)))) {
                    itsPossible = true;
                } else {
                    itsPossible = false;
                }
                i++;
            } while (itsPossible == true && i < copy.size());
            if (itsPossible == false) {
                return false;
            } else {
                return true;
            }
        }
        return false;*/
    //return true;
//}


    /**
     * Method to copy list (so the time of the original appointment won't be changed)
     * @param toClone List that is we pretend to duplicate
     * @return Copy of the previous array list
     */
    public List<Appointment> copyList(List<Appointment> toClone){
        List<Appointment> temporary = new ArrayList<Appointment>();
        for (int i = 0; i < toClone.size(); i++){
            temporary.add(toClone.get(i));
        }
        return temporary;
    }

    /**
     * Method to add appointment to the appointment store
     *
     * @param snsUserNumber     sns nember of the person to whom was scheduled the vaccine
     * @param vaccineType       vaccine type scheduled to be taken by the user
     * @param vaccinationCenter vaccination center chosen by the user to take the vaccine
     * @param date              date chosed by the user to take the vaccine
     * @param time              time chosed by the user take the vaccine
     */
    public void addAppointmentToAppointmentStore(int snsUserNumber, VaccineType vaccineType, VaccinationCenter vaccinationCenter, DateUtils date, TimeUtils time) {
        Appointment appointment = new Appointment(snsUserNumber, vaccineType, vaccinationCenter, date, time);
        this.appointmentList.add(appointment);
    }


    /**
     * Method to get the appointment store
     * @return appointment store
     */
    public List<Appointment> getAppointmentList() {return this.appointmentList;}
}
