package app.domain.store;

import app.domain.comparator.VaccineIDComparator;
import app.domain.exceptions.VaccineNotFoundException;
import app.domain.model.Vaccine;
import app.domain.model.VaccineType;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * class that represents all the vaccines and administrations available to administer
 * @author alexandraipatova
 */
public class VaccineStore implements Serializable {

    /**
     * represents the several vaccines and administrations owned by the company;
     */
    ArrayList<Vaccine> vaccineList;

    public VaccineStore() {
        this.vaccineList = new ArrayList<>();
    }

    /**
     * returns all vaccines in the store
     *
     * @return all vaccines in the store
     */
    public ArrayList<Vaccine> getVaccineList() {
        return vaccineList;
    }

    /**
     * creates a vaccine to add to the vaccine list
     *
     * @param name        name of the vaccine
     * @param brand       brand of the vaccine
     * @param vaccineType vaccine type of the vaccine
     * @return vaccine object created
     */
    public Vaccine createVaccine(int ID, String name, String brand, String technology, VaccineType vaccineType) {

        Vaccine vc = new Vaccine(ID, name, brand, technology, vaccineType);
        return vc;

    }

    /**
     * verifies and adds a vaccine to the vaccine store
     *
     * @param vc the vaccine to be saved
     * @return true if the vaccine was saved successfully
     */
    public boolean saveVaccine(Vaccine vc) {
        boolean flag = false;
        if (vc.validate()) {
            flag = addVaccine(vc);

        }
        return flag;

    }

    /**
     * validates the vaccine's information
     *
     * @param vc the vaccine to validate
     * @return true if vaccine is valid
     */
    public boolean validateVaccineLocally(Vaccine vc) {
        return vc.validate();
    }

    /**
     * checks for duplicate vaccines
     *
     * @param vc the vaccine to validate
     * @return true if non duplicated
     */
    public boolean validateVaccineGlobally(Vaccine vc) {//to alter to comparison of attributes! THIS IS NOT FINAL
        VaccineIDComparator comparator = new VaccineIDComparator();
        if (!vaccineList.contains(vc)) {
            for (Vaccine vax : vaccineList) {
                if (comparator.compare(vax, vc) == 0) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * adds vaccine to the vaccine store
     *
     * @param vc vaccine to be added
     * @return true if sucessfully added
     */
    public boolean addVaccine(Vaccine vc) {
        if (validateVaccineGlobally(vc)) {
            return vaccineList.add(vc);
        } else {
            return false;
        }
    }


    /**
     * Method to see if a vaccine it's in the system by its name
     *
     * @param vaccineName the vaccine name to search
     * @return true if the vaccine name is in the system
     */
    public boolean vaccineIsRegistered(String vaccineName) {
        boolean exists = false;
        int i = 0;
        do {
            if (this.vaccineList.get(i).getName().equals(vaccineName)) {
                exists = true;
            }
            i++;
        } while (i < this.vaccineList.size() && exists == false);
        return exists;
    }


    /**
     * Method to get vaccine short description (designation) by its name
     *
     * @param name name to be found
     * @return vaccine short description
     */
    public String getVaccineDesignationByVaccineName(String name) {
        int i = 0;
        boolean found = false;
        String designation = "";
        do {
            if (name.equals(this.vaccineList.get(i).getName())) {
                found = true;
                designation = this.vaccineList.get(i).getVaccineType().getDesignation();
            }
            i++;
        } while (i < this.vaccineList.size() && found == false);
        return designation;
    }

    /**
     * Method to get vaccine name
     *
     * @param vc The Vaccine
     * @return vaccine name
     */
    public String get(Vaccine vc) throws VaccineNotFoundException {

        for (Vaccine vaccine : this.vaccineList) {
            if (vaccine == vc) {
                return vaccine.getName();
            }
        }

        throw new VaccineNotFoundException("Unknown " + vc + " vaccine");

    }
}

