package app.domain.store;

import app.domain.exceptions.VaccinationCenterNotFoundException;
import app.domain.model.VaccinationCenter;
import app.domain.utils.DateUtils;
import app.domain.utils.TimeUtils;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.io.Serializable;
import java.time.DateTimeException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VaccinationCenterStore  implements Serializable {

    List<VaccinationCenter> vaccinationCenterList;

    public VaccinationCenterStore() {

        this.vaccinationCenterList = new ArrayList<>();
    }

    /**
     *
     * @param vc
     * @return flag
     */
    public boolean saveVaccine(VaccinationCenter vc) {
        boolean flag = false;
        flag = addVaccinationCenter(vc);
        return flag;
    }

    /**
     *
     * @param vc
     * @return vaccinationCenterList.add(vc)
     */
    public boolean addVaccinationCenter(VaccinationCenter vc) {
        return vaccinationCenterList.add(vc);
    }


    public List<VaccinationCenter> getAllVaccinationCenters() {
        return this.vaccinationCenterList;
    }

    /**
     *
     * @param vaccinationCenter
     * @return if the vaccination center exists
     */
    public boolean hasVaccinationCenter(VaccinationCenter vaccinationCenter) {
        for (VaccinationCenter center : this.vaccinationCenterList) {
            if (center.equals(vaccinationCenter)) {
                return true;
            }
        }

        return false;
    }

    /**
     *
     * @param phone
     * @return if the phone number is correct
     */
    public boolean validatePhoneNumber(int phone) {       //MUST HAVE 9 DIGITS
        boolean a = false;

        if (phone >= 100000000 && phone <= 999999999) {
            a = true;
        } else {
            a = false;
            System.out.println("Wrong phone Number (must have 9 digits)!");
        }
        return a;
    }

    public boolean validateFaxNumber(int faxnumber) {       //MUST HAVE 9 DIGITS
        boolean a = false;

        if (faxnumber >= 100000000 && faxnumber <= 999999999) {
            a = true;
        } else {
            a = false;
            System.out.println("Wrong fax Number (must have 9 digits)!");
        }
        return a;
    }
    /**
     *
     * @param openHour
     * @param closeHour
     * @return
     */
    public boolean validateTime(String openHour, String closeHour) {
        boolean a = false;

        try {

            LocalTime openHourAfterParse = LocalTime.parse(openHour);
            LocalTime closeHourAfterParse = LocalTime.parse(closeHour);
            if (openHourAfterParse.isBefore(closeHourAfterParse) || closeHourAfterParse.isAfter(openHourAfterParse)) {
                a = false;
                System.out.printf("Time must lie between %s and %s!", openHourAfterParse, closeHourAfterParse);
            } else {
                a = true;
            }
        } catch (DateTimeException e) {
            a = false;
            System.out.println("Wrong time!");
        }

        return a;
    }

    public boolean validateZIPCode(String zipCode) {
        String regex = "^[0-9]{4}(?:-[0-9]{3})?$";
        Pattern PC = Pattern.compile(regex);
        Matcher matcher = PC.matcher(zipCode);
        boolean result = matcher.find();
        return result;
    }

    public boolean validateStreet(String street) {
        boolean a = false;
        if (street == "") {
            return a;
        } else {
            street.matches("^(\\\\d{1,}) [a-zA-Z0-9\\\\s]+(\\\\,)? [a-zA-Z]+(\\\\,)? [A-Z]{2} [0-9]{5,6}$ ");
            return true;
        }
    }

    public boolean validateCity(String city) {
        boolean a = false;
        if (city == "") {
            return a;
        } else {
            String cityRegex = "[a-zA-Z\\s]+$";
            Pattern pat = Pattern.compile(cityRegex);
            return pat.matcher(city).matches() ;
        }
    }

    public boolean validateEmail(String email) {
        boolean a = false;
        if (email == "") {
            return a;
        } else {
            String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
            Pattern pat = Pattern.compile(emailRegex);
            return pat.matcher(email).matches();
        }

    }

    public boolean validateWebsite(String website) {
        boolean a = false;
        if (website == "") {
            return a;
        } else {
            // String websiteRegex = "^https?:\\\\/\\\\/(?:www\\\\.)?[-a-zA-Z0-9@:%._\\\\+~#=]{1,256}\\\\.[a-zA-Z0-9()]{1,6}\\\\b(?:[-a-zA-Z0-9()@:%_\\\\+.~#?&\\\\/=]*)$";
            String websiteRegex = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
            Pattern pat = Pattern.compile(websiteRegex);
            return pat.matcher(website).matches();
        }
    }

    public boolean validateName(String name) {
        boolean a = false;
        if (name == "") {
            return a;
        } else {
            String nameRegex = "[a-zA-Z\\s]+$";
            Pattern pat = Pattern.compile(nameRegex);
            return pat.matcher(name).matches() ;
        }
    }

    /**
     * gets the vaccination center of the current session's center coordinator
     * @return vaccination center that the current session's center coordinator runs
     */
    public VaccinationCenter getVaccinationCenterOfCoordinator(){
        for(VaccinationCenter vc:vaccinationCenterList){
            if(vc.compareCenterCoordinatorWithCurrentLogin()){
                return vc;
            }
        }
        throw new VaccinationCenterNotFoundException("The center coordinator does not have any center associated, please try logging in again!");
    }

    public VaccinationCenter getVaccinationCenter(int vcId) {
        return this.getVaccinationCenterOfCoordinator();
    }
}
