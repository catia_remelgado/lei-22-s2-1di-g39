package app.domain.store;

import app.domain.model.Vaccine;
import app.domain.model.VaccineType;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author alexandraipatova
 * @author João Miguel
 */
public class VaccineTypeStore implements Serializable {

    /**
     * represents the several vaccine types owned by the company;
     */
    ArrayList<VaccineType> vaccineTypeList;

    /**
     * Vaccine Type Store's empty constructor.
     */
    public VaccineTypeStore() {

        this.vaccineTypeList = new ArrayList<>();
    }

    /**
     * Add a new Vaccine Type (adding it to the vaccine type store)
     * @param code The Vaccine Type's code
     * @param designation The Vaccine Type's designation
     * @return return the addition of the user to the store
     */
    public boolean addVaccineType(String code, String designation) {
        return this.addVaccineType(new VaccineType(code, designation));
    }

    /**
     * Vaccine Type Store constructor.
     *
     * @param vaccineTypeList The Vaccine Type List to be added.
     */
    public VaccineTypeStore(ArrayList<VaccineType> vaccineTypeList) {
        this.vaccineTypeList = vaccineTypeList;
    }

    /**
     * creates a vaccine type to add to the vaccine type list
     * @param code  The Vaccine Type's code
     * @param designation The Vaccine Type's designation
     */
    public VaccineType createVaccineType(String code, String designation) {

        VaccineType vt = new VaccineType(code,designation);
        return vt;

    }

    /**
     * returns the arraylist with all the vaccine types
     * @return arraylist with all vaccine types
     */
    public ArrayList<VaccineType> getVaccineTypeList() {
        return vaccineTypeList;
    }

    /**
     * verifies and adds a vaccine type to the vaccine type store
     * @param vt the vaccine type to be saved
     * @return true if the vaccine type was saved successfully
     */
    public boolean saveVaccine(VaccineType vt){
        boolean flag=false;
            flag=addVaccineType(vt);


        return flag;

    }
    /**
     * validates the vaccine type's information
     * @param vt the vaccine type to validate
     * @return true if vaccine type is valid
     */
    public boolean validateVaccineTypeLocally(Vaccine vt){
        return vt.validate();
    }
    /**
     * checks for duplicate vaccine types
     * @param vt the vaccine type to validate
     * @return true if non duplicated
     */
    public boolean validateVaccineTypeGlobally(VaccineType vt){//to alter to comparison of attributes! THIS IS NOT FINAL
        return !vaccineTypeList.contains(vt);
    }
    /**
     * adds vaccine type to the vaccine typestore
     * @param vt vaccine type to be added
     * @return true if successfully added
     */
    public boolean addVaccineType(VaccineType vt){
        return vaccineTypeList.add(vt);
    }
}
