package app.domain.store;

import app.controller.App;
import app.domain.comparator.CompareUserByCCNumber;
import app.domain.comparator.CompareUserByEmail;
import app.domain.comparator.CompareUserBySNSNumber;
import app.domain.model.Gender;
import app.domain.model.SNSUser;
import app.domain.exceptions.UserUnknownException;
import app.domain.model.UserClone;
import app.domain.shared.Constants;
import app.domain.utils.DateUtils;
import app.domain.utils.PasswordGenerator;
import pt.isep.lei.esoft.auth.AuthFacade;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * @author João Miguel
 */
public class SNSUserStore implements Serializable {



    /**
     * represents the various sns users
     */
    private List<SNSUser> snsUserList;

    /**
     * SNS User Store's empty constructor.
     */
    public SNSUserStore() {
        this.snsUserList = new ArrayList<>();
    }

    /**
     * Checks if the new user already exists in the system or not.
     *
     * @param object     new SNS User
     * @param comparator comparator that checks if the user already exists in the system
     * @return Returns true or false whether the user exists or not
     */

    public boolean userExists(Object object, Comparator comparator) {

        for (SNSUser user : this.snsUserList) {
            if (comparator.compare(object, user) == 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * Add a new SNS User to the system
     *
     * @param snsNumber   sns number of each sns user
     * @param name        name of  each sns user
     * @param birthdate   birthdate of  each sns user
     * @param email       email of  each sns user
     * @param phoneNumber PhoneNumber of  each sns user
     * @param gender      Gender of  each sns user
     * @param CCnumber    Citizen Card number  of  each sns user
     * @param address     Address of  each sns user
     * @throws IllegalArgumentException
     */
    public void addNewSNSUser(int snsNumber, String name, DateUtils birthdate, String email, int phoneNumber, Gender gender, int CCnumber, String address)
            throws IllegalArgumentException {
        Email userEmail = null;


        try {
            userEmail = new Email(email);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Warning: Invalid Email Address (" + email + ")");
        }
        if (snsNumber < 100000000 || snsNumber > 999999999) {
            throw new IllegalArgumentException("Invalid SNS number.");
        }
        if (name == null) {
            throw new IllegalArgumentException("Name is null.");
        }
        if (this.userExists(Integer.valueOf(snsNumber), new CompareUserBySNSNumber())) {
            throw new IllegalArgumentException("Warning: User " + snsNumber + " already exists.");
        }
        if (this.userExists(Integer.valueOf(CCnumber), new CompareUserByCCNumber())) {
            throw new IllegalArgumentException("Warning: User " + CCnumber + " already exists.");
        }
        if (this.userExists(email, new CompareUserByEmail())) {
            throw new IllegalArgumentException("Warning: User " + email + " already exists.");
        }

        this.snsUserList.add(new SNSUser(snsNumber, name, birthdate, userEmail, phoneNumber, gender, CCnumber, address));

    }


    //ANNA

    /**
     * Method to confirm if the given SNS number matches with the SNS number associated with the log in acccount
     *
     * @param email              the email of the log in user
     * @param givenSNSUserNumber the SNS number inputed by the user
     * @return true if given SNS number maatches the SNS number associated with the email
     */
    public boolean confirmSNSUserNumberByEmail(Email email, int givenSNSUserNumber) {
        int i = 0;
        boolean found = false;
        int snsUserNumber = 0;

        if (snsUserList.size() != 0) {
            do {
                if (email.toString().equals(snsUserList.get(i).getEmail().toString())) {
                    found = true;
                }
                i++;
            } while (found == false && i < this.snsUserList.size());

            if (found == true) {
                snsUserNumber = this.snsUserList.get(i - 1).getSnsNumber();
            }
        }
        return (snsUserNumber == givenSNSUserNumber);
    }

    /**
     * Method to verify if an inputed sns number exists
     * @param snsNumber given sns number
     * @return true if given sns number is in store
     */
    public boolean confirmIfUserExists(int snsNumber){

        for (SNSUser user : this.snsUserList) {
            if (user.getSnsNumber() == snsNumber) {
                return true;
            }
        }
        return false;
    }





    /**
     * Get the SNS Use by sns number
     * @param snsNumber The SNS User number
     * @return returns the user
     * @throws UserUnknownException
     */
    public SNSUser get(int snsNumber) throws UserUnknownException {

        for (SNSUser user : this.snsUserList) {
            if (user.getSnsNumber() == snsNumber) {
                return user;
            }
        }

        throw new UserUnknownException("Unknown " + snsNumber + " sns number user");
    }

    /**
     * Get the name of an SNS User by their number
     * @param snsNumber SNS User's number
     * @return
     */
    public String getSNSUSerNameBySNSNumber(int snsNumber){
        int i = 0;
        boolean found = false;
        String name = "";
        if (this.snsUserList.size()!=0) {
            do {
                if (snsNumber == this.snsUserList.get(i).getSnsNumber()) {
                    found = true;
                    name = this.snsUserList.get(i).getName();
                }
                i++;
            } while (i < this.snsUserList.size() && found == false);
        }
        return name;
    }



    //ALEXANDRA

    /**
     * adds a list of sns users to the store with all verifications
     * @param list list of users to add
     */
    public void addListOfSNSUsers(List<SNSUser> list){


        for (SNSUser snsUser : list) {

            String pass= PasswordGenerator.generateRandomPassword();
            System.out.println("\n"+pass);
            App.getInstance().getCompany().getUCStore().addUser(new UserClone(snsUser.getName(),snsUser.getEmail().toString(), pass, Constants.ROLE_SNSUSER));

            this.addNewSNSUser(
                    snsUser.getSnsNumber(),
                    snsUser.getName(),
                    snsUser.getBirthdate(),
                    snsUser.getEmail().toString(),
                    snsUser.getPhoneNumber(),
                    snsUser.getGender(),
                    snsUser.getCCnumber(),
                    snsUser.getAddress());
        }

        showListOfSnsUsers(list);

    }

    public void showListOfSnsUsers(List<SNSUser>list){
        for (SNSUser user : list) {
            System.out.println(user);
        }
    }

    public List<SNSUser> getSnsUserList() {
        return snsUserList;
    }
}
