package app.domain.store;

import app.domain.utils.DateUtils;

import java.io.File;
import java.nio.file.LinkOption;
import java.nio.file.Path;

import static java.nio.file.Files.exists;

public class StatisticsStore {

    public static boolean verifyDateInterval(DateUtils dateBeg, DateUtils dateEnd) {
        boolean a = false;
        int difference = dateEnd.difference(dateBeg);
        if (difference == 0) {
            return a;

        } else {
            a = true;
            return a;
        }

    }

    public boolean validateFileName(String fileName) {       //MUST HAVE 9 DIGITS
        boolean a = false;

        File file = new File(fileName);
        if (file.exists()) {
            a = true;
        } else {
            a = false;
        }
        return a;
    }
}
