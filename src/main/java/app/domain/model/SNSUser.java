package app.domain.model;
import app.domain.utils.DateUtils;
import app.ui.console.utils.Utils;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author João Miguel
 */
public class SNSUser implements Serializable {

    /**
     * sns users counter to know how many there are in the system
     */
    private static int COUNTER = 1;

    /**
     * id of each user in the system
     */
    private int id;

    /**
     * sns number of each sns user
     */
    private int snsNumber;

    /**
     * name of  each sns user
     */
    private String name;

    /**
     *  birthdate of  each sns user
     */
    private DateUtils birthdate;

    /**
     *  email of  each sns user
     */
    private String email;

    /**
     * PhoneNumber of  each sns user
     */
    private int phoneNumber;

    /**
     *  Gender of  each sns user
     */
    private Gender gender;

    /**
     *  Citizen Card number  of  each sns user
     */
    private int CCnumber;

    /**
     *  Address of  each sns user
     */
    private String address;

    /**
     *  List of Vaccines taken by the SNS User
     */
    private List<VaccineShot> takenVaccines;

    /**
     * constructor for an SNS user that needs to be registered
     * @param snsNumber  snsNumber of each sns user
     * @param name  Name of each sns user
     * @param birthdate  birthdate of each sns user
     * @param email   email of each sns user
     * @param phoneNumber PhoneNumber of each sns user
     * @param gender Gender of each sns user
     * @param CCnumber  Citizen Card number  of  each sns user
     * @param address  address of  each sns user
     */
    public SNSUser(int snsNumber, String name, DateUtils birthdate, Email email, int phoneNumber, Gender gender, int CCnumber, String address) {


        this.id = SNSUser.COUNTER++;
        this.snsNumber = snsNumber;
        this.name = name;
        this.birthdate = birthdate;
        this.email = email.toString();
        this.phoneNumber = phoneNumber;
        this.gender = gender;
        this.CCnumber = CCnumber;
        this.address = address;
        this.takenVaccines = new ArrayList<>();
    }

    /**
     * Method to get the Vaccines taken by the SNS User
     * @return returns the List of Taken Vaccines
     */
    public List<VaccineShot> getTakenVaccines() {
        return this.takenVaccines;
    }

    /**
     * Get SNS User ID.
     * @return Returns the SNS User ID.
     */
    public int getId() {
        return id;
    }

    /**
     * Get SNS User Address.
     * @return Returns the SNS User Address.
     */
    public String getAddress() {
        return address;
    }

    /**
     * Get SNS User Number.
     * @return Returns the SNS User Number.
     */
    public int getSnsNumber() {
        return snsNumber;
    }

    /**
     * Set the SNS User Number.
     * @param snsNumber The SNS User Number.
     */
    public void setSnsNumber(int snsNumber) {
        this.snsNumber = snsNumber;
    }

    /**
     * Set the SNS User Address.
     * @param address The SNS User Address.
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Get SNS User Name.
     * @return Returns the SNS User Name.
     */
    public String getName() {
        return name;
    }

    /**
     * Set the SNS User Name.
     * @param name The SNS User Name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get SNS User Birthdate.
     * @return Returns the SNS User Birthdate.
     */
    public DateUtils getBirthdate() {
        return birthdate;
    }

    /**
     * Set the SNS User Birthdate.
     * @param birthdate The SNS User Birthdate.
     */
    public void setBirthdate(DateUtils birthdate) {
        this.birthdate = birthdate;
    }

    /**
     * Get SNS User Email.
     * @return Returns the SNS User Email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set the SNS User Email.
     * @param email The SNS User Email.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Get SNS User Phone Number.
     * @return Returns the SNS User Phone Number.
     */
    public int getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Set the SNS User Phone Number.
     * @param phoneNumber The SNS User Phone Number.
     */
    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Get SNS User Gender.
     * @return Returns the SNS User Gender.
     */
    public Gender getGender() {
        return gender;
    }

    /**
     * Set the SNS User Gender.
     * @param gender The SNS User Gender.
     */
    public void setGender(Gender gender) {
        this.gender = gender;
    }

    /**
     * Get SNS User Citizen Card Number.
     * @return Returns the SNS User Citizen Card Number.
     */
    public int getCCnumber() {
        return CCnumber;
    }

    /**
     * Set the SNS User Citizen Card Number.
     * @param CCnumber The SNS User Citizen Card Number.
     */
    public void setCCnumber(int CCnumber) {
        this.CCnumber = CCnumber;
    }





    //ANNA


    /**
     * Method to verify is SNSUser number its in correct format (has 9 digits)
     * @param snsNumber given SNS user number
     * @return true if given SNS user number has 9 digits
     */
    public static boolean verifyIfSNSUserNumberIsCorrect(int snsNumber){
        if (snsNumber >= 100000000 && snsNumber <= 999999999){
            return true;
        }
        return false;
    }

    /**
     * returns attributes of sns user
     * @return attributes of sns user
     */
    @Override
    public String toString() {
        return "SNSUser #"+ id +"\n"+
                "SNS Number:" + snsNumber + "\n"+
                "Name: " + name + "\n" +
                "Birth Date:" + birthdate +"\n"+
                "E-mail: " + email + "\n"+
                "Phone Number: " + phoneNumber + "\n"+
                "Gender: " + gender + "\n"+
                "Citizen Card Number: " + CCnumber + "\n"+
                "Address: " + address + "\n";
    }

    public int getAge() {

        DateUtils currentDate = DateUtils.currentDate();

        int age = currentDate.getYear() - this.birthdate.getYear();

        if (currentDate.getMonth() < this.birthdate.getMonth() ||
                (currentDate.getMonth() == this.birthdate.getMonth() &&
                        currentDate.getDay() < this.birthdate.getDay())) {
            age--;
        }

        return age;
    }
}
