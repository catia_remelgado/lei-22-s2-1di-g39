package app.domain.model;

import app.domain.utils.DateUtils;
import app.domain.utils.TimeUtils;

import java.io.*;

public class SMS implements Serializable {
    //static final String SMS_MESAGE = "Appointments/SMS.txt";
    static final String FILE_NAME = "Appointments/SMS.txt";
    static final String FILE_RECOVERY = "Recovery/SMS.txt";

    /**
     * Method to create/ change file with the sms
     * @param date date for when it's scheduled the appointment
     * @param time time for when it's scheduled the appointment
     * @param vaccinationCenter vaccination center where will occur the appointment
     * @return true if message was written in the file (and the file was created)
     */
    public boolean SendMessage(DateUtils date, TimeUtils time, VaccinationCenter vaccinationCenter) {
        File file = new File(FILE_NAME);
        boolean sended = false;

        if (!file.exists()) {
            try {
                file.createNewFile();
                sended = true;
            } catch (IOException e) {
                sended = false;
                System.out.println("Something went wrong. Sorry.");
            }
        } else {
            sended = true;
        }

        if (sended == true) {
            try {
                PrintWriter pw = new PrintWriter(file);
                pw.println("This is your schedule data:\nDay: " + date + "\nTime: " + time + "\nVaccination center: " + vaccinationCenter + "\n\n\n");
                pw.close();
                sended = true;
            } catch (FileNotFoundException e) {
                sended = false;
                System.out.println("Something went wrong. Sorry.");
            }
        }

        return sended;
    }

    public boolean SendMessage() {
        File file = new File(FILE_RECOVERY);
        boolean sended = false;

        if (!file.exists()) {
            try {
                file.createNewFile();
                sended = true;
            } catch (IOException e) {
                sended = false;
                System.out.println("Something went wrong. Sorry.");
            }
        } else {
            sended = true;
        }

        if (sended == true) {
            try {
                PrintWriter pw = new PrintWriter(file);
                pw.println("The Recovery Period is over! \nYou can leave the center.\nThank You");
                pw.close();
                sended = true;
            } catch (FileNotFoundException e) {
                sended = false;
                System.out.println("Something went wrong. Sorry.");
            }
        }

        return sended;
    }

}
