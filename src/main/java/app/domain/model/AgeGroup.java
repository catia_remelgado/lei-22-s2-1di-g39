package app.domain.model;

import java.io.Serializable;
import java.util.Collection;

/**
 * AgeGroup represents the age interval to undergo a certain vaccine administration process.
 * @author alexandraipatova
 */
public class AgeGroup  implements Serializable {

    /**
     * the minimum age of the age group/interval to undergo the vaccine administration process
     */
    private int minimumAge;

    /**
     * the maximum age of the age group/interval to undergo the vaccine administration process
     */
    private int maximumAge;

    /**
     * constant that represents the maximum age for human being
     */
    private final int MAXIMUM_AGE_FOR_HUMAN=140;
    private int doseList;

    /**
     * constructor for an age group of a certain vaccine administration process
     * @param minimumAge minimum age to to undergo the vaccine administration process
     * @param maximumAge maximum age to undergo the vaccine administration process
     */
    public AgeGroup(int minimumAge, int maximumAge) {
        this.minimumAge = minimumAge;
        this.maximumAge = maximumAge;
    }

    /**
     * get the minimum age of this age group
     * @return minimum age
     */
    public int getMinimumAge() {
        return minimumAge;
    }
    /**
     * get the maximum age of this age group
     * @return maximum age
     */
    public int getMaximumAge() {
        return maximumAge;
    }

    /**
     * validates an age group to check if parameters are valid
     * @return true or false, if the format is valid or not respectively
     */
    public boolean validate(){
        try {
            if (minimumAge > maximumAge) {
                throw new IllegalArgumentException("The range is invalid!");
            }
            if (minimumAge < 0 || minimumAge > MAXIMUM_AGE_FOR_HUMAN) {
                throw new IllegalArgumentException("The minimum age is invalid!");
            }
            if ( maximumAge > 140) {
                throw new IllegalArgumentException("The minimum age is invalid!");
            }

        }catch(IllegalArgumentException illegalArgumentException){
            return false;
        }
        return true;


    }

    /**
     * Method to see if the SNS User age is in a certain interval
     * @param age The SNS User age
     * @return returns true (if SNS User age is in a certain interval) or false otherwise
     */
    public boolean isValid(int age) {
        return this.minimumAge <= age && this.maximumAge >= age;
    }

    @Override
    public String toString() {
        return "from "+minimumAge+" to "+maximumAge+" years";
    }

}

