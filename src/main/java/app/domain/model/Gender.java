package app.domain.model;

/**
 * @author João Miguel
 */
public enum Gender {
    MALE, FEMALE, NONE
}
