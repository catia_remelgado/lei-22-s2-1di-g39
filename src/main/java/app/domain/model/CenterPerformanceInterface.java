package app.domain.model;

import java.util.List;

/**
 * used to implement the evaluate performance method on any center performance
 * @author alexandraipatova
 */

public interface CenterPerformanceInterface {

    public List<Integer> evaluateCenterPerformance();

}
