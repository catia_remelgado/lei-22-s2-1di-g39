package app.domain.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

public interface LoadFromFile {

    List<List<String>> loadFromFile(File file) throws FileNotFoundException;
}
