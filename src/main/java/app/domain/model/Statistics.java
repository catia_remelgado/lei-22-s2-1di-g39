package app.domain.model;

import app.domain.store.SNSUserStore;
import app.domain.store.VaccineStore;
import app.domain.utils.DateUtils;

import java.util.ArrayList;
import java.util.Date;

public class Statistics {
    private final DateUtils dateBeg;

    private final DateUtils dateEnd;

    public Statistics(DateUtils dateBeg, DateUtils dateEnd) {
        this.dateBeg = dateBeg;
        this.dateEnd = dateEnd;
    }


    public boolean verifyDateInterval() {
        if (this.dateBeg.compareTo(this.dateEnd) < 0 && !this.dateBeg.isBiggerOrEquals(DateUtils.currentDate()) && !this.dateEnd.isBiggerOrEquals(DateUtils.currentDate())) {
            return true;
        } else {
            return false;
        }

    }
}