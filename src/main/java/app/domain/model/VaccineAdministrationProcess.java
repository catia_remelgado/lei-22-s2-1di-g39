package app.domain.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This class represents the whole process of the vaccine administration on a certain age group, which consists on the several dose administrations for a certain age group.
 * @author alexandraipatova
 */
public class VaccineAdministrationProcess implements Serializable {

    /**
     * represents the number of doses to be administered through this vaccine administration process
     */
    private int numberOfDoses;

    /**
     * represents the age group to which this vaccine administration process corresponds
     */
    private AgeGroup ag;

    /**
     * represents the several doses that constitute the vaccine administration process
     */
    ArrayList<DoseAdministration> doseAdministrationList;

    /**
     * Constructor for the vaccine administration process
     * @param numberOfDoses the number of doses of the vaccine administration process
     * @param minimumAge the minimum age of the age group
     * @param maximumAge the maximum age of the age group
     */
    public VaccineAdministrationProcess(Integer minimumAge,Integer maximumAge,Integer numberOfDoses) {
        this.numberOfDoses = numberOfDoses;
        this.ag = new AgeGroup(minimumAge,maximumAge);
        doseAdministrationList=new ArrayList<>();
    }

    /**
     * return the number of doses of this administration process
     * @return number of doses to administer to the age group
     */
    public int getNumberOfDoses() {
        return numberOfDoses;
    }

    /**
     * returns the age group to which the vaccine administration process is administered
     * @return age group of this vaccine administration process
     */
    public AgeGroup getAg() {
        return ag;
    }

    /**
     * creates a dose administration of the vaccine administration process
     * @param doseNumber the number of the dose (e.g. 1st/2nd)
     * @param dosage quantity of vaccine solution in ml of this dose administration
     * @param timePreviousToCurrentDose time spent in days from the previous dose to the current dose of this dose administration
     * @param recoveryTime time in minutes to recover from the dose administration and to watch for potential adverse reactions
     * @return returns a true or false value depending if the dose administration was successfully created
     */
    public DoseAdministration createDoseAdministration(int doseNumber, float dosage, int timePreviousToCurrentDose, int recoveryTime) {
        DoseAdministration da = new DoseAdministration(doseNumber,dosage,timePreviousToCurrentDose,recoveryTime);
        return da;
    }

    /**
     * validates the dose administration locally
     * @param da represents the dose administration
     * @return true if the dose administration is valid locally
     */
    public boolean validateDoseAdministrationLocally(DoseAdministration da){
        return da.validate();
    }

    /**
     * validate the vaccine administration process locally
     * @return true if the vaccine administration process is valid
     */
    public boolean validate(){
        try {
            if (this.numberOfDoses <= 0) {
                throw new IllegalArgumentException("The number of doses is not valid!");
            }
            if (!ag.validate()) {
                throw new IllegalArgumentException("The age group is not valid!");
            }
        }catch(IllegalArgumentException illegalArgumentException){
            return false;
        }
        return true;
    }
    /**
     * adds a dose administration to the administration list
     * @param da represents a dose administration of the vaccine administration process
     * @return returns a true value if the dose administration is added successfully
     */
    public boolean addDoseAdministration(DoseAdministration da){
        return doseAdministrationList.add(da);
    }

    /**
     *this method returns all of the attributes in a string form regarding this vaccine administration process
     * @return string with all of the attributes of vaccine administration process
     */
    @Override
    public String toString() {
        String vap="This vaccine administration process is applied to an age group "+ag.toString()+" with "+numberOfDoses+" applied doses.\n\n";
        if(!doseAdministrationList.isEmpty()) {
            vap=vap+"The doses are described as follows:\n";
            for (DoseAdministration da : doseAdministrationList) {
                vap = vap + da.toString() + "\n\n";
            }
        }
        return vap;
    }

    /**
     * Method to validate a administration
     * @param age The SNS User age
     * @param dose The Vaccine Dose
     * @param daysSinceLastDose The days between now and last dose
     * @return returns true(if its possible to do the administration) or false otherwise
     */
    public boolean validateAdministration(int age, int dose, int daysSinceLastDose) {

        if (!this.ag.isValid(age)) {
            return false;
        }

        if (dose < 1 || dose > this.numberOfDoses) {
            return false;
        }

        if (dose == 1) {
            return true;
        } else {
            for (DoseAdministration doseAdministration : this.doseAdministrationList) {
                if (doseAdministration.getDoseNumber() == dose) {
                    return doseAdministration.getTimePreviousToCurrentDose() <= daysSinceLastDose;
                }
            }
            return false;
        }
    }

    /**
     * Method to get the Dose Administration
     * @param dose The Vaccine Dose
     * @return returns the dose administration
     */
    public DoseAdministration getDoseAdministration(int dose) {


        for (DoseAdministration d : this.doseAdministrationList) {
            if (d.getDoseNumber() == dose) {
                return d;
            }
        }

        return null;
    }


}
