package app.domain.model;

import java.io.Serializable;

/**
 * This class represents a single administration of a single vaccine of a vaccine administration process.
 *
 * @author alexandraipatova
 */
public class DoseAdministration  implements Serializable {

    /**
     * the number of the dose in the administration process (e.g. 1st dose, 2nd dose...)
     */
    private int doseNumber;

    /**
     * quantity in millimeters of the vaccine solution used for dose administration
     */
    private float dosage;

    /**
     * time spent since the last dose was taken in days
     */
    private int timePreviousToCurrentDose;

    /**
     * time spent in minutes in recovery room to ensure that the administration does not cause immediate adverse reactions.
     */
    private int recoveryTime;

    /**
     * Constructor for a dose administration, which corresponds to the administration of a single dose of a certain vaccine and age group
     *
     * @param doseNumber                the number of the dose
     * @param dosage                    the quantity in ml of the dose
     * @param timePreviousToCurrentDose time spent since the last administered dose
     * @param recoveryTime              recovery period for this specific dose
     */
    public DoseAdministration(Integer doseNumber, Float dosage, Integer timePreviousToCurrentDose, Integer recoveryTime) {
        this.doseNumber = doseNumber;
        this.dosage = dosage;
        this.timePreviousToCurrentDose = timePreviousToCurrentDose;
        this.recoveryTime = recoveryTime;
    }

    /**
     * return the number of the dose (e.g. 1st/2nd)
     *
     * @return the number of the dose
     */
    public int getDoseNumber() {
        return doseNumber;
    }

    /*
     * changes the dose number of this dose administration
     * @param doseNumber the number of the dose
     *
    public void setDoseNumber(int doseNumber) {
        this.doseNumber = doseNumber;
    }

    /**
     * returns the dosage in ml of the vaccination solution of this specific dose
     * @return dosage of administrated dose
     */
    public float getDosage() {
        return dosage;
    }

    /*
     * changes the dosage in ml of this specific dose administration
     * @param dosage dosage of administered dose
     *
    public void setDosage(float dosage) {
        this.dosage = dosage;
    }

    /**
     * return the time spent since the last dose to the current one in days
     * @return days spent since the last dose
     */
    public int getTimePreviousToCurrentDose() {
        return timePreviousToCurrentDose;
    }

    /*
     * changes the days spent since last dose
     * @param timePreviousToCurrentDose number of days spent since the last dose
     *
    public void setTimePreviousToCurrentDose(int timePreviousToCurrentDose) {
        this.timePreviousToCurrentDose = timePreviousToCurrentDose;
    }

    **
     * returns the recovery time in minutes of a dose
     * @return minutes of recovery time needed to register immediate adverse reactions
     */
    public int getRecoveryTime() {
        return recoveryTime;
    }

    /**
     * changes the recovery time in minutes of a certain dose
     * @param recoveryTime recovery time of dose in minutes
     *
    public void setRecoveryTime(int recoveryTime) {
    this.recoveryTime = recoveryTime;
    }
     */
    /**
     * validates a dose administration locally
     *
     * @return true or false, if correct formats our not respectively
     */
    public boolean validate() {

        try {
            if (this.doseNumber <= 0) {
                throw new IllegalArgumentException("The dose number is not valid!");
            }
            if (this.dosage <= 0) {
                throw new IllegalArgumentException("The dosage is not valid!");
            }
            if (this.timePreviousToCurrentDose < 0) {
                throw new IllegalArgumentException("The time of the previous dose to the current one is not valid!");
            }
            if (this.recoveryTime < 0) {
                throw new IllegalArgumentException("The recovery time is not valid!");
            }
        } catch (IllegalArgumentException illegalArgumentException) {
            return false;
        }
        return true;

    }

    /**
     * this method returns an accurate description of all attributes of this dose administration
     *
     * @return information about dose administration
     */
    @Override
    public String toString() {
        String da = "Dose administration number " + doseNumber + " has a dosage of " + String.format("%.2f", dosage) + " ml\n" +
                "The time spent from the previous dose to the current one is " + timePreviousToCurrentDose + " days.\n" +
                "Its recovery time is " + recoveryTime + " minutes";

        return da;
    }

}
