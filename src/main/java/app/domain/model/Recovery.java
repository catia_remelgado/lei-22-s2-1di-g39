package app.domain.model;

import app.domain.utils.DateUtils;
import app.domain.utils.TimeUtils;

import java.io.Serializable;

/**
 * Represents an arrival of an sns user
 * @author alexandraipatova
 * @author João Miguel
 */
public class Recovery implements Serializable {
    /**
     * Arrival time of sns user
     */
    private TimeUtils arrivalTime;

    /**
     * Leaving time of sns user
     */
    private TimeUtils leavingTime;

    /**
     * user that performed the arrival
     */
    private SNSUser user;

    /**
     * Constructor for arrival with arrivalTime, user and an appointment
     * @param arrivalTime time of arrival
     * @param user user that performed the arrival
     */
    public Recovery(SNSUser user, TimeUtils arrivalTime) {
        this.arrivalTime = arrivalTime;
        this.leavingTime = null;
        this.user = user;
    }

    public Recovery(SNSUser user) {
        this(user, TimeUtils.currentTime());
    }


    /**
     * returns arrival time
     * @return arrival time
     */
    public TimeUtils getArrivalTime() {
        return arrivalTime;
    }


    /**
     * Method to get the leaving time
     * @return leaving time
     */
    public TimeUtils getLeavingTimeTime() {
        return leavingTime;
    }

    /**
     * Method to check if the SNS User is in the Recovery Room
     * @return returns true or false if the User is in Recovery Room or not
     */
    public boolean isRecovering() {
        return this.leavingTime == null;
    }

    /**
     * Method to get the Time when the SNS User leaves the room
     */
    public void leaveRoom() {
        this.leavingTime = TimeUtils.currentTime();
    }

    /**
     * returns sns number of sns user
     * @return sns number of sns user
     */
    public int getSNSNumber(){
       return user.getSnsNumber();
    }

    /**
     * returns name of sns user
     * @return name of sns user
     */
    public String getName(){
        return user.getName();
    }

    /**
     * returns birthdate of sns number
     * @return birthdate of sns user
     */
    public DateUtils getBirthDate() {
        return user.getBirthdate();
    }

    /**
     * returns sex of sns number
     * @return sex of sns user
     */
    public String getSex() {
        return user.getGender().toString();
    }

    /**
     * returns sns phone number of sns user
     * @return sns phone number of sns user
     */
    public int getPhoneNumber(){
        return user.getPhoneNumber();
    }

}
