package app.domain.model;

import app.controller.App;
import app.domain.mapper.LoadFromFileMapper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Loads information about sns user's from a csv file
 * @author alexandraipatova
 */
public class LoadFromFileCSV implements LoadFromFile, Serializable {


    /**
     * loads all data regarding a user csv file
     *
     * @param file file to be loaded
     * @return The organized List of user's attributes
     *
     * adapted from: https://www.baeldung.com/java-csv-file-array
     * Reading a CSV File into an Array | Baeldung. (n.d.). Retrieved May 29, 2022, from https://www.baeldung.com/java-csv-file-array
     */
    @Override
    public List<List<String>> loadFromFile(File file) throws FileNotFoundException {

        List<List<String>> userInfo = new ArrayList<>();
        Scanner checkDelimiter=new Scanner (file);
        Scanner read = new Scanner(file);
        String delimiter=checkDelimiter.nextLine();

        if(delimiter.contains(";")){
            delimiter=";";
            read.nextLine();
        }else if(delimiter.contains(",")){
            delimiter=",";
        }

        checkDelimiter.close();




        List<List<String>> records = new ArrayList<>();

        while (read.hasNextLine()) {
            records.add(getUserFromLine(read.nextLine(),delimiter));
        }




        read.close();
        return records;
    }

    /**
     *returns the list of attributes of a certain sns user
     * @param line line of text from the csv file that represents a single user
     * @param delimiter delimiter of the file (";" with a header, and "," without a header) as per: https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16687
     * @return list of attributes in String form of a certain sns user
     */
    private List<String> getUserFromLine(String line,String delimiter) {
        List<String> values = new ArrayList<String>();
        try (Scanner rowScanner = new Scanner(line)) {
            rowScanner.useDelimiter(delimiter);
            while (rowScanner.hasNext()) {
                values.add(rowScanner.next());
            }
        }

        return values;
    }

    //add javadoc

    public void insertUsersFromFileData(List<List<String>> userInfo){

        LoadFromFileMapper map=new LoadFromFileMapper();

        App.getInstance().getCompany().getSnsUserStore().addListOfSNSUsers(map.DTOListToUserList(map.fileInfoToDTO(userInfo)));

    }

}
