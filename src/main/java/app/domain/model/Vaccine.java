package app.domain.model;

import app.domain.comparator.VaccineAdministrationProcessAgeComparator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This vaccine represents a vaccine and its administration process
 * @author alexandraipatova
 */
public class Vaccine implements Serializable {
    /**
     *ID of the vaccine
     */
    private int ID;

    /**
     * name of the vaccine
     */
    private String name;

    /**
     * brand of the vaccine
     */
    private String brand;

    /**
     * the technology type of this function (relates to how it functions inside the body)
     */
    private String technology;

    /**
     * the type of vaccine of this vaccine
     */
    private VaccineType vaccineType;

    /**
     * represents the several administration processes of each age group
     */
    ArrayList<VaccineAdministrationProcess> vaccineAdministrationProcessesList;

    /**
     * Contructor of a vaccine
     * @param ID
     * @param name name of the vaccine
     * @param brand brand of the vaccine
     * @param vaccineType type of a vaccine
     * @param technology technology type of a vaccine
     */
    public Vaccine(Integer ID, String name, String brand, String technology ,VaccineType vaccineType) {
        this.ID=ID;
        this.name = name;
        this.brand = brand;
        this.technology=technology;
        this.vaccineType = vaccineType;
        this.vaccineAdministrationProcessesList=new ArrayList<>();
    }

    /**
     * returns the name of this vaccine
     * @return name of vaccine
     */
    public String getName() {
        return name;
    }

    /**
     * returns the brand of this vacicne
     * @return brand of vaccine
     */
    public String getBrand() {
        return brand;
    }

    /**
     * returns the vaccine type of this vaccine
     * @return vaccine type of vaccine
     */
    public VaccineType getVaccineType() {
        return vaccineType;
    }
    /**
     * returns the technology of this vaccine
     * @return technology of vaccine
     */
    public String getTechnology() {
        return technology;
    }

    /**
     * validates the vaccine information (locally)
     * @return true if the vaccine is valid
     */
    public boolean validate(){

        try {
            if(this.ID<=0){
                throw new IllegalArgumentException("The ID is not valid!");
            }
            if (this.name.trim().equals("")) {
                throw new IllegalArgumentException("The name is not valid!");
            }
            if (this.brand.trim().equals("")) {
                throw new IllegalArgumentException("The brand is not valid!");
            }
            if (this.technology.trim().equals("")) {
                throw new IllegalArgumentException("The technology is not valid!");
            }
            if (!vaccineType.validate()) {
                throw new IllegalArgumentException("The vaccine type is not valid!");
            }

        }catch(IllegalArgumentException illegalArgumentException){
            return false;
        }
        return true;


    }

    /**
     * validates the vaccine administration process locally
     * @param vap represents the vaccine administration process
     * @return true if the vaccine administration process is valid locally
     */
    public boolean validateVaccineAdministrationProcessLocally(VaccineAdministrationProcess vap){
        return vap.validate();
    }

    /**
     *the creator method for a vaccine administration process
     * @param minimumAge minimum age of an age group
     * @param maximumAge maximum age of an age group
     * @param numberOfDoses number of doses of the vaccine administration process
     * @return VaccineAdministrationProcess that was created
     */
    public VaccineAdministrationProcess createVaccineAdministrationProcess(int minimumAge, int maximumAge,int numberOfDoses) {

        VaccineAdministrationProcess vap = new VaccineAdministrationProcess(minimumAge,maximumAge,numberOfDoses);
        return vap;

    }

    /**
     * adds a vaccine administration process to the vaccine administration process
     * @param vap represents one vaccine administration process of a vaccine
     * @return returns a true value if the vaccine administration process is added successfully
     */
    public boolean addVaccineAdministrationProcess(VaccineAdministrationProcess vap){
        VaccineAdministrationProcessAgeComparator comparator=new VaccineAdministrationProcessAgeComparator ();
        if(!vaccineAdministrationProcessesList.contains(vap)) {
            for (VaccineAdministrationProcess vapFromList : vaccineAdministrationProcessesList) {
                if (comparator.compare(vapFromList, vap) == 0) {
                    return false;
                }
            }
            return vaccineAdministrationProcessesList.add(vap);
        }else{
            return false;
        }
    }

    /**
     * return the ID of the vaccine
     * @return ID of vaccine
     */
    public int getID() {
        return ID;
    }

    @Override
    public String toString() {
        String vc="Vaccine #"+ID+":"+name+" "+brand+" is of "+vaccineType.toString()+" and uses "+technology+" technology.\n\n";
        if(!vaccineAdministrationProcessesList.isEmpty()){
            vc=vc+"Its vaccine administration processes are described as follows:\n";
            for (VaccineAdministrationProcess vap : vaccineAdministrationProcessesList) {
                vc = vc + vap.toString() + "\n";
            }
        }
        return vc;
    }

    public List<VaccineAdministrationProcess> getVaccineAdministrationProcess() {
        return this.vaccineAdministrationProcessesList;
    }

    public AgeGroup[] getAgeGroupList() {
       return this.getAgeGroupList();
    }
}
