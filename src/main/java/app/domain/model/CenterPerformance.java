package app.domain.model;

import app.domain.adapter.BenchmarkCPAdapter;
import app.domain.adapter.BruteforceCPAdapter;
import app.domain.utils.GetPropertyValues;
import app.domain.utils.TimeUtils;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a center's performance and data relating to it
 *
 * @author alexandraipatova
 */
public class CenterPerformance  implements Serializable {


    /**
     * vaccination center to which the performance refers
     */
    private VaccinationCenter vc;

    /**
     * Vaccine administrations to analyze (by day)
     */
    private List<VaccineAdministration> vaList;

    /**
     * time interval in which to calculate the performance
     */
    private int m;
    /**
     * list that represents the influx/outflow of people in a certain vaccination center
     */
    private List<Integer> differenceList;

    /**
     * list that represents the period of maximum sum of influx of people, which corresponds to the influx/outflow that has worse performance
     */
    private List<Integer> maxSumSubList;

    /**
     * maximum sum of influx of people in the worst performing interval
     */
    private int maxSum;

    /**
     * start time of the worst time period in terms of performance
     */
    private TimeUtils startTime;
    /**
     * end time of the worst time period in terms of performance
     */
    private TimeUtils endTime;

    /**
     * name of the bruteforce algorithm defined in config.properties
     */
    private final String BRUTEFORCE_ALGORITM="Bruteforce";

    /**
     * name of the benchmark algorithm defined in config.properties
     */
    private final String BENCHMARK_ALGORITM="Benchmark";

    /**
     * the execution time of the maximum sum contiguous sublist algorithm
     */
    private long execTime;
    /**
     * constructor that receives data to be calculated
     *
     * @param vc     vaccination center of which we calculate performance
     * @param vaList vaccine administration list of this day
     * @param m      time interval to analyze
     */
    public CenterPerformance(VaccinationCenter vc, List<VaccineAdministration> vaList, int m) {
        this.vc = vc;
        this.vaList = vaList;
        this.m = m;
    }
    public CenterPerformance() {
    }
    public CenterPerformance(VaccinationCenter vc, int m) {
        this.vc = vc;
        this.m = m;
    }

    /**
     * constructor of the center performance (parameters are descriptors of performance)
     *
     * @param differenceList list that represents the influx/outflow of people in a certain vaccination center
     * @param maxSumSubList  list that represents the period of maximum sum of influx of people, which corresponds to the influx/outflow that has worse performance
     * @param maxSum         maximum sum of influx of people in the worst performing interval
     * @param startTime      start time of the worst time period in terms of performance
     * @param endTime        end time of the worst time period in terms of performance
     *
    public CenterPerformance(List<Integer> differenceList, List<Integer> maxSumSubList, int maxSum, TimeUtils startTime, TimeUtils endTime) {
    this.differenceList = differenceList;
    this.maxSumSubList = maxSumSubList;
    this.maxSum = maxSum;
    this.startTime = startTime;
    this.endTime = endTime;
    }*/



    /**
     * returns the list that represents the influx/outflow of people in a certain vaccination center
     *
     * @return list that represents the influx/outflow of people in a certain vaccination center
     */
    public List<Integer> getDifferenceList() {
        return differenceList;
    }

    /**
     * returns list that represents the period of maximum sum of influx of people, which corresponds to the influx/outflow that has worse performance
     *
     * @return list that represents the period of maximum sum of influx of people, which corresponds to the influx/outflow that has worse performance
     */

    public List<Integer> getMaxSumSubList() {
        return maxSumSubList;
    }

    /**
     * returns maximum sum of influx of people in the worst performing interval
     *
     * @return sum of influx of people in the worst performing interval
     */
    public int getMaxSum() {
        return maxSum;
    }

    /**
     * returns start time of the worst time period in terms of performance
     *
     * @return start time of the worst time period in terms of performance
     */
    public TimeUtils getStartTime() {
        return startTime;
    }

    /**
     * returns end time of the worst time period in terms of performance
     *
     * @return end time of the worst time period in terms of performance
     */
    public TimeUtils getEndTime() {
        return endTime;
    }

    /**
     * returns vaccination center which is being analyzed
     * @return  vaccination center which is being analyzed
     */
    public VaccinationCenter getVc() {
        return vc;
    }

    /**
     * returns the execution time of the maximum sum contiguous sublist algorithm
     * @return the execution time of the maximum sum contiguous sublist algorithm
     */
    public long getExecTime() {
        return execTime;
    }

    /**
     * returns time interval in which to calculate the performance
     * @return time interval in which to calculate the performance
     */
    public int getTimeInterval() {
        return m;
    }

    /**
     * set the list that represents the influx/outflow of people in a certain vaccination center
     * @param differenceList the list that represents the influx/outflow of people in a certain vaccination center
     */
    public void setDifferenceList(List<Integer> differenceList) {
        this.differenceList = differenceList;
    }

    /**
     * set the list that represents the period of maximum sum of influx of people, which corresponds to the influx/outflow that has worse performance
     *
     * @param maxSumSubList the list that represents the period of maximum sum of influx of people, which corresponds to the influx/outflow that has worse performance
     *
     */

    public void setMaxSumSubList(List<Integer> maxSumSubList) {
        this.maxSumSubList = maxSumSubList;
    }

    /**
     *
     * set the maximum sum of influx of people in the worst performing interval
     *
     * @param maxSum sum of influx of people in the worst performing interval
     */
    public void setMaxSum(int maxSum) {
        this.maxSum = maxSum;
    }

    /**
     * set the start time of the worst time period in terms of performance
     *
     * @param startTime time of the worst time period in terms of performance
     */
    public void setStartTime(TimeUtils startTime) {
        this.startTime = startTime;
    }

    /**
     * set the end time of the worst time period in terms of performance
     * @param endTime end time of the worst time period in terms of performance
     */
    public void setEndTime(TimeUtils endTime) {
        this.endTime = endTime;
    }

    /**
     * sets the execution time of the maximum sum contiguous sublist algorithm
     * @param execTime execution time of the maximum sum contiguous sublist algorithm
     */
    public void setExecTime(long execTime) {
        this.execTime = execTime;
    }

    /**
     * creates the ist that represents the influx/outflow of people in a certain vaccination center
     *
     * @param vaList vaccine administration's list of a vaccination center
     * @param m      time interval in which to analyze the center's performance
     * @return list that represents the influx/outflow of people in a certain vaccination center
     */
    public List<Integer> createDifferenceList(VaccinationCenter vc, List<VaccineAdministration> vaList, int m) {
        boolean wholeDifference=false;
        differenceList = new ArrayList<>();
        TimeUtils startTime = new TimeUtils(TimeUtils.setTime(vc.getOpenHour()));
        TimeUtils endTime = (new TimeUtils(startTime)).addMinutes(m);
        if(endTime.isBiggerOrEquals(TimeUtils.setTime(vc.getCloseHour()))){
            wholeDifference=true;
        }

        int contIn = 0;
        int contOut = 0;

        do {
            for (VaccineAdministration va : vaList) {
                if (va.getArrivalTime().isBiggerOrEquals(startTime) && endTime.isBigger(va.getArrivalTime())) {
                    contIn++;

                }
                if (va.getLeavingTime().isBiggerOrEquals(startTime) && endTime.isBigger(va.getLeavingTime())) {
                    contOut++;
                }
            }

            differenceList.add(contIn - contOut);

            contIn = 0;
            contOut = 0;

            startTime.addMinutes(m);
            endTime.addMinutes(m);

        } while (TimeUtils.setTime(vc.getCloseHour()).isBiggerOrEquals(endTime) && !wholeDifference);

        return differenceList;
    }



    /**
     * evaluates center performance
     *
     */
    public void evaluatePerformance() throws IOException {
        GetPropertyValues getProps = new GetPropertyValues();
        this.differenceList = createDifferenceList(vc, vaList, m);
        String algorithm = getProps.getPropValues("MaxSumSublistAlgorithm");

        if (algorithm.equals("Bruteforce")) {
            BruteforceCPAdapter bfAdapter = new BruteforceCPAdapter(this);
            bfAdapter.evaluateCenterPerformance();
        } else if (algorithm.equals("Benchmark")) {
            BenchmarkCPAdapter bmAdapter = new BenchmarkCPAdapter(this);
            bmAdapter.evaluateCenterPerformance();
        }

        System.out.println("Execution time in seconds: "+(double) execTime /1000000+" ms");


    }



    /**
     * calculates the time corresponding to the provided index
     * @param i index to evaluate
     * @return time that corresponds to index
     */
    public TimeUtils calculateTimeFromIndex(int i){
        TimeUtils time=TimeUtils.setTime(vc.getOpenHour());

        if(i!=0) {
            for (int j = 0; j < i; j++) {
                time.addMinutes(m);
            }
        }
        return time;
    }
}

