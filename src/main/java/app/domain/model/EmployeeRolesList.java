package app.domain.model;

public enum EmployeeRolesList {
        RECEPTIONIST, NURSE, CENTER_COORDINATOR;
}
