package app.domain.model;

import app.domain.utils.DateUtils;
import app.domain.utils.TimeUtils;


import java.io.Serializable;
import java.util.Date;
import java.util.Date;


/**
 * @author João Miguel
 */
public class Appointment  implements Serializable {

    /**
     * The SNS number of the user that performed this appointment
     */
    private int snsNumber;

    /**
     * Appointment date
     */
    private DateUtils date;

    /**
     * time of appointment
     */
    private TimeUtils time;

    /**
     * Constructor for an appointment
     *
     * @param snsNumber the SNS number of the user that performed this appointment
     * @param date      Appointment date
     * @param time      Time of appointment
     */
    public Appointment(int snsNumber, DateUtils date, TimeUtils time) {
        this.snsNumber = snsNumber;
        this.date = date;
        this.time = time;
    }

    /**
     * Get SNS User Number.
     *
     * @return Returns the SNS User Number.
     */
    public int getSnsNumber() {
        return snsNumber;
    }

    /**
     * Set the SNS User Number.
     *
     * @param snsNumber The SNS User Number.
     */
    public void setSnsNumber(int snsNumber) {
        this.snsNumber = snsNumber;
    }

    /**
     * Get the Appointment Date
     *
     * @return Returns the Appointment Date.
     */
    public DateUtils getDate() {
        return date;
    }

    /**
     * Get the Appointment time
     *
     * @return Returns the time of appointment
     */
    public TimeUtils getTime() {
        return time;
    }

    /**
     * Set the Appointment time.
     *
     * @param time The Appointment time.
     */
    public void setTime(TimeUtils time) {
        this.time = time;
    }

    /**
     * Set the Appointment date.
     *
     * @param date The Appointment date.
     */
    public void setDate(DateUtils date) {
        this.date = date;
    }





    //ANNA

    /**
     * The vaccine type (Covid, tetanus, etc) that will be taken by the SNSUSer
     */
    private VaccineType vaccineType;

    /**
     * The vaccination center chosed by the SNSUser where they want to take the vaccine
     */
    private VaccinationCenter vaccinationCenter;

    /**
     * Constructor for an appointment
     *
     * @param snsNumber         the SNS number of the user that performed this appointment
     * @param vaccineType       The vaccine type (Covid, tetanus, etc) that will be taken by the SNSUSer
     * @param vaccinationCenter The vaccination center chosed by the SNSUser where they want to take the vaccine
     * @param date              Appointment date
     * @param time              Time of appointment
     */
    public Appointment(int snsNumber, VaccineType vaccineType, VaccinationCenter vaccinationCenter, DateUtils date, TimeUtils time) {
        this.snsNumber = snsNumber;
        this.vaccineType = vaccineType;
        this.vaccinationCenter = vaccinationCenter;
        this.date = date;
        this.time = time;
    }


    @Override
    public String toString() {
        return "Appointment data:" + "\n" +
                "SNS Number:" + snsNumber + "\n" +
                "Vaccine:" + vaccineType.getDesignation() + "\n" +
                "Vaccination Center:" + vaccinationCenter.getName() + "\n" +
                "Date:" + date.toString() + "\n" +
                "Time:" + time.toStringHHMM() + "\n";


    }


    /**
     * Get vaccine type.
     *
     * @return Returns the vaccine type.
     */
    public VaccineType getVaccineType() {
        return vaccineType;
    }

    /**
     * Get vaccination center
     * @return returns vaccination center.
     */
    public VaccinationCenter getVaccinationCenter(){return vaccinationCenter;}


}
