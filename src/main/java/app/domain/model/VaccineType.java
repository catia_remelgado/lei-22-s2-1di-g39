package app.domain.model;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * @author alexandraipatova
 * @author João Miguel
 */
public class VaccineType implements Serializable {


    /**
     * validates the vaccine type information (locally)
     * @return true if the vaccine is valid
     */
    public boolean validate(){
        return true;//to be coded
    }

    /**
     * returns information about a certain vaccine type
     * @return information about the vaccine type
     */
    @Override
    public String toString() {
        return designation+" vaccine type #"+code;
    }

    /**
     * The Vaccine Type's code
     */
    private String code;

    /**
     * The Vaccine Type's designation
     */

    private String designation;

    /**
     * Contructor of a Vaccine Type
     * @param code  The Vaccine Type's code
     * @param designation The Vaccine Type's designation
     */
    public VaccineType (String code, String designation) {
        checkCodeRules(code);
        checkDesignationRules(designation);
        this.code = code;
        this.designation = designation;

    }

    /**
     * Get Vaccine Type's code.
     *
     * @return Returns the Vaccine Type's code.
     */
    public String getCode() {
        return code;
    }

    /**
     * Set Vaccine Type's code.
     *
     * @param code The new Vaccine type code.
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Get Vaccine Type's Designation.
     *
     * @return Returns the Vaccine Type's code.
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * Set Vaccine Type's Designation.
     * @param designation The new vaccine type designation.
     */
    public void setDesignation(String designation) {
        this.designation = designation;
    }

    /**
     * Checks if the code is created with the correct rules.
     * @param code The new Vaccine type code.
     */
    private void checkCodeRules(String code) {
        if (StringUtils.isBlank(code))
            throw new IllegalArgumentException("Code cannot be blank.");
        if ( (code.length() < 4) || (code.length() > 8))
            throw new IllegalArgumentException("Code must have 4 to 8 chars.");
    }

    /**
     * Checks if the designation is created with the correct rules.
     * @param designation he new vaccine type designation.
     */
    private void checkDesignationRules(String designation){
        if (StringUtils.isBlank(designation))
            throw new IllegalArgumentException("Designation cannot be blank.");
        if ( (designation.length() > 40 ))
            throw new IllegalArgumentException("Designation cannot have more than 40 chars.");
    }


}
