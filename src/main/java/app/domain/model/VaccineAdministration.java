package app.domain.model;

import app.domain.utils.DateUtils;
import app.domain.utils.TimeUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author João Miguel
 * @author
 */
public class VaccineAdministration implements Serializable {

    /**
     * The SNS number of the user
     */
    private int snsNumber;

    /**
     * The Vaccine Name
     */
    private String vaccineName;

    /**
     * The Vaccine dose number
     */
    private static int doseNumber;

    /**
     * The Vaccine lot number
     */
    private String lotNumber;

    /**
     * Nurse Administration Date
     */
    private DateUtils administrationDate;

    /**
     * SNS User Arrival Time
     */
    private TimeUtils arrivalTime;

    /**
     * Scheduled Time
     */
    private TimeUtils scheduledTime;

    /**
     * Admiinistration time
     */
    private TimeUtils administrationTime;

    /**
     * SNS User Leaving Time
     */
    private TimeUtils leavingTime;
    private VaccineAdministration[] vaccineList;

    /**
     * @param snsNumber          The SNS User number
     * @param vaccineName        The Vaccine Name
     * @param doseNumber         The dose Number
     * @param lotNumber          The Vaccine Lot Number
     * @param administrationDate Nurse Administration Date
     * @param arrivalTime        SNS User arrival Time
     * @param scheduledTime      The Scheduled Time
     * @param leavingTime        SNS User leaving Time
     */
    public VaccineAdministration(int snsNumber, String vaccineName, int doseNumber, String lotNumber, DateUtils administrationDate, TimeUtils arrivalTime, TimeUtils scheduledTime, TimeUtils administrationTime, TimeUtils leavingTime) {

        this.snsNumber = snsNumber;
        this.vaccineName = vaccineName;
        this.doseNumber = doseNumber;
        this.lotNumber = lotNumber;
        this.administrationDate = administrationDate;
        this.arrivalTime = arrivalTime;
        this.scheduledTime = scheduledTime;
        this.administrationTime = administrationTime;
        this.leavingTime = leavingTime;
    }

    /**
     * Get SNS User Number.
     *
     * @return Returns the SNS User Number.
     */
    public int getSnsNumber() {
        return snsNumber;
    }

    /**
     * Get Vaccine Name.
     *
     * @return Returns the Vaccine Name.
     */
    public String getVaccineName() {
        return vaccineName;
    }

    /**
     * Get Dose Number.
     *
     * @return Returns the Dose Number.
     */
    public static int getDoseNumber() {
        return doseNumber;
    }

    /**
     * Get Lot Number.
     *
     * @return Returns the Lot Number.
     */
    public String getLotNumber() {
        return lotNumber;
    }

    /**
     * Get the Nurse Administration Date.
     *
     * @return Returns the Nurse Administration Date.
     */
    public DateUtils getAdministrationDate() {
        return administrationDate;
    }

    /**
     * Get the SNS User Arrival Time.
     *
     * @return Returns SNS User Arrival Time.
     */
    public TimeUtils getArrivalTime() {
        return arrivalTime;
    }

    /**
     * Get the appointment Scheduled Time.
     *
     * @return Returns Appointment Scheduled Time.
     */
    public TimeUtils getScheduledTime() {
        return scheduledTime;
    }

    /**
     * Get the SNS User Leaving Time.
     *
     * @return Returns SNS User Leaving Time.
     */
    public TimeUtils getLeavingTime() {
        return leavingTime;
    }

    /**
     * Set the SNS User Number.
     *
     * @param snsNumber The SNS User Number.
     */
    public void setSnsNumber(int snsNumber) {
        this.snsNumber = snsNumber;
    }

    /**
     * Set the Vaccine Name.
     *
     * @param vaccineName The Vaccine Name.
     */
    public void setVaccineName(String vaccineName) {
        this.vaccineName = vaccineName;
    }

    /**
     * Set the number of Doses administered.
     *
     * @param doseNumber The dose Number.
     */
    public void setDoseNumber(int doseNumber) {
        this.doseNumber = doseNumber;
    }

    /**
     * Set the Vaccine Lot Number.
     *
     * @param lotNumber The Vaccine Lot number.
     */
    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }

    /**
     * Set the Administration Date.
     *
     * @param administrationDate Nurse Administration Date.
     */
    public void setAdministrationDate(DateUtils administrationDate) {
        this.administrationDate = administrationDate;
    }

    /**
     * Set the user arrival Time.
     *
     * @param arrivalTime The SNS User Arrival Time.
     */
    public void setArrivalTime(TimeUtils arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    /**
     * Set the appointment scheduled Time.
     *
     * @param scheduledTime The Appointment Scheduled Time.
     */
    public void setScheduledTime(TimeUtils scheduledTime) {
        this.scheduledTime = scheduledTime;
    }

    /**
     * Set the user Leaving Time.
     *
     * @param leavingTime The SNS User leaving Time.
     */
    public void setLeavingTime(TimeUtils leavingTime) {
        this.leavingTime = leavingTime;
    }

    /**
     * Get the administration time
     *
     * @return administration time
     */
    public TimeUtils getAdministrationTime() {
        return administrationTime;
    }


    /**
     * Method to verify if a lot number is in the correct format
     * (has five alphanumeric characters an hyphen and two numerical characters (examples: 21C16-05 and A1C16-22 ))
     * link to moodle: https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16922#p21821
     *
     * @param lotNumber
     * @return
     */
    public static boolean confirmLotNumber(String lotNumber) {
        boolean correct = false;
        //separates string in parts (criteria to separate is what is the content of string separator being | an or)
        String[] passInArray = lotNumber.split("-");
        //transforms the separated string into an array
        ArrayList<String> passArrayList = new ArrayList<String>(Arrays.asList(passInArray));

        try {
            try {
                int num = Integer.parseInt(passArrayList.get(1));
                correct = true;
                if (passArrayList.get(1).split("").length != 2) {
                    correct = false;
                }
            } catch (NumberFormatException e) {
                correct = false;
            }
            if (correct == true) {
                if (passArrayList.get(0).split("").length != 5) {
                    correct = false;
                }
            }
        } catch (IndexOutOfBoundsException e) {
            correct = false;
        }

        return correct;
    }

    public List<Integer> totalNumberFullyVaccinatedInInterval(List<SNSUser> snsUserList, List<Vaccine> vaccineList, DateUtils date1, DateUtils date2, VaccinationCenter vaccinationCenter) {
        List<Integer> fullyVaccinatedList = new ArrayList<>();
        Date date = DateUtils.dateUtilsToDate(date1);

        do {
            int fullyVaccinated = 0;

            for (SNSUser snsUser : snsUserList) {
                for (Vaccine vaccine : vaccineList) {
                    int totalVaccineAdministration = totalVaccinesAdministration(snsUser, vaccine, DateUtils.dateToDateUtils(date), vaccinationCenter);
                    if (snsUserTotalVaccinated(snsUser, vaccine,date, totalVaccineAdministration)) {
                        fullyVaccinated++;
                    }
                }
            }
            fullyVaccinatedList.add(fullyVaccinated);
            DateUtils.addDays(date, 1);
        }while(date2.isBiggerOrEquals(date1));
        return fullyVaccinatedList;
    }

    private boolean snsUserTotalVaccinated(SNSUser snsUser, Vaccine vaccine,Date date ,int totalVaccineAdministration) {
        boolean snsUserTotalVaccinated = false;

        for(AgeGroup ageGroup : vaccine.getAgeGroupList()){
            int age = DateUtils.currentAge(snsUser.getBirthdate(), DateUtils.currentDate());
            if(age >= ageGroup.getMinimumAge() && age <= ageGroup.getMaximumAge()){
                snsUserTotalVaccinated = VaccineAdministration.getDoseNumber()== totalVaccineAdministration;
            }
        }
        return snsUserTotalVaccinated;
    }

    private int totalVaccinesAdministration(SNSUser snsUser, Vaccine vaccine, DateUtils date, VaccinationCenter vaccinationCenter) {
        int total = 0;
        for (VaccineAdministration list : this.vaccineList) {
            boolean temp = list.getSnsNumber() == snsUser.getSnsNumber();
            if (temp && vaccine.equals(list.getVaccineName()) && date.isBiggerOrEquals(list.getAdministrationDate()) &&
                    list.getVaccinationCenter().equals(vaccinationCenter)){
                total++;

            }
        }
        return total;
    }

    private Object getVaccinationCenter() {
        return this.getVaccinationCenter();
    }
}

