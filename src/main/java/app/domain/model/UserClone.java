package app.domain.model;

import app.domain.shared.Constants;
import pt.isep.lei.esoft.auth.domain.model.Email;
import pt.isep.lei.esoft.auth.domain.model.Password;

import java.io.Serializable;

/**
 * User clone class to serialize user data
 * @author alexandraipatova
 */
public class UserClone implements Serializable {

    private String id;
    private String password;
    private String name;
    private String role;

    public UserClone(String name,String id,String password, String role) {
        this.id = id;
        this.password = password;
        this.name = name;
        this.role = role;
    }

    public String getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getRole() {
        return role;
    }

    @Override
    public String toString() {
        return "UserClone{" +
                "id='" + id + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}
