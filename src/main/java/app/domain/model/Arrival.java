package app.domain.model;

import app.domain.utils.DateUtils;
import app.domain.utils.TimeUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * Represents an arrival of an sns user
 * @author alexandraipatova
 */
public class Arrival  implements Serializable {
    /**
     * Arrival time of sns user
     */
    private TimeUtils arrivalTime;

    /**
     * user that performed the arrival
     */
    private SNSUser user;

    /**
     * appointment that the user has arrived to do
     */
    private Appointment appointment;

    /**
     * Constructor for arrival with arrivalTime, user and an appointment
     * @param arrivalTime time of arrival
     * @param user user that performed the arrival
     * @param appointment appointment that is related to the arrival
     */
    public Arrival(TimeUtils arrivalTime, SNSUser user, Appointment appointment) {
        this.arrivalTime = arrivalTime;
        this.user = user;
        this.appointment = appointment;
    }


    /**
     * returns arrival time
     * @return arrival time
     */
    public TimeUtils getArrivalTime() {
        return arrivalTime;
    }

    /**
     * returns sns number of sns user
     * @return sns number of sns user
     */
    public int getSNSNumber(){
       return user.getSnsNumber();
    }

    /**
     * returns name of sns user
     * @return name of sns user
     */
    public String getName(){
        return user.getName();
    }

    /**
     * returns scheduled time of appointment
     * @return scheduled time of appointment
     */
    public TimeUtils getScheduledTime(){
        return appointment.getTime();
    }

    /**
     * returns birthdate of sns number
     * @return birthdate of sns user
     */
    public DateUtils getBirthDate() {
        return user.getBirthdate();
    }

    /**
     * returns sex of sns number
     * @return sex of sns user
     */
    public String getSex() {
        return user.getGender().toString();
    }

    /**
     * returns sns phone number of sns user
     * @return sns phone number of sns user
     */
    public int getPhoneNumber(){
        return user.getPhoneNumber();
    }

}
