package app.domain.model;

import pt.isep.lei.esoft.auth.domain.model.Email;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Employee implements Serializable {
    EmployeeRolesList role;
    String name, adress;
    String email;
    int phoneNumber;
    int CCNumber;

    /**
     * Constructor of an employee
     * @param role        employee's role
     * @param name        employee's name
     * @param adress      employee's adress
     * @param email       employee's email
     * @param phoneNumber employee's phoneNumber
     * @param CCNumber    employee's Citizen Card Number
     */
    public Employee(EmployeeRolesList role, String name, String adress, Email email, int phoneNumber, int CCNumber) {
        this.role = role;
        this.name = name;
        this.adress = adress;
        this.email = email.getEmail();
        this.phoneNumber = phoneNumber;
        this.CCNumber = CCNumber;
    }

    @Override
    public String toString() {
        return "Employee:" +
                " name='" + name + '\'' +
                ", role=" + role +
                ", adress='" + adress + '\'' +
                ", number=" + phoneNumber +
                ", email=" + email +
                ", CCNUmber=" + CCNumber;
    }

    /**
     *
     * @return citizen card number of an employee
     */
    public int getCCNumber() {
        return CCNumber;
    }

    /**
     *
     * @return the role of an employee
     */
    public EmployeeRolesList getRole() {
        return role;
    }
}
