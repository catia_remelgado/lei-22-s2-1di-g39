package app.domain.model;
/*import app.domain.model.VaccinationCenter;*/
import app.controller.App;
import app.domain.store.*;
import app.domain.utils.DailyAdministrationReportTask;
import app.domain.utils.SerializationHelper;
import org.apache.commons.lang3.StringUtils;
import pt.isep.lei.esoft.auth.AuthFacade;
import pt.isep.lei.esoft.auth.mappers.dto.UserDTO;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Company implements Serializable {


    /**
     * designation of the company
     */
    private String designation;
    /**
     *all owned vaccines by the company
     */
    private VaccineStore vs;
    /**
     * all owned vaccine types by the company
     */
    private VaccineTypeStore vts;
    /**
     * all owned vaccination center by the company
     */
    private VaccinationCenterStore vaccinationCenterStore;


    private AppointmentStore appointmentStore;

    /**
     * register user's vaccination center.
     */
    private HashMap<UserDTO, VaccinationCenter> userVaccinationCenter;

    /**
     * necessary to create authfacade replica after deserializaiton
     */
    private UserCloneStore UCStore;

    private EmployeeStore employeeStore;

    private transient AuthFacade authFacade;

    private SNSUserStore snsUserStore;

    private VaccineAdministration vaccineAdministration;

    private SNSUserStore snsUser;

    public Company(String designation)
    {
        if (StringUtils.isBlank(designation))
            throw new IllegalArgumentException("Designation cannot be blank.");

        this.designation = designation;
        this.authFacade = new AuthFacade();
        this.vs=new VaccineStore();
        this.vts=new VaccineTypeStore();
        this.vaccinationCenterStore = new VaccinationCenterStore();
        this.userVaccinationCenter = new HashMap<>();
        this.appointmentStore = new AppointmentStore();
        this.UCStore=new UserCloneStore();
        this.employeeStore = new EmployeeStore();
        this.snsUserStore= new SNSUserStore();

    }

    public void scheduleDailyAdministrationReport(long delay, long interval){

        Timer timer=new Timer();

        DailyAdministrationReportTask dart= new DailyAdministrationReportTask();


        timer.scheduleAtFixedRate(dart, delay, 1000L * 60L * 60L * 24L);

    }

    public SNSUserStore getSnsUserStore() {
        return snsUserStore;
    }

    public UserCloneStore getUCStore() {
        return UCStore;
    }

    public void setUserVaccinationCenter(UserDTO user, VaccinationCenter center) {
        this.userVaccinationCenter.put(user, center);
    }

    public AppointmentStore getAppointmentStore() {
        return appointmentStore;
    }

    /**
     * Get the vaccination center
     * @param user The SNS User
     * @return returns the vaccination center.
     */
    public VaccinationCenter getUserVaccinationCenter(UserDTO user) {
        return this.userVaccinationCenter.get(user);
    }

    public String getDesignation() {
        return designation;
    }

    public AuthFacade getAuthFacade() {
        return authFacade;
    }

    /**
     * return the vaccine store of the company
     * @return vaccine store of the company
     */
    public VaccineStore getVaccineStore() {
        return vs;
    }

    /**
     * returns the vaccine type store of the company
     * @return vaccine type store of the company
     */
    public VaccineTypeStore getVaccineTypeStore() {
        return vts;
    }

    /**
     * return the vaccination center of the company
     * @return vaccination center of the company
     */
    public VaccinationCenterStore createVaccinationCenter() {
        return this.vaccinationCenterStore;
    }

    private boolean validateVaccinationCenter(VaccinationCenter center){
        return this.vaccinationCenterStore.hasVaccinationCenter(center);
    }

    public VaccinationCenterStore getVaccinationCenterStore() {
        return this.vaccinationCenterStore;
    }

    public EmployeeStore getEmployeeStore(){return this.employeeStore;}

    public SNSUserStore getSNSUser() {
        return this.snsUser;
    }

    public VaccineAdministration getVaccineAdministration() {
        return this.vaccineAdministration;
    }
}
