package app.domain.model;

import java.io.Serializable;

/**
 * Represents address with street, door number, building division, ZIPcode,city
 * @author alexandraipatova
 */
public class Address implements Serializable {

    /**
     * Name of the street that describes address
     * */
    private String street;

    /**
     * Door number of building of this address
     */
    private int doorNumber;

    /**
     * Building Division (if vaccination center is part of a bigger building, this field distinguishes it)
     */
    private String buildingDivision;

    /**
     * ZIP/Postal Code of address
     */
    private String ZIPcode;

    /**
     *city that describes the address
     */
    private String city;

    /**
     * Constructor for Address class
     * @param street Name of the street that describes address
     * @param doorNumber Door number of building of this address
     * @param buildingDivision division of building where the address might be
     * @param ZIPcode ZIP/Postal Code of address
     * @param city city that describes the address
     */
    public Address(String street, int doorNumber, String buildingDivision, String ZIPcode, String city) {
        this.street = street;
        this.doorNumber = doorNumber;
        this.buildingDivision = buildingDivision;
        this.ZIPcode = ZIPcode;
        this.city = city;
    }

    /**
     * Constructor that does not include building division, as it might not be necessary
     * @param street Name of the street that describes address
     * @param doorNumber Door number of building of this address
     * @param ZIPcode ZIP/Postal Code of address
     * @param city city that describes the address
     */
    public Address(String street, int doorNumber, String ZIPcode, String city) {
        this.street = street;
        this.doorNumber = doorNumber;
        this.ZIPcode = ZIPcode;
        this.city = city;
    }

    /**
     * returns street of address
     * @return street of address
     */
    public String getStreet() {
        return street;
    }

    /**
     * returns door number of address
     * @return door number of address
     */
    public int getDoorNumber() {
        return doorNumber;
    }

    /**
     * returns building division of address
     * @return building division of address
     */
    public String getBuildingDivision() {
        return buildingDivision;
    }

    /**
     * returns zip code of address
     * @return zip code of address
     */
    public String getZIPcode() {
        return ZIPcode;
    }

    /**
     * returns city of address
     * @return city of address
     */
    public String getCity() {
        return city;
    }
}
