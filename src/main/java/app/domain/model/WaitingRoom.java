package app.domain.model;

import app.domain.exceptions.UserAlreadyExistsException;
import app.domain.utils.TimeUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents waiting room of a vaccination center
 * @author alexandraipatova
 * @author João Miguel
 */
public class WaitingRoom implements Serializable {

    /**
     * list of arrivals of the waiting room
     */
    private List<Arrival> arrivalList;

    public WaitingRoom(List<Arrival> arrivalList) {
        this.arrivalList = arrivalList;
    }

    public WaitingRoom()
    {
        this.arrivalList = new ArrayList<>();
    }
    /**
     * returns list of current arrivals to the waiting room
     * @return list of current arrivals to the waiting room
     */
    public List<Arrival> getArrivalList() {
        return arrivalList;
    }

    /**
     *  Add a SNS User to the arrivalList
     * @param time The Arrival Time
     * @param user The SNS User
     * @param appointment The SNS appointment
     * @throws UserAlreadyExistsException
     */
    public void add(TimeUtils time, SNSUser user, Appointment appointment) throws UserAlreadyExistsException {

        if (this.userExist(user)) {
            throw new UserAlreadyExistsException("User's with " + user.getSnsNumber() + " number is already in this waiting room.");
        }
        this.arrivalList.add(new Arrival(time, user, appointment));
    }

    /**
     * Method to remove a User from one Waiting Room
     * @param user The SNS User
     * @return returns the SNS User arrival
     */
    public Arrival remove(SNSUser user){

        for(Arrival arrival : this.arrivalList){
            if(arrival.getSNSNumber() == user.getSnsNumber()){
                this.arrivalList.remove(arrival);
                return arrival;
            }
        }
        return null;
    }

    /**
     * Checks if the User already exists.
     * @param user The SNS User
     * @return Returns true(if the sns user exists) or false(if the sns user does not exist)
     */
    public boolean userExist(SNSUser user) {
        for (Arrival arrival : this.arrivalList) {
            if (arrival.getSNSNumber() == user.getSnsNumber()) {
                return true;
            }
        }

        return false;
    }

}
