package app.domain.model;

import app.controller.App;
import app.domain.exceptions.NotAWholeDivisionException;
import app.domain.exceptions.NotPositiveNumberException;
import app.domain.exceptions.UserAlreadyExistsException;
import app.domain.exceptions.VaccineAdministrationsNotFoundException;
import app.domain.utils.DateUtils;
import app.domain.utils.TimeUtils;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class VaccinationCenter implements Serializable {

    /**
     * center coordinator of this vaccination center
     */
    private Employee centerCoordinator;

    /**
     * Waiting room of the vaccination center
     */
    private WaitingRoom waitingRoom;

    private RecoveryRoom recoveryRoom;

    /**
     * type of vaccination center
     */
    private String type;
    /**
     * name of vaccination Center
     */
    private String name;


    /**
     * email of vaccination center
     */
    private String email;
    /**
     * phone of vaccination Center
     */
    private int phone;
    /**
     * fax number of vaccination Center
     */
    private int faxNumber;

    /**
     * website of vaccination Center
     */
    private String website;

    /**
     * open and close Hour of the vaccination Center
     */
    private String openHour;
    private String closeHour;

    /**
     * slot Duration of the vaccines in the vaccination Center
     */
    private int slotDuration;
    /**
     * vaccine Per slot in Vaccination Center
     */
    private int vaccinePerSlot;

    /**
     * max Capacity of the center Vaccination Center
     */
    private int capacity;
    private String street;
    private int doorNumber;
    private String ZIPcode;
    private String city;

    private List<VaccinationCenter> vcList;

    private List<VaccineAdministration> vaList;
    public VaccinationCenter() {

    }

    public VaccinationCenter(String type, String name, String street, int doorNumber, String ZIPcode, String city, int phone, String email, int faxNumber, String website, String openHour
            , String closeHour, int slotDuration, int vaccinePerSlot, int capacity) {
        this.type = type;
        this.name = name;
        this.street = street;
        this.doorNumber = doorNumber;
        this.ZIPcode = ZIPcode;
        this.city = city;
        this.phone = phone;
        this.email = email;
        this.faxNumber = faxNumber;
        this.website = website;
        this.openHour = openHour;
        this.closeHour = closeHour;
        this.slotDuration = slotDuration;
        this.vaccinePerSlot = vaccinePerSlot;
        this.capacity = capacity;
        this.vcList = new ArrayList<>();
        this.vaList = new ArrayList<>();
        this.waitingRoom = new WaitingRoom();
        this.recoveryRoom = new RecoveryRoom();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RecoveryRoom getRecoveryRoom() {
        return recoveryRoom;
    }

    /**
     * returns street of address
     * @return street of address
     */
    public String getStreet() {
        return street;
    }

    /**
     * returns door number of address
     * @return door number of address
     */
    public int getDoorNumber() {
        return doorNumber;
    }


    /**
     * returns zip code of address
     * @return zip code of address
     */
    public String getZIPcode() {
        return ZIPcode;
    }

    /**
     * returns city of address
     * @return city of address
     */
    public String getCity() {
        return city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public int getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(int faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getOpenHour() {
        return openHour;
    }

    public void setCloseHour(String closeHour) {
        this.closeHour = closeHour;
    }

    public void setOpenHour(String openHour) {
        this.openHour = openHour;
    }

    public String getCloseHour() {
        return closeHour;
    }


    public int getSlotDuration() {
        return slotDuration;
    }

    public void setSlotDuration(int slotDuration) {
        this.slotDuration = slotDuration;
    }

    public int getVaccinePerSlot() {
        return vaccinePerSlot;
    }

    public void setVaccinePerSlot(int vaccinePerSlot) {
        this.vaccinePerSlot = vaccinePerSlot;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getType() {
        return type;
    }

    public WaitingRoom getWaitingRoom() {
        return waitingRoom;
    }

    public void setWaitingRoom(WaitingRoom waitingRoom) {
        this.waitingRoom = waitingRoom;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setCenterCoordinator(Employee centerCoordinator) {
        this.centerCoordinator = centerCoordinator;
    }

    public VaccinationCenter createNewVc(String type, String name, String street, int doorNumber, String ZIPcode, String city, int phone, String email, int faxNumber, String website, String openHour
            , String closeHour, int slotDuration, int vaccinePerSlot, int capacity) {
        return new VaccinationCenter(type, name,  street,  doorNumber,  ZIPcode,  city, phone, email, faxNumber, website, openHour
                , closeHour, slotDuration, vaccinePerSlot, capacity);
    }

    public boolean saveVaccinationCenter(VaccinationCenter vc) {
        int cont = 0;

        for (VaccinationCenter newVaccinationCenter : vcList) {
            if (newVaccinationCenter.getName().equalsIgnoreCase(vc.getName()) ||
                    newVaccinationCenter.getStreet().equalsIgnoreCase(vc.getStreet()) ||
                    newVaccinationCenter.getCity().equalsIgnoreCase(vc.getCity()) ||
                    newVaccinationCenter.getZIPcode().equalsIgnoreCase(vc.getZIPcode()) ||
                    newVaccinationCenter.getEmail().equalsIgnoreCase(vc.getEmail())) {
                cont++;
                vcList.add(vc);
            }
        }
        if (cont < 1) {
            vcList.add(vc);
            return true;
        } else return false;

    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (obj instanceof VaccinationCenter) {

            VaccinationCenter center = (VaccinationCenter) obj;

            return center.getName().equals(this.name);
        }

        return false;
    }

    @Override
    public String toString() {
        return name + " Data related to Vaccination Center   \n Street:" + street + "\n Door number: " + doorNumber + "\n Zip Code: "+ ZIPcode+ "\n City: " + city;
    }


    /**
     * Method to verify if a given time is inside the working schedule of the vaccination center (considering the vaccination center's slot duration)
     *
     * @param time given time
     * @return true if time it's between vaccination center's working hours (considering the vaccination center's slot duration)
     */
    public boolean verifyIfTimeIsInsideWorkingSchedule(TimeUtils time) throws ParseException {
        //first possible schedule = to opening hours
        TimeUtils firstPossibleSchedule = TimeUtils.setTime(this.openHour);
        //last possible schedule = to closing hours - slot duration
        TimeUtils lastPossibleSchedule = TimeUtils.setTime(this.closeHour);
        lastPossibleSchedule.addOrTakeMinutes(lastPossibleSchedule.toStringHHMM(), -(this.slotDuration));
        //boolean idk = (firstPossibleSchedule.isBiggerOrEquals(time) && time.isBiggerOrEquals(lastPossibleSchedule));
        //if (firstPossibleSchedule.isBigger(time) && time.isBigger(lastPossibleSchedule)) {
        /*  if (idk == false){
            return false;
        }
        return true;*/

        boolean idk = (time.isBiggerOrEquals(firstPossibleSchedule) && lastPossibleSchedule.isBiggerOrEquals(time));
        if (idk == true) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Add a sns user to waiting room.
     *
     * @param user        The SNS User
     * @param time        The Arrival Time
     * @param appointment The SNS appointment
     * @throws UserAlreadyExistsException
     */
    public void addToWaitingRoom(SNSUser user, TimeUtils time, Appointment appointment) throws UserAlreadyExistsException {

        this.waitingRoom.add(time, user, appointment);
    }

    /**
     * compares the vaccination center's center coordinator with the current login
     * @return true if the current session's user is the same as the center coordinator
     */
    public boolean compareCenterCoordinatorWithCurrentLogin(){
        if(App.getInstance().getCurrentUserSession().getUserId().toString().equals((centerCoordinator.email))){
            return true;
        }else{
            return false;
        }
    }

    /**
     *gets the vaccine administration list of a certain day
     * @return list of vaccine administrations of a center on a certain date
     */
    public List<VaccineAdministration> getVaccineAdministrationListByDay(DateUtils day){
        List<VaccineAdministration> vaListByDay=new ArrayList<>();
        if(vaList==null){
            throw new VaccineAdministrationsNotFoundException("There are no vaccine administrations corresponding to that date in the vaccination center!");
        }

        for(VaccineAdministration va:vaList){
           if( va.getAdministrationDate().equals(day)){
            vaListByDay.add(va);
           }
        }
        if(vaListByDay.isEmpty()){
            throw new VaccineAdministrationsNotFoundException("There are no vaccine administrations corresponding to that date in the vaccination center!");
        }
        return vaListByDay;
    }

    /**
     * evaluates the center's performance with calculations of center performance class
     * @param day day that the user wishes to evaluate the center
     * @param m time interval in minutes to be analyzed
     * @return center performance with all data regarding performance of this center
     */
    public CenterPerformance evaluateCenterPerformance(DateUtils day, int m) throws IOException {
        CenterPerformance cp=new CenterPerformance(this,getVaccineAdministrationListByDay(day),m);
        cp.evaluatePerformance();
        return cp;
    }

    public void verifyTimeIntervalForPerformance (int m) throws NotAWholeDivisionException {
        TimeUtils totalTime = TimeUtils.setTime(openHour).differenceInTime(TimeUtils.setTime(closeHour));
        double totalMinutes = (double)totalTime.getHour()*60 + totalTime.getMinute()/(double)m;
        if(m<=0){
            throw new NotPositiveNumberException("Time interval cannot be zero!");
        }
        if((totalMinutes/m)-(int)(totalMinutes/m)!=0){
            throw new NotAWholeDivisionException("Time interval for performance inserted is invalid!");
        }



    }



    /**
     * Method to verify if an object is new or not to the center' vaccine administration list
     * If the object is new to the list is added to the list
     * @param object object to see if is new
     */
    public void verifyRepeatedObjects (VaccineAdministration object){
        boolean isRepeated = false;
        int i = 0;
        if (!this.vaList.isEmpty()) {
            do {
                if (this.vaList.get(i).getSnsNumber() == object.getSnsNumber() && this.vaList.get(i).getVaccineName().equals(object.getVaccineName()) && this.vaList.get(i).getDoseNumber() == object.getDoseNumber()) {
                    isRepeated = true;
                }
                i++;
            } while (i < this.vaList.size() && isRepeated == false);
        }
        if (isRepeated==false){
            addObjectToVAList(object);
        }
    }

    /**
     * Method to add an vaccine administration object to the center's vaccine administration list
     * @param vacAdmin the object to add to the list
     */
    public void addObjectToVAList(VaccineAdministration vacAdmin){
        this.vaList.add(vacAdmin);
    }

    /**
     * Method to get the vaccination center's vaccine administration list
     * @return the center's vaccine administration list
     */
    public List<VaccineAdministration> getVaList() {
        return vaList;
    }
}
