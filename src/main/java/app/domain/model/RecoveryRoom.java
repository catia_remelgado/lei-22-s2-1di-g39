package app.domain.model;

import app.domain.exceptions.UserAlreadyExistsException;
import app.domain.utils.TimeUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents waiting room of a vaccination center
 * @author alexandraipatova
 * @author João Miguel
 */
public class RecoveryRoom implements Serializable {

    /**
     * list of arrivals of the recovery room
     */
    private List<Recovery> recoveryList;

    public RecoveryRoom()
    {
        this.recoveryList = new ArrayList<>();
    }
    /**
     * returns list of current arrivals to the Recovery room
     * @return list of current arrivals to the Recovery  room
     */
    public List<Recovery> getRecoveryList() {
        return recoveryList;
    }

    /**
     *  Add a SNS User to the Recovery Room
     * @param time The Arrival Time
     * @param user The SNS User
     * @throws UserAlreadyExistsException
     */
    public void add(SNSUser user, TimeUtils time) throws UserAlreadyExistsException {

        if (this.userExist(user)) {
            throw new UserAlreadyExistsException("User's with " + user.getSnsNumber() + " number is already in this waiting room.");
        }
        this.recoveryList.add(new Recovery(user, time));
    }

    /**
     * Add a SNS User to the Recovery Room
     * @param user The SNS User
     * @throws UserAlreadyExistsException
     */
    public void add(SNSUser user) throws UserAlreadyExistsException {
        this.add(user, TimeUtils.currentTime());
    }



    /**
     * Checks if the User already exists.
     * @param user The SNS User
     * @return Returns true(if the sns user exists) or false(if the sns user does not exist)
     */
    public boolean userExist(SNSUser user) {
        for (Recovery recovery : this.recoveryList) {
            if (recovery.getSNSNumber() == user.getSnsNumber()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method to remove a SNS User from the Recovery Room
     * @param snsUser The SNS User
     * @return returns the SNS User recovery
     */
    public Recovery remove(SNSUser snsUser) {

        for(Recovery recovery : this.recoveryList){
            if(recovery.getSNSNumber() == snsUser.getSnsNumber()){
                this.recoveryList.remove(recovery);
                return recovery;
            }
        }
        return null;
    }
}
