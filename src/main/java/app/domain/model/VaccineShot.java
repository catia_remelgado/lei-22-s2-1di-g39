package app.domain.model;

import app.domain.utils.DateUtils;

import java.io.Serializable;

/**
 * @author João Miguel
 */
public class VaccineShot implements Serializable {

    /**
     *  The administered vaccine
     */
    private Vaccine vaccine;

    /**
     * Date of vaccination day
     */
    private DateUtils date;

    /**
     * Constructor for an Vaccine Shot
     * @param vaccine The administered vaccine
     * @param date Date of vaccination
     */
    public VaccineShot(Vaccine vaccine, DateUtils date) {
        this.vaccine = vaccine;
        this.date = date;
    }

    /**
     * Method to get the Vaccine administered
     * @return returns the administered vaccine
     */
    public Vaccine getVaccine() {
        return vaccine;
    }

    /**
     * Method to set the vaccine
     * @param vaccine The administered vaccine
     */
    public void setVaccine(Vaccine vaccine) {
        this.vaccine = vaccine;
    }

    /**
     * Method to get the Vaccine administration day
     * @return returns the date of vaccination
     */
    public DateUtils getDate() {
        return date;
    }

    /**
     * Method to set the date of vaccination
     * @param date The date of vaccination
     */
    public void setDate(DateUtils date) {
        this.date = date;
    }
}
