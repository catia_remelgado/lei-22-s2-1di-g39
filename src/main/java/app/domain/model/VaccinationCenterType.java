package app.domain.model;

import java.io.Serializable;

public enum VaccinationCenterType implements Serializable {
    HealthCareCenter, MassCommunityVaccinationCenter;
}
