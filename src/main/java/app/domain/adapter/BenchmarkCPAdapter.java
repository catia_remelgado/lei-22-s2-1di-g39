package app.domain.adapter;

import app.domain.model.CenterPerformance;
import app.domain.model.CenterPerformanceInterface;
import com.isep.mdis.Sum;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * implements the benchmark algorithm to calculate maximum sum contiguous sublist
 */

public class BenchmarkCPAdapter implements CenterPerformanceInterface {

    /**
     * the center performance object which contains center performance data
     */
    private CenterPerformance cp;

    /**
     * constructor for benchmark algorithm which receiver the center performance object with relevant data
     * @param cp
     */
    public BenchmarkCPAdapter(CenterPerformance cp) {
        this.cp = cp;
    }

    /**
     *
     * evaluates center performance using the benchmark algorithm
     * @return maximum sum contiguous sublist
     */
    @Override
    public List<Integer> evaluateCenterPerformance() {
        int[] diffList =cp.getDifferenceList().stream().mapToInt(i -> i).toArray(); //https://stackoverflow.com/questions/718554/how-to-convert-an-arraylist-containing-integers-to-primitive-int-array
        long start = System.nanoTime();
        int [] sumList=Sum.Max(diffList);
        long end = System.nanoTime();
        cp.setExecTime(end-start);
        cp.setMaxSumSubList(Arrays.stream(sumList).boxed().collect(Collectors.toList())); //https://www.baeldung.com/java-primitive-array-to-list
        setTimeAndMaxSum();
        return cp.getMaxSumSubList();
    }

    /**
     * sets the start and finish time and maximum sum according to the worst performance
     */
    public void setTimeAndMaxSum(){

        int sum=0;
        for(int element : cp.getMaxSumSubList()) {
            sum += element;
        }
        cp.setMaxSum(sum);
        boolean flag=false;
        int i=0;
        int j=0;

        while(!flag && i < cp.getDifferenceList().size() - cp.getMaxSumSubList().size()) {

            for (j = i; j < cp.getMaxSumSubList().size(); j++) {
                if(cp.getDifferenceList().get(j).equals(cp.getMaxSumSubList().get(j))){
                    flag=true;
                }else{
                    flag=false;
                }
            }
            i++;

        }
        cp.setStartTime(cp.calculateTimeFromIndex(i-1));
        cp.setEndTime(cp.calculateTimeFromIndex(j-1));


    }
}
