package app.domain.adapter;

import app.domain.model.CenterPerformance;
import app.domain.model.CenterPerformanceInterface;
import com.isep.mdis.Sum;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * implements the bruteforce algorithm to calculate maximum sum contiguous sublist
 */
public class BruteforceCPAdapter implements CenterPerformanceInterface {
    /**
     * the center performance object which contains center performance data
     */
    private CenterPerformance cp;

    /**
     * constructor for bruteforce algorithm which receiver the center performance object with relevant data
     * @param cp
     */
    public BruteforceCPAdapter(CenterPerformance cp) {
        this.cp = cp;
    }

    /**
     * https://www.baeldung.com/java-primitive-array-to-list
     * evaluates center performance using the benchmark algorithm
     * @return maximum sum contiguous sublist
     */
    @Override
    public List<Integer> evaluateCenterPerformance() {
        calculateMaxSumSubList(cp.getDifferenceList());
        return cp.getMaxSumSubList();
    }

    /**
     * calculates the maximum sum of elements in an array
     */
    public void calculateMaxSumSubList(List<Integer> integerList){
        long start = System.nanoTime();
        int sum = 0;
        int maxi = 0;
        int maxj = 0;
        int maxSum = integerList.get(0);
        for (int i = 0; i < integerList.size(); i++) {
            for (int j = i; j < integerList.size(); j++) {
                sum += integerList.get(j);
                if (sum > maxSum) {
                    maxSum = sum;
                    maxi = i;
                    maxj = j;
                }

            }
            sum = 0;
        }
        long end = System.nanoTime();
        cp.setExecTime(end - start);
        cp.setMaxSumSubList(integerList.subList(maxi, maxj + 1));
        cp.setMaxSum(maxSum);
        cp.setStartTime(cp.calculateTimeFromIndex(maxi));
        cp.setEndTime(cp.calculateTimeFromIndex(maxj));
    }

  /*
    procedure calculateMaxSumSubList(integer,integerList[1],integerList[2],...,integerList[n]:increasing integers)
        sum := 0
        maxi := 0
        maxj := 0
        maxSum = integerList[0]
                for i:= 0 to n
                    for j:= i to n
                        sum = sum + integerList[j]
                        if sum > maxSum
                            maxSum := sum
                            maxi := i-1
                            maxj := j-1
                    sum := 0
        return sublist of integerList from maxi to maxj
        */
}
