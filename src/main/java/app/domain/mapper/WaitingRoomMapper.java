package app.domain.mapper;

import app.domain.dto.ArrivalDTO;
import app.domain.dto.WaitingRoomDTO;
import app.domain.model.Arrival;
import app.domain.model.WaitingRoom;
import app.domain.utils.DateUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * transforms waiting room to a dto
 * @author alexandraipatova
 */
public class WaitingRoomMapper {

    /**
     * transforms a waiting room to a dto with necessary information
     * @param wr waiting room to be transformed
     * @return waiting room dto with necessary information
     */
    public List<ArrivalDTO> waitingRoomToDTO(WaitingRoom wr){
        List<Arrival> arrivals = wr.getArrivalList();
        List<ArrivalDTO> arrivalsDTO = new ArrayList<>();

        for (Arrival arrivalFromList : arrivals) {
            arrivalsDTO.add(arrivalToDTO(arrivalFromList));
        }
        return arrivalsDTO;

    }

    /**
     * transform arrivals to arrival dto's
     * @param arrival arrival to be trasnformed
     * @return arrival dto with necessary information
     */
    private ArrivalDTO arrivalToDTO(Arrival arrival){
        return new ArrivalDTO(
                arrival.getArrivalTime(),
                arrival.getSNSNumber(),
                arrival.getName(),
                new DateUtils(arrival.getBirthDate().getYear(),arrival.getBirthDate().getMonth(),arrival.getBirthDate().getDay()),
                arrival.getSex(),
                arrival.getScheduledTime(),
                arrival.getPhoneNumber());
    }
}
