package app.domain.mapper;

import app.domain.dto.ArrivalDTO;
import app.domain.dto.SNSUserDTO;
import app.domain.model.Gender;
import app.domain.model.SNSUser;
import app.domain.utils.DateUtils;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.util.ArrayList;
import java.util.List;

/**
 * transforms information loaded from a file o a DTO
 * @author alexandraipatova
 */
public class LoadFromFileMapper {

    /**
     * transforms a matrix of strings in a list of dtos with information to create a set of users
     * @param fileUserInfo matrix of strings to be transformed in a list of sns user dtos
     * @return list of sns user dtos
     */

    public List<SNSUserDTO> fileInfoToDTO(List<List<String>> fileUserInfo){
        List<SNSUserDTO> SNSUserDTOs= new ArrayList<>();

        for (List<String> userInfo : fileUserInfo) {
            SNSUserDTOs.add(fileLineToDTO(userInfo));
        }
        return SNSUserDTOs;

    }

    /**
     * tranforms a fileLine in a sns user dto
     * @param fileLine file line that represents the sns user
     * @return an sns user dto
     */
    private SNSUserDTO fileLineToDTO(List<String> fileLine){
        Gender gender = Gender.NONE;

        if (fileLine.get(1).equals("Masculino")) {
            gender = Gender.MALE;
        } else if(fileLine.get(1).equals("Feminino")) {

            gender = Gender.FEMALE;

        }

        return new SNSUserDTO(
                Integer.parseInt(fileLine.get(6)),
                fileLine.get(0),
                DateUtils.setDate(fileLine.get(2),"/"),
                new Email(fileLine.get(5)),
                Integer.parseInt(fileLine.get(4)),
                gender,
                Integer.parseInt(fileLine.get(7)),
                fileLine.get(3));
    }

    /**
     * transforms an sns user dto into an sns user
     * @param snsuserDTO sns user dto to transform into an sns user
     * @return sns user
     */
    public SNSUser DTOtoUser(SNSUserDTO snsuserDTO){

        return new SNSUser(
                snsuserDTO.getSnsNumber(),
                snsuserDTO.getName(),
                snsuserDTO.getBirthdate(),
                snsuserDTO.getEmail(),
                snsuserDTO.getPhoneNumber(),
                snsuserDTO.getGender(),
                snsuserDTO.getCCnumber(),
                snsuserDTO.getAddress());
    }

    /**
     * transforms a list of SNSUserDTO's into an SNS user list
     * @param snsUserDTOList list of sns user dto's
     * @return list of sns users
     */
    public List<SNSUser> DTOListToUserList(List<SNSUserDTO> snsUserDTOList){
        List<SNSUser> SNSUserList= new ArrayList<>();

        for (SNSUserDTO snsUserDTO : snsUserDTOList) {
            SNSUserList.add(DTOtoUser(snsUserDTO));
        }
        return SNSUserList;

    }


}
