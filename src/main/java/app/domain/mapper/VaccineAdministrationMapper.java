package app.domain.mapper;

import app.domain.dto.VaccineAdministrationDTO;
import app.domain.model.VaccineAdministration;
import app.domain.utils.DateUtils;
import app.domain.utils.TimeUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VaccineAdministrationMapper {

    /**
     * Method to transform a dto object to an object
     * @param dto dto object to pass to object
     * @return object of the dto
     */
    public VaccineAdministration dtoToVaccineAdministration(VaccineAdministrationDTO dto) {
        return new VaccineAdministration(dto.getSnsNumber(),
                dto.getVaccineName(),
                dto.getDoseNumber(),
                dto.getLotNumber(),
                dto.getAdministrationDate(),
                dto.getArrivalTime(),
                dto.getScheduledTime(),
                dto.getAdministrationTime(),
                dto.getLeavingTime());
    }

    /**
     * Methos to transform an object into a dto
     * @param vacAdmin object to transform to dto
     * @return dto of the object
     */
    public VaccineAdministrationDTO vaccineAdministrationToDTO(VaccineAdministration vacAdmin) {
        return new VaccineAdministrationDTO(vacAdmin.getSnsNumber(),
                vacAdmin.getVaccineName(),
                vacAdmin.getDoseNumber(),
                vacAdmin.getLotNumber(),
                vacAdmin.getAdministrationDate(),
                vacAdmin.getArrivalTime(),
                vacAdmin.getScheduledTime(),
                vacAdmin.getAdministrationTime(),
                vacAdmin.getLeavingTime());
    }

    /**
     * Method to parse a object represented in strings to dto
     * @param toParse object in strings to pass to dto
     * @return dto of the object
     */
    public VaccineAdministrationDTO StringVaccineAdministrationToDTO(List<String> toParse) {
        int doseNumber = defineDoseNumber(toParse.get(2).toLowerCase().trim());

        return new VaccineAdministrationDTO(Integer.parseInt(toParse.get(0)),
                toParse.get(1),
                doseNumber,
                toParse.get(3),
                new DateUtils(Integer.parseInt(getDate(toParse.get(4)).get(2)), Integer.parseInt(getDate(toParse.get(4)).get(0)), Integer.parseInt(getDate(toParse.get(4)).get(1))),
                new TimeUtils(Integer.parseInt(getTime(toParse.get(5)).get(0)), Integer.parseInt(getTime(toParse.get(5)).get(1))),
                new TimeUtils(Integer.parseInt(getTime(toParse.get(6)).get(0)), Integer.parseInt(getTime(toParse.get(6)).get(1))),
                new TimeUtils(Integer.parseInt(getTime(toParse.get(7)).get(0)), Integer.parseInt(getTime(toParse.get(7)).get(1))),
                new TimeUtils(Integer.parseInt(getTime(toParse.get(8)).get(0)), Integer.parseInt(getTime(toParse.get(8)).get(1))));
    }

    /**
     * Method to pass the dose number that is in the csv file into a valid format
     * @param string the string object representing the dose number
     * @return valid dose number for dto and object
     */
    public int defineDoseNumber(String string) {
        int dose = 0;

        if (string.equals("primeira") || string.equals("um")) {
            dose = 1;
        }

        if (string.equals("segunda") || string.equals("dois")) {
            dose = 2;
        }

        if (string.equals("terceira") || string.equals("três")) {
            dose = 3;
        }

        if (string.equals("quarta") || string.equals("quatro")) {
            dose = 4;
        }

        if (string.equals("quinta") || string.equals("cinco")) {
            dose = 5;
        }

        if (string.equals("sexta")|| string.equals("seis")) {
            dose = 6;
        }

        if (string.equals("sétima")|| string.equals("sete")) {
            dose = 7;
        }

        if (string.equals("oitava")|| string.equals("oito")) {
            dose = 8;
        }

        if (string.equals("nona")|| string.equals("nove")) {
            dose = 9;
        }

        if (string.equals("décima")|| string.equals("dez")) {
            dose = 10;
        }

        return dose;
    }

    /**
     * Method to get the date separated from the time of a string object
     * @param dateAndTime string object with a date and time
     * @return date
     */
    public ArrayList<String> getDate(String dateAndTime) {
        //separates string in parts (criteria to separate is what is the content of string separator)
        String[] passInArray = dateAndTime.split("/");
        //transforms the separated string into an array
        ArrayList<String> passArrayList = new ArrayList<String>(Arrays.asList(passInArray));

        return passArrayList;
    }

    /**
     * Method to get the time separated from the date of a string object
     * @param dateAndTime string object with a date and time
     * @return time
     */
    public ArrayList<String> getTime(String dateAndTime) {
        //separates string in parts (criteria to separate is what is the content of string separator)
        String[] passInArray = dateAndTime.split(":");
        //transforms the separated string into an array
        ArrayList<String> passArrayList = new ArrayList<String>(Arrays.asList(passInArray));
        // String[] timeOnly = passArrayList.get(1).split(":");
        //  ArrayList<String> time = new ArrayList<String>(Arrays.asList(timeOnly));

        return passArrayList;
    }

    /**
     * Method to transform a list of dto into a list of objects
     * @param dtos list of dto to transform into objects
     * @return list of objects
     */
    public List<VaccineAdministration> transformListOfDTOInVaccineAdmin(List<VaccineAdministrationDTO> dtos) {
        List<VaccineAdministration> DTOToVaccineAdmin = new ArrayList<>();
        for (VaccineAdministrationDTO fileLine : dtos) {
            DTOToVaccineAdmin.add(dtoToVaccineAdministration(fileLine));
        }
        return DTOToVaccineAdmin;
    }

    /**
     * Method to transform a list of lists with all attributes in string into a list of dto
     * @param toBeDTO list of lists with all attributes in string to transform into a list of dto
     * @return list of dto
     */
    public List<VaccineAdministrationDTO> transformListOfListsToDTO(List<List<String>> toBeDTO) {
        List<VaccineAdministrationDTO> dto = new ArrayList<>();
        for (List<String> lineToBeDTO : toBeDTO) {
            dto.add(StringVaccineAdministrationToDTO(lineToBeDTO));
        }
        return dto;
    }

    /**
     * Method to transform a list of object into list of dto
     * @param toBeDTO list of objects to transform to dto
     * @return list of dto
     */
    public List<VaccineAdministrationDTO> transformListOfVaccineAdministrationsToDTO(List<VaccineAdministration> toBeDTO){
        List<VaccineAdministrationDTO> dto = new ArrayList<>();
        for (VaccineAdministration lineToBeDTO : toBeDTO) {
            dto.add(vaccineAdministrationToDTO(lineToBeDTO));
        }
        return dto;
    }
}
