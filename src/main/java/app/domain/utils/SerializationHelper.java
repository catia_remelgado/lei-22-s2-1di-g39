package app.domain.utils;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.UserClone;
import pt.isep.lei.esoft.auth.AuthFacade;

import java.io.*;
import java.util.List;

public class SerializationHelper {

    public static void serializeCompany(){
        try {
            FileOutputStream fileOut =
                    new FileOutputStream(System.getProperty("user.dir")+ File.separator+"src"+ File.separator+"main"+File.separator+"data"+ File.separator+"company.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(App.getInstance().getCompany());
            out.close();
            fileOut.close();
            System.out.printf("Serialized data is saved in /data/company.ser");
        } catch (IOException i) {
            System.out.println("Data was not saved!");
        }
    }

    public static void deserializeCompany(App app){
        try {
            FileInputStream fileIn = new FileInputStream(System.getProperty("user.dir")+ File.separator+"src"+ File.separator+"main"+File.separator+"data"+ File.separator+"company.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);

            app.setCompany((Company) in.readObject());
            in.close();
            fileIn.close();
        } catch (IOException i) {

            System.out.println("Could not load data!");
        } catch (ClassNotFoundException c) {
            System.out.println("Company class not found");

        }


    }
}
