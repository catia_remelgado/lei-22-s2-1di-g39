package app.domain.utils;

import app.domain.model.VaccineAdministration;

import java.util.Collections;
import java.util.List;

public class SortByDateAndTime {

    /**
     * Method to decide by which time the Vaccine Administration list will be sorted in bubble sort
     * @param listToSort Vaccine Administration list to sort
     * @param typeOfSort defines how the Vaccine Administration list will be sorted
     */
    public static void bubbleSort(List<VaccineAdministration> listToSort, String typeOfSort){
        if (typeOfSort.equals("Arrival time")){
            bubbleSortForArrivalTime(listToSort);
        }
        if (typeOfSort.equals("Leaving time")){
            bubbleSortForLeavingTime(listToSort);
        }
    }

    /**
     * Method to decide by which time the Vaccine Administration list will be sorted in quick sort
     * @param listToSort Vaccine Administration list to sort
     * @param typeOfSort defines how the Vaccine Administration list will be sorted
     */
    //for more context: https://www.youtube.com/watch?v=Fiot5yuwPAg
    public static void quickSort(List<VaccineAdministration> listToSort, String typeOfSort){
        if (typeOfSort.equals("Arrival time")){
            quickSortArrivalTime(listToSort, 0, listToSort.size()-1);
        }
        if (typeOfSort.equals("Leaving time")){
            quickSortLeavingTime(listToSort, 0, listToSort.size()-1);
        }
    }

    /**
     * Method to sort a Vaccine Administration list by arrival date and time in bubble sort
     * @param listToSort Vaccine Administration list to sort
     */
    public static void bubbleSortForArrivalTime(List<VaccineAdministration> listToSort){
        for (int i = 0; i < listToSort.size()-1; i++){
            for (int j = 0; j < listToSort.size()-1-i; j++){
                if (listToSort.get(j).getAdministrationDate().isBiggerOrEquals(listToSort.get(j+1).getAdministrationDate()) &&
                        listToSort.get(j).getArrivalTime().isBiggerOrEquals(listToSort.get(j+1).getArrivalTime())){
                    //https://stackoverflow.com/questions/6934577/how-to-exchange-the-position-of-two-objects-in-a-arraylist
                    Collections.swap(listToSort, j, j+1);
                }
            }
        }
    }


    /**
     * Method to sort a Vaccine Administration list by leaving date and time in bubble sort
     * @param listToSort Vaccine Administration list to sort
     */
    public static void bubbleSortForLeavingTime(List<VaccineAdministration> listToSort){
        for (int i = 0; i < listToSort.size()-1; i++){
            for (int j = 0; j < listToSort.size()-1-i; j++){
                if (listToSort.get(j).getAdministrationDate().isBiggerOrEquals(listToSort.get(j+1).getAdministrationDate()) &&
                        listToSort.get(j).getLeavingTime().isBiggerOrEquals(listToSort.get(j+1).getLeavingTime())){
                   //https://stackoverflow.com/questions/6934577/how-to-exchange-the-position-of-two-objects-in-a-arraylist
                    Collections.swap(listToSort, j, j+1);
                }
            }
        }
    }


    /**
     * Found and adapted from: https://stackoverflow.com/questions/21967328/quicksort-algorithm-not-working-in-java
     * For context: https://www.youtube.com/watch?v=Fiot5yuwPAg
     * Method to sort a Vaccine Administration list by leaving date and time in quick sort
     * @param listToSort Vaccine Administration list to sort
     * @param lower first Vaccine Administration list index
     * @param higher last Vaccine Administration list index
     */
    public static void quickSortLeavingTime(List<VaccineAdministration> listToSort, int lower, int higher)
    {
        if (lower < higher)
        {
            int i = partitionLeavingTime(listToSort, lower, higher);
            quickSortLeavingTime(listToSort, lower, i - 1);
            quickSortLeavingTime(listToSort, i + 1, higher);
        }
    }


    /**
     * Found and adapted from: https://stackoverflow.com/questions/21967328/quicksort-algorithm-not-working-in-java
     * For context: https://www.youtube.com/watch?v=Fiot5yuwPAg
     * Method to sort a Vaccine Administration list by leaving date and time in quick sort
     * @param listToSort Vaccine Administration list to sort
     * @param lower first Vaccine Administration list index
     * @param higher last Vaccine Administration list index
     */
   public static int partitionLeavingTime(List<VaccineAdministration> listToSort, int lower, int higher)
    {
        VaccineAdministration x = listToSort.get(higher);
        int i = lower - 1;

        for (int j = lower; j < higher; j++)
        {
            if (x.getAdministrationDate().isBigger(listToSort.get(j).getAdministrationDate())){
                i++;
                Collections.swap(listToSort, i, j);
            } else if (x.getAdministrationDate().equals(listToSort.get(j).getAdministrationDate())){
                if (x.getLeavingTime().isBigger(listToSort.get(j).getLeavingTime())){
                    i++;
                    Collections.swap(listToSort, i, j);
                } else if (x.getLeavingTime().equals(listToSort.get(j).getLeavingTime())){
                    if (x.getArrivalTime().isBigger(listToSort.get(j).getArrivalTime())){
                        i++;
                        Collections.swap(listToSort, i, j);
                    } else if (x.getArrivalTime().equals(listToSort.get(j).getArrivalTime())){
                        i++;
                        Collections.swap(listToSort, i, j);}
                }
            }
        }
        Collections.swap(listToSort, i+1, higher);
        return i + 1;
    }


    /**
     * Found and adapted from: https://stackoverflow.com/questions/21967328/quicksort-algorithm-not-working-in-java
     * For context: https://www.youtube.com/watch?v=Fiot5yuwPAg
     * Method to sort a Vaccine Administration list by arrival date and time in quick sort
     * @param listToSort Vaccine Administration list to sort
     * @param lower first Vaccine Administration list index
     * @param higher last Vaccine Administration list index
     */
    public static void quickSortArrivalTime(List<VaccineAdministration> listToSort, int lower, int higher)
    {
        if (lower < higher)
        {
            int i = partitionArrivalTime(listToSort, lower, higher);
            quickSortArrivalTime(listToSort, lower, i - 1);
            quickSortArrivalTime(listToSort, i + 1, higher);
        }
    }


    /**
     * Found and adapted from: https://stackoverflow.com/questions/21967328/quicksort-algorithm-not-working-in-java
     * For context: https://www.youtube.com/watch?v=Fiot5yuwPAg
     * Method to sort a Vaccine Administration list by arrival date and time in quick sort
     * @param listToSort Vaccine Administration list to sort
     * @param lower first Vaccine Administration list index
     * @param higher last Vaccine Administration list index
     */
    public static int partitionArrivalTime(List<VaccineAdministration> listToSort, int lower, int higher)
    {
        VaccineAdministration pivot = listToSort.get(higher);
        int i = lower - 1;

        for (int j = lower; j < higher; j++)
        {
            if (pivot.getAdministrationDate().isBigger(listToSort.get(j).getAdministrationDate())){
                i++;
                Collections.swap(listToSort, i, j);
            } else if (pivot.getAdministrationDate().equals(listToSort.get(j).getAdministrationDate())){
                if (pivot.getArrivalTime().isBigger(listToSort.get(j).getArrivalTime())){
                    i++;
                    Collections.swap(listToSort, i, j);
                } else if (pivot.getArrivalTime().equals(listToSort.get(j).getArrivalTime())){
                    if (pivot.getLeavingTime().isBigger(listToSort.get(j).getLeavingTime())){
                        i++;
                        Collections.swap(listToSort, i, j);
                    } else if (pivot.getLeavingTime().equals(listToSort.get(j).getLeavingTime())){
                        i++;
                        Collections.swap(listToSort, i, j);}
                }
            }
        }
        Collections.swap(listToSort, i+1, higher);
        return i + 1;
    }

}
