package app.domain.utils;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Represents date with year, month and day as integers
 *
 * @author ISEP-DEI-PPROG
 */
public class DateUtils implements Comparable<DateUtils>, Serializable {

    /**
     * Year of date
     */
    private int year;

    /**
     * Month of date
     */
    private int month;

    /**
     * Day of date.
     */
    private int day;

    /**
     * Default year
     */
    private static final int DEFAULT_YEAR = 1;

    /**
     * Default month
     */
    private static final int DEFAULT_MONTH = 1;

    /**
     * Default day
     */
    private static final int DEFAULT_DAY = 1;

    /**
     * Name of the days of the week
     */
    private static String[] nameDaysOfWeek = {"Sunday", "Monday",
            "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

    /**
     * Number of days in months
     */
    private static int[] daysInMonths = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30,
            31, 30, 31};

    /**
     * Name of months of the year.
     */
    private static String[] nameMonth = {"Invalid", "January", "February",
            "March", "April", "May", "June", "July", "August", "September",
            "October", "November", "December"};

    /**
     * Constructor of an instance of data with year, month and day
     *
     * @param year  year of date.
     * @param month month of date.
     * @param day   day of date.
     */
    public DateUtils(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    /**
     * Constructs date by default
     */
    public DateUtils() {
        year = DEFAULT_YEAR;
        month = DEFAULT_MONTH;
        day = DEFAULT_DAY;
    }

    /**
     * Constructs another instance of date with attributes of otherDate
     *
     * @param otherDate date to copy to new instance.
     */
    public DateUtils(DateUtils otherDate) {
        year = otherDate.year;
        month = otherDate.month;
        day = otherDate.day;
    }

    public static int currentAge(DateUtils birthDate, DateUtils currentDate) {
       return currentDate.difference(birthDate);

    }

    /**
     * Returns the year of date
     *
     * @return year of date
     */
    public int getYear() {
        return year;
    }

    /**
     * Return month of date
     *
     * @return month of date
     */
    public int getMonth() {
        return month;
    }

    /**
     * Returns day of date
     *
     * @return day of date
     */
    public int getDay() {
        return day;
    }

    /**
     * Alter the year, month and day
     *
     * @param year  new year of date
     * @param month new month of date.
     * @param day   new day of date.
     */
    public void setData(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    /**
     * Returns a message with description of date
     *
     * @return content of date described.
     */
    @Override
    public String toString() {
        return String.format("%s, day %d of %s of %d", dayOfTheWeek(), day,
                nameMonth[month], year);
    }

    /**
     * Returns date in the following format:%04d/%02d/%02d.
     *
     * @return date content.
     */
    public String toAnoMesDiaString() {
        return String.format("%04d/%02d/%02d", year, month, day);
    }

    /**
     * Compares date with other date.
     *
     * @param otherObject the object to compare to our current insatnce of date
     * @return true if the given object is equivalent to out current instance of date. If not, returns false.
     */
    @Override
    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null || getClass() != otherObject.getClass()) {
            return false;
        }
        DateUtils otherDate = (DateUtils) otherObject;
        return year == otherDate.year && month == otherDate.month
                && day == otherDate.day;
    }

    /**
     * Compares current instance of date with other date received by parameters.
     *
     * @param otherDate the date to be compared.
     * @return 0 if otherDate is the same as our current instance;
     * -1 if otherDate is later than our current instance of data;
     * 1 if otherDate is before our current instance of date
     */
    @Override
    public int compareTo(DateUtils otherDate) {
        return (otherDate.isBigger(this)) ? -1 : (isBigger(otherDate)) ? 1 : 0;
    }

    /**
     * Returns day of week of this date
     *
     * @return day of week of date
     */
    public String dayOfTheWeek() {
        int totalDias = countDays();
        totalDias = totalDias % 7;

        return nameDaysOfWeek[totalDias];
    }


    /**
     * Returns true if date is bigger than otherDate.
     * Returns false if date is lesser than otherDate.
     *
     * @param otherDate otherDate to be compared to.
     * @return true if date is bigger than otherDate. If not, return false
     */
    public boolean isBigger(DateUtils otherDate) {
        int totalDays = countDays();
        int totalDays1 = otherDate.countDays();

        return totalDays > totalDays1;
    }

    /**
     * Returns number of days between current instance of date and otherDate
     *
     * @param otherDate received by the function to know the number of days between otherDate and current instance of date
     * @return Difference in days between the two dates
     */
    public int difference(DateUtils otherDate) {
        int totalDays = countDays();
        int totalDays1 = otherDate.countDays();

        return Math.abs(totalDays - totalDays1);
    }

    /**
     * Returns the days between two dates: the current instance of date and a new date, with year, month and day parameters
     *
     * @param year  the year of the date to calculate the number of days between our current instance of date
     * @param month the month of the date to calculate the number of days between our current instance of date
     * @param day   the day of the date to calculate the number of days between our current instance of date
     * @return Difference in days between the two dates, where one of the dates is received by parameter with year, month and day
     */
    public int difference(int year, int month, int day) {
        int totalDays = countDays();
        DateUtils otherDate = new DateUtils(year, month, day);
        int totalDays1 = otherDate.countDays();

        return Math.abs(totalDays - totalDays1);
    }

    /**
     * Returns true if year is leap year, If not returns false
     *
     * @param year the year to validate
     * @return true if the year is leap year
     */
    public static boolean isLeapYear(int year) {
        return year % 4 == 0 && year % 100 != 0 || year % 400 == 0;
    }

    /**
     * Returns current date of system
     *
     * @return system's current date
     */
    public static DateUtils currentDate() {
        Calendar today = Calendar.getInstance();
        int year = today.get(Calendar.YEAR);
        int month = today.get(Calendar.MONTH) + 1;    // january represented by 0
        int day = today.get(Calendar.DAY_OF_MONTH);
        return new DateUtils(year, month, day);
    }

    /**
     * Returns  number of days since 1/1/1 to the current instance of date
     *
     * @return number of days since 1/1/1 to the current instance of date
     */
    private int countDays() {
        int totalDays = 0;

        for (int i = 1; i < year; i++) {
            totalDays += isLeapYear(i) ? 366 : 365;
        }
        for (int i = 1; i < month; i++) {
            totalDays += daysInMonths[i];
        }
        totalDays += (isLeapYear(year) && month > 2) ? 1 : 0;
        totalDays += day;

        return totalDays;
    }

    /**
     * Converts a native Date class to DateUtils class
     *
     * @param otherDate the date that is in "Date" format
     * @return DateUtils date converted from other date received by parameter
     * Seedoubleyew, V., &#38; Guillaume, F. (n.d.). I want to get Year, Month, Day, etc from Java Date to compare with Gregorian Calendar date in Java. Is this possible? - Stack Overflow. Retrieved May 26, 2022, from https://stackoverflow.com/questions/9474121/i-want-to-get-year-month-day-etc-from-java-date-to-compare-with-gregorian-cal
     * Based on https://stackoverflow.com/questions/9474121/i-want-to-get-year-month-day-etc-from-java-date-to-compare-with-gregorian-cal
     */
    public static DateUtils dateToDateUtils(Date otherDate) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Paris"));
        cal.setTime(otherDate);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        return new DateUtils(year, month, day);
    }


    //ANNA

    /**
     * method to see if string is wrote in asked format (dd/mm/yyyy)
     *
     * @param dateString String provided by the user
     * @return true if user wrote the string in the asked format
     */
    public static boolean seeIfItsPossibleToConvertStringToDateUtils(String dateString, String separator, int pretendedSize) {
        boolean success = false;

        //separates string in parts (criteria to separate is what is the content of string separator)
        String[] passInArray = dateString.split(separator);
        //transforms the separated string into an array
        ArrayList<String> passArrayList = new ArrayList<String>(Arrays.asList(passInArray));

        //if array's size is pretendedSize (it's ine step further to be valid)
        if (passArrayList.size() == pretendedSize) {
            //Confirms if all the strings are parsable to an number
            success = seeIfItsANumber(passArrayList);
            if (success == true) {
                success = confirmYear(Integer.parseInt(passArrayList.get(2)));
                if (success == true && Integer.parseInt(passArrayList.get(1)) <= 12 && Integer.parseInt(passArrayList.get(1)) > 0) {
                    success = confirmDay(Integer.parseInt(passArrayList.get(0)), Integer.parseInt(passArrayList.get(1)));
                } else {
                    success = false;
                }
            }
        }
        return success;
    }

    /**
     * method to confirm if all elements of an array of strings are parsable to an number
     *
     * @param passArrayList array list to be verified
     * @return true if all elements of array list are parsable to number
     */
    public static boolean seeIfItsANumber(ArrayList<String> passArrayList) {
        boolean success = false;
        int i = 0;
        do {
            try {
                String string = passArrayList.get(i);
                int number = Integer.parseInt(string);
                i++;
                success = true;
            } catch (IllegalArgumentException e) {
                success = false;
            }
        } while (success == true && i < 3);
        return success;
    }


    /**
     * Method that confirms if given day by the user is correct (considering the given month)
     *
     * @param day   day given by the user
     * @param month month given by the user
     * @return true if given day is correct considering the given month
     */
    public static boolean confirmDay(int day, int month) {
        boolean wright = false;

        //for january, march, may, july, august, october, december
        if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
            if (day > 0 && day <= 31) {
                wright = true;
            }
            // for april, june, september and november
        } else if (month == 4 || month == 6 || month == 9 || month == 11) {
            if (day > 0 && day <= 30) {
                wright = true;
            }
            // for when february is not a leap year
        } else if ((month % 4) != 0) {
            if (day > 0 && day <= 28) {
                wright = true;
                // for when february is a leap year
            } else {
                if (day > 0 && day <= 29) {
                    wright = true;
                }
            }
        }
        return wright;

    }


    /**
     * Confirms if year given by the user is at least equal or bigger than current year
     *
     * @param year year given by the user
     * @return true if year given by the user equals or its bigger than the acyual year
     */
    public static boolean confirmYear(int year) {
        boolean wright = false;
        Date actualDate = new Date();
        int actualYear = actualDate.getYear() + 1900;

        if (year >= actualYear) {
            wright = true;
        }
        return wright;
    }


    /**
     * Sets date provided by the user as DateUtils
     * @param dateString
     * @param dateString date provided by the user
     * @return dateString in object DateUtils
     */
    public static DateUtils setDate(String dateString, String separator) {
        DateUtils date;
        String[] passInArray = dateString.split(separator);
        ArrayList<String> passArrayList = new ArrayList<String>(Arrays.asList(passInArray));

        date = new DateUtils(Integer.parseInt(passArrayList.get(2)), Integer.parseInt(passArrayList.get(1)), Integer.parseInt(passArrayList.get(0)));

        return date;
    }

    /**
     * Converts a DateUtils to Date
     *
     * @param otherDate the date that is in DateUtils format
     * @return date in Date format
     */
    public static Date dateUtilsToDate(DateUtils otherDate) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        date.setYear(otherDate.getYear()-1900);
        date.setMonth(otherDate.getMonth()-1);
        date.setDate(otherDate.getDay());
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        return date;
    }


    /**
     * Method to confirm if an inputted date it's not in the past
     * @param date date that was inputted
     * @return false if inputed date is in the past
     */
    public static boolean confirmIfDateItsNotPast(DateUtils date){
        DateUtils currentDate = new DateUtils().currentDate();
        if (currentDate.isBigger(date)){
            return false;
        }else {
            return true;
        }
    }

    /**
     *Method to confirm if an inputted date it's not too in the future (this is, for creating an appointment, not allow someone to try schedule, for example
     * in the year 2000000)
     * @param date date inputted to compare
     * @return true if inputed date it's too big
     */
    public static boolean confirmIfDateItsTooInTheFuture(DateUtils date){
        Calendar future = Calendar.getInstance();
        future.add(Calendar.YEAR, 150);
        int year = future.get(Calendar.YEAR);
        int month = future.get(Calendar.MONTH) + 1;    // january represented by 0
        int day = future.get(Calendar.DAY_OF_MONTH);
        DateUtils futureInDateUtils = new DateUtils(year, month, day);
        if (date.isBigger(futureInDateUtils)){
            return true;
        }else {
            return false;
        }
    }

    /**
     * Returns date in the following format:%04d/%02d/%02d.
     *
     * @return date content.
     */
    public String toDiaMesAnoString() {
        return String.format("%02d/%02d/%04d", day, month, year);
    }


    /**
     * Returns true if date is bigger than or equal to otherDate.
     * Returns false if date is lesser than otherDate.
     *
     * @param otherDate otherDate to be compared to.
     * @return true if date is bigger than or equal otherDate. If not, return false
     */
    public boolean isBiggerOrEquals(DateUtils otherDate) {
        int totalDays = countDays();
        int totalDays1 = otherDate.countDays();

        return totalDays >= totalDays1;
    }

    /**
     * Method to confirm if a date in the system legacy is valid/parsable
     * @param dateString date from the legacy system csv file
     * @param separator criterium to sepate values of date
     * @param pretendedSize
     * @return true if date is valid and parsable
     */
    public static boolean seeIfItsPossibleToConvertStringToDateUtilsOfCSVFile(String dateString, String separator, int pretendedSize) {
        boolean success = false;
        //separates string in parts (criteria to separate is what is the content of string separator)
        String[] passInArray = dateString.split(separator);
        //transforms the separated string into an array
        ArrayList<String> passArrayList = new ArrayList<String>(Arrays.asList(passInArray));
        //if array's size is pretendedSize (it's ine step further to be valid)
        if (passArrayList.size() == pretendedSize) {
            //Confirms if all the strings are parsable to an number
            success = seeIfItsANumber(passArrayList);
            if (success == true) {
                if (success == true && Integer.parseInt(passArrayList.get(0)) <= 12 && Integer.parseInt(passArrayList.get(0)) > 0) {
                    success = confirmDay(Integer.parseInt(passArrayList.get(1)), Integer.parseInt(passArrayList.get(0)));
                } else {
                    success = false;
                }
            }
        }
        return success;
    }
    
    /**
     *
     * @param date date to add days
     * @param days number of days adding
     * @return
     */

    public static Date addDays(Date date, int days) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();

    }

    /**
     *
     * @param dateToConvert
     * @return
     */
    public static Date convertLocalDateToDate(LocalDate dateToConvert) {
        return java.util.Date.from(dateToConvert.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }
}

