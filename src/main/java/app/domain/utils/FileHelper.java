package app.domain.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Date;
import java.util.List;

import static app.domain.utils.DateUtils.addDays;
import static app.domain.utils.DateUtils.dateUtilsToDate;

/**
 * loads a file to the application from a path
 *
 * @author alexandraipatova & CátiaRemelgado
 */
public class FileHelper {

    /**
     *
     * returns a file with a chosen name from a path inserted in the parameter,
     * it also saves the file to a files folder
     * @param path path of chosen file
     * @param newFileName new name for the file
     * @return file with chosen name from a path
     * @throws IOException since the file was not found
     */
    public static File saveFile(String path, String newFileName) throws IOException {

        String extension = path.substring(path.length() - 4, path.length());
        Path path1 = Path.of(path);
        File file = null;
        file = new File(path1.toString());
        //Files.deleteIfExists(Paths.get("."+File.separator+"src"+File.separator+"main"+File.separator+"files" + File.separator + newFileName + extension));
        //Files.copy(Paths.get(path1.toString()), Paths.get("."+File.separator+"src"+File.separator+"main"+File.separator+"files" + File.separator + newFileName + extension));

        return file;


    }

    public static void exportFileOfVaccinatedPeopleInAllCenters(List<String> vaccinationCenterNames,List<Integer> numberOfVaccinatedPeople) {
        try {
            FileWriter writer = new FileWriter("src"+File.separator+"main"+File.separator+"files"+File.separator+"NumberOfVaccinatedPeopleToday.csv");
            writer.write("Number of vaccinated people at " + DateUtils.currentDate()+"\n");
            int i=0;
            for (Integer count : numberOfVaccinatedPeople) {
                writer.write( vaccinationCenterNames.get(i) + " : " + count + "\n");
                i++;
            }

            writer.close();

        } catch (IOException e) {

        }

    }

    public static boolean exportFile(DateUtils date, String fileName, List<Integer> numberOfFullyVaccinatedPeople) {
        try {
            FileWriter writer = new FileWriter("src"+File.separator+"main"+File.separator+"files"+File.separator+ fileName + ".csv");

            for (Integer count : numberOfFullyVaccinatedPeople) {
                writer.write("In " + date.toDiaMesAnoString() + " there are " + count + " fully vaccinated persons. \n");
                Date date1 = dateUtilsToDate(date);
                addDays(date1, 1);
            }

            writer.close();

        } catch (IOException e) {

        }

        return false;
    }
}
