package app.domain.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TreatLegacySystemData {

    /**
     * Method to verify if data from the csv file is parsable to dto
     * @param csvFileContens list of list of the csv file contens
     * @return parsable lines to dto from csv file
     */
    public List<List<String>> treatSystemLegacyData(List<List<String>> csvFileContens) {
        List<List<String>> treated = new ArrayList<>();
        boolean save = false;
        for (List<String> fileLine : csvFileContens) {
            try {
                int num = Integer.valueOf(fileLine.get(0));
                save = true;
            } catch (NumberFormatException e) {
                save = false;
            }

            if (save == true) {
                save = seeIfDateAndTimeAreValid(fileLine);
            }

            if (save == true) {
                treated.add(fileLine);
            }

            save = false;
        }
        return treated;
    }

    /**
     * method to verify if the dates and times from the list of list of the csv files contens are valid
     * @param datesAndTimes specific line of the list of list of the csv file contents
     * @return true if all dates and times from the line are valid
     */
    public boolean seeIfDateAndTimeAreValid(List<String> datesAndTimes) {
        boolean save = true;

        for (int i = 4; i < datesAndTimes.size(); i++) {
            if (save == true) {
                //separates string in parts (criteria to separate is what is the content of string separator)
                String[] passInArray = datesAndTimes.get(i).split(" ");
                //transforms the separated string into an array
                ArrayList<String> passArrayList = new ArrayList<String>(Arrays.asList(passInArray));
                //save = ctrl.seeIfDateIsCorrect(passArrayList.get(0), "/|-");
                try {
                    save = DateUtils.seeIfItsPossibleToConvertStringToDateUtilsOfCSVFile(passArrayList.get(0), "/|-", 3);
                } catch (IndexOutOfBoundsException e) {
                    save = false;
                }

                if (save == true) {
                    try {
                        //save = ctrl.seeIfTimeIsCorrect(passArrayList.get(1));
                        save = TimeUtils.seeIfItsPossibleToConvertStringToTimeUtils(passArrayList.get(1), ":", 2);
                    } catch (IndexOutOfBoundsException e) {
                        save = false;
                    }
                }
            }
        }

        return save;
    }

    /**
     * Creates and array list to do the array list of dto
     * @param toTreat data from the csv file that is parsable
     * @return parsable data to send to dto
     */
    public List<List<String>> treatToMakeDTOOfVaccineAdministration(List<List<String>> toTreat){
        List<List<String>> treated = new ArrayList<>();

        for (List<String> line: toTreat){
            treated.add(lineToAdd(line));
        }
        return treated;
    }

    /**
     * Method to get each wanted atribute from each line of the list of lists of csv file contents
     * @param toTreat line of the csv file contens with wanted data
     * @return line with thw atributes to do dto
     */
    public List<String> lineToAdd (List<String> toTreat){
        List<String> newLine = new ArrayList<>();

        newLine.add(toTreat.get(0));
        newLine.add(toTreat.get(1));
        newLine.add(toTreat.get(2));
        newLine.add(toTreat.get(3));
        newLine.add(getDate(toTreat.get(6)));
        newLine.add(getTime(toTreat.get(5)));
        newLine.add(getTime(toTreat.get(4)));
        newLine.add(getTime(toTreat.get(6)));
        newLine.add(getTime(toTreat.get(7)));

        return newLine;
    }

    /**
     * Method to separate the date from the time of the list of lists of the csv file contents
     * @param dateAndTime the wanted atribute of the line to convert (that has date and time)
     * @return the date
     */
    public String getDate(String dateAndTime){
        //separates string in parts (criteria to separate is what is the content of string separator)
        String[] passInArray = dateAndTime.split(" ");
        //transforms the separated string into an array
        ArrayList<String> passArrayList = new ArrayList<String>(Arrays.asList(passInArray));
        //save = ctrl.seeIfDateIsCorrect(passArrayList.get(0), "/|-");

        return passArrayList.get(0);
    }

    /**
     * Method to separate the time from the date of the list of lists of the csv file contents
     * @param dateAndTime the wanted atribute of the line to convert (that has date and time)
     * @return the date
     */
    public String getTime (String dateAndTime){
        //separates string in parts (criteria to separate is what is the content of string separator)
        String[] passInArray = dateAndTime.split(" ");
        //transforms the separated string into an array
        ArrayList<String> passArrayList = new ArrayList<String>(Arrays.asList(passInArray));
        //save = ctrl.seeIfDateIsCorrect(passArrayList.get(0), "/|-");

        return passArrayList.get(1);
    }
}
