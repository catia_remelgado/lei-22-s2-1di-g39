package app.domain.utils;

import app.controller.App;
import app.domain.exceptions.VaccineAdministrationsNotFoundException;
import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineAdministration;

import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

/**
 * @author alexandraipatova
 */
public class DailyAdministrationReportTask extends TimerTask {
    @Override
    public void run() {

        List<VaccinationCenter> vcList =App.getInstance().getCompany().getVaccinationCenterStore().getAllVaccinationCenters();
        List<String> vcNames = new ArrayList<>();
        List<Integer> vcVaccinatedPeople = new ArrayList<>();
        List<VaccineAdministration> vaList = new ArrayList<>();
        if(!vcList.isEmpty()) {
            for (VaccinationCenter vc : vcList) {
                try {
                    vaList = vc.getVaccineAdministrationListByDay(DateUtils.currentDate());
                } catch (VaccineAdministrationsNotFoundException e) {

                }
                vcNames.add(vc.getName());
                vcVaccinatedPeople.add(vaList.size());
            }
            FileHelper.exportFileOfVaccinatedPeopleInAllCenters(vcNames,vcVaccinatedPeople);
        }




    }
}
