package app.domain.utils;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class TableFileContens {

    /**
     * Property that represents the SNS user name
     */
    private StringProperty snsUserName;

    /**
     * Property that represents the SNS user number
     */
    private StringProperty snsUserNumber;

    /**
     * Property that represents the vaccine name
     */
    private StringProperty vaccineName;

    /**
     * Property that represents the vaccine type short description
     */
    private StringProperty vaccineShortDescription;

    /**
     * Property that represents the dose of the vaccine
     */
    private StringProperty dose;

    /**
     * Property that represents the lot number
     */
    private StringProperty lotNumber;

    /**
     * Property that represents the date
     */
    private StringProperty date;

    /**
     * Property that represents the scheduled time
     */
    private StringProperty scheduledTime;

    /**
     * Property that represents the arrival time
     */
    private StringProperty arrivalTime;

    /**
     * Property that represents the time of vaccine's administration
     */
    private StringProperty administrationTime;

    /**
     * Property that represents the leaving time
     */
    private StringProperty leavingTime;



    public TableFileContens(String snsUserName, String snsUserNumber, String vaccineName,
                            String vaccineTypeShortDescription, String dose, String lotNumber, String date,
                            String scheduledDateTime, String arrivalDateTime, String nurseAdministrationDateTime,
                            String leavingDateTime) {
        this.snsUserNumber = new SimpleStringProperty(snsUserNumber);
        this.snsUserName = new SimpleStringProperty(snsUserName);
        this.vaccineName = new SimpleStringProperty(vaccineName);
        this.vaccineShortDescription = new SimpleStringProperty(vaccineTypeShortDescription);
        this.dose = new SimpleStringProperty(dose);
        this.lotNumber = new SimpleStringProperty(lotNumber);
        this.date = new SimpleStringProperty(date);
        this.scheduledTime = new SimpleStringProperty(scheduledDateTime);
        this.arrivalTime = new SimpleStringProperty(arrivalDateTime);
        this.administrationTime = new SimpleStringProperty(nurseAdministrationDateTime);
        this.leavingTime = new SimpleStringProperty(leavingDateTime);
    }

    /**
     * Method to return the SNS user name property
     *
     * @return The SNS user name property
     */
    public StringProperty snsUserNameProperty() {
        return snsUserName;
    }

    /**
     * Method to return the SNS user number property
     *
     * @return The SNS user number property.
     */
    public StringProperty snsUserNumberProperty() {
        return snsUserNumber;
    }

    /**
     * Method to return the vaccine name property
     *
     * @return The vaccine name property
     */
    public StringProperty vaccineNameProperty() {
        return vaccineName;
    }

    /**
     * Method to return the vaccine type short description property
     *
     * @return The vaccine type short description property
     */
    public StringProperty vaccineShortDescriptionProperty() {
        return vaccineShortDescription;
    }

    /**
     * Method to return the dose property
     *
     * @return The dose property
     */
    public StringProperty doseProperty() {
        return dose;
    }

    /**
     * Method to return the lot number property
     *
     * @return The lot number property
     */
    public StringProperty lotNumberProperty() {
        return lotNumber;
    }

    /**
     * Method to return the date property
     * @return the date property
     */
    public StringProperty dateProperty() {
        return date;
    }

    /**
     * Method to return the scheduled date and time property
     *
     * @return The scheduled date and time property
     */
    public StringProperty scheduledTimeProperty() {
        return scheduledTime;
    }

    /**
     * Method to return the SNS user arrival date and time property
     *
     * @return The SNS user arrival date and time property
     */
    public StringProperty arrivalTimeProperty() {
        return arrivalTime;
    }

    /**
     * Method to return the nurse vaccine administration date and time propert
     *
     * @return The nurse vaccine administration date and time property
     */
    public StringProperty administrationTimeProperty() {
        return administrationTime;
    }

    /**
     * Method to return the leaving date and time by the SNS user property
     *
     * @return The leaving date and time by the SNS user property
     */
    public StringProperty leavingTimeProperty() {
        return leavingTime;
    }
}
