package app.domain.utils;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

/**
 * Represents time through hours, minutes and seconds
 *
 * @author ISEP-DEI-PPROG
 */
public class TimeUtils implements Comparable<TimeUtils>, Serializable {

    /**
     * Hour of the time;
     */
    private int hour;

    /**
     * Minute of the time.
     */
    private int minute;

    /**
     * Second of the time.
     */
    private int second;

    /**
     * Default hour
     */
    private static final int DEFAULT_HOUR = 0;

    /**
     * Default minute
     */
    private static final int DEFAULT_MINUTE = 0;

    /**
     * Default second
     */
    private static final int DEFAULT_SECOND = 0;

    /**
     * Constructor for instance of time that receiver hour, minute and second
     *
     * @param hour   hour of time
     * @param minute minute of time
     * @param second second of time
     */
    public TimeUtils(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    /**
     * Constructor of an insatnce of class receiving only hour and minute, defaulting the second of time
     *
     * @param hour   hour of the time
     * @param minute minute of the time
     */
    public TimeUtils(int hour, int minute) {
        this.hour = hour;
        this.minute = minute;
        second = DEFAULT_SECOND;
    }

    /**
     * Constructor of an instance of time receiving only the hour, and defaulting minute and second
     *
     * @param hour as hour do tempo.
     */
    public TimeUtils(int hour) {
        this.hour = hour;
        minute = DEFAULT_MINUTE;
        second = DEFAULT_SECOND;
    }

    /**
     * Constructor for time by default
     *
     * @param time
     * @param amount
     */
    public TimeUtils(String time, int amount) {
        hour = DEFAULT_HOUR;
        minute = DEFAULT_MINUTE;
        second = DEFAULT_SECOND;
    }

    /**
     * Constructor for an instance of time with data of another time
     *
     * @param otherTime time with attributes to copy
     */
    public TimeUtils(TimeUtils otherTime) {

        hour = otherTime.hour;
        minute = otherTime.minute;
        second = otherTime.second;
    }

    /**
     * Returns hour of time
     *
     * @return hour of time
     */
    public int getHour() {
        return hour;
    }

    /**
     * Returns minute of time
     *
     * @return minute of time
     */
    public int getMinute() {
        return minute;
    }

    /**
     * Returns second of time
     *
     * @return second of time.
     */
    public int getSecond() {
        return second;
    }

    /**
     * Modifies hour of time
     *
     * @param hour new hour of time.
     */
    public void setHour(int hour) {
        this.hour = hour;
    }

    /**
     * Modifies minute of time
     *
     * @param minute new minute of time.
     */
    public void setMinute(int minute) {
        this.minute = minute;
    }

    /**
     * Modifies second of time
     *
     * @param second new second of time.
     */
    public void setSecond(int second) {
        this.second = second;
    }

    /**
     * Modifies hour, minute and second of time
     *
     * @param hour   new hour of time
     * @param minute new minute of time
     * @param second nem second of time
     */
    public void setTempo(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    /**
     * Returns the content of time in the following format: %02d:%02d:%02d AM/PM.
     *
     * @return content of time
     */
    @Override
    public String toString() {
        return String.format("%02d:%02d:%02d %s",
                (hour == 12 || hour == 0) ? 12 : hour % 12,
                minute, second, hour < 12 ? "AM" : "PM");
    }

    /**
     * Returns content of time in the following format: %02d%02d%02d.
     *
     * @return content of time
     */
    public String toStringHHMMSS() {
        return String.format("%02d:%02d:%02d", hour, minute, second);
    }

    /**
     * Compares time to received object
     *
     * @param otherObject the object with which we want to comapre.
     * @return true if the object is equivalent to our current instance of time, If not returns false
     */
    @Override
    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null || getClass() != otherObject.getClass()) {
            return false;
        }
        TimeUtils otherTime = (TimeUtils) otherObject;
        return hour == otherTime.hour && minute == otherTime.minute
                && second == otherTime.second;
    }

    /**
     * Compares current instance of time with time received by parameter.
     *
     * @param otherTime the time to be comapred.
     * @return 0 if otherTime is equal to current insatnce of time;
     * -1 if otherTime is later than current instance of time;
     * 1 if otherTime is sonner than current insatnce of time.
     */
    @Override
    public int compareTo(TimeUtils otherTime) {
        return (otherTime.isBigger(this)) ? -1 : (isBigger(otherTime)) ? 1 : 0;
    }

    /**
     * Add 1 second to the time
     */
    public void tick() {
        second = ++second % 60;
        if (second == 0) {
            minute = ++minute % 60;
            if (minute == 0) {
                hour = ++hour % 24;
            }
        }
    }

    /**
     * Returns true if time is bigger than time received by parameter. If not, return false if lesser or equal
     *
     * @param otherTime the otherTime to be compared.
     * @return true if current instance of time is bigger than the one received by parameter
     */
    public boolean isBigger(TimeUtils otherTime) {
        return toSeconds() > otherTime.toSeconds();
    }

    /*
     * Solução alternativa
     * public boolean isMaior(Tempo outroTempo){
     *      if ( horas > outroTempo.horas ||
     *          (horas==outroTempo.horas && minutos>outroTempo.minutos) ||
     *          (horas==outroTempo.horas && minutos==outroTempo.minutos &&
     *           segundos > outroTempo.segundos) )
     *         return true;
     *      return false;
     * }
     */

    /**
     * Returns true if time is bigger than other time (hour,minute second) received by parameter.
     * Returns false if time is less or equal to the other time
     *
     * @param hour   the hour of the time to be compared
     * @param minute the minute of the time to be compared.
     * @param second the second of the time.
     * @return true if time is bigger than other time received by parameter (hour,minute,second).
     */
    public boolean isBigger(int hour, int minute, int second) {
        TimeUtils otherTime = new TimeUtils(hour, minute, second);
        return this.toSeconds() > otherTime.toSeconds();
    }

    /**
     * Returns difference in second between two times
     *
     * @param otherTime the other time with which we want to compare
     * @return difference in second between the two times (current instance a parameter time)
     */
    public int differenceInSeconds(TimeUtils otherTime) {
        return Math.abs(toSeconds() - otherTime.toSeconds());
    }

    /**
     * Returns an instance of time that represents the difference of time between the current instance of time and the time received by parameter
     *
     * @param otherTime other time received by parameter to calculate the difference of time with the current instance of class
     * @return instance of time representative of the time difference between currents instance of time and time received by parameter
     */
    public TimeUtils differenceInTime(TimeUtils otherTime) {
        int dif = differenceInSeconds(otherTime);
        int s = dif % 60;
        dif = dif / 60;
        int m = dif % 60;
        int h = dif / 60;
        return new TimeUtils(h, m, s);
    }

    /**
     * Returns the current system time
     *
     * @return current system time.
     */
    public static TimeUtils currentTime() {
        Calendar now = Calendar.getInstance();
        int hour = now.get(Calendar.HOUR_OF_DAY);
        int minute = now.get(Calendar.MINUTE);
        int second = now.get(Calendar.SECOND);
        return new TimeUtils(hour, minute, second);
    }

    /**
     * Returns the current instance of time in seconds
     *
     * @return time to number of seconds
     */
    private int toSeconds() {
        return hour * 3600 + minute * 60 + second;
    }

    public TimeUtils addTime(TimeUtils otherTime){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalTime time = LocalTime.parse(this.toStringHHMMSS(),dtf);
        time = time.plus(otherTime.getHour(), ChronoUnit.HOURS);
        time = time.plus(otherTime.getMinute(), ChronoUnit.MINUTES);
        time = time.plus(otherTime.getSecond(), ChronoUnit.SECONDS);
        TimeUtils timeUtils = new TimeUtils(TimeUtils.setTime(time.toString()));
        this.setHour(timeUtils.getHour());
        this.setMinute(timeUtils.getMinute());
        this.setSecond(timeUtils.getSecond());
        return timeUtils;
    }

    public TimeUtils addMinutes(int minutes){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalTime time = LocalTime.parse(this.toStringHHMMSS(),dtf);
        time = time.plus(minutes, ChronoUnit.MINUTES);
        TimeUtils timeUtils = new TimeUtils(TimeUtils.setTime(time.toString()));
        this.setHour(timeUtils.getHour());
        this.setMinute(timeUtils.getMinute());
        this.setSecond(timeUtils.getSecond());
        return timeUtils;
    }


    //ANNA

    /**
     * Meethod to add or take minutes to a given time
     *
     * @param timeString
     * @param ammount
     * @return
     * @throws ParseException
     */
    public TimeUtils addOrTakeMinutes(String timeString, int ammount) throws ParseException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
        LocalTime time = LocalTime.parse(timeString);
        time = time.plus(ammount, ChronoUnit.MINUTES);
        TimeUtils timeUtils = new TimeUtils(TimeUtils.setTime(time.toString()));
        this.setHour(timeUtils.getHour());
        this.setMinute(timeUtils.getMinute());
        return new TimeUtils(this.hour, this.minute);
         }


    /**
     * method to see if string is wrote in asked format (hh:mm)
     *
     * @param timeString    String provided by the user
     * @param separator
     * @param pretendedSize
     * @return true if user wrote the string in the asked format
     */
    public static boolean seeIfItsPossibleToConvertStringToTimeUtils(String timeString, String separator, int pretendedSize) {
        boolean success = false;

        //separates string in parts (criteria to separate is what is the content of string separator)
        String[] passInArray = timeString.split(separator);
        //transforms the separated string into an array
        ArrayList<String> passArrayList = new ArrayList<String>(Arrays.asList(passInArray));

        //if array's size is pretendedSize (it's ine step further to be valid)
        if (passArrayList.size() == pretendedSize) {
            //Confirms if all the strings are parsable to an number
            success = seeIfItsANumber(passArrayList);
            if (success == true) {
                if (Integer.parseInt(passArrayList.get(0)) < 0 || Integer.parseInt(passArrayList.get(0)) >= 24 || Integer.parseInt(passArrayList.get(1)) < 0 || Integer.parseInt(passArrayList.get(1)) > 59) {
                    success = false;
                }
            }
        }
        return success;
    }


    /**
     * method to confirm if all elements of an array of strings are parsable to an number
     *
     * @param passArrayList array list to be verified
     * @return true if all elements of array list are parsable to number
     */
    public static boolean seeIfItsANumber(ArrayList<String> passArrayList) {
        boolean success = false;
        int i = 0;
        do {
            try {
                String string = passArrayList.get(i);
                int number = Integer.parseInt(string);
                i++;
                success = true;
            } catch (IllegalArgumentException e) {
                success = false;
            }
        } while (success == true && i < passArrayList.size());
        return success;
    }


    /**
     * Sets time provided by the user as TimeUtils
     *
     * @param timeString time provided by the user
     * @return timeString in object TimeUtils
     */
    public static TimeUtils setTime(String timeString) {
        TimeUtils time;
        String[] passInArray = timeString.split(":");
        ArrayList<String> passArrayList = new ArrayList<String>(Arrays.asList(passInArray));

        time = new TimeUtils(Integer.parseInt(passArrayList.get(0)), Integer.parseInt(passArrayList.get(1)));

        return time;
    }


    /**
     * Returns content of time in the following format: %02d%02d.
     *
     * @return content of time
     */
    public String toStringHHMM() {
        return String.format("%02d:%02d", hour, minute);
    }


    /**
     * Compares current instance of time with time received by parameter.
     *
     * @param otherTime the time to be comapred.
     * @return false if parameter time is biiger than instance
     */
    public boolean isBiggerOrEquals(TimeUtils otherTime) {
        return toSeconds() >= otherTime.toSeconds();
    }

}

