package app.domain.utils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class PasswordGenerator {
    public static String generateRandomPasswordOld(int len) {
        String chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi"
                +"jklmnopqrstuvwxyz!@#$%&";
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(chars.charAt(rnd.nextInt(chars.length())));
        return sb.toString();
    }

    /**
     * ADAPTED FROM: https://www.geeksforgeeks.org/generate-random-string-of-given-size-in-java/
     * https://www.geeksforgeeks.org/how-to-convert-a-string-to-arraylist-in-java/
     * https://stackoverflow.com/questions/29016999/printing-strings-from-an-array-in-random-order-java
     * https://stackoverflow.com/questions/13695547/arraylist-of-strings-to-one-single-string
     *
     * PS: Probably, there's a more efficient way to do this
     *
     * Method to generate a random Password with 7 alphanumerical characters
     * that has 2 capital letters and 2 digits
     *
     * @return string with 2 capital letters, 2 digits and 3 non capital letters
     */
    public static String generateRandomPassword() {
        //Possible capital letters
        String alphaCapString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        //Possible numerical letters
        String numeric = "0123456789";

        //Possible non capital letters
        String alphaOtherString = "abcdefghijklmnopqrstuvxyz!@#$%&";

        //Final set of 2 capital letters, 2 digits and 3 non capital letters
        //from alphaCapString, numeric and alphaOtherString
        String finalString = "";


        //To save the random items that were choosen to the finalString
        StringBuilder pass = new StringBuilder(7);


        //Chooses 3 capital letters randomly
        for (int i = 0; i < 3; i++) {
            int index = (int) (alphaCapString.length() * Math.random());
            finalString = finalString + (alphaCapString.charAt(index));
        }


        //Chooses 2 numbers randomly
        for (int i = 0; i < 2; i++) {
            int index = (int) (numeric.length() * Math.random());
            finalString = finalString + (numeric.charAt(index));
        }

        //Chooses 2 non capital letters randomly
        for (int i = 0; i < 2; i++) {
            int index = (int) (alphaOtherString.length() * Math.random());
            finalString = finalString + (alphaOtherString.charAt(index));
        }

        //Splits the string (criteria: no spaces (""))
        String[] passInArray = finalString.split("");

        //Converts the splited string to an array list
        ArrayList<String> passArrayList = new ArrayList<String>(Arrays.asList(passInArray));

        //To randomize te content of the array list
        Collections.shuffle(passArrayList);

        //To transform the array list into a string
        String joined = String.join("",passArrayList);

        return (joined);
    }
}
