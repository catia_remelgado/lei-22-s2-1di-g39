package app.domain.exceptions;

import java.util.NoSuchElementException;

/**
 * to indicate that a vaccination center was not found after a search
 * @author alexandraipatova
 */
public class VaccinationCenterNotFoundException extends NoSuchElementException {
    public VaccinationCenterNotFoundException(String message) {
        super(message);
    }
}
