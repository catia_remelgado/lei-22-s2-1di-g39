package app.domain.exceptions;
/**
 * @author João Miguel
 */
public class UserAlreadyExistsException extends Exception {

    public UserAlreadyExistsException(String message) {
        super(message);
    }
}
