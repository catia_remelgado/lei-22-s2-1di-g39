package app.domain.exceptions;

public class NotAWholeDivisionException extends ArithmeticException {
    public NotAWholeDivisionException(String message) {
        super(message);
    }
}
