package app.domain.exceptions;
/**
 * @author João Miguel
 */
public class UserUnknownException extends Exception {

    public UserUnknownException(String message) {
        super(message);
    }
}
