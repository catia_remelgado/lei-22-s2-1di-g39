package app.domain.exceptions;
/**
 * @author João Miguel
 */
public class AppointmentNotFoundException extends Exception {

    public AppointmentNotFoundException(String message) {
        super(message);
    }

}
