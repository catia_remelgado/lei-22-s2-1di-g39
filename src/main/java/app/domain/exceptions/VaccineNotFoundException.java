package app.domain.exceptions;


/**
 * to indicate that a vaccine has not found
 * @author João Miguel
 */
public class VaccineNotFoundException extends Exception {

    public VaccineNotFoundException(String message) {
        super(message);
    }
}