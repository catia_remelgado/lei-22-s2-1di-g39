package app.domain.exceptions;
/**
 * @author João Miguel
 */
public class NoWaitingRoomException extends Exception {
    public NoWaitingRoomException(String message) {
        super(message);
    }
    }

