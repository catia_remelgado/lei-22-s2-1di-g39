package app.domain.exceptions;

import java.util.NoSuchElementException;

/**
 * to indicate that a vaccination center was not found after a search
 * @author alexandraipatova
 */
public class VaccineAdministrationsNotFoundException extends NoSuchElementException {
    public VaccineAdministrationsNotFoundException(String message) {
        super(message);
    }
}
