package app.domain.exceptions;

public class NotPositiveNumberException extends IllegalArgumentException{
    public NotPositiveNumberException(String message) {
        super(message);
    }
}
