package app.domain.comparator;

import app.domain.model.SNSUser;

import java.util.Comparator;

/**
 * @author João Miguel
 */
public class CompareUserBySNSNumber implements Comparator {

    /**
     * compare sns numbers to check if they are the same or different
     * @param o1 sns number of the first user
     * @param o2 sns number of the second user
     */
    @Override
    public int compare(Object o1, Object o2) {

        if (o1 instanceof Integer && o2 instanceof SNSUser) {
            Integer snsNumber = (Integer) o1;
            SNSUser snsUser = (SNSUser) o2;

            return snsUser.getSnsNumber() - snsNumber;
        }

        return 0;
    }
}
