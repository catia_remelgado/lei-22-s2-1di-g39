package app.domain.comparator;

import app.domain.model.VaccineAdministrationProcess;

import java.util.Comparator;

/**
 * this class is used to comparate ID's between vaccines
 * @author alexandraipatova
 */
public class VaccineAdministrationProcessAgeComparator implements Comparator<VaccineAdministrationProcess> {
    /**
     * compares vaccines administration processes by their age
     * @param v1 vaccine administration process 1
     * @param v2 vaccine administration proces 2 (to be compared to)
     * @return returns comparison between v1 and v2. If v1's age group is smaller than v2's age group, then it returns -1. If v1's age group is bigger than age group from v2 it returns 1.
     */
    public int compare(VaccineAdministrationProcess v1, VaccineAdministrationProcess v2) {//must be improved
        int minAge1= v1.getAg().getMinimumAge();
        int minAge2 = v2.getAg().getMinimumAge();
        int maxAge1= v1.getAg().getMaximumAge();
        int maxAge2 = v2.getAg().getMaximumAge();

        if(maxAge1<maxAge2 && minAge1<minAge2)/*e.g.
                                                age group 1: 17-24
                                                age group 2: 35-65
                                                24<65 && 17<35 -> true */
            return -1;
        else if(maxAge1>maxAge2 && minAge1>minAge2)
            return 1;
        else if(maxAge1-minAge1<maxAge2-minAge2)/*e.g.
                                                24-17=7
                                                65-35=30
                                                7<30 -> true */
            return -1;
        else if(maxAge1-minAge1>maxAge2-minAge2)
            return 1;
        else
            return 0; //must be the same
    }
}
