package app.domain.comparator;

import app.domain.model.SNSUser;

import java.util.Comparator;

/**
 * @author João Miguel
 */
public class CompareUserByEmail implements Comparator {

    /**
     * compare emails to check if they are the same or different
     * @param o1 email of the first user
     * @param o2 email of the second user
     */
    @Override
    public int compare(Object o1, Object o2) {

        if (o1 instanceof String && o2 instanceof SNSUser) {
            String email = (String) o1;
            SNSUser snsUser = (SNSUser) o2;

            return snsUser.getEmail().toString().compareTo(email);
        }

        return 0;
    }
}
