package app.domain.comparator;

import app.domain.model.Vaccine;

import java.util.Comparator;

/**
 * this class is used to comparate ID's between vaccines
 * @author alexandraipatova
 */
public class VaccineIDComparator implements Comparator<Vaccine> {
    /**
     * compares vaccines by their ID
     * @param v1 vaccine 1
     * @param v2 vaccine 2 (to be compared to)
     * @return the comparison between v1 and v2. If v1 has a bigger ID than v2, it returns 1, if v2 has a bigger ID than v1 it returns -1, If they're the same it returns 0
     */
    public int compare(Vaccine v1, Vaccine v2) {
        int ID1 = v1.getID();
        int ID2 = v2.getID();

        if(ID1<ID2)
            return -1;
        else if(ID1>ID2)
            return 1;
        else
            return 0;
    }
}
