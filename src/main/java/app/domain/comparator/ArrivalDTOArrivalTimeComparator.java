package app.domain.comparator;

import app.domain.dto.ArrivalDTO;
import app.domain.utils.TimeUtils;

/**
 * compares arrival time of two arrival dto's
 * @author alexandraipatova
 */
public class ArrivalDTOArrivalTimeComparator {

    /**
     * compares arrivals DTO by their arrival
     * @param arrival1 arrival DTO 1
     * @param arrival2 arrival DTO 2 (to be compared to)
     * @return the comparison between arrival1 and arrival2. If arrival1 has a greater arrival time than arrival2, it returns 1, if arrival2 has a greater arrival time than arrival1 it returns -1, If they're the same it returns 0
     */
    public int compare(ArrivalDTO arrival1, ArrivalDTO arrival2) {
        TimeUtils time1 = arrival1.getArrivalTime();
        TimeUtils time2 = arrival2.getArrivalTime();

        if(time2.isBigger(time1))
            return -1;
        else if(time1.isBigger(time2))
            return 1;
        else
            return 0;
    }
}
