package app.domain.comparator;

import app.domain.model.SNSUser;

import java.util.Comparator;

/**
 * @author João Miguel
 */
public class CompareUserByCCNumber implements Comparator {

    /**
     * compare Citizen Card Numbers to check if they are the same or different
     * @param o1 Citizen Card Number of the first user
     * @param o2 Citizen Card Number of the second user
     */
    @Override
    public int compare(Object o1, Object o2) {

        if (o1 instanceof Integer && o2 instanceof SNSUser) {
            Integer ccNumber = (Integer) o1;
            SNSUser snsUser = (SNSUser) o2;

            return snsUser.getCCnumber() - ccNumber;
        }

        return 0;
    }
}
