package app.domain.dto;
import app.domain.utils.DateUtils;
import app.domain.utils.TimeUtils;

/**
 * Data transfer object for specific attributes of arrival class
 * @author alexandraipatova
 */
public class ArrivalDTO {

    /**
     * The time of arrival of the user
     */
    private TimeUtils arrivalTime;

    /**
     * the SNS number of the user that performed this arrival
     */
    private int  SNSNumber;

    /**
     * name of sns user that performed the arrival
     */
    private String name;

    /**
     * birthdate of sns user that performed the arrival
     */
    private DateUtils birthDate;

    /**
     * sex of sns user that performed the arrival
     */
    private String sex;

    /**
     * the scheduled time of the sns user that performed the arrival
     */
    private TimeUtils scheduledTime;

    /**
     * phone number of user that performed the arrival
     */
    private int phoneNumber;


    /**
     * constructor for an arrival DTO, receiving only pertinent information
     * @param arrivalTime the time of arrival of the sns user
     * @param SNSNumber sns  number of sns user that performed the arrival
     * @param name name of sns user  that performed arrival
     * @param scheduledTime scheduled time of sns user appointment
     */
    public ArrivalDTO(TimeUtils arrivalTime, int SNSNumber, String name, DateUtils birthDate, String sex, TimeUtils scheduledTime,int phoneNumber) {
        this.arrivalTime = arrivalTime;
        this.SNSNumber = SNSNumber;
        this.name = name;
        this.birthDate = birthDate;
        this.sex = sex;
        this.scheduledTime = scheduledTime;
        this.phoneNumber=phoneNumber;
    }

    /**
     * Returns true if the arrival dto is equivalent to our current instance of arrival.If not, returns false
     * @param arrivalDTO data transfer object that contains necessary information
     * @return true if the arrival dto is equivalent to our current instance of arrival.If not, returns false
     */
    public boolean equals(ArrivalDTO arrivalDTO) {
        if( this.arrivalTime == arrivalDTO.getArrivalTime() &&
            this.SNSNumber == arrivalDTO.getSNSNumber()&&
            this.name.equals(arrivalDTO.getName())&&
            this.birthDate == arrivalDTO.getBirthDate()&&
            this.sex.equals(arrivalDTO.getSex())&&
            this.scheduledTime == arrivalDTO.getScheduledTime()&&
            this.phoneNumber == arrivalDTO.getPhoneNumber()){

            return true;
        }
        else{
            return false;
        }
    }

    /**
     * returns arrival time of sns user
     * @return arrival time of sns user
     */
    public TimeUtils getArrivalTime() {
        return arrivalTime;
    }

    /**
     * returns sns number of sns user
     * @return sns number of sns number
     */
    public int getSNSNumber() {
        return SNSNumber;
    }

    /**
     * returns name of sns number
     * @return name of sns user
     */
    public String getName() {
        return name;
    }

    /**
     * returns scheduled time of appointment
     * @return scheduled time of appointment
     */
    public TimeUtils getScheduledTime() {
        return scheduledTime;
    }

    /**
     * returns birthdate of sns number
     * @return birthdate of sns user
     */
    public DateUtils getBirthDate() {
        return birthDate;
    }

    /**
     * returns sex of sns number
     * @return sex of sns user
     */
    public String getSex() {
        return sex;
    }

    /**
     * returns phone number of user that performed arrival
     * @return phone user of user that performed arrival
     */
    public int getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * returns content of this particular arrival
     * @return string that contains content of this particular arrival
     */
    @Override
    public String toString() {
        return arrivalTime.toStringHHMMSS()+"-> #"+SNSNumber+" "+name+"-> "+scheduledTime.toStringHHMMSS()+
                "\n"+"Date of birth: "+birthDate.toString()+
                "\nSex: "+sex+
                "\n Phone number: "+phoneNumber;
    }
}
