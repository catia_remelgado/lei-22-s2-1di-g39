package app.domain.dto;

import app.domain.comparator.ArrivalDTOArrivalTimeComparator;
import app.domain.model.Arrival;

import java.util.ArrayList;
import java.util.List;

/**
 * represents the data transfer object with list of information of waiting room
 * @author alexandraipatova
 */
public class WaitingRoomDTO {
    /**
     * list of arrival dto's of the waiting room to show
     */
    private List<ArrivalDTO> arrivalDTOList;

    /**
     * empty constructor of waiting room dto
     */
    public WaitingRoomDTO(List<ArrivalDTO> laDTO) {
        this.arrivalDTOList = laDTO;
    }

    /**
     * empty constructor of waiting room dto
     */
    public WaitingRoomDTO() {
        this.arrivalDTOList = new ArrayList<>();
    }

    /**
     * returns arrivalDTO list
     * @return arrivalDTO list
     */
    public List<ArrivalDTO> getArrivalDTOList() {
        return arrivalDTOList;
    }

    /**
     * returns waiting room arrivals and their full description
     * @return waiting room arrivals and their full description
     */
    @Override
    public String toString() {
        String waitingRoomArrivals="";
        for (ArrivalDTO arrivalDTOFromList : arrivalDTOList) {
            waitingRoomArrivals+=arrivalDTOFromList.toString()+"\n\n";
        }
        return waitingRoomArrivals;
    }
}
