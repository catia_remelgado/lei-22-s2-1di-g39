package app.domain.dto;

import app.domain.model.Gender;
import app.domain.model.SNSUser;
import app.domain.utils.DateUtils;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.util.List;

/**
 * DTO that contain all data from a file line, where each line represents an sns user
 * @author alexandraipatova
 */
public class SNSUserDTO {

    /**
     * sns number of each sns user
     */
    private int snsNumber;

    /**
     * name of  each sns user
     */
    private String name;

    /**
     *  birthdate of  each sns user
     */
    private DateUtils birthdate;

    /**
     *  email of  each sns user
     */
    private Email email;

    /**
     * PhoneNumber of  each sns user
     */
    private int phoneNumber;

    /**
     *  Gender of  each sns user
     */
    private Gender gender;

    /**
     *  Citizen Card number  of  each sns user
     */
    private int CCnumber;

    /**
     *  Address of  each sns user
     */
    private String address;

    /**
     * constructor for an SNS user that needs to be registered
     * @param snsNumber  snsNumber of each sns user
     * @param name  Name of each sns user
     * @param birthdate  birthdate of each sns user
     * @param email   email of each sns user
     * @param phoneNumber PhoneNumber of each sns user
     * @param gender Gender of each sns user
     * @param CCnumber  Citizen Card number  of  each sns user
     * @param address  address of  each sns user
     */
    public SNSUserDTO(int snsNumber, String name, DateUtils birthdate, Email email, int phoneNumber, Gender gender, int CCnumber, String address) {

        this.snsNumber = snsNumber;
        this.name = name;
        this.birthdate = birthdate;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.gender = gender;
        this.CCnumber = CCnumber;
        this.address = address;
    }

    /**
     * sns number of each sns user
     * @return sns number of each sns user
     */
    public int getSnsNumber() {
        return snsNumber;
    }

    /**
     *name of each sns user
     * @return name of each sns user
     */
    public String getName() {
        return name;
    }

    /**
     *birthdate of each sns user
     * @return birthdate of each sns user
     */
    public DateUtils getBirthdate() {
        return birthdate;
    }
    /**
     *email of each sns user
     * @return email of each sns user
     */
    public Email getEmail() {
        return email;
    }
    /**
     *phone number of each sns user
     * @return phone number of each sns user
     */
    public int getPhoneNumber() {
        return phoneNumber;
    }

    /**
     *gender of each sns user
     * @return gender of each sns user
     */
    public Gender getGender() {
        return gender;
    }
    /**
     *citizen card number of each sns user
     * @return citizen card number of each sns user
     */
    public int getCCnumber() {
        return CCnumber;
    }

    /**
     *address of each sns user
     * @return address of each sns user
     */
    public String getAddress() {
        return address;
    }
}
