package app.domain.dto;

import app.domain.model.VaccineAdministration;
import app.domain.utils.DateUtils;
import app.domain.utils.TimeUtils;

import java.util.List;

public class VaccineAdministrationDTO {

    /**
     * Sns number dto
     */
    private int snsNumber;

    /**
     * Vaccine name dto
     */
    private String vaccineName;

    /**
     * Dose number dto
     */
    private int doseNumber;

    /**
     * Lot number dto
     */
    private String lotNumber;

    /**
     * Administration date dto
     */
    private DateUtils administrationDate;

    /**
     * Arrival time dto
     */
    private TimeUtils arrivalTime;

    /**
     * Scheduled time dto
     */
    private TimeUtils scheduledTime;

    /**
     * Administration time dto
     */
    private TimeUtils administrationTime;

    /**
     * Leaving time dto
     */
    private TimeUtils leavingTime;

    /**
     * Constructor of the vaccine administration dto
     *
     * @param snsNumber          The SNS User number
     * @param vaccineName        The Vaccine Name
     * @param doseNumber         The dose Number
     * @param lotNumber          The Vaccine Lot Number
     * @param administrationDate Nurse Administration Date
     * @param arrivalTime        SNS User arrival Time
     * @param scheduledTime      The Scheduled Time
     * @param leavingTime        SNS User leaving Time
     */
    public VaccineAdministrationDTO(int snsNumber, String vaccineName, int doseNumber, String lotNumber, DateUtils administrationDate, TimeUtils arrivalTime, TimeUtils scheduledTime, TimeUtils administrationTime, TimeUtils leavingTime) {

        this.snsNumber = snsNumber;
        this.vaccineName = vaccineName;
        this.doseNumber = doseNumber;
        this.lotNumber = lotNumber;
        this.administrationDate = administrationDate;
        this.arrivalTime = arrivalTime;
        this.scheduledTime = scheduledTime;
        this.administrationTime = administrationTime;
        this.leavingTime = leavingTime;
    }


    /**
     * Get the sns number dto
     *
     * @return the sns nunber dto
     */
    public int getSnsNumber() {
        return snsNumber;
    }

    /**
     * Get the vaccine name dto
     *
     * @return the vaccine name dto
     */
    public String getVaccineName() {
        return vaccineName;
    }

    /**
     * Get the dose number dto
     *
     * @return the dose nuber dto
     */
    public int getDoseNumber() {
        return doseNumber;
    }

    /**
     * Get the lot number dto
     *
     * @return lot number dto
     */
    public String getLotNumber() {
        return lotNumber;
    }

    /**
     * Get the administration date dto
     *
     * @return administration date dto
     */
    public DateUtils getAdministrationDate() {
        return administrationDate;
    }

    /**
     * Get the arrival time dto
     *
     * @return arrival time dto
     */
    public TimeUtils getArrivalTime() {
        return arrivalTime;
    }

    /**
     * Get the scheduled time dto
     *
     * @return scheduled time dto
     */
    public TimeUtils getScheduledTime() {
        return scheduledTime;
    }

    /**
     * Get the dministration time dto
     *
     * @return dministration time dto
     */
    public TimeUtils getAdministrationTime() {
        return administrationTime;
    }

    /**
     * get the leaving time dto
     *
     * @return leaving time dto
     */
    public TimeUtils getLeavingTime() {
        return leavingTime;
    }
}
