package app.ui.console;

import app.controller.App;
import app.controller.SNSController;
import app.domain.model.Gender;
import app.domain.model.UserClone;
import app.domain.shared.Constants;
import app.domain.utils.PasswordGenerator;
import app.domain.utils.SerializationHelper;
import app.ui.console.utils.Utils;

import java.util.Date;
import app.domain.utils.DateUtils;
/**
 * @author João Miguel
 */
public class RegisterSNSUserUI implements Runnable {

    /**
     * Run the SNS User UI Menu.
     */
    @Override
    public void run() {

        boolean success = true;

        Integer snsNumber = Utils.readIntegerFromConsole("SNS Number: ");
        String name = Utils.readLineFromConsole("Name: ");
        Date birthdate = Utils.readDateFromConsole("Birthdate (dd-mm-aaaa): ");
        String email = Utils.readLineFromConsole("Email: ");
        Integer phoneNumber =Utils.readIntegerFromConsole("PhoneNumber:  ");
        String genderString = Utils.readLineFromConsole("Gender (M - Male, F - Female, N - None): ");
        Integer CCnumber = Utils.readIntegerFromConsole("CC number: ");
        String address = Utils.readLineFromConsole("Address: ");
        

        try {

            Gender gender = Gender.NONE;

            if (genderString.equals("M") || genderString.equals("m")) {
                gender = Gender.MALE;
            } else {
                if (genderString.equals("F") || genderString.equals("f")) {
                    gender = Gender.FEMALE;
                }
            }


            App.getInstance().getCompany().getSnsUserStore().addNewSNSUser(snsNumber, name, DateUtils.dateToDateUtils(birthdate), email, phoneNumber, gender, CCnumber, address);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            success = false;
        }
        String pass= PasswordGenerator.generateRandomPassword();
        System.out.println("\n"+pass);
        if (success) {
            App.getInstance().getCompany().getUCStore().addUser(new UserClone(name,email, pass, Constants.ROLE_SNSUSER));
            App.getInstance().addUser(name, email, pass, Constants.ROLE_SNSUSER);
            SerializationHelper.serializeCompany();
            System.out.println("Info: User was successfully created.");
        } else {
            System.out.println("Info: User was not successfully created.");
        }


    }
}
