package app.ui.console;

import app.controller.App;
import app.domain.model.VaccinationCenter;
import app.ui.console.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class EmployeeUI {

    private static EmployeeSession empSession;


    public void run(){

        List<VaccinationCenter> allVC=App.getInstance().getCompany().getVaccinationCenterStore().getAllVaccinationCenters();
        if( allVC.size()!=0) {
            System.out.println("Select the employee's vaccination center:");
            empSession = new EmployeeSession((VaccinationCenter) Utils.showAndSelectOne(App.getInstance().getCompany().getVaccinationCenterStore().getAllVaccinationCenters(), "Vaccination Center List"));
        }else{
            System.out.println("\n\nThere are no registered vaccination centers! Please register one first before logging in as an employee!");
            MainMenuUI menu = new MainMenuUI();
            try {
                menu.run();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static EmployeeSession getEmpSession() {
        return empSession;
    }
}
