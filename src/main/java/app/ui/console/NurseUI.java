package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */

public class NurseUI extends EmployeeUI implements Runnable{
    public NurseUI()
    {
    }

    public void run()
    {

        super.run();
        List<MenuItem> options = new ArrayList<MenuItem>();
        options.add(new MenuItem("Consult waiting Room. ", new ConsultWaitingRoomUI()));

        int option = 0;
        do
        {
            option = Utils.showAndSelectIndex(options, "\n\nNurse Menu:");

            if ( (option >= 0) && (option < options.size()))
            {
                options.get(option).run();
            }
        }
        while (option != -1 );
    }
}
