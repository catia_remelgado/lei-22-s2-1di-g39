package app.ui.console;

import app.controller.LoadUsersFromFileController;
import app.domain.utils.SerializationHelper;
import app.ui.console.utils.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class LoadUsersFromFileUI implements Runnable{

    @Override
    public void run(){
        LoadUsersFromFileController ctrl=new LoadUsersFromFileController();
        boolean flag=true;
        String path;
        File usersFile;



        while(flag){

            try{

                path= Utils.readLineFromConsole("Insert the path to the user's file (including the file itself):");
                usersFile=ctrl.saveFile(path,"SnsUsers");
                ctrl.loadUsersFromFile(usersFile);

                flag=false;

            } catch (FileNotFoundException e) {
                System.out.println("The file was not found!");
                flag=true;
            }catch(IOException e){
                System.out.println("An error has occurred: the file was not saved into the system");
                flag=true;
            }
            catch(IllegalArgumentException e){
                System.out.println("Incorrect data was found so the file was not loaded!");
                flag=true;

            }
        }

        System.out.println("Set of users added from file successfully!");
        SerializationHelper.serializeCompany();
    }
}
