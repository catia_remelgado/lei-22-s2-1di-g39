package app.ui.console;

import app.controller.App;
import app.controller.RegisterEmployeeController;
import app.domain.model.Employee;
import app.domain.model.EmployeeRolesList;
import app.domain.model.UserClone;
import app.domain.model.VaccinationCenter;
import app.domain.shared.Constants;
import app.domain.utils.PasswordGenerator;
import app.domain.utils.SerializationHelper;
import app.ui.console.utils.Utils;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;


public class RegisterEmployeeUI implements Runnable {
    /**
     * Controller of RegisterEmployeeUI
     */
    private final RegisterEmployeeController registerEmployeeController = new RegisterEmployeeController();

    @Override
    public void run() {
        System.out.println("You are in Register Employee UI. \n");

        ArrayList<EmployeeRolesList> roles2 = new ArrayList<EmployeeRolesList>(EnumSet.allOf(EmployeeRolesList.class));
        EmployeeRolesList role = null;
        int options = 0;

        options = Utils.showAndSelectIndex(roles2, "Select the employee's role:");

        role = roles2.get(options);
        VaccinationCenter vc=null;
        if(role.equals(EmployeeRolesList.CENTER_COORDINATOR)){
            List<VaccinationCenter> allVC=App.getInstance().getCompany().getVaccinationCenterStore().getAllVaccinationCenters();
            if( allVC.size()!=0) {
                System.out.println("Select the center coordinator's vaccination center:");
                vc= (VaccinationCenter) Utils.showAndSelectOne(App.getInstance().getCompany().getVaccinationCenterStore().getAllVaccinationCenters(), "Vaccination Center List");
            }else{
                System.out.println("\n\nThere are no registered vaccination centers! Please register one first before registering a center coordinator!");
                MainMenuUI menu = new MainMenuUI();
                try {
                    menu.run();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        System.out.println("\nIntroduce the employee data:");

        boolean flag = true;
        String name;
        String adress;
        String emailString = null;
        int phoneNumber = 0;
        int CCNumber = 0;

        name = Utils.readLineFromConsole("Name:");
        name = registerEmployeeController.seeIfStringIsValid(name, "name");

        adress = Utils.readLineFromConsole("Adress:");
        adress = registerEmployeeController.seeIfStringIsValid(adress, "adress");

        boolean seeIfIsValid = false;
        do {
            emailString = Utils.readLineFromConsole("E-mail:");
            emailString = registerEmployeeController.seeIfStringIsValid(emailString, "e-mail");
            seeIfIsValid = registerEmployeeController.seeIfItsPossibleToConvertStringToEmail(emailString);
            if (seeIfIsValid == false) {
                System.out.println("This email doesn't exist. Try again");
            }
        } while (seeIfIsValid == false);

        Email mail = new Email(emailString);


        do {
            phoneNumber = registerEmployeeController.seeIfItsANumber(phoneNumber, "Insert phone number:");
            if (phoneNumber < 100000000 || phoneNumber > 999999999) {
                System.out.println("\nInvalid number, it must have 9 digit.");
            }
        } while (phoneNumber < 100000000 || phoneNumber > 999999999);


        do {
            CCNumber = registerEmployeeController.seeIfItsANumber(CCNumber, "Insert Citizen Card Number:");
            if (CCNumber < 10000000 || CCNumber > 99999999) {
                System.out.println("\nInvalid number, it must have 8 digit.");
            }
        } while (CCNumber < 10000000 || CCNumber > 99999999);


        Employee emp = new Employee(role, name, adress, mail, phoneNumber, CCNumber);
        if (registerEmployeeController.verifyIfItsNewCCNumber(CCNumber)) {
            //registerEmployeeController.verifyIfIsUnique(emp)

            String pass=PasswordGenerator.generateRandomPassword();
            System.out.println(pass);

            App.getInstance().addUser(name, String.valueOf(mail), pass, String.valueOf(role));
            App.getInstance().getCompany().getUCStore().addUser(new UserClone(name, String.valueOf(mail), pass, String.valueOf(role)));
            Employee employee=registerEmployeeController.getInstance().addEmployee(role, name, adress, mail, phoneNumber, CCNumber);
            if(vc!=null){
                vc.setCenterCoordinator(employee);
            }
            System.out.println("This employee was successfully registered.");
            SerializationHelper.serializeCompany();
        } else {
            System.out.println("This employee can't be registered because there's already an employee with this Citizen Card Number registered in the system.");
        }




    }
}