package app.ui.console;

import app.controller.App;
import app.controller.CreateVaccineController;
import app.controller.VaccineTypeController;
import app.domain.model.DoseAdministration;
import app.domain.model.Vaccine;
import app.domain.model.VaccineAdministrationProcess;
import app.domain.model.VaccineType;
import app.domain.utils.SerializationHelper;
import app.ui.console.utils.Utils;

import java.util.ArrayList;

public class CreateVaccineUI implements Runnable {

    /**
     * run function of UI, presents and interacts with the actor
     */
    @Override
    public void run() {

        //flag used for validations
        boolean flag = true;



        VaccineType vaccineType=null;
        AdminUI ui=new AdminUI();
        Object option = null;
        System.out.println("Please introduce data regarding the vaccine:");
        do{

            if(!App.getInstance().getCompany().getVaccineTypeStore().getVaccineTypeList().isEmpty()) {
                option=Utils.showAndSelectOneWithoutCancel(App.getInstance().getCompany().getVaccineTypeStore().getVaccineTypeList(), "Vaccine types available:");
                vaccineType = (VaccineType) option;
            }else if(App.getInstance().getCompany().getVaccineTypeStore().getVaccineTypeList().isEmpty()){
                System.out.println("There are no vaccine types available! Redirecting to admin menu...");

                ui.run();
            }else{
                System.out.println("Invalid option! Try again!");
            }



        }while(vaccineType==null && !App.getInstance().getCompany().getVaccineTypeStore().getVaccineTypeList().isEmpty());

        int ID;
        String name;
        String brand;
        String technology;
        Vaccine vc = null;


        //create and validate vaccine

        while (flag) {
            try {
                ID = Utils.readIntegerFromConsole("ID: ");
                name = Utils.readLineFromConsole("Name: ");
                brand = Utils.readLineFromConsole("Brand: ");
                technology = Utils.readLineFromConsole("Technology: ");
                vc = (Vaccine) CreateVaccineController.getInstance().createVaccine(ID, name, brand,technology, vaccineType);
                flag = false;

            } catch (NumberFormatException ex) {
                System.out.println("The ID has to be a number! Try again");
                flag = true;
            } catch (ClassCastException ex) {//if the value returned is not a valid vaccine, it will return false, not being a valid cast
                System.out.println("One of the parameters is not valid! Try again");
                flag = true;
            }
        }

        int numberOfAgeGroups = 0;

        flag = true;

        //validate number of age groups
        while (flag) {
            try {
                System.out.println("Please introduce the number of age groups to apply this vaccine");
                numberOfAgeGroups = Utils.readIntegerFromConsole("Number of age groups: ");
                if (numberOfAgeGroups <= 0) {
                    throw new IllegalArgumentException();
                }
                flag = false;
            } catch (NumberFormatException ex) {
                System.out.println("The number of age groups has to be a number! Try again");
                flag = true;
            } catch (IllegalArgumentException ex) {
                System.out.println("The number of age groups is not valid! Try again");
                flag = true;
            }

        }
        flag = true;
        boolean confirmationFlag = true;
        int minAge;
        int maxAge;
        int numberOfDoses = 0;
        VaccineAdministrationProcess vap = null;
        DoseAdministration da = null;
        int doseNumber;
        int timePreviousToCurrentDose;
        float dosage;
        int recoveryTime;

        //insertion of several vaccine administration processes per age group
        for (int i = 0; i < numberOfAgeGroups && confirmationFlag; i++) {
            System.out.println("Please introduce the data regarding age group number " + (i + 1));
            //create vaccine administration process and validate

            while (flag) {
                try {
                    minAge = Utils.readIntegerFromConsole("Minimum Age: ");
                    maxAge = Utils.readIntegerFromConsole("Maximum Age: ");
                    numberOfDoses = Utils.readIntegerFromConsole("Number of doses:");
                    vap = (VaccineAdministrationProcess) CreateVaccineController.getInstance().createVaccineAdministrationProcess(minAge, maxAge, numberOfDoses, vc);
                    flag = false;

                } catch (NumberFormatException ex) {
                    System.out.println("The field has to be a number! Try again");
                    flag = true;
                } catch (ClassCastException ex) {
                    System.out.println("One of the parameters is not valid! Try again");
                    flag = true;
                }
            }
            flag = true;
            //confirm if user wants to add vaccine administration process, if not, return to adminUI
            System.out.println(vap.toString());
            if (Utils.confirm("Are you sure that you want to add this vaccine administration process? (s/n) (Saying no will exit the registration process)")) {

                if (vc.addVaccineAdministrationProcess(vap)) {
                    System.out.println("Vaccine administration process was successfully added!");


                    for (int j = 0; j < numberOfDoses && confirmationFlag; j++) {
                        System.out.println("Please introduce the data regarding dose number " + (j + 1));
                        doseNumber = j + 1;
                        //create dose administration and validate
                        while (flag) {
                            try {
                                if(j!=0) {
                                    timePreviousToCurrentDose = Utils.readIntegerFromConsole("Time between previous and current dose in days: ");
                                }else{
                                    timePreviousToCurrentDose=0;
                                }
                                dosage = (float) Utils.readDoubleFromConsole("Dosage in ml: ");
                                recoveryTime = Utils.readIntegerFromConsole("Recovery Time in minutes: ");
                                da = (DoseAdministration) CreateVaccineController.getInstance().createDoseAdministration(doseNumber, dosage, timePreviousToCurrentDose, recoveryTime, vap);
                                flag = false;

                            } catch (NumberFormatException ex) {
                                System.out.println("The number of doses has to be a number! Try again");
                                flag = true;
                            } catch (ClassCastException ex) {
                                System.out.println("One of the parameters is not valid! Try again");
                                flag = true;
                            }
                        }
                        flag = true;
                        //confirm if user wants to add dose administration, if not, return to adminUI
                        System.out.println(da.toString());
                        if (Utils.confirm("Are you sure that you want to add this dose administration?(s/n) (Saying no will exit the registration process)")) {
                            vap.addDoseAdministration(da);
                        } else {
                            System.out.println("No vaccine or dose administration was added");
                            confirmationFlag = false;
                        }
                    }

                    if (confirmationFlag) {
                        //confirm if user wants to add vaccine, if not, return to adminUI
                        System.out.println(vc.toString());
                        if (Utils.confirm("Are you sure that you want to add this vaccine? (s/n)  (Saying no will exit the registration process)")) {
                            if (CreateVaccineController.getInstance().saveVaccine(vc)) {
                                ArrayList<Vaccine> v=App.getInstance().getCompany().getVaccineStore().getVaccineList();
                                System.out.println("Vaccine added successfully!");
                            }else{
                                System.out.println("Vaccine was not added! The vaccine cannot have same ID as others.");
                            }
                        } else {
                            System.out.println("No vaccine or administration was added");
                            confirmationFlag = false;
                        }
                    }
                }else{
                    System.out.println("Vaccine administration process already exists for this age group! Exited registration...");
                    confirmationFlag = false;
                }
            } else {
                System.out.println("No vaccine or vaccine administration process was added.");
                confirmationFlag = false;
            }
        }

        SerializationHelper.serializeCompany();
    }

}
