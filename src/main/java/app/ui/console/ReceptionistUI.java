package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 * @author João Miguel
 *
 */

public class ReceptionistUI extends EmployeeUI implements Runnable{

    public ReceptionistUI()
    {

    }
    /**
     * Run the Receptionist UI Menu.
     */
    public void run()
    {
        super.run();
        System.out.println(EmployeeSession.getVaccinationCenter());

        List<MenuItem> options = new ArrayList<MenuItem>();
        options.add(new MenuItem("Register SNS User.", new RegisterSNSUserUI()));
        options.add(new MenuItem("Schedule a vaccination.", new AppointmentUI()));
        options.add(new MenuItem("Register the SNS User Arrival.", new RegisterArrivalSNSUserUI()));



        int option = 0;
        do
        {
            option = Utils.showAndSelectIndex(options, "\n\nReceptionist Menu:");

            if ( (option >= 0) && (option < options.size()))
            {
                options.get(option).run();
            }
        }
        while (option != -1 );
    }
}

