package app.ui.console;

import app.controller.App;
import app.controller.CreateVaccinationCenterController;
import app.domain.utils.SerializationHelper;
import app.ui.console.utils.Utils;
import app.domain.model.VaccinationCenterType;

import java.util.ArrayList;
import java.util.EnumSet;


public class CreateVaccinationCenterUI implements Runnable {
    private final CreateVaccinationCenterController createVaccinationCenterr = new CreateVaccinationCenterController(App.getInstance());

    private int slotDuration;
    private String closeHour;
    private int vaccinePerSlot;
    private int capacity;
    private int phone;
    private int faxNumber;
    private String openHour;
    private String ZIPCode;
    private String street;
    private String city;
    private int doorNumber;
    private String email;
    private String website;
    private String name;


    @Override

    public void run() {

        System.out.println("\n****** Vaccination Center ******");
        //podes por um enumerado por enquanto acerca do type e fazer com que o utilizador escolha entre duas opções
        // (mas o ideal seria criar a subclasse de HealthCareCenter e MassCommunityVaccinationCenter)
        ArrayList<VaccinationCenterType> type = new ArrayList<>(EnumSet.allOf(VaccinationCenterType.class));
        VaccinationCenterType types = null;
        int options = 0;

        options = Utils.showAndSelectIndex(type,"Select the type of vaccination center:");
        types = type.get(options);




        boolean flag = true;

        try {
            name = Utils.readLineFromConsole("Insert name:");
            while(!createVaccinationCenterr.validateName(name)){
                name = Utils.readLineFromConsole("Insert name:");
            }

            website = Utils.readLineFromConsole("Insert website (ex: https://exemplo.com :");
            while (!createVaccinationCenterr.validateWebsite(website)) {
                website = Utils.readLineFromConsole("Insert website:");
            }

            email = Utils.readLineFromConsole("Insert email :");

            while (!createVaccinationCenterr.validateEmail(email)) {
                email = Utils.readLineFromConsole("Insert email (ex: exemplo@gmail.com):");
            }

            try {
                phone = Utils.readIntegerFromConsole("Insert phone number (With 9 digist):");

            } catch (NumberFormatException ex) {
                flag = true;
            }

            while (!createVaccinationCenterr.validatePhoneNumber(phone)) {
                phone = Utils.readIntegerFromConsole("Insert again the phone number (With 9 digist):");
            }


            try {
                faxNumber = Utils.readIntegerFromConsole("Insert fax number:");
            } catch (NumberFormatException ex) {
                flag = true;
            }

            while (!createVaccinationCenterr.validateFaxNumber(faxNumber)) {
                faxNumber = Utils.readIntegerFromConsole("Insert again the fax number (With 9 digist):");
            }

            try {
                //verificar se não é zero
                capacity = Utils.readIntegerFromConsole("Insert capacity:");
            } catch (NumberFormatException ex) {
                flag = true;
            }

            try {
                //verificar se não é zero
                slotDuration = Utils.readIntegerFromConsole("Insert slot duration:");
            } catch (NumberFormatException ex) {
                flag = true;
            }
            try {
                //verificar se não é zero
                vaccinePerSlot = Utils.readIntegerFromConsole("Insert max number of vaccines:");

            } catch (NumberFormatException ex) {
                flag = true;
            }

            try {
                //transformar num objeto da classe TimeUtils
                openHour = Utils.readLineFromConsole("Insert hour of opening (ex 10:00): ");
            } catch (NumberFormatException ex) {
                flag = true;
            }

            try {
                //transformar num objeto da classe TimeUtils
                closeHour = Utils.readLineFromConsole("Insert hour of closing (ex 22:00): ");
            } catch (NumberFormatException ex) {
                flag = true;
            }
            /**
             * Data regarding address
             */
            System.out.println("\n Please insert the data, regarding the address:");
            try {
                ZIPCode = Utils.readLineFromConsole("Insert zip code (ex. 4470-771): ");
            } catch (NumberFormatException e) {
                flag = true;
            }

            while (!createVaccinationCenterr.validateZIPCode(ZIPCode)) {
                ZIPCode = Utils.readLineFromConsole("Insert zip code (ex. 4470-771): ");
            }

            street = Utils.readLineFromConsole("Insert street:");

            while (!createVaccinationCenterr.validateStreet(street)) {
                street = Utils.readLineFromConsole("Insert street:");
            }

            doorNumber = Utils.readIntegerFromConsole("Insert door number: ");

            city = Utils.readLineFromConsole("Insert city: ");


            while (!createVaccinationCenterr.validateCity(city)) {
                city = Utils.readLineFromConsole("Insert city: ");
            }


        } catch (NumberFormatException ex) {
            System.out.println("The field has to be a number! Try again");
            flag = true;
        }
        flag = true;

        if (CreateVaccinationCenterController.createVaccinationCenter(String.valueOf(types), name, street, doorNumber, ZIPCode, city, phone, email, faxNumber, website, openHour
                , closeHour, slotDuration, vaccinePerSlot, capacity)) {
            System.out.println("Vaccination center registered successfully!");
        } else {
            System.out.println("Vaccination center registration failed!");
        }
        SerializationHelper.serializeCompany();
    }
}