package app.ui.console;

import app.controller.App;
import app.controller.VaccineTypeController;

import app.ui.console.utils.Utils;
import org.apache.commons.lang3.StringUtils;

/**
 * @author João Miguel
 */

public class VaccineTypeUI implements Runnable {

    /**
     * run function of UI, presents and interacts with the actor
     */

    @Override
    public void run() {

        String option;
        boolean success=false;
        String code= "";
        String designation= "";

        do {
            code = Utils.readLineFromConsole("Code: ");
            if (StringUtils.isBlank(code)){
                System.out.println("Code cannot be blank.");
                success=false;
            }
            if ( (code.length() < 4) || (code.length() > 8)) {
                System.out.println("Code must have 4 to 8 chars.");
                success=false;
            }
            if((code.length() >= 4) && (code.length() <= 8)){
                success=true;
            }
        }while(success==false);

        do {
            designation = Utils.readLineFromConsole("Designation: ");
            if (StringUtils.isBlank(designation)){
                System.out.println("Designation cannot be blank.");
                success=false;
            }
            if ( (designation.length() > 40 )) {
                System.out.println("Designation cannot have more than 40 chars.");
                success=false;
            }
        }while(success==false);

        System.out.println("New Vaccination Type: ");
        System.out.println("\tCode: " + code);
        System.out.println("\tDesignation: " + designation);

        do {
            option = Utils.readLineFromConsole("Do you want to proceed? (Y/N)");

        } while (!option.equals("Y") && !option.equals("N") && !option.equals("y") && !option.equals("n"));

        if (option.equals("Y") || option.equals("y")) {
            try{
                if  (App.getInstance().getCompany().getVaccineTypeStore().addVaccineType(code, designation)) {
                    App.getInstance().getCompany().getVaccineTypeStore().createVaccineType(code, designation);
                    System.out.println("Vaccine Type was successfully created.");
                }
            }catch (IllegalArgumentException e){
                System.out.println("Vaccine Type was not successfully created.");
            }

        }
    }
}
