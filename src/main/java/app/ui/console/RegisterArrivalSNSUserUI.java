package app.ui.console;

import app.controller.*;

import app.domain.exceptions.NoWaitingRoomException;
import app.domain.model.*;
import app.domain.utils.DateUtils;
import app.domain.utils.TimeUtils;
import app.domain.utils.SerializationHelper;
import app.ui.console.utils.Utils;



/**
 * @author João Miguel
 */
public class RegisterArrivalSNSUserUI implements Runnable {

    /**
     * Run the SNS User Arrival UI Menu.
     */
    @Override
    public void run() {
        VaccinationCenter vaccinationCenter=new VaccinationCenter();
        String dateString = "";
        DateUtils date;
        String timeString = "";
        TimeUtils time = TimeUtils.setTime("00:00");

        boolean success = false;
        int snsNumber;
        String option = "";

        do{
             snsNumber = Utils.readIntegerFromConsole("SNS Number: ");
             success = SNSUser.verifyIfSNSUserNumberIsCorrect(snsNumber);

             if (!success) {
                 System.out.println("\nInvalid SNS Number(must have 9 digits)!");
             }

        }while(!success);


        do {
            try {
                vaccinationCenter = (VaccinationCenter) Utils.showAndSelectOneWithoutCancel(App.getInstance().getCompany().getVaccinationCenterStore().getAllVaccinationCenters(), "\nSelect the Appointment vaccination center:");
                success=true;
            }catch (Exception e){
                System.out.println("\nInfo: You should select a valid Vaccination Center !");
                success=false;
         }
        }while(!success);


        do {
            do {
                dateString = Utils.readLineFromConsole("Write the Appointment day (dd-mm-yyyy):");
                success = AppointmentController.getInstance().seeIfDateIsCorrect(dateString, "-");
                if (success == false) {
                    System.out.println("\nThis date was incorrectly written, type again.");
                }
            } while (success == false);
            date = AppointmentController.getInstance().setDate(dateString, "-");
            if (AppointmentController.getInstance().confirmIfDateItsNotInThePast(date) == false) {
                System.out.println("\nIt seems that someone is trying to travel in time...\nUnfortunately, we still don't have that kind of technology.\nBut you can always travel to the future ;)\n(But not too far.)");
                success = false;
            }
            if (AppointmentController.getInstance().confirmIfDateItsNotTooInTheFuture(date) == true){
                System.out.println("\n This date was incorrectly written, type again.");
                success = false;
            }
        } while (success == false);

        do {
            timeString = Utils.readLineFromConsole("Write the Arrival Time (hh:mm):");
            success = AppointmentController.getInstance().seeIfTimeIsCorrect(timeString);
            if (success == false) {
                System.out.println("\nThis time was incorrectly written, type again.");
            }
            if (success == true) {
                time = AppointmentController.getInstance().setTime(timeString);
                }
        } while (success == false);


        try{

            //AuthController authController = new AuthController();
           // VaccinationCenter center = authController.getUserVaccinationCenter();

            SNSUser user = App.getInstance().getCompany().getSnsUserStore().get(snsNumber);
            //Appointment appointment = AppointmentController.get(user,date, time);
            //Appointment appointment =AppointmentController.getInstance().verifyIfTheresAnAppointment(user,date, time);
            Appointment appointment =  App.getInstance().getCompany().getAppointmentStore().get(user,date, time);


            do {
                option = Utils.readLineFromConsole("Do you want to add the User to the Waiting Room? (Y/N)");

            } while (!option.equals("Y") && !option.equals("N") && !option.equals("y") && !option.equals("n"));

            if (option.equals("Y") || option.equals("y")) {
              vaccinationCenter.addToWaitingRoom(user,time,appointment);
                System.out.println("Info: User Arrival was successfully registered and was placed in the waiting room.");
            }else{
                throw new NoWaitingRoomException("Info:The user has not been placed in the waiting room.");
            }

        }catch (Exception e){
            System.out.println("Error: " + e.getMessage());
            System.out.println("\nInfo: User Arrival was not successfully registered.");
        }
        SerializationHelper.serializeCompany();
    }
}
