package app.ui.console;

import app.controller.App;
import app.controller.AppointmentController;
import app.controller.VaccineTypeController;
import app.domain.model.SMS;
import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineType;
import app.domain.shared.Constants;
import app.domain.utils.DateUtils;
import app.domain.utils.TimeUtils;
import app.domain.utils.SerializationHelper;
import app.ui.console.utils.Utils;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.text.ParseException;
import java.util.*;

public class AppointmentUI implements Runnable {
    @Override
    public void run() {
        AppointmentController ctrl = new AppointmentController().getInstance();
        SNSUserUI menu = new SNSUserUI();
        VaccineType vaccineType = new VaccineType("_404_", "Not_Found");
        VaccinationCenter vaccinationCenter = new VaccinationCenter("type", "name","street", 256, "ZIPCode", "city,", 100000000, "email@email.email", 100000000, "website.website", "10:00"
                , "22", 0, 0, 0);
        String dateString = "";
        DateUtils date;
        String timeString = "";
        TimeUtils time = TimeUtils.setTime("00:00");
        int snsUserNumber = 0;
        //UserSession session = App.getInstance().getCurrentUserSession();
        //Email mail = (session.getUserId());
        Email mail = App.getInstance().getCurrentUserSession().getUserId();
        List<String> somethingWentWrong = new ArrayList<String>();
        somethingWentWrong.add("Cancel schedule.");
        somethingWentWrong.add("Choose again.");


        //System.out.println("This user email is: "+mail.toString());

        boolean success = false;

        //To write and confirm SNS user number
        if (App.getInstance().getCurrentUserSession().isLoggedInWithRole(Constants.ROLE_SNSUSER)) {
            do {
                do {
                    snsUserNumber = Utils.readIntegerFromConsole("\nInsert your SNS number:");
                    success = ctrl.verifySNSUserNumberFormat(snsUserNumber);
                    if (success == false) {
                        System.out.println("\nSorry, but this number was incorrectly written.\nRemember that SNS number has 9 digits.\nPlease type again.");
                    }
                } while (success == false);
                success = ctrl.searchSNSUserNumber(mail, snsUserNumber);
                if (success == false) {
                    System.out.println("\nThe given SNS number doesn't match with the SNS number associated to the actual logged account.\nPlease, type the number again.");
                }
            } while (success == false);
        }

        if (App.getInstance().getCurrentUserSession().isLoggedInWithRole(Constants.ROLE_RECEPTIONIST)){
            do {
                do {
                    snsUserNumber = Utils.readIntegerFromConsole("\nInsert the SNS number of the person:");
                    success = ctrl.verifySNSUserNumberFormat(snsUserNumber);
                    if (success == false) {
                        System.out.println("\nSorry, but this number was incorrectly written.\nRemember that SNS number has 9 digits.\nPlease type again.");
                    }
                } while (success== false);
                success = ctrl.verifyIfSNSUserNumberExists(snsUserNumber);
                if (success == false) {
                    System.out.println("\nIt seems that this number it's not registered in the system.\nPlease, type the number again.");
                }
            } while (success == false);
        }


        success = false;
        //To choose a vaccine type
        do {
            if (ctrl.vaccineTypeStoreExists() == true) {
                vaccineType = (VaccineType) Utils.showAndSelectOneWithoutCancel(App.getInstance().getCompany().getVaccineTypeStore().getVaccineTypeList(), "\nSelect the vaccine type to be taken:");
            } else {
                System.out.println("\nSorry, there are no available vaccine types.\nYou can't schedule a vaccine.");
                menu.run();
            }
            success = ctrl.confirmIfItsFirstSchedule(vaccineType, snsUserNumber);
            if (success == false) {
                int option = Utils.showAndSelectIndex(somethingWentWrong, "\nThis vaccine type was already scheduled for this user.\nWhat do you want to do?\n");
                if (option == 0 || option == -1) {
                    menu.run();
                }
            }
        } while (success == false);


        //To choose a vaccination center
        if (ctrl.vaccinationCentersStoreExists()) {
            vaccinationCenter = (VaccinationCenter) Utils.showAndSelectOneWithoutCancel(App.getInstance().getCompany().getVaccinationCenterStore().getAllVaccinationCenters(), "\nSelect the desired vaccination center:");
        } else {
            System.out.println("\nSorry, there are no registered vaccination centers.\nYou can't schedule a vaccine.");
            menu.run();
        }

        success = false;

        //To choose a date
        do {
            do {
                dateString = Utils.readLineFromConsole("Write the desired date (dd-mm-yyyy):");
                success = ctrl.seeIfDateIsCorrect(dateString, "-");
                if (success == false) {
                    System.out.println("\nThis date was incorrectly written, type again.");
                }
            } while (success == false);
            date = ctrl.setDate(dateString, "-");
            if (ctrl.confirmIfDateItsNotInThePast(date) == false) {
                System.out.println("\nIt seems that someone is trying to travel in time...\nUnfortunately, we still don't have that kind of technology.\nBut you can always travel to the future ;)\n(But not too far.)");
                success = false;
            }
            if (ctrl.confirmIfDateItsNotTooInTheFuture(date) == true){
                System.out.println("\nIt seems that you are trying to schedule your appointment to an date where you may be not present. It's better to try another date that's closer to the present.");
                success = false;
            }
        } while (success == false);

        //System.out.println(date.toString());


        success = false;

        //To choose time
        do {
            timeString = Utils.readLineFromConsole("Write the desired time (hh:mm):");
            success = ctrl.seeIfTimeIsCorrect(timeString);
            //success = TimeUtils.seeIfItsPossibleToConvertStringToDateUtils(timeString, ":", 2);
            if (success == false) {
                System.out.println("\nThis time was incorrectly written, type again.");
            }
            if (success == true) {
                time = ctrl.setTime(timeString);
                try {
                    success = ctrl.verifyIfTimeIsInsideTheWorkingSchedule(time, vaccinationCenter);
                } catch (ParseException e) {
                    System.out.println("Something went wrong.");
                    //e.printStackTrace();
                }
                if (success == false) {
                    // System.out.println("Sorry, but the chosen vaccination center doesn't work at this time.");
                    int option = Utils.showAndSelectIndex(somethingWentWrong, "\nSorry, but the chosen vaccination center doesn't work at this time.\nWhat do you want to do?\n");
                    if (option == 0 || option == -1) {
                        menu.run();
                    }
                }
            }
        } while (success == false);

        //time = ctrl.setTime(timeString);

        //System.out.println(time.toStringHHMM());


        //success = ctrl.verifyIfItsPossibleToSchedule(snsUserNumber, vaccineType, vaccinationCenter, date, time);


        try {
            if (ctrl.verifyIfItsPossibleToSchedule(vaccinationCenter, date, time) == true) {
                ctrl.scheduleAppointment(snsUserNumber, vaccineType, vaccinationCenter, date, time);
                success = true;
                //if (success==true){
                System.out.println("\nYour appointment was successfully registered.");
                // } else {
                // System.out.println("Your appointment was NOT successfully registered.");
                //}
            } else {
                success = false;
                System.out.println("\nSorry, but this day it's not available. You will need to repeat this process.");
            }
        } catch (ParseException e) {
            success = false;
            System.out.println("\nSomething went wrong, sorry.");
        }

        boolean sended = false;

        if (success == true) {
            String option = "";
            do {
                option = Utils.readLineFromConsole("\nDo you want to receive a message about the appointment tha you just scheduled? (Y/N)");

            } while (!option.equalsIgnoreCase("Y") && !option.equalsIgnoreCase("N"));

            if (option.equals("Y") || option.equals("y")) {
                sended = new SMS().SendMessage(date, time, vaccinationCenter);
            } else {
                success = false;
                System.out.println("You will not receive a message.");
            }

            if (sended == true) {
                System.out.println("Your message was successfuly sended.");
            } else {
                System.out.println("Sorry, something went wrong.");
            }
        }

        //Date dataDate = DateUtils.dateUtilsToDate(date);
        //System.out.println(dataDate.toString());
        SerializationHelper.serializeCompany();
    }

}


