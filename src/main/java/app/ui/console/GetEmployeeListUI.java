package app.ui.console;

import app.controller.GetEmployeeListController;
import app.domain.model.Employee;
import app.domain.store.EmployeeStore;

import java.util.ArrayList;

public class GetEmployeeListUI implements Runnable {

    private final GetEmployeeListController getEmployeeListController = new GetEmployeeListController();

    @Override
    public void run() {
        /**
         * represents the position of the wanted role in the Enum lis of roles
         */
        int option = getEmployeeListController.showRolesSelectionList();

        /**
         * To make the employee list with the role that the admin pretends
         */
        ArrayList<Employee> employeesWithWantedRole = getEmployeeListController.makeListOfEmployeesWithWantedRole(option);

        /**
         * Shows the list
         */
        if (employeesWithWantedRole.size()!=0) {
            for (int i = 0; i < employeesWithWantedRole.size(); i++) {
                System.out.println(employeesWithWantedRole.get(i));
            }
        } else {
            System.out.println("There are no employees with this role " + new EmployeeStore().convertOptionToRole(option)+".");
        }
    }
}
