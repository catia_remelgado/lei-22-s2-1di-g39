package app.ui.console;

import app.domain.model.VaccinationCenter;

public class EmployeeSession {

    private static VaccinationCenter vc;

    public EmployeeSession(VaccinationCenter vc) {
        this.vc = vc;
    }

    public static VaccinationCenter getVaccinationCenter() {
        return vc;
    }

    public void setVaccinationCenter(VaccinationCenter vc) {
        this.vc = vc;
    }
}
