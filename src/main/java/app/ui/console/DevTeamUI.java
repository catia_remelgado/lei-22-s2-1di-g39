package app.ui.console;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class DevTeamUI implements Runnable{

    public DevTeamUI()
    {

    }
    public void run()
    {
        System.out.println("\n");
        System.out.printf("Development Team:\n");
        System.out.printf("\t Student Alexandra Ipatova - 1211274@isep.ipp.pt \n");
        System.out.printf("\t Student Anna Vasylyshyna - 1211313@isep.ipp.pt \n");
        System.out.printf("\t Student Cátia Remelgado  - 1210787@isep.ipp.pt \n");
        System.out.printf("\t Student Fernando Silva   - 1191666@isep.ipp.pt \n");
        System.out.printf("\t Student João Miguel      - 1211334@isep.ipp.pt \n");
        System.out.println("\n");
    }
}
