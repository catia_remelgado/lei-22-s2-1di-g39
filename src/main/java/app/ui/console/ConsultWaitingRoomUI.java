package app.ui.console;

import app.controller.ConsultWaitingRoomController;
import app.domain.dto.ArrivalDTO;
import app.domain.model.*;
import app.domain.utils.DateUtils;
import app.domain.utils.TimeUtils;
import app.ui.console.utils.Utils;
import pt.isep.lei.esoft.auth.domain.model.Email;

import java.util.ArrayList;
import java.util.List;

/**
 * UI to interacte with nurse and show waiting room list
 * @author alexandraipatova
 */
public class ConsultWaitingRoomUI implements Runnable{
    /**
     * run function of UI, presents and interacts with the actor
     */
    @Override
    public void run() {

        //Setting dummy vaccination center
        VaccinationCenter vc = new VaccinationCenter("healthcare center",
                "Centro de Saúde do Barão do Corvo",
                "R. Barão do Corvo ",
                676, "4400-037"," Vila Nova de Gaia",
                223747010,
                "usf.arcoprado@arsnorte.min-saude.pt",
                93848374,
                "baraodocorvo.com",
                "09:00", "20:00", 45, 30, 2);

        //Seting expected result
        ArrayList<Arrival> arrivalList=new ArrayList<>();
        ArrivalDTO ar1=new ArrivalDTO(new TimeUtils(12,43,15),123456788,"João Ratão",new DateUtils(1997,12,13),"MALE",new TimeUtils(13,0,0),738273849);
        ArrivalDTO ar2=new ArrivalDTO(new TimeUtils(15,5,37),738273849,"Maria Joaninha",new DateUtils(2005,7,1),"FEMALE",new TimeUtils(15,30,0),273849365);
        ArrivalDTO ar3=new ArrivalDTO(new TimeUtils(16,58,23),829384028,"Carochinha",new DateUtils(1967,9,29),"FEMALE",new TimeUtils(17,0,0),374859376);
        List<ArrivalDTO> arrivalListDTOExpected=new ArrayList<>();
        arrivalListDTOExpected.add(ar1);
        arrivalListDTOExpected.add(ar2);
        arrivalListDTOExpected.add(ar3);



        //Setting converted result
        ConsultWaitingRoomController ctrl = new ConsultWaitingRoomController();


        //ConsultWaitingRoomController ctrl = new ConsultWaitingRoomController();
        //ctrl.getWaitingRoom(ctrl.getSessionVaccinationCenter());
        //if(list==null&&list.isEmpty()){
            //System.out.println("There is no one in the waiting room!");
        //}else{
            Utils.showList(arrivalListDTOExpected, "Waiting Room List by order of arrival:");

        //}

    }
}
