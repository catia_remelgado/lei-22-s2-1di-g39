package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */

public class AdminUI implements Runnable{
    public AdminUI()
    {
    }

    public void run()
    {
        List<MenuItem> options = new ArrayList<MenuItem>();
        options.add(new MenuItem("Register new vaccine and administration process. ", new CreateVaccineUI()));
        options.add(new MenuItem("Specify a new vaccine type. ", new VaccineTypeUI()));
        options.add(new MenuItem("Register new employee.", new RegisterEmployeeUI()));
        options.add(new MenuItem("Get list of employees with a certain role.", new GetEmployeeListUI()));
        options.add(new MenuItem("Register new vaccination center.", new CreateVaccinationCenterUI()));
        options.add(new MenuItem("Load a set of users from a CSV file.", new LoadUsersFromFileUI()));

        int option = 0;
        do
        {
            option = Utils.showAndSelectIndex(options, "\n\nAdmin Menu:");

            if ( (option >= 0) && (option < options.size()))
            {
                options.get(option).run();
            }
        }
        while (option != -1 );
    }
}
