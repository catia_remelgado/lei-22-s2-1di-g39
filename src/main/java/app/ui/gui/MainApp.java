package app.ui.gui;

import app.controller.App;
import app.controller.AuthController;
import app.domain.model.VaccinationCenter;
import app.domain.shared.Constants;
import app.ui.console.EmployeeSession;
import app.ui.console.MainMenuUI;
import app.ui.console.MenuItem;
import app.ui.console.utils.Utils;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import pt.isep.lei.esoft.auth.UserSession;
import pt.isep.lei.esoft.auth.mappers.dto.UserDTO;
import pt.isep.lei.esoft.auth.mappers.dto.UserRoleDTO;

import javax.management.relation.Role;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

/**
 * Main application UI
 * @author alexandraipatova
 */
public class MainApp extends Application {

    /**
     * stage of the view
     */
    private Stage stage;
    /**
     * scene of the view
     */
    private Scene scene;

    /**
     * root of the view
     */
    private Parent root;
    /**
     * used to redirect to authfacade functions
     */
    private AuthController ctrl=new AuthController();

    /**
     * attempts taken at login
     */
    private int maxAttempts = 3;

    /**
     * text field where the email is inserted
     */

    @FXML
    private TextField txtEmail;

    /**
     * password field where the email is inserted
     */
    @FXML
    private PasswordField txtPassword;
    /**
     * label that indicates if login is invalid
     */
    @FXML
    private Label lblInvalidLogin;


    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(MainApp.class.getResource("/fxml/login.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 500, 500);
        stage.setTitle("Login");
        stage.setScene(scene);
        stage.show();
    }



    public static void main(String[] args) {
        launch();
    }

    /**
     * verifies email and login and if it exists as a user
     * redirects to center coordinator view
     * @param event clicking on login button
     * @throws IOException thrown when the redirecting UI is not loaded correctly
     */
    @FXML
    void redirectToMain(ActionEvent event) throws IOException {

        EmployeeSession empSession;
        if(maxAttempts>0) {
            boolean success = false;
            maxAttempts--;
            String id = txtEmail.getText();
            String pwd = txtPassword.getText();

            success = ctrl.doLogin(id, pwd);
            if (!success) {
                lblInvalidLogin.setVisible(true);
                lblInvalidLogin.setText("Invalid UserId and/or Password. \n You have  " + maxAttempts + " more attempt(s).");
                System.out.println("Invalid UserId and/or Password. \n You have  " + maxAttempts + " more attempt(s).");
            }


            if (success) {
                lblInvalidLogin.setVisible(false);

                if (App.getInstance().getCurrentUserSession().isLoggedInWithRole(Constants.ROLE_CENTER_COORDINATOR)) {
                    root = FXMLLoader.load(getClass().getResource("/fxml/center-coordinator-view.fxml"));
                    stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    stage.setTitle("Center Coordinator Menu");
                    scene = new Scene(root);
                    stage.setScene(scene);
                    stage.show();
                } else if (App.getInstance().getCurrentUserSession().isLoggedInWithRole(Constants.ROLE_NURSE)) {

                    List<VaccinationCenter> allVC=App.getInstance().getCompany().getVaccinationCenterStore().getAllVaccinationCenters();
                    if( allVC.size()!=0) {
                        System.out.println("Select the employee's vaccination center:");
                        empSession = new EmployeeSession((VaccinationCenter) Utils.showAndSelectOne(App.getInstance().getCompany().getVaccinationCenterStore().getAllVaccinationCenters(), "Vaccination Center List"));
                    }else{
                        System.out.println("\n\nThere are no registered vaccination centers! Please register one first before logging in as an employee!");
                        MainMenuUI menu = new MainMenuUI();
                        try {
                            menu.run();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    root = FXMLLoader.load(getClass().getResource("/fxml/record-vaccine-administration.fxml"));
                    stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    stage.setTitle("Record vaccine administration");
                    scene = new Scene(root);
                    stage.setScene(scene);
                    stage.show();
                }


            }
        }else{
            lblInvalidLogin.setVisible(true);
            lblInvalidLogin.setText("You have run out of attempts!");
        }
    }
}
