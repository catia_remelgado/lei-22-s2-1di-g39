package app.ui.gui;

import app.controller.*;
import app.domain.dto.SNSUserDTO;
import app.domain.exceptions.UserAlreadyExistsException;
import app.domain.exceptions.UserUnknownException;
import app.domain.exceptions.VaccineNotFoundException;
import app.domain.model.SMS;
import app.domain.model.SNSUser;
import app.domain.model.Vaccine;
import app.domain.utils.DateUtils;
import app.domain.utils.SerializationHelper;
import app.ui.console.NurseUI;
import app.ui.gui.MainApp;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.layout.Background;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class RecordVaccineAdministrationUI implements Runnable {

    @FXML
    private Button btnClose;

    @FXML
    private Button btnOk;

    @FXML
    private TextField txtName;

    @FXML
    private TextField txtLotNumber;

    @FXML
    private ListView<Integer> listWaitingRoom;

    @FXML
    private TextField txtAdvRec;

    @FXML
    private ListView<String> listVaccines;

    @FXML
    private TextField txtAge;

    @FXML
    private Label lblLotNumber;


    private RecordVaccineAdministrationController controller;

    private HashMap<Integer, SNSUser> snsUserList;

    private int age;

    private SNSUser snsUser=null;

    String lotNumber;

    @FXML
    public void SelectSNSUser(MouseEvent mouseEvent) throws UserUnknownException {

        int usernumber;
        ObservableList list = listWaitingRoom.getSelectionModel().getSelectedItems();

        Integer[] array = new Integer[this.snsUserList.keySet().size()];
        int i = 0;
        for (Integer number : this.snsUserList.keySet()) {
            array[i++] = number;
        }
        usernumber = (Integer) list.get(0);


        txtName.setText(snsUserList.get(usernumber).getName());

        age = DateUtils.currentDate().getYear() - snsUserList.get(usernumber).getBirthdate().getYear();
        txtAge.setText(toString());
        txtAdvRec.setText("No adverse Reactions");

        snsUser = App.getInstance().getCompany().getSnsUserStore().get(usernumber);

        List<Vaccine> vaccineList = controller.getVaccinationList(App.getInstance().getCompany().getSnsUserStore().get(usernumber));


            for (Vaccine vaccine : vaccineList) {
                listVaccines.getItems().add(vaccine.getName());
            }
            listVaccines.setDisable(false);
            listVaccines.getSelectionModel();

    }

    @Override
    public String toString() {
        return "" + age +
                "";
    }

    @FXML
    public void SelectVaccine(MouseEvent mouseEvent) {
        txtLotNumber.setDisable(false);

        if (listVaccines.getItems().size() > 0) {

            ObservableList list = listVaccines.getSelectionModel().getSelectedItems();
            String vaccineName = (String) list.get(0);

            btnOk.setDisable(false);
        }
    }
    @FXML
    public void OkMenu(javafx.event.ActionEvent actionEvent) throws UserAlreadyExistsException {
      //  boolean sucess=false ;
         lotNumber = txtLotNumber.getText();

        if (!controller.verifyIflotNumberIsCorrect(lotNumber)){
            lblLotNumber.setText("Lot Number: Invalid Lot Number(example: 21C16-05)!");
            lblLotNumber.setTextFill(Color.web("#FF0000"));
            return;
        }

        if (listVaccines.getItems().size() == 0) {
            return;
        }

        List<Vaccine> vaccineList = controller.getVaccinationList(snsUser);
        ObservableList list = listVaccines.getSelectionModel().getSelectedItems();
        String vaccineName = (String) list.get(0);

        Vaccine selected = null;

        for (Vaccine vaccine : vaccineList) {
            if (vaccine.getName().equals(vaccineName)) {
                selected = vaccine;
                break;
            }
        }

        Stage stage = (Stage) btnOk.getScene().getWindow();
        controller.VaccineShot(snsUser, selected,lotNumber);
        controller.AddRecoveryRoom(snsUser);
        new SMS().SendMessage();
        controller.leaveCenter(snsUser);

        stage.close();

    }
    @FXML
    public void CloseMenu(javafx.event.ActionEvent actionEvent) {
        Stage stage = (Stage) btnClose.getScene().getWindow();
        stage.close();
        NurseUI menu = new NurseUI();
        menu.run();


    }

    public RecordVaccineAdministrationUI() {

        this.controller = new RecordVaccineAdministrationController();
        this.snsUserList = controller.getSNSUsersWaiting();

    }


    @Override
    public void run() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(MainApp.class.getResource("/fxml/record-vaccine-administration.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 750, 750);
            this.initialize();
        } catch (IOException e) {
            System.out.println("Something went wrong!");
        }
    }



    @FXML
    void initialize() {
        assert btnClose != null : "fx:id=\"btnClose\" was not injected: check your FXML file 'record-vaccine-administration.fxml'.";
        assert btnOk != null : "fx:id=\"btnOk\" was not injected: check your FXML file 'record-vaccine-administration.fxml'.";
        assert txtName != null : "fx:id=\"txtName\" was not injected: check your FXML file 'record-vaccine-administration.fxml'.";
        assert txtLotNumber != null : "fx:id=\"txtLotNumber\" was not injected: check your FXML file 'record-vaccine-administration.fxml'.";
        assert listWaitingRoom != null : "fx:id=\"listWaitingRoom\" was not injected: check your FXML file 'record-vaccine-administration.fxml'.";
        assert txtAdvRec != null : "fx:id=\"txtAdvRec\" was not injected: check your FXML file 'record-vaccine-administration.fxml'.";
        assert listVaccines != null : "fx:id=\"listVaccines\" was not injected: check your FXML file 'record-vaccine-administration.fxml'.";
        assert txtAge != null : "fx:id=\"txtAge\" was not injected: check your FXML file 'record-vaccine-administration.fxml'.";

        listWaitingRoom.getItems().addAll(snsUserList.keySet());

    }

}
