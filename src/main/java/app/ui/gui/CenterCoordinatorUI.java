package app.ui.gui;
import app.controller.AuthController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * UI view for the center coordinator with available functionalities
 * @author alexandraipatova
 */
public class CenterCoordinatorUI {
    /**
     * stage of the view
     */
    private Stage stage;
    /**
     * scene of the view
     */
    private Scene scene;

    /**
     * root of the view
     */
    private Parent root;

    /**
     * used to redirect to authfacade functions
     */
    private AuthController ctrl=new AuthController();

    /**
     * menuBar that contains all redirection menuItems
     */
    @FXML
    private MenuBar menuBar;
    /**
     * redirects to statistics view regarding he fully vaccinated
     * @param event clicking on the menuItem
     * @throws IOException thrown when the redirecting UI is not loaded correctly
     */
    @FXML
    void redirectFullyVaccinated(ActionEvent event) throws IOException {
        root= FXMLLoader.load(getClass().getResource("/fxml/statistics-view.fxml"));
        stage=(Stage) menuBar.getScene().getWindow();
        stage.setTitle("Fully Vaccinated Users Statistics");
        scene =new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    /**
     * redirects to importing legacy file view
     * @param event clicking on the menuItem
     * @throws IOException thrown when the redirecting UI is not loaded correctly
     */
    @FXML
    void redirectImportLegacy(ActionEvent event) throws IOException {
        root= FXMLLoader.load(getClass().getResource("/fxml/import-legacy-view.fxml"));
        stage=(Stage) menuBar.getScene().getWindow();
        stage.setTitle("Import Legacy File");
        scene =new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    /**
     * logs out of current acccount
     * redirects to login view
     * @param event clicking on the menuItem
     * @throws IOException thrown when the redirecting UI is not loaded correctly
     */
    @FXML
    void logOut(ActionEvent event) throws IOException{
        AuthController auth=new AuthController();
        auth.doLogout();
        root= FXMLLoader.load(getClass().getResource("/fxml/login.fxml"));
        stage=(Stage) menuBar.getScene().getWindow();
        stage.setTitle("Login");
        scene =new Scene(root);
        stage.setScene(scene);
        stage.show();

    }

    /**
     * redirects to center perfoamnce view
     * @param event clicking on the menuItem
     * @throws IOException thrown when the redirecting UI is not loaded correctly
     */

    @FXML
    void redirectCenterPerformance(ActionEvent event) throws IOException {
        root= FXMLLoader.load(getClass().getResource("/fxml/center-performance-view.fxml"));
        stage=(Stage)menuBar.getScene().getWindow();
        stage.setTitle("Analyze Center Performance");
        scene=new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    /**
     * redirects to center perfoamnce view
     * @param event clicking on the button
     * @throws IOException thrown when the redirecting UI is not loaded correctly
     */
    @FXML
    void btnRedirectCenterPerformance(ActionEvent event) throws IOException {
        root= FXMLLoader.load(getClass().getResource("/fxml/center-performance-view.fxml"));
        stage=(Stage)((Node) event.getSource()).getScene().getWindow();
        stage.setTitle("Analyze Center Performance");
        scene=new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    /**
     * redirects to statistics view regarding he fully vaccinated
     * @param event clicking on the button
     * @throws IOException thrown when the redirecting UI is not loaded correctly
     */
    @FXML
    void btnRedirectFullyVaccinated(ActionEvent event) throws IOException {
        root= FXMLLoader.load(getClass().getResource("/fxml/statistics-view.fxml"));
        stage=(Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setTitle("Fully Vaccinated Users Statistics");
        scene =new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    /**
     * redirects to importing legacy file view
     * @param event clicking on the button
     * @throws IOException thrown when the redirecting UI is not loaded correctly
     */
    @FXML
    void btnRedirectImportLegacy(ActionEvent event) throws IOException {

        root= FXMLLoader.load(getClass().getResource("/fxml/import-legacy-view.fxml"));
        stage=(Stage)((Node) event.getSource()).getScene().getWindow();
        stage.setTitle("Import Legacy File");
        scene=new Scene(root);
        stage.setScene(scene);
        stage.show();

    }

}
