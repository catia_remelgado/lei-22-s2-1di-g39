package app.ui.gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import app.controller.StatisticsController;
import app.domain.model.Statistics;
import app.domain.store.StatisticsStore;
import app.domain.utils.DateUtils;
import app.domain.utils.FileHelper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class StatisticsUI implements Runnable {

    @Override
    public void run() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(MainApp.class.getResource("/fxml/statistics-view.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 800, 700);
            this.initialize();
        } catch (IOException e) {
            System.out.println("Something went wrong!");
        }
    }

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btnBack;

    @FXML
    private Button btnClean;

    @FXML
    private Button btnInsert;

    @FXML
    private Button btnLeave;

    @FXML
    private DatePicker dpBeg;

    @FXML
    private DatePicker dpEnd;

    @FXML
    private TextField txtFileName;

    @FXML
    private Label lblInvalidDate;

    @FXML
    private Label lblSuccessfull;

    @FXML
    private Label lblInvalidFileName;

    public app.controller.StatisticsController controller;

    /**
     *
     */
    private Statistics statistics;
    /**
     *
     */
    private FileHelper writeToFile;

    @FXML
    private BarChart<?,?> barChart;

    @FXML
    private Label lblSuccess;


    @FXML
    void Clean(ActionEvent event) {
        dpBeg.setValue(null);
        dpEnd.setValue(null);
        txtFileName.setText("");
        lblSuccessfull.setText("");
        lblInvalidDate.setText("");
        lblInvalidFileName.setText("");

    }

    @FXML
    void Insert() throws FileNotFoundException {
        DateUtils dateBeg = new DateUtils(
                dpBeg.getValue().getYear(),
                dpBeg.getValue().getMonth().getValue(),
                dpBeg.getValue().getDayOfMonth()
        );
        DateUtils dateEnd = new DateUtils(
                dpEnd.getValue().getYear(),
                dpEnd.getValue().getMonth().getValue(),
                dpEnd.getValue().getDayOfMonth()
        );


        String dateBeg1 = dateBeg.toDiaMesAnoString();
        String dateEnd1 = dateEnd.toDiaMesAnoString();

        this.statistics = new Statistics(dateBeg, dateEnd);

        if (statistics.verifyDateInterval()) {
            String fileName = txtFileName.getText();
            List<Integer> listInteger = controller.totalNumberFullyVaccinatedInInterval(dateBeg, dateEnd);

            if (writeToFile.exportFile(dateBeg, fileName, listInteger)) {
                writeToFile.exportFile(dateBeg, fileName, listInteger);
                lblSuccessfull.setText("The file has been successfully exported.");
            } else {
                lblInvalidFileName.setText("Exportation of the file unsuccessful.");
            }
        } else {
            lblInvalidDate.setText("Impossible to analize date interval.");
        }
    }

    public void graphicFill(List<Integer> vaccineArray, DateUtils dateBeg){
        barChart.getData().clear();
        Date dateBeg1 = DateUtils.dateUtilsToDate(dateBeg);
        XYChart.Series series1 = new XYChart.Series();
        series1.setName("People fully vaccinated");

        for (int i = 0; i < vaccineArray.size(); i++) {
        series1.getData().add(new XYChart.Data(dateBeg.toDiaMesAnoString(), vaccineArray.get(i)));

       dateBeg1 = DateUtils.addDays(dateBeg1, 1);
        }
    }
    @FXML
    void back(ActionEvent event) {

    }

    @FXML
    void leave(ActionEvent event) {


    }

    @FXML
    void initialize() {
        assert btnBack != null : "fx:id=\"btnBack\" was not injected: check your FXML file 'statistics-view.fxml'.";
        assert btnClean != null : "fx:id=\"btnClean\" was not injected: check your FXML file 'statistics-view.fxml'.";
        assert btnInsert != null : "fx:id=\"btnInsert\" was not injected: check your FXML file 'statistics-view.fxml'.";
        assert btnLeave != null : "fx:id=\"btnLeave\" was not injected: check your FXML file 'statistics-view.fxml'.";
        assert dpBeg != null : "fx:id=\"dpBeg\" was not injected: check your FXML file 'statistics-view.fxml'.";
        assert dpEnd != null : "fx:id=\"dpEnd\" was not injected: check your FXML file 'statistics-view.fxml'.";
        assert lblInvalidDate != null : "fx:id=\"lblInvalidDateBeg\" was not injected: check your FXML file 'statistics-view.fxml'.";
        assert lblInvalidFileName != null : "fx:id=\"lblInvalidFileName\" was not injected: check your FXML file 'statistics-view.fxml'.";
        assert txtFileName != null : "fx:id=\"txtFileName\" was not injected: check your FXML file 'statistics-view.fxml'.";

    }

}
