package app.ui.gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import app.controller.AuthController;
import app.controller.ImportLegacyV2Controller;
import app.domain.model.LoadFromFileCSV;
import app.domain.model.VaccinationCenter;
import app.domain.utils.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.commons.lang3.time.StopWatch;

public class ImportLegacyV2UI implements Runnable {

    @Override
    public void run() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(MainApp.class.getResource("/fxml/center-performance-view.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 800, 700);
            this.initialize();
        } catch (IOException e) {
            System.out.println("Something went wrong!");
        }
    }

    /**
     * List of list to save the csv file contents
     */
    private List<List<String>> csvFileContents = new ArrayList<>();

    /**
     * Controller of the UI
     */
    private ImportLegacyV2Controller ctrl = new ImportLegacyV2Controller().getInstance();

    /**
     * Text that will appear in the first combobox (to chose between leaving or arrival time)
     */
    private final ObservableList<String> optionsForSortTime =
            FXCollections.observableArrayList(
                    "Arrival time",
                    "Leaving time"
            );

    /**
     * Text that will appear at the second combobox (to chose between ascending or descending)
     */
    private final ObservableList<String> optionsForSortAscOrDesc =
            FXCollections.observableArrayList(
                    "Ascending",
                    "Descending"
            );

    /**
     * menuBar that contains all redirection menuItems
     */
    @FXML
    private MenuBar menuBar;

    @FXML
    private Button btnLogOut;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btnSelectFile;

    @FXML
    private Button btnShowInfo;

    @FXML
    private ComboBox<String> cbForSortAscOrDesc;

    @FXML
    private ComboBox<String> cbForSortTime;

    @FXML
    private TableView<TableFileContens> tblFileContents;

    @FXML
    private TableColumn<TableFileContens, String> adminTime;

    @FXML
    private TableColumn<TableFileContens, String> arrivalTime;

    @FXML
    private TableColumn<TableFileContens, String> date;

    @FXML
    private TableColumn<TableFileContens, String> dose;

    @FXML
    private TableColumn<TableFileContens, String> leavingTime;

    @FXML
    private TableColumn<TableFileContens, String> lotNumber;

    @FXML
    private TableColumn<TableFileContens, String> name;

    @FXML
    private TableColumn<TableFileContens, String> scheduleTime;

    @FXML
    private TableColumn<TableFileContens, String> snsNumber;

    @FXML
    private TableColumn<TableFileContens, String> vacDesc;

    @FXML
    private TableColumn<TableFileContens, String> vacName;

    @FXML
    void ShowAndSelectASort(MouseEvent event) {
    }

    /**
     * Found and adapted from  https://www.youtube.com/watch?v=AGYOKManNYw
     * Method associatd with the button chose file
     * when clicked saves the csv file contents int the list of list declared before
     *
     * @param event works when the button is clicked with the mouse
     */
    @FXML
    void choseLegacyFile(MouseEvent event) {
        FileChooser fileChooser = new FileChooser();
        File selectedFile = fileChooser.showOpenDialog(new Stage());

        if (selectedFile != null) {
            btnShowInfo.setDisable(false);
            try {
                csvFileContents = new LoadFromFileCSV().loadFromFile(selectedFile);
            } catch (FileNotFoundException e) {
                System.out.println("It seems that something's wrong. :( ");
            }

        }
    }


    @FXML
    void initialize() {
        assert btnSelectFile != null : "fx:id=\"btnSelectFile\" was not injected: check your FXML file 'import-legacy-view.fxml'.";
        assert btnShowInfo != null : "fx:id=\"btnShowInfo\" was not injected: check your FXML file 'import-legacy-view.fxml'.";
        assert cbForSortAscOrDesc != null : "fx:id=\"cbForSortAscOrDesc\" was not injected: check your FXML file 'import-legacy-view.fxml'.";
        assert cbForSortTime != null : "fx:id=\"cbForSortTime\" was not injected: check your FXML file 'import-legacy-view.fxml'.";
        assert tblFileContents != null : "fx:id=\"tblFileContents\" was not injected: check your FXML file 'import-legacy-view.fxml'.";
        assert btnLogOut != null : "fx:id=\"btnLogOut\" was not injected: check your FXML file 'import-legacy-view.fxml'.";
        assert adminTime != null : "fx:id=\"adminTime\" was not injected: check your FXML file 'import-legacy-view.fxml'.";
        assert arrivalTime != null : "fx:id=\"arrivalTime\" was not injected: check your FXML file 'import-legacy-view.fxml'.";
        assert date != null : "fx:id=\"date\" was not injected: check your FXML file 'import-legacy-view.fxml'.";
        assert dose != null : "fx:id=\"dose\" was not injected: check your FXML file 'import-legacy-view.fxml'.";
        assert leavingTime != null : "fx:id=\"leavingTime\" was not injected: check your FXML file 'import-legacy-view.fxml'.";
        assert lotNumber != null : "fx:id=\"lotNumber\" was not injected: check your FXML file 'import-legacy-view.fxml'.";
        assert menuBar != null : "fx:id=\"menuBar\" was not injected: check your FXML file 'import-legacy-view.fxml'.";
        assert menuHelp != null : "fx:id=\"menuHelp\" was not injected: check your FXML file 'import-legacy-view.fxml'.";
        assert menuImport != null : "fx:id=\"menuImport\" was not injected: check your FXML file 'import-legacy-view.fxml'.";
        assert menuItemCenterPerformance != null : "fx:id=\"menuItemCenterPerformance\" was not injected: check your FXML file 'import-legacy-view.fxml'.";
        assert menuItemFullyVaccinated != null : "fx:id=\"menuItemFullyVaccinated\" was not injected: check your FXML file 'import-legacy-view.fxml'.";
        assert menuItemImportLegacy != null : "fx:id=\"menuItemImportLegacy\" was not injected: check your FXML file 'import-legacy-view.fxml'.";
        assert menuItemlogOut != null : "fx:id=\"menuItemlogOut\" was not injected: check your FXML file 'import-legacy-view.fxml'.";
        assert menuLogout != null : "fx:id=\"menuLogout\" was not injected: check your FXML file 'import-legacy-view.fxml'.";
        assert menuPerformance != null : "fx:id=\"menuPerformance\" was not injected: check your FXML file 'import-legacy-view.fxml'.";
        assert name != null : "fx:id=\"name\" was not injected: check your FXML file 'import-legacy-view.fxml'.";
        assert scheduleTime != null : "fx:id=\"scheduleTime\" was not injected: check your FXML file 'import-legacy-view.fxml'.";
        assert snsNumber != null : "fx:id=\"snsNumber\" was not injected: check your FXML file 'import-legacy-view.fxml'.";
        assert vacDesc != null : "fx:id=\"vacDesc\" was not injected: check your FXML file 'import-legacy-view.fxml'.";
        assert vacName != null : "fx:id=\"vacName\" was not injected: check your FXML file 'import-legacy-view.fxml'.";


        cbForSortTime.setItems(optionsForSortTime);
        cbForSortTime.getSelectionModel().selectFirst();

        cbForSortAscOrDesc.setItems(optionsForSortAscOrDesc);
        cbForSortAscOrDesc.getSelectionModel().selectFirst();

        tblFileContents.isResizable();

    }

    /**
     * method associated with the button show file information
     * when clicked processes the csv file information and populates the table
     *
     * @param event works when the button is clicked with the mouse
     * @throws IOException
     */
    @FXML
    void showFileInformation(MouseEvent event) throws IOException {

        VaccinationCenter vaccinationCenter = ctrl.getVaccinationCenterOfCoordinator();
        List<List<String>> treatedCSVFileContentsToSeeIfAreParsable = new TreatLegacySystemData().treatSystemLegacyData(csvFileContents);
        List<List<String>> finalDataAllTogether = ctrl.treatRawData(treatedCSVFileContentsToSeeIfAreParsable, vaccinationCenter, cbForSortTime.getValue());
        ObservableList<TableFileContens> obs = FXCollections.observableArrayList();


        for (List<String> data : finalDataAllTogether) {
            TableFileContens toTablepw = new TableFileContens(data.get(0), data.get(1), data.get(2), data.get(3), data.get(4),
                    data.get(5), data.get(6), data.get(7), data.get(8), data.get(9), data.get(10));
            obs.add(toTablepw);
            name.setCellValueFactory(cellData -> cellData.getValue().snsUserNameProperty());
            snsNumber.setCellValueFactory(cellData -> cellData.getValue().snsUserNumberProperty());
            vacName.setCellValueFactory(cellData -> cellData.getValue().vaccineNameProperty());
            vacDesc.setCellValueFactory(cellData -> cellData.getValue().vaccineShortDescriptionProperty());
            dose.setCellValueFactory(cellData -> cellData.getValue().doseProperty());
            lotNumber.setCellValueFactory(cellData -> cellData.getValue().lotNumberProperty());
            date.setCellValueFactory(cellData -> cellData.getValue().dateProperty());
            scheduleTime.setCellValueFactory(cellData -> cellData.getValue().scheduledTimeProperty());
            arrivalTime.setCellValueFactory(cellData -> cellData.getValue().arrivalTimeProperty());
            adminTime.setCellValueFactory(cellData -> cellData.getValue().administrationTimeProperty());
            leavingTime.setCellValueFactory(cellData -> cellData.getValue().leavingTimeProperty());

        }
        if (cbForSortAscOrDesc.getSelectionModel().isSelected(1)) {
            Collections.reverse(obs);
        }

        tblFileContents.setItems(obs);

        SerializationHelper.serializeCompany();
    }

    /**
     * stage of the view
     */
    private Stage stage;
    /**
     * scene of the view
     */
    private Scene scene;

    /**
     * root of the view
     */
    private Parent root;


    @FXML
    private Menu menuHelp;

    @FXML
    private Menu menuImport;

    @FXML
    private MenuItem menuItemCenterPerformance;

    @FXML
    private MenuItem menuItemFullyVaccinated;

    @FXML
    private MenuItem menuItemImportLegacy;

    @FXML
    private MenuItem menuItemlogOut;

    @FXML
    private Menu menuLogout;

    @FXML
    private Menu menuPerformance;


    /**
     * redirects to statistics view regarding he fully vaccinated
     *
     * @param event clicking on the menuItem
     * @throws IOException thrown when the redirecting UI is not loaded correctly
     */
    @FXML
    void redirectFullyVaccinated(ActionEvent event) throws IOException {
        root = FXMLLoader.load(getClass().getResource("/fxml/statistics-view.fxml"));
        stage = (Stage) menuBar.getScene().getWindow();
        stage.setTitle("Fully Vaccinated Users Statistics");
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * redirects to importing legacy file view
     *
     * @param event clicking on the menuItem
     * @throws IOException thrown when the redirecting UI is not loaded correctly
     */
    @FXML
    void redirectImportLegacy(ActionEvent event) throws IOException {
        root = FXMLLoader.load(getClass().getResource("/fxml/import-legacy-view.fxml"));
        stage = (Stage) menuBar.getScene().getWindow();
        stage.setTitle("Import Legacy File");
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * logs out of current acccount
     * redirects to login view
     *
     * @param event clicking on the menuItem
     * @throws IOException thrown when the redirecting UI is not loaded correctly
     */
    @FXML
    void logOut(ActionEvent event) throws IOException {
        AuthController auth = new AuthController();
        auth.doLogout();
        root = FXMLLoader.load(getClass().getResource("/fxml/login.fxml"));
        stage = (Stage) menuBar.getScene().getWindow();
        stage.setTitle("Login");
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

    }

    /**
     * redirects to center perfoamnce view
     * @param event clicking on the menuItem
     * @throws IOException thrown when the redirecting UI is not loaded correctly
     */

    @FXML
    void redirectCenterPerformance(ActionEvent event) throws IOException {
        root= FXMLLoader.load(getClass().getResource("/fxml/center-performance-view.fxml"));
        stage=(Stage)menuBar.getScene().getWindow();
        stage.setTitle("Analyze Center Performance");
        scene=new Scene(root);
        stage.setScene(scene);
        stage.show();
    }


}
