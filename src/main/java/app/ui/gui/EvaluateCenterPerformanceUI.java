package app.ui.gui;

import app.controller.AuthController;
import app.controller.EvaluateCenterPerformanceController;
import app.domain.exceptions.NotAWholeDivisionException;
import app.domain.exceptions.NotPositiveNumberException;
import app.domain.exceptions.VaccinationCenterNotFoundException;
import app.domain.exceptions.VaccineAdministrationsNotFoundException;
import app.domain.model.CenterPerformance;
import app.domain.model.VaccinationCenter;
import app.domain.utils.DateUtils;
import app.domain.utils.TimeUtils;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.scene.chart.*;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.scene.Scene;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * View for evaluating a center's performance
 * @author alexandraipatova
 */
public class EvaluateCenterPerformanceUI{




    /**
     * stage of the view
     */
    private Stage stage;
    /**
     * scene of the view
     */
    private Scene scene;

    /**
     * root of the view
     */
    private Parent root;

    /**
     * controller of the view, that redirects to methods of calculatons
     */
    EvaluateCenterPerformanceController ctrl = new EvaluateCenterPerformanceController();

    /**
     * indicates whether the inserted date is valid or not
     */
    boolean dateFlag = false;

    /**
     * indicates whether the inserted time is valid or not
     */
    boolean timeFlag = false;

    /**
     * menuBar that contains all redirection menuItems
     */
    @FXML
    private MenuBar menuBar;

    /**
     * Error that indicates and error of the time interval
     */
    @FXML
    private Label lblError;
    /**
     * text field where the time interval to be analyzed is inserted
     */
    @FXML
    private TextField txtTimeInterval;

    public void initalize() {
        txtTimeInterval.textProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println("textfield changed from " + oldValue + " to " + newValue);
        });
    }
    /**
     * button that calculates and shows performance data
     */
    @FXML
    private Button btnCalculate;

    /**
     * date picker to pick the date to analyze
     */
    @FXML
    private DatePicker datePicker;

    /**
     * label that indicates when the time is invalid
     */
    @FXML
    private Label lblInvalidTime;

    /**
     * label the indicates when the date is invalid
     */
    @FXML
    private Label lblInvalidDate;

    /**
     * chart that visually represents the center's performance
     */
    @FXML
    private BarChart<String, Number> barChartCenterPerformance;
    @FXML
    private StackedBarChart<?, ?> sbc;

    /**
     * non-editable text field that shows the maximum sum of the subcontiguous sublist
     */
    @FXML
    private TextField txtMaxSum;

    /**
     * non-editable text field that shows the input list
     */
    @FXML
    private TextArea txtInputList;

    /**
     * non-editable text field that shows the maximum sum subcontiguous sublist
     */
    @FXML
    private TextArea txtMaxSumSubList;

    /**
     * non-editable text field that shows the time interval of worst performance
     */
    @FXML
    private TextField txtWorstTimeInterval;


    /**
     * redirects to statistics view regarding he fully vaccinated
     * @param event clicking on the menuItem
     * @throws IOException thrown when the redirecting UI is not loaded correctly
     */
    @FXML
    void redirectFullyVaccinated(ActionEvent event) throws IOException {
        root= FXMLLoader.load(getClass().getResource("/fxml/statistics-view.fxml"));
        stage=(Stage) menuBar.getScene().getWindow();
        stage.setTitle("Fully Vaccinated Users Statistics");
        scene =new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    /**
     * redirects to importing legacy file view
     * @param event clicking on the menuItem
     * @throws IOException thrown when the redirecting UI is not loaded correctly
     */
    @FXML
    void redirectImportLegacy(ActionEvent event) throws IOException {
        root= FXMLLoader.load(getClass().getResource("/fxml/import-legacy-view.fxml"));
        stage=(Stage) menuBar.getScene().getWindow();
        stage.setTitle("Import Legacy File");
        scene =new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * logs out of current acccount
     * redirects to login view
     * @param event clicking on the menuItem
     * @throws IOException thrown when the redirecting UI is not loaded correctly
     */
    @FXML
    void logOut(ActionEvent event) throws IOException{
        AuthController auth=new AuthController();
        auth.doLogout();
        root= FXMLLoader.load(getClass().getResource("/fxml/login.fxml"));
        stage=(Stage) menuBar.getScene().getWindow();
        stage.setTitle("Login");
        scene =new Scene(root);
        stage.setScene(scene);
        stage.show();

    }
    /**
     * calculates and generates graph data, as well as writing the performance data in corresponding the text fields
     * @param event clicking on the button
     * @throws IOException thrown when the redirecting UI is not loaded correctly
     */
    @FXML
    void evaluateAndShowCenterPerformance(ActionEvent event) throws IOException {


        barChartCenterPerformance.getData().clear();

        /*VaccinationCenter vc = new VaccinationCenter("healthcare center",
                "Centro de Saúde do Barão do Corvo",
                "R. Barão do Corvo ",
                676, "4400-037", " Vila Nova de Gaia",
                223747010,
                "usf.arcoprado@arsnorte.min-saude.pt",
                93848374,
                "baraodocorvo.com", "08:00", "20:00", 45, 30, 2);
        //Setting dummy vaccine administrations
        VaccineAdministration va1 = new VaccineAdministration(
                161593121,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(8, 1),
                new TimeUtils(8),
                new TimeUtils(8, 17),
                new TimeUtils(9, 25));
        VaccineAdministration va2 = new VaccineAdministration(
                161593120,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(8, 24),
                new TimeUtils(8),
                new TimeUtils(9, 11),
                new TimeUtils(9, 43));

        VaccineAdministration va3 = new VaccineAdministration(
                161593121,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(8, 45),
                new TimeUtils(9, 7),
                new TimeUtils(9, 17),
                new TimeUtils(9, 25));
        VaccineAdministration va4 = new VaccineAdministration(
                161593121,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(9, 20),
                new TimeUtils(9, 21),
                new TimeUtils(9, 22),
                new TimeUtils(9, 45));
        VaccineAdministration va5 = new VaccineAdministration(
                161593121,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(9, 20),
                new TimeUtils(9, 21),
                new TimeUtils(9, 22),
                new TimeUtils(9, 45));

        VaccineAdministration va6 = new VaccineAdministration(
                161593121,
                "Spikevax",
                1,
                "21C16-05",
                new DateUtils(2022, 5, 30),
                new TimeUtils(9, 20),
                new TimeUtils(9, 21),
                new TimeUtils(9, 22),
                new TimeUtils(9, 45));

        //creating dummy vaccine administration list
        List<VaccineAdministration> vaList = new ArrayList<>();
        vaList.add(va1);
        vaList.add(va2);
        vaList.add(va3);
        vaList.add(va4);
        vaList.add(va5);
        vaList.add(va6);


        CenterPerformance cp = new CenterPerformance(vc,24);
*/
        CenterPerformance cp=new CenterPerformance();
        try {
             cp= ctrl.evaluateCenterPerformance(
                    ctrl.getVaccinationCenterOfCoordinator(),
                    new DateUtils(
                            datePicker.getValue().getYear(),
                            datePicker.getValue().getMonth().getValue(),
                            datePicker.getValue().getDayOfMonth()
                    ), Integer.parseInt(txtTimeInterval.getText().trim()));
        }catch(IOException e){
            lblError.setText("Something went wrong!");
        }
        System.out.println(cp.getDifferenceList().size());

        barChartCenterPerformance.setTitle("Inflow/Outflow of people in center");
        barChartCenterPerformance.getXAxis().setLabel("TimeInterval");
        barChartCenterPerformance.getYAxis().setLabel("Influx/Outflow");
        XYChart.Series seriesWorst = new XYChart.Series();
        XYChart.Series seriesBest = new XYChart.Series();
        TimeUtils time = new TimeUtils(TimeUtils.setTime(cp.getVc().getOpenHour()));
        String inputList="[";
        String subList="[";
        for (int i = 0; i < cp.getDifferenceList().size(); i++) {
            inputList+=cp.getDifferenceList().get(i)+", ";
            if (time.isBiggerOrEquals(cp.getStartTime()) && cp.getEndTime().isBigger(time)) {
                subList+=cp.getDifferenceList().get(i)+", ";
                seriesWorst.getData().add(new XYChart.Data(time.toStringHHMM(), cp.getDifferenceList().get(i)));

            } else {
                seriesBest.getData().add(new XYChart.Data(time.toStringHHMM(), cp.getDifferenceList().get(i)));
            }

            time.addMinutes( cp.getTimeInterval());

        }
        inputList=inputList.substring(0,inputList.length()-2);
        inputList+="]";
        if(!subList.equals("[")) {
            subList = subList.substring(0, subList.length() - 2);
            subList+="]";

        }else{
            cp.setEndTime(cp.getEndTime().addMinutes(cp.getTimeInterval()));
            subList=inputList;
        }

        seriesWorst.setName("Best Performance");
        seriesBest.setName("Worst Performance");
        barChartCenterPerformance.getData().addAll(seriesWorst, seriesBest);
        txtMaxSum.setText(String.valueOf(cp.getMaxSum()));
        txtInputList.setText(inputList);
        txtMaxSumSubList.setText(subList);
        DateUtils date = new DateUtils(
                datePicker.getValue().getYear(),
                datePicker.getValue().getMonth().getValue(),
                datePicker.getValue().getDayOfMonth()
        );

        txtWorstTimeInterval.setText("["+date.getMonth()+"/"+date.getDay()+"/"+date.getYear()+" "+cp.getStartTime().toStringHHMM()+", "+date.getMonth()+"/"+date.getDay()+"/"+date.getYear()+" "+cp.getEndTime().toStringHHMM()+"]");

    }

    /**
     * verifies if the chosen date is valid with available data
     * @param event changing text field content
     */
    @FXML
    void verifyDate(ActionEvent event) {
        DateUtils date = new DateUtils(
                datePicker.getValue().getYear(),
                datePicker.getValue().getMonth().getValue(),
                datePicker.getValue().getDayOfMonth()
        );

        if (date.isBigger(DateUtils.currentDate()) || date.equals(DateUtils.currentDate())) {
            lblInvalidDate.setVisible(true);
            dateFlag = false;
        } else {
            lblInvalidDate.setVisible(false);
            dateFlag = true;
            try {
                ctrl.getVaccinationCenterOfCoordinator().getVaccineAdministrationListByDay(date);

            } catch (VaccineAdministrationsNotFoundException ex) {
                lblInvalidDate.setText(ex.getMessage());
                lblInvalidDate.setVisible(true);
                dateFlag = false;
            } catch (VaccinationCenterNotFoundException e) {
                lblInvalidDate.setText(e.getMessage());
                lblInvalidDate.setVisible(true);
                dateFlag = false;
            }

        }
        if (dateFlag && timeFlag) {
            btnCalculate.setDisable(false);
        } else {
            btnCalculate.setDisable(true);
        }


    }
    /**
     * verifies if the chosen time is valid with available data
     * @param event changing text field content
     */

    @FXML
    void verifyTime(KeyEvent event) {

        lblInvalidTime.setVisible(false);
        timeFlag = true;
        int m;
        try {
            System.out.println(txtTimeInterval.getText());
            m = Integer.parseInt(txtTimeInterval.getText().trim());
            if(txtTimeInterval.getText().trim().equals("")){
                m = 0;
            }
            ctrl.verifyTimeIntervalForPerformance(ctrl.getVaccinationCenterOfCoordinator(),m );

        } catch (NotPositiveNumberException e) {
            lblInvalidTime.setText(e.getMessage());
            lblInvalidTime.setVisible(true);
            timeFlag = false;
        } catch (NotAWholeDivisionException e) {
            lblInvalidTime.setText(e.getMessage());
            lblInvalidTime.setVisible(true);
            timeFlag = false;
        }
        catch (NumberFormatException e) {
            lblInvalidTime.setText("Please insert a number!");
            lblInvalidTime.setVisible(true);
            timeFlag = false;
        }
        if (dateFlag && timeFlag) {

            btnCalculate.setDisable(false);
        } else {
            btnCalculate.setDisable(true);
        }
    }


}

